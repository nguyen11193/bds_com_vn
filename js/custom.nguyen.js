function ShowErrEmail(email, frm, email_err, msg){
    if ($(email).val() != "") {
        if(IsEmail($(email).val()) == false){
            showErrMsg(email, frm, email_err, msg);
            return false;
        } else {
            RemoveErr(frm, email_err);
            return true;
        }
    }
}
  
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
        return false;
    } else {
        return true;
    }
}

function showErrMsg(txtname, frm, name_err, name, require = ''){
    $(frm).addClass("has-error");
    $(name_err).html(name + require);
    $(name_err).show();
    scrollToErrorField(txtname);
}

function changeMenuParent(obj){
    var value = $(obj).val();
    scrollToErrorField(value);
}

function ShowErr(txtname, frm, name_err, name, require){
    if ($(txtname).val() == "" || $(txtname).val() == null) {
        showErrMsg(txtname, frm, name_err, name, require);
        return false;
    } else {
        RemoveErr(frm, name_err);
        return true;
    }
}

function ValidateNumber(param){
    $(param).keydown(function(e) {
        -1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
    });
}

function AjaxURL(data_string, url){
    var v = "";
    blockSystem();
    $.ajax({
        async: false,
        url: url,
        type: "POST",
        data: data_string,
        dataType: 'json',
        cache: false,
        success: function (result) {
            v = result;
        }
    });
    unBlockSystem();
    return v;
}

function CheckUnique(param, name, frm, name_err, already_exist){
    var canCreate = true;
    if(param == "yes"){
        $(frm).addClass("has-error");
        $(name_err).html(name + already_exist);
        $(name_err).show();
        canCreate = false;
    }
    return canCreate;
}

function RemoveErr(frm, name_err){
    $(frm).removeClass("has-error");
    $(name_err).hide();
}

function RemoveErr2(obj, level = 1){
    var frm = $(obj).parent();
    switch(level) {
        case 2: 
        frm = $(obj).parent().parent();
        break;
    }
    var nameErr = frm.find('.error');
    frm.removeClass("has-error");
    nameErr.hide();
}

function SwalWarning(title, text){
    swal({
        title: title,
        text: text,
        type: "warning",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    });
}

function SwalConfirm(title, text, url, btn_confirm, btn_cancel){
    swal({
        title: title,
        text: text,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: btn_confirm,
        cancelButtonText: btn_cancel,
        closeOnConfirm: true
    },
    function () {
        blockSystem();
        $(location).attr('href', url)
    })
}

function Dateformat(param){
    $(param).datetimepicker({
        format: "j M Y",
        timepicker: false,
        closeOnDateSelect: true,
    });
}

function datetimepickerCustom(txt, format){
    $(txt).datetimepicker({
        format: format,
        timepicker: false,
        closeOnDateSelect: true,
        lang: 'vi',
        //scrollMonth : false,
	    scrollInput : false
    });
}

function readImg(txt, frm, imgDisplay, remove, txt_err) {
    var input = $(txt)[0];
    if (input.files && input.files[0]) {
        var file_size = input.files[0].size;
        //var image = new Image();
        if (input.files[0].type.match(/image.*/)) {
            if (file_size > 4194304) {
                $(imgDisplay).hide();
                $(remove).hide();
                $(txt).val("");
                $(frm).addClass("has-error");
                $(txt_err).html("Kích thước hình ảnh lớn hơn 4MB");
                $(txt_err).show();
            } else {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        //Determine the Height and Width.
                        var height = this.height;
                        var width = this.width;
                        if (width > height) {
                            $(imgDisplay).hide();
                            $(remove).hide();
                            $(txt).val("");
                            $(frm).addClass("has-error");
                            $(txt_err).html("Chiều cao phải lớn hơn chiều rộng.");
                            $(txt_err).show();
                        } else {
                            $(imgDisplay).show();
                            $(remove).show();
                            $(frm).removeClass("has-error");
                            $(txt_err).hide();
                            $(imgDisplay).attr('src', e.target.result).width(40).height(40);
                        }
                    };
                };
                reader.readAsDataURL(input.files[0]);
            }
        } else {
            $(imgDisplay).hide();
            $(remove).hide();
            $(txt).val("");
            $(frm).addClass("has-error");
            $(txt_err).html("Vui lòng chọn tập tin hình ảnh.");
            $(txt_err).show();
        }
    }
}

function addPhone(){
    row = '<br/><input type="text" id="customer_phone_'+row_no+'" name="CustomerPhone[phone'+row_no+']" maxlength="255" value="" class="form-control input_number_only customer_info">';
    //row += '<input type="hidden" value="' + row_no + '" id="sids" class="sids">';
    $('#customer_phone_err').after(row);
    ValidateNumber("#customer_phone_"+row_no);
    row_no++;
}

function addPhoneConsumer(){
    row = '<br/><input type="text" id="consumer_phone_'+row_no+'" name="ConsumerPhone[phone'+row_no+']" maxlength="255" value="" class="form-control">';
    row += '<input type="hidden" value="' + row_no + '" id="sids_consumer" class="sids_consumer">';
    $('#consumer_phone_err').after(row);
    ValidateNumber("#consumer_phone_"+row_no);
    row_no++;
}

function morePhoneNumber(){
    row = '<br/><input type="text" id="customer_phone_'+row_no+'" name="CustomerPhone[phone'+row_no+']" maxlength="255" value="" class="form-control">';
    row += '<input type="hidden" value="' + row_no + '" id="sids" class="sids">';
    //row += '<br/>';
    $('#newphone-location-indicator').before(row);
    ValidateNumber("#customer_phone_"+row_no);
    row_no++;
}

function addImage(msg1, msg2) {
    row = '<tbody id="rowMultiImage-' + row_no + '">';
    row +=      '<tr>';
    row +=          '<td hidden><input type="hidden" value="' + row_no + '" id="sids" class="sids"></td>';
    row +=          '<td><input onchange="readImg(' + row_no + ', \'#ProductImage_upload_\', \'#imgDisplay_\', \'#ProductImage_upload_err_\', \''+msg1+'\', \''+msg2+'\' );" id="ProductImage_upload_' + row_no + '" name="ProductImage_upload_' + row_no + '" type="file" accept="image/*"><div class="error" id="ProductImage_upload_err_' + row_no + '" style="display: hidden"></div></td>';
    row +=          '<td><div class="i-checks"><input id="checkboxImages_'+ row_no +'" type="radio" value="'+ row_no +'" name="checkboxImages"></div></td>';
    row +=          '<td><img id="imgDisplay_' + row_no + '" src="" alt="" /></td>';
    row +=          '<td><a onclick="$(\'#rowMultiImage-' + row_no + '\').remove();" href="#" class="btn btn-danger"><i class="fa fa-fw fa-times"></i></a></td>';
    row +=      '</tr>';
    row += '</tbody>';
    $('#multiImage tfoot').before(row);
    row_no++;
    $('.i-checks').iCheck({
        radioClass: 'iradio_square-green',
    });
}

function loadPrice(url, line, sel){
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (json) {
            if (json['resultInfo'] != '') {
                var qty = parseFloat($('#product_qty_' + line).val().replace(/,/g, ''));
                var price = 0;
                if(sel.value == 3){
                    $('#product_price_'+line).prop('readonly', true);
                } else {
                    var sale_price = parseFloat(json['resultInfo']['sale_price']);
                    var product_price = parseFloat($('#product_price_' + line).val().replace(/,/g, ''));
                    if (!isNaN(product_price)){
                        if(sale_price == product_price){
                            price = sale_price;
                        } else {
                            if(product_price == 0){
                                 price = sale_price;
                            } else {
                                price = product_price;
                            }
                        }
                    } else {
                        price = sale_price;
                    }
                    $('#product_price_'+line).prop('readonly', false);
                }
                $('#product_price_'+line).val(formatAmount(price));
                var line_price = 0;
                if (!isNaN(qty) && !isNaN(price)) {
                    line_price = qty * price;

                    var discount_value = parseFloat($('#product_discount_' + line).val().replace(/,/g, ''));
                    if (!isNaN(discount_value)) {
                        line_price = line_price - discount_value;
                        if (line_price > 0) {
                            line_price = line_price;
                        } else {
                            line_price = 0;
                        }
                    }
                }
                $("#product_subtotal_" + line).val(formatAmount(line_price));
            }
            calculateGrandTotal();
        }
    });
}

function loadProductInfo(url, line, isOrder = true){
    $.ajax({
        url: url,
        dataType: 'json',
        success: function (json) {
            var product_name = '';
            var qty = '';
            var price = '';
            var subtotal = '';
            var discount_line = '';
            var discount_line_percent = '';
            var product_type_line = $('#product_type_'+line).val();
           
            if (json['resultInfo'] != '') {
                product_name = json['resultInfo']['name'];
                qty = 1;
                if(product_type_line == 3){
                    price = 0;
                    $('#product_price_'+line).prop('readonly', true);
                } else {
                    price = json['resultInfo']['sale_price'];
                    $('#product_price_'+line).prop('readonly', false);
                }
                var sub = qty * price;
                /*var value = sub.toLocaleString(
                  undefined, // leave undefined to use the browser's locale,
                             // or use a string like 'en-US' to override it.
                  { minimumFractionDigits: 0 }
                );*/
                subtotal = sub;
            }
            $("#product_name_"+line).val(product_name);
            $("#product_price_"+line).val(formatAmount(price));
            $("#product_qty_"+line).val(qty);
            $("#product_subtotal_"+line).val(formatAmount(subtotal));
            $('#product_discount_'+line).val(discount_line);
            $('#product_discount_percent_'+line).val(discount_line_percent);
            
            changeWarehouseLine(line, isOrder);
            
            if (isOrder) {
                calculateGrandTotal();
            }
        }
    });
}

function settingDatatableDefaults(targets, searchPlaceholder){
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: true,
        columnDefs: [{ 
            orderable: false,
            //width: '100px',
            targets: [targets]
        }],
        "pageLength":100,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Bộ lọc:</span> _INPUT_',
            searchPlaceholder: searchPlaceholder,
            lengthMenu: '<span>Hiển Thị:</span> _MENU_',
            "zeroRecords": "Không tìm thấy kết quả - Xin lỗi",
            "infoEmpty": "Không tìm thấy kết quả",
            "info": "Hiển thị trang _PAGE_ của _PAGES_",
            "infoFiltered": "(được lọc từ _MAX_ kết quả)",
            "emptyTable": "Không có dữ liệu trong bảng",
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    
    $('.datatable-basic').DataTable();
}

function loadCustomerInfo(data) {
    $('#customer_code').val(data.code);
    $('#customer_name').val(data.name);
    var birthday = '';
    if (!empty(data.birthday)) {
        birthday = data.birthday;
    }
    $('#customer_birthday').val(birthday);
    $('#customer_address').val(data.address);
    $('#customer_email').val(data.email);
    $('#customer_comments').val(data.comment);
    if (data.disabled == 1) {
        disabledRefeshCustomerField(true);
    }
}

function disabledRefeshCustomerField(disabled = true){
    $('.customer_info').prop('disabled', disabled);
}

function changeDiscount() {
    calculateGrandTotal();
}

function lineChangeDiscountPercent(line){
    var id_product = $("#product_code_" + line).val();
    var qty = parseFloat($('#product_qty_' + line).val().replace(/,/g, ''));
    var price = parseFloat($('#product_price_' + line).val().replace(/,/g, ''));
    
    var line_price = 0;
    if (id_product != 0 && !isNaN(qty) && !isNaN(price)) {
        line_price = qty * price;

        var discount_percent = parseFloat($('#product_discount_percent_' + line).val());
        if (!isNaN(discount_percent)) {
            var discount_value = parseFloat((line_price * discount_percent) / 100);
            $("#product_discount_" + line).val(formatAmount(discount_value));
            line_price = line_price - discount_value;
            if (line_price > 0) {
                line_price = line_price;
            } else {
                line_price = 0;
            }
        } else {
            $("#product_discount_" + line).val('');
        }
    }
    $("#product_subtotal_" + line).val(formatAmount(line_price));
    
    calculateGrandTotal();
}

function changeWarehouseLine(line, isOrder = true) {
    var productId = $("#product_code_" + line).val();
    var warehouseId = $("#product_warehouse_" + line).val();
    var qty = parseFloat($('#product_qty_' + line).val().replace(/,/g, ''));
    var html = '';
    $("#product_batch_" + line).html(html);
    if (!empty(productId) && !empty(warehouseId) && !empty(qty) && !isNaN(qty)) {
        var exsitedBatch = [];
        var batch = [];
        for (var i = 1; i < $("#row_product_line_no").val(); i++) {
            if (typeof $("#product_code_" + i).val() !== 'undefined') {
                var proId = $("#product_code_" + i).val();
                var warId = $("#product_warehouse_" + i).val();
                var batId = $("#product_batch_" + i).val();
                var quantity = parseFloat($('#product_qty_' + i).val().replace(/,/g, ''));
                if (proId == productId && warId == warehouseId && i != line && !empty(batId) && !empty(quantity)) {
                    if (batch.includes(batId) == true) {
                        for (var b=0; b < exsitedBatch.length; b++){
                            var lineUpdate = exsitedBatch[b];
                            if (lineUpdate.batch_id == batId) {
                                lineUpdate.qty = parseFloat(lineUpdate.qty + quantity);
                                break;
                            }
                        }
                    } else {
                        batch.push(batId);
                        exsitedBatch.push({
                            batch_id: batId,
                            qty: quantity,
                        });
                    }
                }
            }
        }
        
        var data_string = {
            'productId': productId,
            'warehouseId': warehouseId,
            'qty': qty,
            'exsitedBatch': exsitedBatch,
        };
        $.ajax({
            async: false,
            url: '/Order/getBatch',
            type: "POST",
            data: data_string,
            dataType: 'json',
            cache: false,
            success: function (result) {
                if (empty(result.data)) {
                    SwalWarning('Vui lòng chọn kho khác', 'Kho bạn đang chọn không có sản phẩm bạn muốn');
                } else {
                    var batch = result.data;
                    var remainQty = result.remainQty;
                    var takenQty = result.takenQty;
                    for (var i = 0; i < batch.length; i++) {
                        html += '<option value="'+batch[i]['id']+'" >'+batch[i]['code']+'</option>';
                        $("#product_batch_" + line).html(html);
                        $("#product_qty_" + line).val(formatNumberWithCommasAndDots(batch[i]['qty']));
                        lineChangeQty(line, false, isOrder);
                    }
                    if (!empty(remainQty)) {
                        SwalWarning('Vui lòng thêm 1 dòng mới', 'Lô hàng chỉ đủ số lượng '+takenQty+' ');
                    }
                }
            }
        });
    }
}

function removeOrderDetailLine(obj, isOrder = true){
    obj.closest(".tr-order-detail").remove();
    if (isOrder) {
        calculateGrandTotal();
    }
}

function lineChangeQty(line, changeWarehouse = false, isOrder = true) {
    if (changeWarehouse) {
        changeWarehouseLine(line, isOrder);
    }
    
    if (isOrder) {
        var id_product = $("#product_code_" + line).val();
        var qty = parseFloat($('#product_qty_' + line).val().replace(/,/g, ''));
        var price = parseFloat($('#product_price_' + line).val().replace(/,/g, ''));

        var line_price = 0;
        if (id_product != 0 && !isNaN(qty) && !isNaN(price)) {
            line_price = qty * price;

            var discount_value = parseFloat($('#product_discount_' + line).val().replace(/,/g, ''));
            if (!isNaN(discount_value)) {
                if(line_price > 0){
                    var percent = parseFloat((discount_value / line_price) * 100);
                    $("#product_discount_percent_" + line).val(percent);
                } else {
                    $("#product_discount_percent_" + line).val('');
                }
                line_price = line_price - discount_value;
                if (line_price > 0) {
                    line_price = line_price;
                } else {
                    line_price = 0;
                }
            } else {
                $("#product_discount_percent_" + line).val('');
            }
        }
        $("#product_subtotal_" + line).val(formatAmount(line_price));

        calculateGrandTotal();
    }
}

function calculateGrandTotal(){
    var subtotalLinePlus = 0;
    var tax = '';
    /*for (var i = 1; i < $("#row_product_line_no").val(); i++) {
        if (typeof $("#product_subtotal_" + i).val() !== 'undefined') {
            var subtotalLine = parseFloat($("#product_subtotal_" + i).val().replace(/,/g, ''));
            if (!isNaN(subtotalLine)) {
                subtotalLinePlus += subtotalLine;
            }
        }
    }*/
    if (typeof $("#order_price").val() !== 'undefined') {
        var subtotalLine = parseFloat($("#order_price").val().replace(/,/g, ''));
        if (!isNaN(subtotalLine)) {
            subtotalLinePlus += subtotalLine;
        }
    }
    var discount = parseFloat($("#order_discount").val().replace(/,/g, ''));
    if (!isNaN(discount)) {
        var discount_v = parseFloat((subtotalLinePlus * discount) / 100);
        subtotalLinePlus = parseFloat(subtotalLinePlus - discount_v);
    }
    if ($('#order_use_tax').is(':checked')) {
        var tax = parseFloat(subtotalLinePlus * 0.1);
        subtotalLinePlus = parseFloat(subtotalLinePlus + tax);
    }
    if (!empty(tax)) {
        $('#order_tax').val(formatAmount(tax));
    } else {
        $('#order_tax').val('');
    }
    var ship_cost = 0;//parseFloat($("#order_ship_cost").val().replace(/,/g, ''));
    if (!isNaN(ship_cost)) {
        subtotalLinePlus = parseFloat(subtotalLinePlus + ship_cost);
    } else {
        subtotalLinePlus = parseFloat(subtotalLinePlus);
    }
    if(subtotalLinePlus > 0){
        $("#order_final_price").val(formatAmount(subtotalLinePlus));
    } else {
        $("#order_final_price").val(0);
    }
}

function changeShipCost(){
    calculateGrandTotal();
}

function reverseString(str, prefix_split, prefix_join){
    return str.split(prefix_split).reverse().join(prefix_join);
}

function calculateAge(current, target){
    var birthday = $(current).val();
    var age = 0;
    if(birthday != '' || birthday != null){
        var d = new Date();
        var currentYear = parseFloat(d.getFullYear());
        var birthdayReverse = reverseString(birthday, '/', '-');
        var dob = new Date(birthdayReverse);
        var yearOfDOB = parseFloat(dob.getFullYear());
        var ageActually = (currentYear - yearOfDOB);
        if(ageActually >= 0){
            age = ageActually;
        }
    }
    $(target).val(age);
}

function validateCustomerInfo(boolNoErrors, noNeedRequired = false){
    if (ShowErr("#customer_name", "#frm-customer-name", "#customer_name_err", "Tên Khách hàng", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    if (!noNeedRequired) {
        if(ShowErr("#customer_address", "#frm-customer-address", "#customer_address_err", "Địa chỉ", " thì không được trống.") == false){
            boolNoErrors = false;
        }
    } else {
        $("#frm-customer-address").removeClass("has-error");
        $("#customer_address_err").hide();
    }
    if (ShowErr("#customer_phone_0", "#frm-customer-phone", "#customer_phone_err", "Số điện thoại", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    if (ShowErr("#customer_email", "#frm-customer-email", "#customer_email_err", "Email", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    if (ShowErrEmail("#customer_email", "#frm-customer-email", "#customer_email_err", "Vui lòng nhập đúng dạng của email.") == false) {
        boolNoErrors = false;
    }
    /*if (ShowErr("#customer_birthday", "#frm-customer-birthday", "#customer_birthday_err", "Ngày sinh", " thì không được trống.") == false) {
        boolNoErrors = false;
    }*/
    return boolNoErrors;
}

function validateOrderInfo(boolNoErrors) {
    if (ShowErr("#customer_address_shipto", "#frm-customer-address-shipto", "#customer_address_shipto_err", "Địa chỉ giao hàng", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    if (ShowErr("#order_receive_date", "#frm-order-receive-date", "#order_receive_date_err", "Ngày nhận hàng", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    if (ShowErr("#order_ship_date", "#frm-order-ship-date", "#order_ship_date_err", "Ngày giao hàng", " thì không được trống.") == false) {
        boolNoErrors = false;
    }
    return boolNoErrors;
}

function setCustomerPhone() {
    var checkedValues = $('input:hidden.sids').map(function () {
        return this.value;
    }).get();

    $('#convertids').val(checkedValues);
}

function setConsumerPhone() {
    var checkedValuesConsumer = $('input:hidden.sids_consumer').map(function () {
        return this.value;
    }).get();

    $('#convertids_consumer').val(checkedValuesConsumer);
}

function activeTabOrder(tabActive, contentActive, tabInactive_1, contentInactive_1, tabInactive_2, contentInactive_2){
    $(tabActive).addClass('active');
    $(contentActive).addClass('active');
    $(tabInactive_1).removeClass('active');
    $(contentInactive_1).removeClass('active');
    $(tabInactive_2).removeClass('active');
    $(contentInactive_2).removeClass('active');
}

function scrollToErrorField(field) {
    $('html, body').animate({
        scrollTop: $(field).offset().top
    }, 100);
}

function reloadURL() {
    var search = reURL.indexOf('?');
    var prefix = (search > 0) ? '&' : '?';
    var reloadURL = reURL
            + prefix +"from=" + $("#dateFrom").val()
            + "&to=" + $("#dateTo").val();
    window.location = reloadURL;
}

function parseDateValue(rawDate, onlyYear = 0) {
    var dateTemp = rawDate.split(' ');
    var dateArray = dateTemp[0].split('/');
    var parsedDate = dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];
    if (onlyYear == 1) {
        parsedDate = dateArray[2];
    }
    return parsedDate;
}

function toTimestamp(strDate){
    var datum = Date.parse(strDate);
    return datum/1000;
}

function compareDateFilter(txtname, txtname1, frm, name_err, msg){
    if (parseFloat(toTimestamp(parseDateValue($(txtname).val()))) > parseFloat(toTimestamp(parseDateValue($(txtname1).val())))) {
        showErrMsg(txtname, frm, name_err, msg);
        return false;
    } else {
        RemoveErr(frm, name_err);
        return true;
    }
}

function downloadExcel(result, fileName){
    if (result.res == 1 && result.file) {
        var $a = $("<a>");
        $a.attr("href", result.file);
        $("body").append($a);
        $a.attr("download", fileName+".xls");
        $a[0].click();
        $a.remove();
    } else {
        SwalWarning(result.msg, '');
    }
    return;
}

function removeFieldInputGet(){
    $("input").each(function (index, obj) {
        if ($(obj).val() == "") {
            $(obj).remove();
        }
    });
}

function removeFieldSelectGet(){
    $("select").each(function (index, obj) {
        if ($(obj).val() == "") {
            $(obj).remove();
        }
    });
}

function formatAmount(amount){
    return amount.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

$(document).on('keyup', '.number-with-comma', function () {
    $(this).val(formatNumberWithCommasAndDots($(this).val()));
});

function formatNumberWithCommasAndDots(num) {
    if(!num){
        return 0;
    }else{
        num = num + '';
        var decs = num.split(".");
        num = decs[0];
        num = num.replace(/,/gi, "").split("").reverse().join("");

        var num2 = RemoveRogueChar(num.replace(/(.{3})/g, "$1,").split("").reverse().join(""));
        if (num2 + '' == 'NaN') {
            num2 = 0;
        }
        
        if (decs.length > 1) {
            num2 += '.' + decs[1];
        }
        return num2;
    }
}

function RemoveRogueChar(convertString) {
    if (convertString.substring(0, 1) == ",") {
        return convertString.substring(1, convertString.length)
    }
    return convertString;
}

function unBlockSystem(){
    $('body').unblock();
}

function blockSystem(){
    $('body').block({
        message: '<i style="font-size:50px" class="icon-spinner9 spinner"></i>',
        centerY: false,
        centerX: false,
        overlayCSS: {
            backgroundColor: '#1B2024',
            opacity: 0.85,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none',
            color: '#fff',
            position: 'fixed',
            margin: 'auto',
            textAlign: 'center',
            top: ($(window).height() / 2) + 'px',
        }
    });
}

function showHideOrderDetail(obj, orderId, title = 'ĐH') {
    var self = $(obj);
    if (self.hasClass('icon-plus3')) {
        $('#tr_order_detail_' + orderId).show();
        self.removeClass('icon-plus3');
        self.addClass('icon-minus3');
        self.attr('title', 'Ẩn chi tiết ' + title);
    } else {
        $('#tr_order_detail_' + orderId).hide();
        self.removeClass('icon-minus3');
        self.addClass('icon-plus3');
        self.attr('title', 'Xem chi tiết ' + title);
    }
}

function empty(val) {
    return val == undefined || val == null || val == '' || val == 0 || val == false || (typeof val == 'object' && !Object.keys(val).length);
}

function validateOrderDetail(msg){
    var boolNoErrors = true;
    var no = 0;
    var projectId = $('#id_project').val();
    if (empty(projectId)) {
        SwalWarning(msg +'. Vì chưa chọn dự án.', 'Vui lòng chọn dự án');
        boolNoErrors = false;
    }
    var productId = $('#id_product').val();
    if (boolNoErrors && empty(productId)) {
        SwalWarning(msg +'. Vì chưa chọn sản phẩm.', 'Vui lòng chọn sản phẩm');
        boolNoErrors = false;
    }
    return boolNoErrors;
}

function setTimepickerAndSelect2(){
    setTimeout(function() {
        $(".date-custom").datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            closeOnDateSelect: true,
        });
        // Default initialization
        $('.select').select2({
            // minimumResultsForSearch: Infinity
        });
    }, 300);
}

var colorDataTemplate = [
    ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
    ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
    ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
    ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
    ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
    ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
    ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
    ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
]

$(document).ready(function($){
    
    $('.multiselect-select-all-filtering').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        nonSelectedText: (typeof nonSelectedTextCustom !== 'undefined') ? nonSelectedTextCustom : 'Không được chọn',
        allSelectedText: 'Tất cả được chọn',
        filterPlaceholder: 'Tìm kiếm',
        selectAllText: 'Chọn tất cả',
        templates: {
            filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        },
        onSelectAll: function() {
            $.uniform.update();
        }
    });
    
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice',
    });
    
    autosize($('.elastic'));
    
    $(".file-styled-maihan").uniform({
        fileButtonClass: 'action btn bg-green',
        fileButtonHtml: 'Chọn hình',
        //fileDefaultHtml: 'Không có hình ảnh nào được chọn',
    });
    
    $(".file-contract-styled-longtoc").uniform({
        fileButtonClass: 'action btn bg-green',
        fileButtonHtml: 'Chọn file',
        fileDefaultHtml: 'Có thể chọn được nhiều file',
    });
    
    $('.images-contract-styled-longtoc').uniform({
        fileButtonClass: 'action btn bg-green',
        fileButtonHtml: 'Chọn hình',
        fileDefaultHtml: 'Có thể chọn được nhiều hình',
    });
    
    $('#dateFrom').change(function () {
        if (ShowErr("#dateFrom", "#frm-date-from", "#dateFrom_err", 'Từ ngày', ' thì không được trống.') == false) {

        } else if (ShowErr("#dateTo", "#frm-date-to", "#dateTo_err", 'Đến ngày', ' thì không được trống.') == false) {

        } else if (compareDateFilter("#dateFrom", "#dateTo", "#frm-date-from", "#dateFrom_err", 'Từ ngày thì không được lớn hơn Đến ngày') == false) {
            
        } else if (typeof reURL !== 'undefined') {
            reloadURL()
        }
    });
        
    $('#dateTo').change(function () {
        if (ShowErr("#dateTo", "#frm-date-to", "#dateTo_err", 'Đến ngày', ' thì không được trống.') == false) {

        } else if (ShowErr("#dateFrom", "#frm-date-from", "#dateFrom_err", 'Từ ngày', ' thì không được trống.') == false) {

        } else if (compareDateFilter("#dateFrom", "#dateTo", "#frm-date-from", "#dateFrom_err", 'Từ ngày thì không được lớn hơn Đến ngày') == false) {
            
        } else if (typeof reURL !== 'undefined') {
            reloadURL();
        }
    });
    
    $("#dateFrom_err").hide();
    $("#dateTo_err").hide();
    $('.field-error').hide();
        
    datetimepickerCustom("#dateFrom", "d/m/Y");
    datetimepickerCustom("#dateTo", "d/m/Y");
    datetimepickerCustom(".date-custom", "d/m/Y");
    
    ValidateNumber('.input_number_only');
    $('.error_hide').hide();
});


