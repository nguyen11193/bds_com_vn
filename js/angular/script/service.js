if(!app){
	var app = angular.module("app", []); 
}

app.factory('ajaxService', ['$http', function($http){
	var _get = function(url, config) {
		return $http.get(url, config);
	};
	
	var _post = function(url, data, config) {
		return $http.post(url, data, config);
	};

  var _customPost = function(url, data){
    var postData = "";
    var firstIndex = true;
    for (var key in data) {
      if (!data.hasOwnProperty(key)) continue;
      var obj = data[key];
      if(firstIndex){
        postData += key + "=" + obj;
        firstIndex = false;
      }else{     
        postData += "&" + key + "=" + obj;
      }
    }
    return $http.post(url, postData, {headers:{'Content-Type': "application/x-www-form-urlencoded"}})
  }
	
	var _put = function(url, data, config) {
		return $http.put(url, data, config);
	};
	
	var _delete = function(url, config, data) {
		if(data == undefined) {
			return $http.delete(url, config);	
		} else {
			return $http.post(url, data, config);
		}		
	};
	
	return {
		get:_get,
		post:_post,
		put:_put,
		delete:_delete,
		customPost: _customPost
	};
}]);

app.factory('commonService', function(){

	var _mysqlDateToReadableDate = function(mysqlDate){
    var dateParts = mysqlDate.split("-");
    var myDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
    var formatedDate = myDate.getDate() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getFullYear();
    return formatedDate;
	}

  var _beautifyNumber = function(num){
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
		
	return {
		mysqlDateToReadableDate:_mysqlDateToReadableDate,
    beautifyNumber: _beautifyNumber,
	};
});