// function selectExportedWarehouse(index){

//     var warehouseId = document.getElementById("warehouse-export-" + index).value;
//     var html = "<option value='-1'>Chọn mã lô hàng</option>";

//     if(warehouseId != -1){
//         var data = {
//             'warehouseId': warehouseId
//         };

//         $.ajax({
//             async: false,
//             url: '/Warehouse/getBatches',
//             type: "POST",
//             data: data,
//             dataType: 'json',
//             cache: false,
//             success: function (result) {
//                 if (empty(result)) {
//                     SwalWarning('Vui lòng chọn kho khác', 'Kho bạn chọn không có lô hàng nào !');
//                 } else {
//                     var batches = result;
//                     for (var i = 0; i < batches.length; i++) {
//                         html += '<option value="'+batches[i]['id']+'" >'+batches[i]['batch_code']+'  ('+batches[i]['quantity']+' SP)</option>';
//                     }
//                 }
//             }
//         });
//     }

//     $("#batch-id-" + index).html(html);
// }

function selectExportedWarehouse(index){
    var warehouseId = document.getElementById("warehouse-export-" + index).value;
    var html = "<option value='-1'>Chọn mã sản phẩm</option>";

    if(warehouseId != -1){
        var data = {
            'warehouseId': warehouseId
        };

        $.ajax({
            async: false,
            url: '/Warehouse/getProducts',
            type: "POST",
            data: data,
            dataType: 'json',
            cache: false,
            success: function (result) {
                if (empty(result)) {
                    SwalWarning('Vui lòng chọn kho khác', 'Kho bạn chọn không có sản phẩm nào !');
                } else {
                    var products = result;
                    for (var i = 0; i < products.length; i++) {
                        html += '<option value="'+products[i]['id_product']+'" >'+products[i]['product_code']+'</option>';
                    }
                }
            }
        });
    }

    $("#product-id-" + index).html(html);
}

function selectProduct(index){
    var productId = document.getElementById("product-id-" + index).value;
    var warehouseId = document.getElementById("warehouse-export-" + index).value;

    var html = "<option value='-1'>Chọn mã lô hàng</option>";

    if(warehouseId != -1 && productId != -1){
        var data = {
            'warehouseId': warehouseId,
            'productId': productId
        };

        $.ajax({
            async: false,
            url: '/Warehouse/getBatchesByProduct',
            type: "POST",
            data: data,
            dataType: 'json',
            cache: false,
            success: function (result) {
                if (empty(result.batches)) {
                    SwalWarning('Vui lòng chọn sản phẩm khác', 'Sản phẩm bạn chọn không có trong lô hàng nào hoặc sản phẩm đã được lên đơn hàng hết !');
                } else {
                    $("#product-name-" + index).val(result.product.name);
                    var batches = result.batches;
                    for (var i = 0; i < batches.length; i++) {
                        html += '<option value="'+batches[i]['id_batch']+'" >'+batches[i]['batch_code']+' ('+batches[i]['quantity']+' SP - '+batches[i]['expiryDate']+')</option>';
                    }
                }
            }
        });
        $("#batch-id-" + index).html(html);
    }

}

function selectBatch(index){
    var batchId = document.getElementById("batch-id-" + index).value;
    var warehouseId = document.getElementById("warehouse-export-" + index).value;
    var data = {
        batchId: batchId,
        warehouseId: warehouseId
    }
    var batch = AjaxURL(data, "/Batch/getDetail");
    $("#max-quantity-" + index).val(batch.quantity);
}

function submitForm(isFinish){
    var numBatch =  $("#numBatch").val();

    var boolNoErrors = true;

    // Kiểm tra thông số required
    for(var i=1;i<=numBatch;i++){
        if (typeof $("#warehouse-export-" + i).val() !== 'undefined') {
            if(validateSelectField("#warehouse-export-"+i, "#frm-warehouse-export-"+i, "#warehouse-export-err-"+i, "Chưa chọn kho xuất") == false){
                boolNoErrors = false;
            }

            if(validateSelectField("#warehouse-import-"+i, "#frm-warehouse-import-"+i, "#warehouse-import-err-"+i, "Chưa chọn kho nhập") == false){
                boolNoErrors = false;
            }else if(validateImportWarehouse(i) == false){
                boolNoErrors = false;
            }

            if(validateSelectField("#product-id-"+i, "#frm-product-id-"+i, "#product-id-err-"+i, "Chưa chọn sản phẩm") == false){
                boolNoErrors = false;
            }

            if(validateSelectField("#batch-id-"+i, "#frm-batch-id-"+i, "#batch-id-err-"+i, "Chưa chọn lô hàng") == false){
                boolNoErrors = false;
            }

            if(validateField("#product-quantity-"+i, "#frm-product-quantity-"+i, "#product-quantity-err-"+i, "Số lượng SP không được trống") == false){
                boolNoErrors = false;
            }else if(validateQuantity(i) == false){
                boolNoErrors = false;
            }

            if(validateSelectField("#transfer-method-"+i, "#frm-transfer-method-"+i, "#transfer-method-err-"+i, "Chưa chọn phương thức điều chuyển") == false){
                boolNoErrors = false;
            }
        }
    }

    if(boolNoErrors){
        $('#isFinish').val(isFinish);
        document.frmObj.submit(blockSystem());
    }

}

function validateImportWarehouse(i){
    var exportWarehouseId = $("#warehouse-export-"+i).val();
    var importWarehouseId = $("#warehouse-import-"+i).val();
    if(importWarehouseId == exportWarehouseId){
        $('#frm-warehouse-import-'+i).addClass("has-error");
        $('#warehouse-import-err-'+i).html(`Kho nhập phải khác kho xuất`);
        $('#warehouse-import-err-'+i).show();
        return false;
    }else{
        $('#frm-warehouse-import-'+i).removeClass("has-error");
        $('#warehouse-import-err-'+i).hide();
        return true;
    }
}

function validateQuantity(i){
    var maxQuantity = $("#max-quantity-"+i).val();
    var inputQuantity = $("#product-quantity-"+i).val();
    if(parseInt(inputQuantity) > parseInt(maxQuantity)){
        $('#frm-product-quantity-'+i).addClass("has-error");
        $('#product-quantity-err-'+i).html(`Số lượng (${inputQuantity}) nhiều hơn số SP trong lô (${maxQuantity})`);
        $('#product-quantity-err-'+i).show();
        return false;
    }else{
        $('#frm-product-quantity-'+i).removeClass("has-error");
        $('#product-quantity-err-'+i).hide();
        return true;
    }
}

function isEmpty(e) {
    switch (e) {
        case "":
        case 0:
        case "0":
        case null:
        case false:
        case typeof(e) == "undefined":
            return true;
        default:
            return false;
    }
}

function validateSelectField(txtname, frm, name_err, errTxt){
    var e = $(txtname).find(":selected").val();
    if (e < 0 || e == null) {
        $(frm).addClass("has-error");
        $(name_err).html(errTxt);
        $(name_err).show();
        return false;
    } else {
        $(frm).removeClass("has-error");
        $(name_err).hide();
        return true;
    }
}

function validateField(txtname, frm, name_err, errTxt){
    if ($(txtname).val() == "" || $(txtname).val() == null) {
        $(frm).addClass("has-error");
        $(name_err).html(errTxt);
        $(name_err).show();
        return false;
    } else {
        $(frm).removeClass("has-error");
        $(name_err).hide();
        return true;
    }
}