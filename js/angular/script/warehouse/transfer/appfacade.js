angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {

    var _queryInitInfo = function(id) {
        return ajaxService.get("/Warehouse/init");
    }

    return {
        queryInitInfo: _queryInitInfo,
    }

}]);