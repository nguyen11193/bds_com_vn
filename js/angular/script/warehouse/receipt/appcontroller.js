angular.module("app").controller('appController', ['$scope', '$rootScope', 'appFacade', 'commonService', 'backendVar', function($scope, $rootScope, appFacade, commonService, backendVar) {

$scope.products = {};
$scope.warehouses = {};
$scope.suppliers = {};
$scope.wsources = {};
$scope.formData = [];
$scope.quantity = [];
$scope.unitPrice = [];
$scope.netPrice = [];

$scope.totalNetPrice = '0';
$scope.vatPrice = '0';
$scope.totalGrossPrice = '0';

$scope.isVat = vat;

var dataRow = {
    batchCode : '',
    warehouseId : '',
    productId : '',
    productPrice : '',
    supplierId : '',
    manufacturingDate  : '',
    expiryDate : '',
    productQuantity : '',
};

var rowIndex = 0;

var init = function(){
    //$scope.formData.push(rowIndex => dataRow);
    $scope.formData.push(rowIndex);
    //$scope.quantity.push(0);
    appFacade.queryInitInfo(receiptId).then(function success(response) {
        var batchs = response.data.batchs;
        $scope.formData = batchs;
        $scope.products = response.data.products;
        $scope.warehouses = response.data.warehouses;
        $scope.suppliers = response.data.suppliers;
        $scope.wsources = response.data.wsources;
        var header = response.data.header;
        //$scope.isVat = (empty(header.isVat)) ? false : true;
        $scope.totalNetPrice = header.totalNetPrice;
        $scope.vatPrice = header.vatPrice;
        $scope.totalGrossPrice = header.totalGrossPrice;
        for (var i = 0; i < batchs.length; i++) {
            var batch = batchs[i];
            $scope.quantity[i] = (!empty(batch.quantity)) ? formatNumberWithCommasAndDots(batch.quantity) : batch.quantity;
            $scope.unitPrice[i] = (!empty(batch.unit_price)) ? formatNumberWithCommasAndDots(batch.unit_price) : batch.unit_price;
            $scope.netPrice[i] = (!empty(batch.net_price)) ? formatNumberWithCommasAndDots(batch.net_price) : batch.net_price;
            
        }
    }, function error(response) {
        console.log(response);
    });
    
    setTimepickerAndSelect2();

    // Fixed width. Single select
    /*$('.select').select2({
        minimumResultsForSearch: Infinity,
        width: 250,
        containerCssClass: 'select-lg'
    });*/
    
}

$scope.addBatch = function(){
    rowIndex++;
    $scope.formData.push(rowIndex);
    //$scope.quantity.push(0);
    setTimepickerAndSelect2();
}

$scope.removeBatch = function(i){
    $scope.formData.splice(i, 1);
    $scope.quantity.splice(i, 1);
    $scope.unitPrice.splice(i, 1);
    $scope.netPrice.splice(i, 1);
    calcTotalNetPrice();
    $scope.calcVatAndGrossPrice();
    //console.log($scope.formData);
}

$scope.submitForm = function(isFinish){
    var boolNoErrors = true;
    var batchCodeArr = [];

    for (i = 0; i < $scope.formData.length; i++) {

        var thisBatchCode = $("#batchCode"+i).val();
        var batchId = $("#batchCode"+i).data('batchid');
        if (empty(batchId)) {
            batchId = 0;
        }
        if(validateField("#batchCode" + i, "#frm-code" + i, "#code_err" + i, "Mã lô hàng không được để trống") == false){
            boolNoErrors = false;
        }else if(validateBatchCode(thisBatchCode) == false){
            boolNoErrors = false;
        }else if(checkBatchCodeDuplicity(thisBatchCode, batchId, batchCodeArr) == false){
            boolNoErrors = false;
        }else{
            batchCodeArr.push(thisBatchCode);
        }

        if(validateField("#product-quantity" + i, "#frm-product-quantity" + i, "#product-quantity-err" + i, "Số lượng SP không được để trống") == false){
            boolNoErrors = false;
        }

        if(validateField("#net-price" + i, "#frm-net-price" + i, "#net-price-err" + i, "Giá vốn không được để trống") == false){
            boolNoErrors = false;
        }

        if(validateField("#manufacturing-date" + i, "#frm-manufacturing-date" + i, "#manufacturing-date-err" + i, "Ngày sản xuất không được để trống") == false){
            boolNoErrors = false;
        }

        if(validateField("#expiry-date" + i, "#frm-expiry-date" + i, "#expiry-date-err" + i, "Ngày hết hạn không được để trống") == false){
            boolNoErrors = false;
        }

        if(validateSelectField("#product-id" + i, "#frm-product-id" + i, "#product-id-err" + i, "Vui lòng chọn sản phẩm") ==false){
            boolNoErrors = false;
        }

        if(validateSelectField("#warehouse-id" + i, "#frm-warehouse-id" + i, "#warehouse-id-err" + i, "Vui lòng chọn kho nhập") ==false){
            boolNoErrors = false;
        }

        if(validateSelectField("#supplier-id" + i, "#frm-supplier-id" + i, "#supplier-id-err" + i, "Vui lòng chọn nhà cung cấp") ==false){
            boolNoErrors = false;
        }
        
        if(validateSelectField("#source-id" + i, "#frm-source-id" + i, "#source-id-err" + i, "Vui lòng chọn xuất xứ") ==false){
            boolNoErrors = false;
        }

    }

    //Perform Submission
    if (boolNoErrors) {
        $('#isFinish').val(isFinish);
        document.frmObj.submit(blockSystem());
    }
}

$scope.calcVatAndGrossPrice = function(){

    if($scope.isVat){
        var totalNetPrice = parseInt($scope.totalNetPrice.replace(/,/g, ''));

        var vatPrice = Math.round(totalNetPrice * 0.1);
        var totalGrossPrice = totalNetPrice + vatPrice;

        $scope.totalGrossPrice = totalGrossPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $scope.vatPrice = vatPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }else{
        $scope.vatPrice = '0';
        $scope.totalGrossPrice = $scope.totalNetPrice;
    }
}


$scope.calcUnitPrice = function(i){
    if((typeof($scope.quantity[i]) !== "undefined" ) && (typeof($scope.netPrice[i]) !== 'undefined')){
        if($scope.quantity[i] != '' && $scope.netPrice[i] != ''){
            var quantity = parseInt($scope.quantity[i].replace(/,/g, ''));
            var netPrice = parseInt($scope.netPrice[i].replace(/,/g, ''));

            // Tính đơn giá nếu số lượng > 0
            if(quantity > 0){
                var unitPrice = Math.round(netPrice / quantity);
                $scope.unitPrice[i] = unitPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
        }
    }

    if(typeof($scope.netPrice[i]) !== 'undefined'){
        calcTotalNetPrice();
        $scope.calcVatAndGrossPrice();
    }
}

var calcTotalNetPrice = function(){
    var totalNetPrice = 0;
    for(var i = 0; i < $scope.netPrice.length; i++){
        if(typeof($scope.netPrice[i]) !== 'undefined'){
            if($scope.netPrice[i] == ''){
                $scope.netPrice[i] = '0';
            }
            totalNetPrice += parseInt($scope.netPrice[i].replace(/,/g, ''));
        }
    }
    $scope.totalNetPrice = totalNetPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var checkBatchCodeDuplicity = function(batchCode, batchId, batchCodeArr){

    var duplicate = false;

    // check duplicate locally in form
    if(batchCodeArr.indexOf(batchCode) !== -1){
       duplicate = true;
    } 

    // check duplicate in database
    if(!duplicate){
        var data = {
            'id' : batchId,
            'code' : batchCode,
        }
        var result = AjaxURL(data, "/Batch/checkDuplicity/");
        if(result.errName == "yes"){
            duplicate = true;
        }
    }
    
    if(duplicate){
        $('#frm-code'+i).addClass("has-error");
        $("#code_err"+i).html("Mã '"+batchCode+"' đã tồn tại, vui lòng nhập mã khác");
        $("#code_err"+i).show();
        return false;
    } else {
        $('#frm-code'+i).removeClass("has-error");
        $("#code_err"+i).hide();
        return true;
    }
}

var validateBatchCode = function(batchCode){

    if(/^[0-9a-zA-Z_.-]+$/.test(batchCode)){
        $('#frm-code'+i).removeClass("has-error");
        $("#code_err"+i).hide();
        return true;
    } else {
        $('#frm-code'+i).addClass("has-error");
        $("#code_err"+i).html("Chỉ được phép dùng ký tự và số (A-Z,a-z,0-9) hoặc ._- ");
        $("#code_err"+i).show();
        return false;
    }
}

var validateSelectField = function(txtname, frm, name_err, errTxt){
    var e = $(txtname).find(":selected").val();
    if (e < 0 || e == null) {
        $(frm).addClass("has-error");
        $(name_err).html(errTxt);
        $(name_err).show();
        return false;
    } else {
        $(frm).removeClass("has-error");
        $(name_err).hide();
        return true;
    }
}

var validateField = function (txtname, frm, name_err, errTxt){
    if ($(txtname).val() == "" || $(txtname).val() == null) {
        $(frm).addClass("has-error");
        $(name_err).html(errTxt);
        $(name_err).show();
        return false;
    } else {
        $(frm).removeClass("has-error");
        $(name_err).hide();
        return true;
    }
}

init();

}]);
