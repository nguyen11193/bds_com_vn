angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {

    var _queryInitInfo = function(id) {
        return ajaxService.get("/Warehouse/init?id=" + id);
    }

    return {
        queryInitInfo: _queryInitInfo,
    }

}]);