angular.module("app").controller('appController', ['$scope', '$rootScope', 'appFacade', 'commonService', 'backendVar', function($scope, $rootScope, appFacade, commonService, backendVar) {
    $scope.selectedCustomer = {};
    $scope.selectedInteractionDetail = {};

    $scope.loading = false;

    /**
     * Giới hạn chỉ hiện thị 5 đơn hàng & 5 tương tác (nút xem thêm/thu gọn)
     */
    $scope.interactionLimit = {
        limitation: 5,
        isNoLimit: false,
        readmore: 'Xem thêm ...',
        hidden: 'Thu gọn ...'
    };

    $scope.readMoreInteraction = function(){
        if($scope.interactionLimit.isNoLimit){
            return $scope.interactionLimit.hidden;
        }else{
            return $scope.interactionLimit.readmore;
        }
    };

    $scope.orderLimit = {
        limitation: 5,
        isNoLimit: false,
        readmore: 'Xem thêm ...',
        hidden: 'Thu gọn ...'
    };

    $scope.readMoreOrder = function(){
        if($scope.orderLimit.isNoLimit){
            return $scope.orderLimit.hidden;
        }else{
            return $scope.orderLimit.readmore;
        }
    };
    //////////////////////////////////////////////////////////


    $scope.newInteraction = {
        comment: '',
        id_user: backendVar.userId,
        id_customer: 0,
    };
    $scope.newSkinstatus = {
        skin_status: '',
        comment: '',
        id_user: backendVar.userId,
        id_customer: 0,
    };
    $scope.noti = {
        success: {
            display: false,
            msg: ""
        },
        error: {
            display: false,
            msg: ""
        }
    };

    $scope.comment_err = "";

    $scope.interactionSubmit = function() {
        var boolNoErrors = true;
        if (ShowErr("#comment", "#frm-comment", "#comment_err", "Nội dung tương tác", " không được để trống.") == false) {
            boolNoErrors = false;
        }

        if (ShowErr("#recall-date", "#frm-recall", "#recall_err", "Ngày hẹn gọi lại", " không được để trống.") == false) {
            boolNoErrors = false;
        }

        if (boolNoErrors) {
            document.frmObj.submit(blockSystem());
        }
    }

    $scope.recallSubmit = function() {
        document.recallForm.submit();
    }

    $scope.displayCustomerDetail = function($customerId) {
        $('#customer-detail-modal').modal();
        $scope.loading = true;
        appFacade.queryCustomer($customerId).then(function success(response) {
            $scope.selectedCustomer = {
                'info': response.data.customer,
                'phones': response.data.customerPhones,
                'interactions': response.data.customerInteractions,
                'customerOrders': response.data.customerOrders,
            };
            
            if($scope.selectedCustomer.info.birthday)
                $scope.selectedCustomer.info.birthday = commonService.mysqlDateToReadableDate($scope.selectedCustomer.info.birthday);

            $scope.selectedCustomer.customerOrders.forEach(function(element){
                element.displayDetail = false;
                element.final_price = formatNumberWithCommasAndDots(element.final_price) + ' VND';
                element.tax = formatNumberWithCommasAndDots(element.tax) + ' VND';
                element.order_detail.forEach(function(detail){
                    detail.unit_price = formatNumberWithCommasAndDots(detail.unit_price) + ' VND';
                    detail.discount = formatNumberWithCommasAndDots(detail.discount) + ' VND';
                    detail.final_price = formatNumberWithCommasAndDots(detail.final_price) + ' VND';
                });
            });

            // chose new customer --> refresh interaction and history
            refresh();
            var list = document.getElementsByClassName('id_customer');
            var n;
            for (n = 0; n < list.length; ++n) {
                list[n].value = $customerId;
            }

            $scope.loading = false;

        }, function error(response) {
            $('#customer-detail-modal').modal('hide');
            $scope.loading = false;
            SwalWarning(response.status + " ERROR", "Lỗi Kết Nối Hệ Thống");
            console.log("queryCustomer GET error")
            console.log(response);
        });
        
    }

    $scope.displayInteractionDetail = function($interactionId) {
        $scope.selectedInteractionDetail = {};
        appFacade.queryInteraction($interactionId).then(function success(response) {
            $scope.selectedInteractionDetail = response.data;
        }, function error(response) {
            console.log(response);
        });
    }

    $scope.deleteInteraction = function($interactionId) {
        var redirectUrl = BACKEND_URL + "/ConsumerInteraction/delete?id=" + $interactionId;
        SwalConfirm("Xóa tương tác này?", "Bạn sẽ không được hoàn tác lại.", redirectUrl, "Xác nhận xóa", "Hủy bỏ");
    }

    var refresh = function() {

        $scope.newInteraction = {
            comment: '',
            id_user: backendVar.userId,
            id_customer: 0,
        };
        $scope.noti = {
            success: {
                display: false,
                msg: ""
            },
            error: {
                display: false,
                msg: ""
            }
        };
        $scope.newSkinstatus = {
            skin_status: '',
            comment: '',
            id_user: backendVar.userId,
            id_customer: 0,
        };

        $scope.interactionLimit.isNoLimit = false;
        $scope.orderLimit.isNoLimit = false;
    }

    $scope.deleteCustomer = function($customerId) {
        appFacade.verifyDeleteRequest($customerId).then(function success(response) {
            var status = response.data.status;
            var redirectUrl = BACKEND_URL + "/" + ACTION_PATH + "/delete?id=" + $customerId;
            if (status == "yes") {
                SwalWarning("Không thể xóa. Tồn tại đơn hàng của khách hàng này", "Vui lòng kiểm tra lại đơn hàng");
            } else {
                SwalConfirm("Xóa khách hàng này?", "Bạn sẽ không được hoàn tác lại.", redirectUrl, "Xác nhận xóa", "Hủy bỏ");
            }
        }, function error(response) {
            console.log("verifyDeleteRequest GET error")
            console.log(response);
        });
    }

    $scope.submitNewInteraction = function() {
        $('#modal-customer-interaction').modal('hide');

        // Check if interaction is empty
        if ($scope.newInteraction.comment === "") {
            // 1. interaction is empty --> display error
            $scope.noti.error.display = true;
            $scope.noti.error.msg = "Error: Tương tác không được để trống";
        } else {
            // 2. interaction is ok, submit to insert to database
            $scope.newInteraction.id_customer = $scope.selectedCustomer.info.id;
            if ($scope.newInteraction.id_customer != 0) {
                appFacade.createNewInteraction($scope.newInteraction).then(function success(response) {
                    // 2.1 Submit Success --> refresh and display success noti
                    if (response.data.status) {
                        $scope.noti.success.display = true;
                        $scope.noti.success.msg = response.data.message;
                    } else {
                        $scope.noti.error.display = true;
                        $scope.noti.error.msg = response.data.message;
                    }
                }, function error(response) {
                    // 2.2 Ajax Failed --> check console log
                    $scope.noti.error.display = true;
                    $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
                    console.log(response);
                });
            }
        }
    }

    $scope.submitNewSkinstatus = function() {
        $('#modal-customer-skinstatus').modal('hide');

        // Check if interaction is empty
        if ($scope.newSkinstatus.skin_status === "") {
            // 1. interaction is empty --> display error
            $scope.noti.error.display = true;
            $scope.noti.error.msg = "Error: tình trạng da không được để trống";
        } else {
            // 2. interaction is ok, submit to insert to database
            $scope.newSkinstatus.id_customer = $scope.selectedCustomer.info.id;
            if ($scope.newSkinstatus.id_customer != 0) {
                appFacade.createNewSkinstatus($scope.newSkinstatus).then(function success(response) {
                    // 2.1 Submit Success --> refresh and display success noti
                    if (response.data.status) {
                        $scope.noti.success.display = true;
                        $scope.noti.success.msg = response.data.message;
                    } else {
                        $scope.noti.error.display = true;
                        $scope.noti.error.msg = response.data.message;
                    }
                }, function error(response) {
                    // 2.2 Ajax Failed --> check console log
                    $scope.noti.error.display = true;
                    $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
                    console.log(response);
                });
            }
        }
    }

}]);