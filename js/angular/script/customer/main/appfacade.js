angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {
    var _queryCustomer = function(id) {
        return ajaxService.get(BACKEND_URL + "/customer/detail?id=" + id);
    }

    var _verifyDeleteRequest = function(customerId) {
        return ajaxService.get(BACKEND_URL + "/customer/verifyDeleteRequest?id=" + customerId);
    }

    /*
    var _createNewInteraction = function(interaction){
      return ajaxService.get(BACKEND_URL + "/customerInteraction/create?comment=" + interaction.comment + "&id_user=" + interaction.id_user + "&id_customer=" + interaction.id_customer);
    }
    */

    var _createNewInteraction = function(interaction) {
        return ajaxService.customPost(BACKEND_URL + "/customerInteraction/create", interaction);
    }

    var _createNewSkinstatus = function(skinstatus) {
        return ajaxService.customPost(BACKEND_URL + "/customerSkinstatus/create", skinstatus);
    }

    return {
        queryCustomer: _queryCustomer,
        verifyDeleteRequest: _verifyDeleteRequest,
        createNewInteraction: _createNewInteraction,
        createNewSkinstatus: _createNewSkinstatus,
    }
}]);