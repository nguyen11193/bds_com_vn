angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {
    var _queryUserInfo = function(id) {
        return ajaxService.get(BACKEND_URL + "/Useraccount/cusinfo?userId=" + id);
    }


    return {
        queryUserInfo: _queryUserInfo,
    }
}]);