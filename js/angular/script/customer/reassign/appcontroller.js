app.filter('range', function() {
    return function(input, total) {
      total = parseInt(total);
  
      for (var i=0; i<total; i++) {
        input.push(i);
      }
  
      return input;
    };
  });

angular.module("app").controller('reassignController', ['$scope', '$rootScope', 'appFacade', 'commonService', function($scope, $rootScope, appFacade, commonService) {

    $scope.isUserChosen = false;
    $scope.userInfo = {};
    $scope.loading = false;
    $scope.numOfTimerange = 1;

    $scope.selectedUserChange = function(){
        $scope.loading = true;
        appFacade.queryUserInfo($scope.selectedUserId).then(function success(response) {
            $scope.isUserChosen = true;
            $scope.loading = false;
            $scope.userInfo = response.data;
        }, function error(response) {
            $scope.loading = false;
            console.log("queryUserInfo GET error")
            SwalWarning(response.status + " ERROR", "Lỗi Kết Nối Hệ Thống");
            console.log(response);
        });

    }

    $scope.incNumOfTimeRange = function(){
        $scope.numOfTimerange = $scope.numOfTimerange + 1;
        setTimeout(function (){
            datetimepickerCustom(".dateFormat", "d/m/Y");
            $('.bootstrap-select').selectpicker();
          }, 300);
        
    }

    $scope.submitForm = function(){
      var boolNoErrors = true;
      if (validateEmptyField("#fromUser", "#frm-fromUser", "#fromUser_err", "User", " không được để trống.") == false) {
          boolNoErrors = false;
      }

      if (boolNoErrors) {
        document.frmReassignObj.submit();
      }
    }

    var validateEmptyField = function(txtname, frm, name_err, name, require){
      if ($(txtname).val() == "" || $(txtname).val() == null) {
          $(frm).addClass("has-error");
          $(name_err).html(name + require);
          $(name_err).show();
          return false;
      } else {
          $(frm).removeClass("has-error");
          $(name_err).hide();
          return true;
      }
    }

    $scope.removeErr = function(txtName, i){
      var frame = '#frm-' + txtName + i;
      var txtErr = '#' + txtName + i + '_err';
      console.log(frame);
      console.log(txtErr);
      RemoveErr(frame, txtErr);
    }

}]);