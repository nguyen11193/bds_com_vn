angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {
    var _queryCustomer = function(id) {
        return ajaxService.get(BACKEND_URL + "/customer/detail?id=" + id);
    }

    var _verifyDeleteRequest = function(customerId) {
        return ajaxService.get(BACKEND_URL + "/customer/verifyDeleteRequest?id=" + customerId);
    }

    var _createNewInteraction = function(interaction) {
        return ajaxService.customPost(BACKEND_URL + "/customerInteraction/create", interaction);
    }

    var _updateInteraction = function(interaction) {
        return ajaxService.customPost(BACKEND_URL + "/customerInteraction/update", interaction);
    }

    var _deleteInteraction = function(interaction) {
        return ajaxService.customPost(BACKEND_URL + "/customerInteraction/delete", interaction);
    }

    var _createNewSkinstatus = function(skinstatus) {
        return ajaxService.customPost(BACKEND_URL + "/customerSkinstatus/create", skinstatus);
    }

    var _updateSkinstatus = function(skinstatus) {
        return ajaxService.customPost(BACKEND_URL + "/customerSkinstatus/update", skinstatus);
    }

    var _deleteSkinstatus = function(skinstatus) {
        return ajaxService.customPost(BACKEND_URL + "/customerSkinstatus/delete", skinstatus);
    }

    return {
        queryCustomer: _queryCustomer,
        verifyDeleteRequest: _verifyDeleteRequest,
        createNewInteraction: _createNewInteraction,
        updateInteraction: _updateInteraction,
        deleteInteraction: _deleteInteraction,
        createNewSkinstatus: _createNewSkinstatus,
        updateSkinstatus: _updateSkinstatus,
        deleteSkinstatus: _deleteSkinstatus
    }
}]);