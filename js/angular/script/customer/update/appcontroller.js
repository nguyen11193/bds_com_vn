angular.module("app").controller('appController', ['$scope', '$rootScope', 'appFacade', 'commonService', 'backendVar', function($scope, $rootScope, appFacade, commonService, backendVar){
  $scope.selectedCustomer = {};
  $scope.bgColor = {
    interaction: "bg-violet",
    skinstatus: "bg-warning"
  };
  $scope.newInteraction = {
    comment: '',
    id_user: backendVar.userId,
    id_customer: 0,
  };
  $scope.selectedInteraction = {
    comment: '',
    id_user: backendVar.userId,
    id_customer: 0,
  };
  $scope.newSkinstatus = {
    skin_status: '',
    comment: '',
    id_user: backendVar.userId,
    id_customer: 0,
  };
  $scope.selectedSkinstatus = {
    skin_status: '',
    comment: '',
    id_user: backendVar.userId,
    id_customer: 0,
  };
  $scope.noti = {
    success:{
      display: false,
      msg: ""
    },
    error:{
      display: false,
      msg: ""
    }
  };

  var init = function(){
    refreshCustomerInfo();
  }

  // update new customer info, using after every change (insert/update)
  var refreshCustomerInfo = function(){
    appFacade.queryCustomer(backendVar.customerId).then(function success(response){
      $scope.selectedCustomer = {
        'info': response.data.customer,
        'phones': response.data.customerPhones,
        'interactions': response.data.customerInteractions,
        'customerSkinstatus': response.data.customerSkinstatus,
      };  
      $scope.selectedCustomer.info.birthday = commonService.mysqlDateToReadableDate($scope.selectedCustomer.info.birthday);
    },function error(response){
      console.log("queryCustomer GET error")
      console.log(response);
    });

  }

  var refresh = function(){
    $scope.newInteraction = {
      comment: '',
      id_user: backendVar.userId,
      id_customer: 0,
    };
    $scope.noti = {
      success:{
        display: false,
        msg: ""
      },
      error:{
        display: false,
        msg: ""
      }
    };
    $scope.newSkinstatus = {
      skin_status: '',
      comment: '',
      id_user: backendVar.userId,
      id_customer: 0,
    };
  }

  var clearNotification = function(){
    $scope.noti = {
      success:{
        display: false,
        msg: ""
      },
      error:{
        display: false,
        msg: ""
      }
    };
  }

  $scope.deleteCustomer = function($customerId){
    appFacade.verifyDeleteRequest($customerId).then(function success(response){
      var status = response.data.status;
      var redirectUrl = BACKEND_URL + "/" + ACTION_PATH + "/delete?id=" + $customerId;
      if(status == "yes"){
        var orderList = "";
        if(response.data.orders != NULL){
          for(var order in response.data.orders){
            orderList = orderList + order.id + " "; 
          }
        }
        SwalWarning("Không thể xóa. Tồn tại đơn hàng của khách hàng này", "Vui lòng check đơn hàng" + orderList);
      }else{
        SwalConfirm("Xóa khách hàng này?", "Bạn sẽ không được hoàn tác lại.", redirectUrl, "Xác nhận xóa", "Hủy bỏ");
      }
    },function error(response){
      console.log("verifyDeleteRequest GET error")
      console.log(response);
    });
  }

  $scope.selectInteraction = function(key){
    $scope.selectedInteraction = $scope.selectedCustomer.interactions[key];
  }

  $scope.submitEditInteraction = function(){
    clearNotification();
    if($scope.selectedInteraction.comment === ""){
      // 1. interaction is empty --> display error
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "Error: Tương tác không được để trống";
    }else{
      // 2. interaction is ok, submit to insert to database
      $scope.selectedInteraction.id_customer = $scope.selectedCustomer.info.id;
      if($scope.selectedInteraction.id_customer != 0){
        appFacade.updateInteraction($scope.selectedInteraction).then(function success(response){
          // 2.1 Submit Success --> refresh and display success noti
          if(response.data.status){
            $scope.noti.success.display = true;
            $scope.noti.success.msg = response.data.message;
            refreshCustomerInfo();
            $scope.selectedInteraction = {
              comment: '',
              id_user: backendVar.userId,
              id_customer: 0,
            };
          }else{
            $scope.noti.error.display = true;
            $scope.noti.error.msg = response.data.message;
          }
        },function error(response){
          // 2.2 Ajax Failed --> check console log
          $scope.noti.error.display = true;
          $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
          console.log(response);
        });
      }
    }   
  }

  $scope.deleteInteraction = function(key){
    clearNotification();
    $scope.selectInteraction(key);
    appFacade.deleteInteraction($scope.selectedInteraction).then(function success(response){
      // 1 Submit Success --> refresh and display success noti
      if(response.data.status){
        $scope.noti.success.display = true;
        $scope.noti.success.msg = response.data.message;
        refreshCustomerInfo();
        $scope.selectedInteraction = {
          comment: '',
          id_user: backendVar.userId,
          id_customer: 0,
        };
      }else{
        $scope.noti.error.display = true;
        $scope.noti.error.msg = response.data.message;
      }
    },function error(response){
      // 2 Ajax Failed --> check console log
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
      console.log(response);
    });
  }

  $scope.selectSkinstatus = function(key){
    $scope.selectedSkinstatus = $scope.selectedCustomer.customerSkinstatus[key];
  }

  $scope.submitEditSkinstatus = function(){
    clearNotification();
    if($scope.selectedSkinstatus.skin_status === ""){
      // 1. interaction is empty --> display error
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "ERROR: tình trạng da không được để trống";
    }else{
      // 2. interaction is ok, submit to insert to database
      $scope.selectedSkinstatus.id_customer = $scope.selectedCustomer.info.id;
      if($scope.selectedSkinstatus.id_customer != 0){
        appFacade.updateSkinstatus($scope.selectedSkinstatus).then(function success(response){
          // 2.1 Submit Success --> refresh and display success noti
          if(response.data.status){
            $scope.noti.success.display = true;
            $scope.noti.success.msg = response.data.message;
            refreshCustomerInfo();
            $scope.selectedSkinstatus = {
              skin_status: '',
              comment: '',
              id_user: backendVar.userId,
              id_customer: 0,
            };
          }else{
            $scope.noti.error.display = true;
            $scope.noti.error.msg = response.data.message;
          }
        },function error(response){
          // 2.2 Ajax Failed --> check console log
          $scope.noti.error.display = true;
          $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
          console.log(response);
        });
      }
    }
  }

  $scope.deleteCustomerskin = function(key){
    clearNotification();
    $scope.selectSkinstatus(key);
    appFacade.deleteSkinstatus($scope.selectedSkinstatus).then(function success(response){
      // 1 Submit Success --> refresh and display success noti
      if(response.data.status){
        $scope.noti.success.display = true;
        $scope.noti.success.msg = response.data.message;
        refreshCustomerInfo();
        $scope.selectedSkinstatus = {
          skin_status: '',
          comment: '',
          id_user: backendVar.userId,
          id_customer: 0,
        };
      }else{
        $scope.noti.error.display = true;
        $scope.noti.error.msg = response.data.message;
      }
    },function error(response){
      // 2 Ajax Failed --> check console log
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
      console.log(response);
    });
  }

  $scope.confirmDelete = function(title, text, redirectFunc, key){
    swal({
        title: title,
        text: text,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Xác nhận xóa",
        cancelButtonText: "Hủy bỏ",
        closeOnConfirm: true
    },
    function () {
        redirectFunc(key);
    })
  }

  var SwalConfirm = function(title, text, url, btn_confirm, btn_cancel){
    swal({
        title: title,
        text: text,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: btn_confirm,
        cancelButtonText: btn_cancel,
        closeOnConfirm: true
    },
    function () {
        $(location).attr('href', url)
    })
}

  $scope.submitNewInteraction = function(){
    clearNotification();

    // Check if interaction is empty
    if($scope.newInteraction.comment === ""){
      // 1. interaction is empty --> display error
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "Error: Tương tác không được để trống";
    }else{
      // 2. interaction is ok, submit to insert to database
      $scope.newInteraction.id_customer = $scope.selectedCustomer.info.id;
      if($scope.newInteraction.id_customer != 0){
        appFacade.createNewInteraction($scope.newInteraction).then(function success(response){
          // 2.1 Submit Success --> refresh and display success noti
          if(response.data.status){
            $scope.noti.success.display = true;
            $scope.noti.success.msg = response.data.message;
            refreshCustomerInfo();
            $scope.newInteraction = {
              comment: '',
              id_user: backendVar.userId,
              id_customer: 0,
            };
          }else{
            $scope.noti.error.display = true;
            $scope.noti.error.msg = response.data.message;
          }
        },function error(response){
          // 2.2 Ajax Failed --> check console log
          $scope.noti.error.display = true;
          $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
          console.log(response);
        });
      }
    }   
  }

  $scope.submitNewSkinstatus = function(){
    clearNotification();

    // Check if interaction is empty
    if($scope.newSkinstatus.skin_status === ""){
      // 1. interaction is empty --> display error
      $scope.noti.error.display = true;
      $scope.noti.error.msg = "Error: tình trạng da không được để trống";
    }else{
      // 2. interaction is ok, submit to insert to database
      $scope.newSkinstatus.id_customer = $scope.selectedCustomer.info.id;
      if($scope.newSkinstatus.id_customer != 0){
        appFacade.createNewSkinstatus($scope.newSkinstatus).then(function success(response){
          // 2.1 Submit Success --> refresh and display success noti
          if(response.data.status){
            $scope.noti.success.display = true;
            $scope.noti.success.msg = response.data.message;
            // insert successfully, refresh customer info
            refreshCustomerInfo();
            $scope.newSkinstatus = {
              skin_status: '',
              comment: '',
              id_user: backendVar.userId,
              id_customer: 0,
            };
          }else{
            $scope.noti.error.display = true;
            $scope.noti.error.msg = response.data.message;
          }
        },function error(response){
          // 2.2 Ajax Failed --> check console log
          $scope.noti.error.display = true;
          $scope.noti.error.msg = "ERROR: Lỗi kết nối mạng, vui lòng thử lại";
          console.log(response);
        });
      }
    }   
  }

  init();

}]);
