angular.module("app").controller('appController', ['$scope', '$rootScope', 'appFacade', 'commonService', 'backendVar', function($scope, $rootScope, appFacade, commonService, backendVar) {
    $scope.selectedPrecustomer = {};
    $scope.selectedInteractions = {};
    $scope.selectedInteractionDetail = {};
    $scope.newInteraction = {
        comment: "",
    };

    $scope.callInteractionFromOutside = function(precustomerId, precustomerCode, precustomerName) {
        $("#modal-customer-interaction").modal()
        $scope.popupInteraction(precustomerId, precustomerCode, precustomerName);
    }

    $scope.popupInteraction = function(precustomerId, precustomerCode, precustomerName) {

        $scope.selectedPrecustomer = {
            id: precustomerId,
            code: precustomerCode,
            name: precustomerName
        };

        appFacade.queryPrecustomer(precustomerId).then(function success(response) {
            $scope.selectedInteractions = response.data.interactions;
            $scope.selectedPrecustomer.comment = response.data.comment;

            var list = document.getElementsByClassName('id_customer');
            var n;
            for (n = 0; n < list.length; ++n) {
                list[n].value = precustomerId;
            }

            var linkPrefix = document.getElementById("order-create-btn").href;
            document.getElementById("order-create-btn").href = linkPrefix + precustomerId;

        }, function error(response) {
            console.log("queryPrecustomer GET error")
            console.log(response);
        });
    }

    $scope.interactionSubmit = function() {
        var boolNoErrors = true;
        if (ShowErr("#comment", "#frm-comment", "#comment_err", "Nội dung tương tác", " không được để trống.") == false) {
            boolNoErrors = false;
        }

        if (ShowErr("#recall-date", "#frm-recall", "#recall_err", "Ngày hẹn gọi lại", " không được để trống.") == false) {
            boolNoErrors = false;
        }

        if (boolNoErrors) {
            document.frmObj.submit(blockSystem());
        }
    }

    $scope.displayInteractionDetail = function($interactionId) {
        $scope.selectedInteractionDetail = {};
        appFacade.queryInteraction($interactionId).then(function success(response) {
            $scope.selectedInteractionDetail = response.data;
            console.log($scope.selectedInteractionDetail);
        }, function error(response) {
            console.log(response);
        });
    }

    $scope.deleteInteraction = function($interactionId) {
        var redirectUrl = BACKEND_URL + "/PrecustomerInteraction/delete?id=" + $interactionId;
        SwalConfirm("Xóa tương tác này?", "Bạn sẽ không được hoàn tác lại.", redirectUrl, "Xác nhận xóa", "Hủy bỏ");
    }

    $scope.toProcessing = function(precustomerId, precustomerCode, precustomerName){
        $scope.selectedPrecustomer = {
            id: precustomerId,
            code: precustomerCode,
            name: precustomerName
        };
        if($scope.selectedPrecustomer.id){
            $('#modal-customer-processing').modal();
        }
    }

    $scope.reset = function(){
        $scope.selectedPrecustomer = {};
        $scope.selectedInteractions = {};
        $scope.selectedInteractionDetail = {};
        $scope.newInteraction = {
            comment: "",
        };
    }

}]);