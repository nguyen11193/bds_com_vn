angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {

    var _queryInteractions = function(precustomerId) {
        return ajaxService.get(BACKEND_URL + "/PrecustomerInteraction/queryinteractions?precustomerId=" + precustomerId);
    }

    var _queryInteraction = function(id) {
        return ajaxService.get(BACKEND_URL + "/PrecustomerInteraction/detail?id=" + id);
    }

    var _queryPrecustomer = function(id){
        return ajaxService.get(BACKEND_URL + "/Precustomer/detail?id=" + id);
    }

    return {
        queryInteractions: _queryInteractions,
        queryInteraction: _queryInteraction,
        queryPrecustomer: _queryPrecustomer
    }

}]);