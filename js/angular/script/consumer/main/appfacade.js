angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService) {
    var _queryCustomer = function(id) {
        return ajaxService.get(BACKEND_URL + "/Consumer/detail?id=" + id);
    }

    var _queryInteraction = function(id) {
        return ajaxService.get(BACKEND_URL + "/ConsumerInteraction/detail?id=" + id);
    }

    var _verifyDeleteRequest = function(consumerId) {
        return ajaxService.get(BACKEND_URL + "/Consumer/verifyDeleteRequest?id=" + consumerId);
    }

    var _queryAllConsumerImages = function(consumerId){
        return ajaxService.get(BACKEND_URL + "/Consumer/getAllImages?consumerId=" + consumerId)
    }

    var _createNewInteraction = function(interaction) {
        return ajaxService.customPost(BACKEND_URL + "/ConsumerInteraction/create", interaction);
    }

    var _createNewSkinstatus = function(skinstatus) {
        return ajaxService.customPost(BACKEND_URL + "/ConsumerSkinstatus/create", skinstatus);
    }

    var _updateConsumerUsingProduct = function(data){
        return ajaxService.customPost(BACKEND_URL + "/Consumer/updateUsingProduct", data);
    } 

    return {
        queryCustomer: _queryCustomer,
        queryInteraction: _queryInteraction,
        verifyDeleteRequest: _verifyDeleteRequest,
        createNewInteraction: _createNewInteraction,
        createNewSkinstatus: _createNewSkinstatus,
        queryAllConsumerImages: _queryAllConsumerImages,
        updateConsumerUsingProduct: _updateConsumerUsingProduct
    }
}]);