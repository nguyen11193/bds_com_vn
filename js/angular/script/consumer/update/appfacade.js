angular.module("app").factory("appFacade", ["ajaxService", function(ajaxService){
	var _queryCustomer = function(id){
		return ajaxService.get(BACKEND_URL + "/consumer/detail?id=" + id);
	}

  var _verifyDeleteRequest = function(customerId){
    return ajaxService.get(BACKEND_URL + "/consumer/verifyDeleteRequest?id=" + customerId);
  }

  var _createNewInteraction = function(interaction){
    return ajaxService.customPost(BACKEND_URL + "/consumerInteraction/create", interaction);
  }

  var _updateInteraction = function(interaction){
    return ajaxService.customPost(BACKEND_URL + "/consumerInteraction/update", interaction);
  }

  var _deleteInteraction = function(interaction){
    return ajaxService.customPost(BACKEND_URL + "/consumerInteraction/delete", interaction);
  }

  var _createNewSkinstatus = function(skinstatus){
    return ajaxService.customPost(BACKEND_URL + "/consumerSkinstatus/create", skinstatus);
  }

  var _updateSkinstatus = function(skinstatus){
    return ajaxService.customPost(BACKEND_URL + "/consumerSkinstatus/update", skinstatus);
  }

  var _deleteSkinstatus = function(skinstatus){
    return ajaxService.customPost(BACKEND_URL + "/consumerSkinstatus/delete", skinstatus);
  }

	return{
		queryCustomer : _queryCustomer,
    verifyDeleteRequest: _verifyDeleteRequest,
    createNewInteraction: _createNewInteraction,
    updateInteraction: _updateInteraction,
    deleteInteraction: _deleteInteraction,
    createNewSkinstatus: _createNewSkinstatus,
    updateSkinstatus: _updateSkinstatus,
    deleteSkinstatus: _deleteSkinstatus
	}
}]);