app.factory("appFacade", ["ajaxService", function(ajaxService) {

    var _queryInteractions = function(id){
        return ajaxService.get("/InitcustomerInteraction/getByInitcustomerId?initcustomerId=" + id);
    }

    var _submitNewInteraction = function(interaction) {
        return ajaxService.customPost("/InitcustomerInteraction/create", interaction);
    }

    var _deleteInteractions = function(data){
        return ajaxService.customPost("/InitcustomerInteraction/delete", data);
    }

    return {
        queryInteractions: _queryInteractions,
        submitNewInteraction: _submitNewInteraction,
        deleteInteractions: _deleteInteractions,
    }
}]);