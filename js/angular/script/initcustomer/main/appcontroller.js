app.controller('initCustomerController', ['$scope', '$rootScope', 'appFacade', 'commonService', function($scope, $rootScope, appFacade, commonService) {
 
    $scope.userId;

    var init = function(){
        $scope.reset();
    }

    var selectInitcustomer = function(id, name){
        $scope.selectedInitcustomer = {
            'id':id,
            'name':name,
        }
    }

    $scope.popupInteraction = function(inicustomerId, initcustomerName, userId){

        selectInitcustomer(inicustomerId, initcustomerName);
        $scope.userId = userId;

        $scope.loading = true;
        
        $('#initcustomer-interaction-modal').modal();

        appFacade.queryInteractions(inicustomerId).then(function success(response) {
            $scope.loading = false;
            $scope.interactions = response.data;
        }, function error(response) {
            $scope.reset();
            $('#initcustomer-interaction-modal').modal('hide');
            $scope.loading = false;
            console.log("queryInitcustomer GET error")
            SwalWarning(response.status + "ERROR", "Lỗi Kết Nối Hệ Thống");
            console.log(response);
        });
    }

    $scope.readMoreInteraction = function(){
        if($scope.interactionLimit.isNoLimit){
            return $scope.interactionLimit.hidden;
        }else{
            return $scope.interactionLimit.readmore;
        }
    };

    $scope.interactionSubmit = function(){
        var boolNoErrors = true;
        var reason = $('#reason').val();
        if (reason == 0) {
            if (ShowErr("#comment", "#frm-comment", "#comment_err", "Nội dung tương tác", " không được để trống.") == false) {
                boolNoErrors = false;
            }
        }

        if (boolNoErrors) {
            $('#initcustomer-interaction-modal').modal('hide');
            var data = {
                'initCustomerId': $scope.selectedInitcustomer.id,
                'salesId': $scope.userId,
                'comment': $scope.interaction.comment,
                'reason': reason,
            }

            // submit new interaction
            appFacade.submitNewInteraction(data).then(function success(response) {
                $scope.reset();
                if(response.data.status == true){
                    $scope.noti.success.display = true;
                    $scope.noti.success.msg = response.data.message;
                } else if (response.data.status == 'reload') {
                    window.location = "/Initcustomer/main";
                } else{
                    $scope.noti.error.display = true;
                    $scope.noti.error.msg = response.data.message;
                }
            }, function error(response) {
                $scope.reset();
                $('#initcustomer-interaction-modal').modal('hide');
                $scope.loading = false;
                console.log("submitNewInteraction POST error")
                SwalWarning(response.status + "ERROR", "Lỗi Kết Nối Hệ Thống");
                console.log(response);
            });
        }
    }



    $scope.deleteInteraction = function(id){
        $('#initcustomer-interaction-modal').modal('hide');

        $scope.deleteInteractionId = id;

        $('#delete_warning_modal').modal();
    }

    $scope.confirmDelete = function(){
        $('#delete_warning_modal').modal('hide');
        var data = {
            'id': $scope.deleteInteractionId,
        }
        appFacade.deleteInteractions(data).then(function success(response) {
            $scope.reset();
            if(response.data.status == false){
                $scope.noti.error.display = false;
                $scope.noti.error.msg = "Lỗi không thể xóa tương tác";
            }else{
                $scope.noti.success.display = true;
                $scope.noti.success.msg = "Đã xóa tương tác thành công";
            }
        }, function error(response) {
            $scope.reset();
            $scope.loading = false;
            console.log("deleteInteractions POST error")
            SwalWarning(response.status + "ERROR", "Lỗi Kết Nối Hệ Thống");
            console.log(response);
        });
    }

    $scope.deleteInitcustomer = function(id){
        var redirectUrl = "/Initcustomer/delete?id=" + id;
        SwalConfirm("Xóa khách hàng này?", "Bạn sẽ không được hoàn tác lại.", redirectUrl, "Xác nhận xóa", "Hủy bỏ");
    }

    $scope.reset = function(){
        $scope.interactions = {};

        $scope.interaction = {};

        $scope.selectedInitcustomer = {};

        $scope.deleteInteractionId = "";

        /**
         * Giới hạn chỉ hiện thị 5 đơn hàng & 5 tương tác (nút xem thêm/thu gọn)
         */
        $scope.interactionLimit = {
            limitation: 5,
            isNoLimit: false,
            readmore: 'Xem thêm ...',
            hidden: 'Thu gọn ...'
        };

        $scope.noti = {
            success: {
                display: false,
                msg: ""
            },
            error: {
                display: false,
                msg: ""
            }
        };
    }

    init();

}]);

