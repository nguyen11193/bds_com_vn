/*
Navicat MySQL Data Transfer

Source Server         : admin
Source Server Version : 100138
Source Host           : localhost:3306
Source Database       : bds_com_vn

Target Server Type    : MYSQL
Target Server Version : 100138
File Encoding         : 65001

Date: 2022-03-06 23:14:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_asc_system
-- ----------------------------
DROP TABLE IF EXISTS `tbl_asc_system`;
CREATE TABLE `tbl_asc_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `valcount` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `current_month` int(11) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_asc_system
-- ----------------------------
INSERT INTO `tbl_asc_system` VALUES ('1', 'Mã KH', 'KH', '3', null, '2022-03-06 21:39:32', '0');
INSERT INTO `tbl_asc_system` VALUES ('2', 'Mã ĐH', 'ĐH', '4', null, '2022-03-06 18:16:42', '0');
INSERT INTO `tbl_asc_system` VALUES ('3', 'Mã HĐ', 'HĐ', '3', null, '2022-03-06 22:16:04', '0');

-- ----------------------------
-- Table structure for tbl_contract_attach
-- ----------------------------
DROP TABLE IF EXISTS `tbl_contract_attach`;
CREATE TABLE `tbl_contract_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_contract_attach
-- ----------------------------
INSERT INTO `tbl_contract_attach` VALUES ('1', '1', '2', 'contract_file_1_1.pdf', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('2', '1', '1', 'contract_image_1_1.png', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('3', '1', '1', 'contract_image_1_2.png', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('4', '1', '1', 'contract_image_1_3.png', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('5', '1', '1', 'contract_image_1_4.png', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('6', '1', '1', 'contract_image_1_5.png', '2022-03-05 15:24:44', '2022-03-05 15:24:44');
INSERT INTO `tbl_contract_attach` VALUES ('7', '1', '2', 'contract_file_1_2.pdf', '2022-03-05 15:36:01', '2022-03-05 15:36:01');
INSERT INTO `tbl_contract_attach` VALUES ('8', '1', '1', 'contract_image_1_7.jpg', '2022-03-05 21:18:00', '2022-03-05 21:18:00');
INSERT INTO `tbl_contract_attach` VALUES ('9', '3', '2', 'contract_file_3_1.pdf', '2022-03-06 21:59:16', '2022-03-06 21:59:16');
INSERT INTO `tbl_contract_attach` VALUES ('10', '3', '1', 'contract_image_3_1.jpeg', '2022-03-06 21:59:16', '2022-03-06 21:59:16');
INSERT INTO `tbl_contract_attach` VALUES ('11', '2', '2', 'contract_file_2_1.pdf', '2022-03-06 22:16:04', '2022-03-06 22:16:04');

-- ----------------------------
-- Table structure for tbl_customer
-- ----------------------------
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `id_sales` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `revenue` bigint(20) DEFAULT '0',
  `comment` varchar(2000) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ID_INDEX` (`id`,`phone`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO `tbl_customer` VALUES ('1', 'KH000001', null, 'Nguyễn Đình Nguyên', '10 chu văn an, tân thành, tân phú', 'nguyendinhnguyen11193@gmail.com', '1993-01-11', '0', '2022-02-25 13:51:47', '0', 'test', '2022-03-05 23:42:14', '0');
INSERT INTO `tbl_customer` VALUES ('2', 'KH000002', null, 'Nguyễn Chí Thành', '10 chu văn an, tân thành, tân phú', 'thanh@gmail.com', '2022-03-06', '0', '2022-03-06 21:39:32', '0', '', '2022-03-06 21:40:00', '1');

-- ----------------------------
-- Table structure for tbl_customer_phone
-- ----------------------------
DROP TABLE IF EXISTS `tbl_customer_phone`;
CREATE TABLE `tbl_customer_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_customer_phone
-- ----------------------------
INSERT INTO `tbl_customer_phone` VALUES ('1', '1', '0937454451', '2022-03-05 23:42:14', '2022-02-25 13:51:47');
INSERT INTO `tbl_customer_phone` VALUES ('2', '1', '0936217827', '2022-03-05 23:42:14', '2022-03-04 23:32:49');

-- ----------------------------
-- Table structure for tbl_group_permission_map
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group_permission_map`;
CREATE TABLE `tbl_group_permission_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_group` int(11) NOT NULL,
  `id_permission` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='permission and user group mapping';

-- ----------------------------
-- Records of tbl_group_permission_map
-- ----------------------------
INSERT INTO `tbl_group_permission_map` VALUES ('246', '1', '6', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('247', '1', '7', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('248', '1', '8', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('249', '1', '9', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('250', '1', '10', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('251', '1', '11', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('252', '1', '12', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('253', '1', '13', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('254', '1', '14', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('255', '1', '15', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('256', '1', '16', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('257', '1', '17', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('258', '1', '18', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('259', '1', '19', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('260', '1', '20', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('261', '1', '21', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('262', '1', '22', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('263', '1', '23', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('264', '1', '24', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('265', '1', '25', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('266', '1', '26', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('267', '1', '27', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('268', '1', '28', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('269', '1', '29', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('270', '1', '30', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('271', '1', '31', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('272', '1', '1', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('273', '1', '2', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('274', '1', '3', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('275', '1', '4', '2022-03-06 13:35:24', '2022-03-06 13:35:24');
INSERT INTO `tbl_group_permission_map` VALUES ('276', '1', '5', '2022-03-06 13:35:24', '2022-03-06 13:35:24');

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu_parent` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='menu detail';

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO `tbl_menu` VALUES ('1', '1', 'Nhóm người dùng', '/UserGroup/main', '2', null, null);
INSERT INTO `tbl_menu` VALUES ('2', '1', 'Người Dùng', '/Useraccount/main', '1', null, null);
INSERT INTO `tbl_menu` VALUES ('3', '2', 'Danh sách dự án', '/Project/main', '1', '2022-01-14 08:42:26', '2022-01-14 08:42:26');
INSERT INTO `tbl_menu` VALUES ('4', '3', 'Danh sách sản phẩm', '/Product/main', '1', '2022-01-16 00:51:43', '2022-01-16 00:51:43');
INSERT INTO `tbl_menu` VALUES ('5', '4', 'Danh sách khách hàng', '/Customer/main', '1', '2022-02-25 09:38:14', '2022-02-25 09:38:14');
INSERT INTO `tbl_menu` VALUES ('6', '5', 'Danh sách đơn hàng', '/Order/main', '1', '2022-03-04 22:34:11', '2022-03-04 22:34:11');

-- ----------------------------
-- Table structure for tbl_menu_parent
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu_parent`;
CREATE TABLE `tbl_menu_parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `sort_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_menu_parent
-- ----------------------------
INSERT INTO `tbl_menu_parent` VALUES ('1', 'User', 'icon-user-tie', '6', null, null);
INSERT INTO `tbl_menu_parent` VALUES ('2', 'Dự án', 'fa fa-product-hunt', '1', '2022-01-14 08:41:08', '2022-03-06 08:55:14');
INSERT INTO `tbl_menu_parent` VALUES ('3', 'Sản phẩm', 'icon-city', '2', '2022-01-16 00:50:43', '2022-01-16 00:50:43');
INSERT INTO `tbl_menu_parent` VALUES ('4', 'Khách hàng', 'icon-user-check', '3', '2022-02-25 09:37:28', '2022-02-25 09:37:28');
INSERT INTO `tbl_menu_parent` VALUES ('5', 'Đơn hàng', 'icon-cart', '4', '2022-03-04 22:33:34', '2022-03-04 22:33:34');

-- ----------------------------
-- Table structure for tbl_order
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `contract_code` varchar(255) DEFAULT NULL,
  `id_status` int(11) DEFAULT '1',
  `id_sales` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_project` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `unit_price` double DEFAULT '0',
  `discount` double DEFAULT '0' COMMENT 'chiet khau',
  `final_price` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `comments` varchar(2000) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `completed_at` datetime DEFAULT NULL COMMENT 'ngày hoàn tất đơn hàng',
  `is_deleted` tinyint(2) DEFAULT '0',
  `use_tax` tinyint(1) DEFAULT '0',
  `tax` double DEFAULT '0',
  `contract_comment` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_order
-- ----------------------------
INSERT INTO `tbl_order` VALUES ('1', 'ĐH000001', 'HĐ000001', '1', '1', '1', '3', '1', '500000000', '10', '450000000', '2022-02-25 14:48:56', 'test fdfs', '2022-03-06 22:22:14', null, '1', '0', '0', 'tạo hợp đồng f');
INSERT INTO `tbl_order` VALUES ('2', 'ĐH000002', 'HĐ000002', '3', '1', '1', '2', '3', '10000000', '0', '10000000', '2022-03-06 13:56:23', '', '2022-03-06 22:16:04', null, '0', '0', '0', '');
INSERT INTO `tbl_order` VALUES ('3', 'ĐH000003', 'HĐ000001', '4', '1', '1', '3', '2', '5555555555', '0', '5555555555', '2022-03-06 18:16:42', '', '2022-03-06 21:59:16', null, '0', '0', '0', '');

-- ----------------------------
-- Table structure for tbl_order_log
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order_log`;
CREATE TABLE `tbl_order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `comment` text,
  `status_before` int(11) DEFAULT NULL,
  `status_after` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_order_log
-- ----------------------------
INSERT INTO `tbl_order_log` VALUES ('1', '1', '1', 'test', '4', '5', '2022-03-06 13:33:58', '2022-03-06 13:33:58', '0');
INSERT INTO `tbl_order_log` VALUES ('2', '1', '1', 'test', '5', '1', '2022-03-06 13:35:42', '2022-03-06 13:35:42', '0');
INSERT INTO `tbl_order_log` VALUES ('3', '2', '1', '', '0', '2', '2022-03-06 13:56:23', '2022-03-06 13:56:23', '0');
INSERT INTO `tbl_order_log` VALUES ('4', '3', '1', '', '0', '2', '2022-03-06 18:16:42', '2022-03-06 18:16:42', '0');
INSERT INTO `tbl_order_log` VALUES ('5', '3', '1', '', '2', '4', '2022-03-06 21:59:16', '2022-03-06 21:59:16', '0');
INSERT INTO `tbl_order_log` VALUES ('6', '2', '1', '', '2', '3', '2022-03-06 22:16:04', '2022-03-06 22:16:04', '0');

-- ----------------------------
-- Table structure for tbl_permission
-- ----------------------------
DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE `tbl_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_by` int(11) DEFAULT '1',
  `code` varchar(255) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='user permission';

-- ----------------------------
-- Records of tbl_permission
-- ----------------------------
INSERT INTO `tbl_permission` VALUES ('1', '1', 'Xem danh sách nhóm người dùng', '1', 'User_Group_List', '2022-03-06 08:55:33', null);
INSERT INTO `tbl_permission` VALUES ('2', '1', 'Xem chi tiết nhóm người dùng', '1', 'User_Group_View', null, null);
INSERT INTO `tbl_permission` VALUES ('3', '1', 'Thêm nhóm người dùng', '1', 'User_Group_Add', null, null);
INSERT INTO `tbl_permission` VALUES ('4', '1', 'Sửa nhóm người dùng', '1', 'User_Group_Edit', null, null);
INSERT INTO `tbl_permission` VALUES ('5', '1', 'Xóa nhóm người dùng', '1', 'User_Group_Delete', null, null);
INSERT INTO `tbl_permission` VALUES ('6', '2', 'Xem danh sách người dùng', '1', 'User_List', null, null);
INSERT INTO `tbl_permission` VALUES ('7', '2', 'Thêm người dùng', '1', 'User_Add', null, null);
INSERT INTO `tbl_permission` VALUES ('8', '2', 'Sửa người dùng', '1', 'User_Edit', null, null);
INSERT INTO `tbl_permission` VALUES ('9', '2', 'Xóa người dùng', '1', 'User_Delete', null, null);
INSERT INTO `tbl_permission` VALUES ('10', '2', 'Xem và sửa thông tin cá nhân', '1', 'User_Profile', null, null);
INSERT INTO `tbl_permission` VALUES ('11', '3', 'Xem danh sách dự án', '1', 'Project_List', '2022-01-14 08:43:17', '2022-01-14 08:43:17');
INSERT INTO `tbl_permission` VALUES ('12', '3', 'Thêm dự án', '2', 'Project_Add', '2022-01-16 00:31:20', '2022-01-16 00:31:20');
INSERT INTO `tbl_permission` VALUES ('13', '3', 'Sửa dự án', '3', 'Project_Edit', '2022-01-16 00:31:53', '2022-01-16 00:31:53');
INSERT INTO `tbl_permission` VALUES ('14', '3', 'Xóa dự án', '4', 'Project_Delete', '2022-01-16 00:32:28', '2022-01-16 00:32:28');
INSERT INTO `tbl_permission` VALUES ('15', '4', 'Xem danh sách sản phẩm', '1', 'Product_List', '2022-01-16 00:52:22', '2022-01-16 00:52:22');
INSERT INTO `tbl_permission` VALUES ('16', '4', 'Thêm sản phẩm', '2', 'Product_Add', '2022-01-16 00:52:57', '2022-01-16 00:52:57');
INSERT INTO `tbl_permission` VALUES ('17', '4', 'Sửa sản phẩm', '3', 'Product_Edit', '2022-01-16 00:54:31', '2022-01-16 00:53:20');
INSERT INTO `tbl_permission` VALUES ('18', '4', 'Xóa sản phẩm', '4', 'Product_Delete', '2022-01-16 00:54:16', '2022-01-16 00:54:16');
INSERT INTO `tbl_permission` VALUES ('19', '5', 'Xem danh sách khách hàng', '1', 'Customer_List_All', '2022-02-25 09:40:41', '2022-02-25 09:40:41');
INSERT INTO `tbl_permission` VALUES ('20', '5', 'Thêm khách hàng', '2', 'Customer_Add', '2022-02-25 09:41:31', '2022-02-25 09:41:31');
INSERT INTO `tbl_permission` VALUES ('21', '5', 'Sửa khách hàng', '3', 'Customer_Edit', '2022-02-25 09:42:14', '2022-02-25 09:42:14');
INSERT INTO `tbl_permission` VALUES ('22', '5', 'Xóa khách hàng', '4', 'Customer_Delete', '2022-02-25 09:42:47', '2022-02-25 09:42:47');
INSERT INTO `tbl_permission` VALUES ('23', '6', 'Xem danh sách đơn hàng', '1', 'Order_List_All', '2022-03-04 22:37:42', '2022-03-04 22:37:42');
INSERT INTO `tbl_permission` VALUES ('24', '6', 'Thêm đơn hàng', '2', 'Order_Add', '2022-03-04 22:39:01', '2022-03-04 22:39:01');
INSERT INTO `tbl_permission` VALUES ('25', '6', 'Sửa đơn hàng', '3', 'Order_Edit', '2022-03-04 22:39:45', '2022-03-04 22:39:45');
INSERT INTO `tbl_permission` VALUES ('26', '6', 'Xóa đơn hàng', '4', 'Order_Delete', '2022-03-04 22:41:48', '2022-03-04 22:41:48');
INSERT INTO `tbl_permission` VALUES ('27', '6', 'Tạo hợp đồng', '5', 'Contract_Add', '2022-03-06 08:54:12', '2022-03-06 08:54:12');
INSERT INTO `tbl_permission` VALUES ('28', '6', 'Sửa hợp đồng', '6', 'Contract_Edit', '2022-03-06 08:54:53', '2022-03-06 08:54:53');
INSERT INTO `tbl_permission` VALUES ('29', '6', 'Duyệt hợp đồng lần 1', '7', 'Contract_Approve_1', '2022-03-06 08:59:59', '2022-03-06 08:59:59');
INSERT INTO `tbl_permission` VALUES ('30', '6', 'Duyệt hợp đồng lần 2', '8', 'Contract_Approve_2', '2022-03-06 09:00:23', '2022-03-06 09:00:23');
INSERT INTO `tbl_permission` VALUES ('31', '6', 'Hủy hợp đồng', '9', 'Contract_Deny', '2022-03-06 09:01:08', '2022-03-06 09:01:08');

-- ----------------------------
-- Table structure for tbl_product
-- ----------------------------
DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_project` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `acreage` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_product
-- ----------------------------
INSERT INTO `tbl_product` VALUES ('1', '3', 'A-1', '100', '500000000', '1', '0', '2022-01-17 14:24:22', '2022-03-06 18:16:42');
INSERT INTO `tbl_product` VALUES ('2', '3', 'A-2', '344', '5555555555', '2', '0', '2022-01-17 14:35:35', '2022-03-06 18:46:17');
INSERT INTO `tbl_product` VALUES ('3', '2', 'A-1', '45', '10000000', '1', '0', '2022-01-17 14:36:00', '2022-01-17 14:38:53');
INSERT INTO `tbl_product` VALUES ('4', '3', 't', '5', '55', '1', '1', '2022-03-06 21:26:57', '2022-03-06 21:27:22');
INSERT INTO `tbl_product` VALUES ('5', '3', 'y', '6', '6', '1', '1', '2022-03-06 21:28:22', '2022-03-06 21:28:37');

-- ----------------------------
-- Table structure for tbl_project
-- ----------------------------
DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE `tbl_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_project
-- ----------------------------
INSERT INTO `tbl_project` VALUES ('1', 'test', 'test', 'daklak', '1', '1', '2022-01-15 15:19:24', '2022-03-06 21:07:56');
INSERT INTO `tbl_project` VALUES ('2', 'test 2', 'test 2', 'hcm', '0', '1', '2022-01-15 15:22:24', '2022-01-15 15:22:24');
INSERT INTO `tbl_project` VALUES ('3', 'nguyen', 'nguyen', 'bình dương', '0', '1', '2022-01-15 15:23:07', '2022-01-15 15:23:07');

-- ----------------------------
-- Table structure for tbl_user_account
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_account`;
CREATE TABLE `tbl_user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `date_last_login` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UserID` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_user_account
-- ----------------------------
INSERT INTO `tbl_user_account` VALUES ('1', '$2y$13$.ETGFn898ByT4W9LTCbvduPcxU2QbcOcE0vphK8hW748l7m7eFopq', null, 'admin', 'admin', 'dinhnguyen@9itgroup.com', '1', '1.jpg', '1_thumb.jpg', '0', '2022-03-06 08:49:58', '2022-03-06 08:49:58', null);
INSERT INTO `tbl_user_account` VALUES ('2', '$2y$13$NLY.zuOP23gFVe4nsvE.FecanYJ5eDL2x.JUpILZOTzrv6vz3GOBC', '_450684', 'admin 1', 'test', 'abc@gmail.com', '1', null, null, '1', null, '2022-01-10 00:11:01', '2022-01-10 00:05:23');

-- ----------------------------
-- Table structure for tbl_user_group
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_group`;
CREATE TABLE `tbl_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_user_group
-- ----------------------------
INSERT INTO `tbl_user_group` VALUES ('1', 'Giám Đốc', 'Đây là nhóm giám đốc', '0', '2022-03-06 13:35:24', null);
INSERT INTO `tbl_user_group` VALUES ('2', 'test', 'test', '1', '2022-01-11 13:44:17', '2022-01-11 13:43:03');

-- 24/07/2023

CREATE TABLE `tbl_department`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `tbl_user_group` ADD `id_department` INT(11) NULL DEFAULT 0;
ALTER TABLE `tbl_user_account` ADD `id_department` INT(11) NULL DEFAULT 0;

-- 30/07/2023
CREATE TABLE `tbl_schedule_calendar`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_user` int NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_completed` tinyint(1) NULL DEFAULT 0,
  `is_all_day` tinyint(1) NULL DEFAULT 0,
  `type` tinyint NULL DEFAULT NULL,
  `repeat_type` tinyint NULL DEFAULT NULL,
  `start_date` date NULL DEFAULT NULL,
  `end_date` date NULL DEFAULT NULL,
  `start_time` time(0) NULL DEFAULT NULL,
  `end_time` time(0) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `tbl_schedule_calendar_type`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

ALTER TABLE `tbl_schedule_calendar` CHANGE COLUMN `type` `type` INT(11) NULL DEFAULT 0;
ALTER TABLE `tbl_schedule_calendar` ADD `id_child_task` INT(11) NULL DEFAULT 0;
ALTER TABLE `tbl_schedule_calendar` ADD `color` VARCHAR(10) NULL DEFAULT NULL;

CREATE TABLE `tbl_schedule_calendar_action_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_schedule_calendar` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `action_date` date NULL DEFAULT NULL,
  `action_type` int NULL DEFAULT NULL,
  `delete_type` int NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- 31/07/2023
ALTER TABLE `tbl_schedule_calendar_type` ADD `color` VARCHAR(10) NULL DEFAULT NULL;

-- 01/08/2023
ALTER TABLE `tbl_schedule_calendar` ADD `is_deleted` tinyint(1) NULL DEFAULT 0;

-- 02/08/2023
ALTER TABLE `tbl_schedule_calendar_action_history` ADD `updated_info` TEXT NULL DEFAULT NULL;

-- 20/08/2023

CREATE TABLE `tbl_regulation_category`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_department` tinyint(1) NULL DEFAULT 0,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `tbl_regulation`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_regulation_category` int NULL DEFAULT NULL,
  `id_department` int NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `tbl_regulation_log`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_regulation` int NULL DEFAULT NULL,
  `id_user` int NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `log_type` tinyint NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `tbl_regulation_content`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_regulation` int NULL DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `disciplined` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `example` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;