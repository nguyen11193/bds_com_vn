<div class='language-selector-inline <?php echo $cssClass; ?>' style="text-align:right; margin-right: 20px">
    <?php foreach ($languages as $lang => $langName): ?>
        <?php if ($lang == Yii::app()->language): ?>

            <span class='language-selector-active'><?php echo '<img src="' . Yii::app()->request->baseUrl . '/images/language/' . $lang . '1.png"> ' . $langName; ?></span>&nbsp;&nbsp;

        <?php else: ?>

            <span class='language-selector-notactive'>
                <?php
                echo CHtml::link(
                        '<img src="' . Yii::app()->request->baseUrl . '/images/language/' . $lang . '1.png"> ' . $langName . '&nbsp;&nbsp;', EMHelper::createMultilanguageReturnUrl($lang), array('class' => '')
                );
                ?>
            </span>

        <?php endif ?>
    <?php endforeach ?>
</div>
<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
