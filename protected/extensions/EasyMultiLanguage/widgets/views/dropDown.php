<div class='language-selector-dropdown <?php echo $cssClass; ?>'>
    <?php echo CHtml::beginForm('', 'post', array('class' => 'form-horizontal')); ?>

    <?php foreach ($languages as $lang => $langName): ?>
        <?php echo CHtml::hiddenField($lang, EMHelper::createMultilanguageReturnUrl($lang)); ?>
    <?php endforeach ?>
    <?php
    echo CHtml::dropDownList(
            '_language_selector', Yii::app()->language, $languages, array('onchange' => 'this.form.submit()', 'class' => 'form-control')
    );
    ?>
    <?php echo CHtml::endForm(); ?>
</div>
