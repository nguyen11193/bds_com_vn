<?php

class SideMenu extends CWidget {

    public function run() {
        $user = Yii::app()->user;
        $userId = $user->id;
        $userGroupId = $user->groupid;
        $userPermissions = getPermissionsOfUserGroup($userGroupId);
        
        $menu = array();
        foreach ($userPermissions as $permission) {
            if (empty($menu[$permission['menu_parent_id']])) {
                $menu[$permission['menu_parent_id']] = array(
                    'menu_parent_id' => $permission['menu_parent_id'],
                    'menu_parent_icon' => $permission['menu_parent_icon'],
                    'menu_parent_name' => $permission['menu_parent_name'],
                    'menu_child' => array(),
                );
            }
            if (empty($menu[$permission['menu_parent_id']]['menu_child'][$permission['menu_id']])) {
                $menu[$permission['menu_parent_id']]['menu_child'][$permission['menu_id']] = array(
                    'menu_url' => $permission['menu_url'],
                    'menu_name' => $permission['menu_name'],
                );
            }
        }
        
        $this->render('layout/sidemenu', array(
            'menu' => $menu,
        ));
    }

}

?>