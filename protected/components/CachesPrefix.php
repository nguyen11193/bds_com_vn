<?php

define('USER_PERMISSION_CACHE_KEY_PREFIX', 'user_permission_cache');
define('USER_GROUP_PERMISSION_CACHE_KEY_PREFIX', 'user_group_permission_cache');
define('PROJECT_CACHE_KEY_PREFIX', 'project_cache');
define('PRODUCT_CACHE_KEY_PREFIX', 'product_cache');
define('USER_GROUP_CACHE_KEY_PREFIX', 'user_group_cache');
define('DEPARTMENT_CACHE_KEY_PREFIX', 'department_cache');
define('SCHEDULE_CALENDAR_TYPE_CACHE_KEY_PREFIX', 'schedule_calendar_type_cache');
define('MENU_PARENT_CACHE_KEY_PREFIX', 'menu_parent_cache');
define('MENU_CACHE_KEY_PREFIX', 'menu_cache');
define('PERMISSION_CACHE_KEY_PREFIX', 'permission_cache');
define('REGULATION_CATEGORY_CACHE_KEY_PREFIX', 'regulation_category_cache');
define('REGULATION_CACHE_KEY_PREFIX', 'regulation_cache');

