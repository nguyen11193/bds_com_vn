<?php

function createCode($code){
    return $code->value.str_pad($code->valcount, 6, '0', STR_PAD_LEFT);
}

function convertNumberWithCommaToNumber($number){
    return floatval(trim(str_replace(',', '', $number)));
}

function correctDate($date){
    return date('Y-m-d', strtotime(str_replace('/', '-', $date)));
}

function renderPartial($view, $data = null) {
    extract($data);
    ob_start();
    include Yii::getPathOfAlias('application.views') . '/' . $view . '.php';
    return ob_get_clean();
}

function encrypt_url($string, $inKey = false) {
    $key = empty($inKey) ? ENCRYPT_KEY : $inKey; //key to encrypt and decrypts.
    $result = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) + ord($keychar));
        $result .= $char;
    }
    return urlencode(base64_encode($result));
}

function decrypt_url($string, $inKey = false) {
    $key = empty($inKey) ? ENCRYPT_KEY : $inKey; //key to encrypt and decrypts.
    $result = '';
    $string = base64_decode(urldecode($string));
    for ($i = 0; $i < strlen($string); $i++) {
        $char = substr($string, $i, 1);
        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
        $char = chr(ord($char) - ord($keychar));
        $result .= $char;
    }
    return $result;
}

function jcustom_decode($str, $default = []){
    $result = @json_decode($str, true);
    return is_array($result) ? $result : $default;
}

function prepareRadioFilter($name, $lable, $value, $id, $checked) {
    return array(
        'name' => $name,
        'label' => $lable,
        'checked' => $checked,
        'value' => $value,
        'id' => $id,
    );
}

function insertMultipleModel($models, $primaryKey = 'id') {
    if (empty($models)) {
        return true;
    }

    if (defined('NO_BULK_UPDATE')) {
        foreach ($models as $key => $model) {
            $model->save();
        }
        return true;
    } else {
        $insertModels = [];
        $updateModels = [];
        foreach ($models as $key => $item) {
            if (!empty($item->$primaryKey)) {
                $updateModels[] = $item;
            } else {
                $insertModels[] = $item;
            }
        }
        $builder = $models[0]->getCommandBuilder();
        $table = $models[0]->getMetaData()->tableSchema;
        $data = array();

        foreach ($updateModels as $model) {
            if (!$model->save()) {
                return false;
            }
        }

        if (empty($insertModels)) {
            return true;
        }

        foreach ($insertModels as $model) {
            $model->prepareData();
            $modelData = $model->getAttributes();
            unset($modelData[$primaryKey]);
            $data[] = $modelData;
        }
        $command = $builder->createMultipleInsertCommand($table, $data);
        if ($command->execute()) {
            return true;
        }
        return false;
    }
}

function use_esms_api(){
    return defined('USE_ESMS_API');
}

function setCache($key, $value, $expiration = DEFAULT_CACHE_TIME) {
    try {
        @Yii::app()->cache->set($key, $value, $expiration);
    } catch (Exception $e) {
        
    }
}

function getCache($key, $func, $expiration = DEFAULT_CACHE_TIME) {
    $val = get_cache($key, false);
    if (($val === false || $val === null) && !empty($func)) {
        $val = $func();
        setCache($key, $val, $expiration);
    }
    return $val;
}

function get_cache($key, $default = false, $retry = false) {
    $data = false;
    $count = 0;
    while (true) {
        try {
            $data = @Yii::app()->cache->get($key, $default);
        } catch (Exception $e) {
            
        }
        if ($retry && ($data === false || $data === null)) {
            $count++;
            if ($count >= 2) {
                break;
            } else {
                sleep(1);
            }
        } else {
            break;
        }
    }

    return $data === false ? $default : $data;
}

function deleteCache($key){
    Yii::app()->cache->delete($key);
}

function getPermissionsOfUserGroup($userGroupId){
    return getCache(USER_GROUP_PERMISSION_CACHE_KEY_PREFIX . $userGroupId, function() use($userGroupId) {
        return Yii::app()->db->createCommand()
                ->select('ug.id AS group_id, ug.name as group_name, p.id AS permission_id, p.name AS permission_name, p.code AS permission_code, mn.id AS menu_id, mn.name AS menu_name, mn.url AS menu_url, mn.sort_by AS menu_sort_by, mnp.id AS menu_parent_id, mnp.name AS menu_parent_name, mnp.icon AS menu_parent_icon, mnp.sort_by AS menu_parent_sort_by')
                ->from('tbl_user_group ug', 'ua.user_group_id = ug.id')
                ->join('tbl_group_permission_map m', 'ug.id = m.id_user_group')
                ->join('tbl_permission p', 'm.id_permission = p.id')
                ->join('tbl_menu mn', 'p.id_menu = mn.id')
                ->join('tbl_menu_parent mnp', 'mn.id_menu_parent = mnp.id')
                ->where('ug.id = :userGroupId', [':userGroupId' => $userGroupId])
                ->order('mnp.sort_by, mn.sort_by')
                ->queryAll();
    });
}

function getPermissionsOfUser($userId){
    return getCache(USER_PERMISSION_CACHE_KEY_PREFIX . $userId, function() use($userId) {
        return Yii::app()->db->createCommand()
                ->select('ua.id AS user_id, ua.user_name, ug.id AS group_id, ug.name as group_name, p.id AS permission_id, p.name AS permission_name, p.code AS permission_code, mn.id AS menu_id, mn.name AS menu_name, mn.url AS menu_url, mn.sort_by AS menu_sort_by, mnp.id AS menu_parent_id, mnp.name AS menu_parent_name, mnp.icon AS menu_parent_icon, mnp.sort_by AS menu_parent_sort_by')
                ->from('tbl_user_account ua')
                ->join('tbl_user_group ug', 'ua.user_group_id = ug.id')
                ->join('tbl_group_permission_map m', 'ug.id = m.id_user_group')
                ->join('tbl_permission p', 'm.id_permission = p.id')
                ->join('tbl_menu mn', 'p.id_menu = mn.id')
                ->join('tbl_menu_parent mnp', 'mn.id_menu_parent = mnp.id')
                ->where('ua.id = :userId', [':userId' => $userId])
                ->order('mnp.sort_by, mn.sort_by')
                ->queryAll();
    });
}

function getListStatusOfOrder(){
    return array(
        STATUS_OF_ORDER_CANCEL => 'Hủy đơn hàng',
        STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT => 'Chờ lên hợp đồng',
        STATUS_OF_ORDER_CONTRACTED => 'Đã lên hợp đồng (chưa hoàn tất)',
        STATUS_OF_ORDER_PENDING_TO_APPROVE_1 => 'Chờ duyệt lần 1',
        STATUS_OF_ORDER_PENDING_TO_APPROVE_2 => 'Chờ duyệt lần 2',
    );
}

function redirectNoPermission(){
    Yii::app()->request->redirect(Yii::app()->request->baseUrl . '/site/noPermission');
    Yii::app()->end();
}

function getScheduleCalendarType(){
    return ScheduleCalendarTypeExtend::findAllScheduleCalendarTypeFromCache();
}

function getTimes($interval = '+5 minutes') {//$default = '08:00'
    $output = '';
    $current = strtotime('00:00');
    $end = strtotime('23:59');

    while ($current <= $end) {
        $time = date('H:i', $current);
        //$selected = $time === $default ? ' selected' : '';
        //$output .= "<option value=\"{$time}\"{$selected}>" . date('h.i A', $current) . '</option>';
        $output .= "<option value=\"{$time}\">" . $time . '</option>';
        $current = strtotime($interval, $current);
    }

    return $output;
}

function getScheduleCalendarRepeatType(){
    return [
        SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT => 'Không lặp lại',
        SCHEDULE_CALENDAR_REPEAT_TYPE_DAILY => 'Lặp lại hằng ngày',
        SCHEDULE_CALENDAR_REPEAT_TYPE_WEEKLY => 'Lặp lại hằng tuần',
        SCHEDULE_CALENDAR_REPEAT_TYPE_MONTHLY => 'Lặp lại hằng tháng',
    ];
}

function getScheduleCalendarChildTask(){
    return [
        SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK => 'Làm việc',
        SCHEDULE_CALENDAR_CHILD_TASK_TYPE_ASSIGN_TASK => 'Giao việc',
        SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK_CHECK => 'Kiểm tra kiểm soát công việc',
    ];
}

function get_sessionID(){
    return Yii::app()->getSession()->getSessionID();
}

function get_session($key, $default = null){
    return isset(Yii::app()->session[$key]) ? Yii::app()->session[$key] : $default;
}

function set_session($key, $value){
    Yii::app()->session[$key] = $value;
}

function delete_session($key) {
    unset(Yii::app()->session[$key]);
}

function getScheduleCalendarDeleteOption(){
    return [
        SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE => 'Lần này',
        SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE_TO_END => 'Lần này và các lần sau',
        SCHEDULE_CALENDAR_DELETE_TYPE_ALL => 'Tất cả',
    ];
}

function getScheduleCalendarUpdateOption(){
    return [
        SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE => 'lần này',
        SCHEDULE_CALENDAR_UPDATE_TYPE_ALL => 'Tất cả',
    ];
}
