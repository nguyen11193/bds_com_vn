<?php

class Common extends Controller {
    // Start for Long Toc
    
    public static function messageField($idMessageType, $object, &$messageModels, $idOrderStatus = null) {
        $messageType = MessageTypeExtend::model()->findByPk($idMessageType);
        $content = '';
        
        $objectArr = array();
        if (is_object($object) && ($idMessageType == MESSAGE_TYPE_CUSTOMER || $idMessageType == MESSAGE_TYPE_PRECUSTOMER)) {
            $content = sprintf($messageType->content, $object->id, $object->name);
            $objectArr[] = array(
                'id' => $object->id,
                'id_sales' => $object->id_sales,
                'content' => $content,
            );
        } else if (is_array($object) && $idMessageType == MESSAGE_TYPE_ORDER) {
            $orderStatusName = OrderStatusExtend::findByID($idOrderStatus)->name;
            foreach ($object as $ob) {
                $objectArr[] = array(
                    'id' => $ob['id'],
                    'id_sales' => $ob['id_sales'],
                    'content' => sprintf($messageType->content, $ob['id'], $ob['code'], $orderStatusName),
                );
            }
        } else if($idMessageType == MESSAGE_TYPE_NOTI_REASSIGNMENT){
            $statusComment = "";
            if($object['status'] == APPROVED){
                $statusComment = "được duyệt";
            }else if($object['status'] == REJECTED){
                $statusComment = "bị từ chối";
            }
            $content = sprintf($messageType->content, $object['name_request_user'], $object['name_assign_user'],
                                                    $statusComment, $object['name_approval_user']);
            $objectArr[] = array(
                'id' => $object['id'],
                'id_sales' => $object['id_request_user'],
                'content' => $content,
            );

            // Nếu được approved thì cũng gửi thông báo đến cho sales nhận
            if($object['status'] == APPROVED){
                $objectArr[] = array(
                    'id' => $object['id'],
                    'id_sales' => $object['id_assign_user'],
                    'content' => $content,
                );
            }
        }
        
        if (!empty($objectArr)) {
            foreach ($objectArr as $target) {
                $message = new MessageExtend();
                $message->id_message_type = $idMessageType;
                $message->id_user = $target['id_sales'];
                $message->id_order_status = $idOrderStatus;
                $message->id_target = $target['id'];
                $message->content = $target['content'];
                $messageModels[] = $message;
            }
        }
    }
    
    public static function replaceDateForSql($date, $search = '/', $replace = '-'){
        return date('Y-m-d', strtotime(str_replace($search, $replace, $date)));
    }

    public static function filterDateReport($paramDateFrom, $paramDateTo, $other = array()){
        date_default_timezone_set(Yii::app()->params['timezone']);
        if(!empty($paramDateTo) && !empty($paramDateFrom)){
            $dateFrom = $paramDateFrom;
            $dateTo = $paramDateTo;
            
            $oLast = self::replaceDateForSql($dateTo);
            $oFirst = self::replaceDateForSql($dateFrom);
        } else {
            if (!empty($other['day'])) {
                $day = $other['day'];
                $oLast = date('Y-m-d');
                $oFirst = date('Y-m-d', strtotime('-' . $day . ' day', strtotime($oLast)));
            } else {
                $day = 1;
                $oFirst = date_format(new DateTime('first day of this month'), 'Y-m-d');
                $oLast = date_format(new DateTime('last day of this month'), 'Y-m-d');
            }
            
            $dateFrom = date('d/m/Y', strtotime($oFirst));
            $dateTo = date('d/m/Y', strtotime($oLast));
        }
        
        $begin = new DateTime($oFirst);
        $end = new DateTime($oLast);
        $dayReport = array();
        for($i = $begin; $i <= $end; $i->modify('+1 day')){
            $dayReport[$i->format("d/m/Y")] = $i->format("d/m/Y");
        }
        $result['dateFrom'] = $dateFrom;
        $result['dateTo'] = $dateTo;
        $result['dayReport'] = $dayReport;
        return $result;
    }

    public static function filterDate($paramDateFrom, $paramDateTo){
        date_default_timezone_set(Yii::app()->params['timezone']);
        if(!empty($paramDateTo) && !empty($paramDateFrom)){
            $dateFrom = $paramDateFrom;
            $dateTo = $paramDateTo;
        } else {
            $oFirst = new DateTime('first day of this month');
            $oLast = new DateTime('last day of this month');
            $dateFrom = date_format($oFirst, 'd/m/Y');
            $dateTo = date_format($oLast, 'd/m/Y');
        }
        
        $result['dateFrom'] = $dateFrom;
        $result['dateTo'] = $dateTo;
        return $result;
    }
    
    public static function assoc_item($data, $key, $isObject = false) {
        $result = array();
        foreach ($data as $row) {
            if ($isObject && isset($row->$key) ||
                    isset($row[$key])) {
                if ($isObject) {
                    $k = $row->$key;
                } else {
                    $k = $row[$key];
                }
                $result[$k] = $row;
            }
        }
        return $result;
    }
    
    public static function assoc_array($data, $key, $isObject = false) {
        $result = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                if (($isObject && isset($row->$key)) || isset($row[$key])) {
                    $k = $isObject ? $row->$key : $row[$key];
                    $result[$k][] = $row;
                }
            }
        }
        return $result;
    }
    
    public static function changeAlias($str) {
        $str = mb_strtolower($str);
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", "e", $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", "i", $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
        $str = preg_replace("/(đ)/", "d", $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "a", $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "i", $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "o", $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "u", $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "y", $str);
        $str = preg_replace("/(Đ)/", "d", $str);
        $str = preg_replace("/(!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_)/", "-", $str);
        $str = preg_replace("/(^\-+|\-+$)/", "", $str);
        return $str;
    }
    
    public static function commonExcel($sheet){
        $sheet->setShowGridlines(false);
        $sheet->setShowRowColHeaders(true);
        $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setScale(90);
        $sheet->getPageSetup()->setFitToPage(true);
        $sheet->getPageSetup()->setFitToWidth(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        return $sheet;
    }
    
    public static function styleArrayExcel() {
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        return $styleArray;
    }
    
    public static function styleFontExcel() {
        $styleFont = array(
            'font' => array(
                'bold' => true,
                'size' => 15,
                'name' => 'Arial'
            )
        );
        return $styleFont;
    }
    
    public static function shorter($text, $limit) {
        $text = trim($text);
        $str = '';
        if (strlen($text) > $limit) {
            $newText = substr($text, 0, $limit);
            $strs = explode(' ', $newText);
            for ($i = 0; $i < count($strs) - 1; $i++) {
                $str .= $strs[$i] . ' ';
            }
            return $str . "...";
        } else {
            return $text;
        }
    }
    
    public static function beginTransaction() {
        $transaction = Yii::app()->db->getCurrentTransaction();
        if (empty($transaction)) {
            $transaction = Yii::app()->db->beginTransaction();
        }
        return $transaction;
    }

    public static function commitTransaction() {
        $transaction = Yii::app()->db->getCurrentTransaction();
        if (!empty($transaction)) {
            $transaction->commit();
        }
    }

    public static function rollbackTransaction() {
        $transaction = Yii::app()->db->getCurrentTransaction();
        if (!empty($transaction)) {
            $transaction->rollback();
        }
    }
    
    public static function allowImages(){
        return array('jpg', 'jpeg', 'gif', 'png');
    }

    public static function getWorkingDays($startDate, $endDate)
    {
        $begin = strtotime($startDate);
        $end   = strtotime($endDate);
        if ($begin > $end) {
            echo "startdate is larger than enddate! <br />";
            return 0;
        } else {
            $noDays  = 0;
            $weekends = 0;
            while ($begin <= $end) {
                $noDays++; // no of days in the given interval
                $whichDay = date("N", $begin);
                if ($whichDay > 6) { // 7 is weekend day (Sunday)
                    $weekends++;
                };
                $begin += 86400; // +1 day
            };
            $noWorkingDays = $noDays - $weekends;

            return $noWorkingDays;
        }
    }
    
    public static function findCustomerByPhone($phone){
        $custIdArr = array();
        $customerPhone = CustomerPhoneExtend::findCustomerByPhoneSearch(OPERATOR_LIKE, $phone);
        if (!empty($customerPhone)) {
            foreach ($customerPhone as $custId) {
                $custIdArr[] = $custId['id_customer'];
            }
        }
        return $custIdArr;
    }
    
    public static function prepareCustomerField($dataPost, &$customerFields){
        foreach ($dataPost as $custK => $custV) {
            if ($custK == 'birthday') {
                $customerFields[$custK] = (!empty($custV)) ? correctDate(trim($custV)) : null;
            } else if ($custK == 'province_id') {
                $customerFields[$custK] = trim($custV);
                $customerFields['province_name'] = ProvinceExtend::model()->findByPk(trim($custV))->name;
            } else if ($custK == 'gender_id') {
                $customerFields[$custK] = trim($custV);
                $customerFields['gender'] = GenderExtend::model()->findByPk(trim($custV))->name;
            } else if ($custK == 'id_category') {
                $customerFields[$custK] = trim($custV);
                $customerFields['category_name'] = CustomerCategoryExtend::model()->findByPk(trim($custV))->name;
            } else {
                $customerFields[$custK] = trim($custV);
            }
        }
    }
    
    public static function prepareCustomerInfo($customer){
        $customerPhone = array();
        $customerPhone[] = array(
            'phone' => '',
        );
        $info = array(
            'id' => 0,
            'code' => '',
            'name' => '',
            'address' => '',
            'email' => '',
            'birthday' => '',
            'phone' => $customerPhone,
            'comment' => '',
        );
        if (!empty($customer)) {
            $custId = $customer->id;
            $customerPhone = CustomerPhoneExtend::findOneCustomerPhonesByCondition(array('id_customer' => $custId));
            $info['id'] = $custId;
            $info['code'] = $customer->code;
            $info['name'] = $customer->name;
            $info['address'] = $customer->address;
            $info['email'] = $customer->email;
            $info['birthday'] = date('d/m/Y', strtotime($customer->birthday));
            $info['phone'] = $customerPhone;
            $info['comment'] = $customer->comment;
        }
        return array(
            'customer_info' => $info,
            'customer_phone' => $customerPhone,
        );
    }
    
    public static function getPhonesOfCustomer($items){
        $custIds = array();
        if (!empty($items)) {
            foreach ($items as $item) {
                if (!in_array($item['id_customer'], $custIds)) {
                    $custIds[] = $item['id_customer'];
                }
            }
        }
        $phones = array();
        if (!empty($custIds)) {
            $phones = CustomerPhoneExtend::getAllPhones($custIds);
            $phones = Common::assoc_array($phones, 'id_customer');
        }
        return $phones;
    }
    
    public static function getAllContractAttachs(&$files, &$images, $orderId, $prefix = ''){
        $contractAttachs = ContractAttachExtend::getAllContractAttach($orderId);
        if (!empty($contractAttachs)) {
            foreach ($contractAttachs as $attach) {
                if ($attach['type'] == CONTRACT_ATTACH_FILE) {
                    $files[$attach['id']] = $prefix . CONTRACT_FILE_PATH . $attach['file_name'];
                } else if ($attach['type'] == CONTRACT_ATTACH_IMAGE) {
                    $images[$attach['id']] = $prefix . CONTRACT_IMAGE_PATH . $attach['file_name'];
                }
            }
        }
    }

    public static function prepareDataScheduleIndex($request)
    {
        $defaultDateCalendar = date('Y-m-d');

        if (!empty($request->getParam('defaultDate'))) {
            $defaultDateCalendar = trim($request->getParam('defaultDate'));
        }

        return [
            'defaultDate' => $defaultDateCalendar,
            'users' => UserAccountExtend::sqlfindAllUserAccount(),
            'currentUserId' => Yii::app()->user->id,
        ];
    }

    // End for Long Toc
}

