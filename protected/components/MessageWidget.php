<?php

class MessageWidget extends CWidget {
    
    public function run() {
        $countMsg = 0;
        if (!empty(Yii::app()->user->id)) {
            $countMsg = MessageExtend::countMessage(Yii::app()->user->id);
        }
        $this->render('messageWidget', array(
            'countMsg' => $countMsg,
        ));
    }
}

