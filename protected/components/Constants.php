<?php

// Status's Order
define('STATUS_OF_ORDER_CANCEL', 1);
define('STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT', 2);
define('STATUS_OF_ORDER_CONTRACTED', 3);
define('STATUS_OF_ORDER_PENDING_TO_APPROVE_1', 4);
define('STATUS_OF_ORDER_PENDING_TO_APPROVE_2', 5);
define('STATUS_OF_ORDER_NEXT', 6);

// JS and CSS
define('JS_CUSTOM_NGUYEN_VERSION', 3);
define('CSS_MAIHAN_CUSTOM_STYLE_VERSION', 2);

// PATH, DIRECTORY, FILENAME
define('USER_PROFILE_PATH', 'images/profile_images/');
define('CONTRACT_IMAGE_PATH', 'contracts/images/');
define('CONTRACT_FILE_PATH', 'contracts/files/');

// find one or find all
define('FIND_ONE', 'one');
define('FIND_ALL', 'all');

// Message Type
define('MESSAGE_TYPE_CUSTOMER', 1);
define('MESSAGE_TYPE_PRECUSTOMER', 2);
define('MESSAGE_TYPE_ORDER', 3);
define('MESSAGE_TYPE_NOTI_REASSIGNMENT', 4);

// Limit of Message
define('LIMIT_MESSAGE', 10);

// Limit of items
define('LIMIT_ITEMS', 100);

// Limit of page
define('MAX_VISIBLE', 5);

// Type of Report
define('TYPE_REPORT_PERCENT', 1);
define('TYPE_REPORT_NORMAL', 2);
define('TYPE_REPORT_CURRENCY', 3);
define('TYPE_REPORT_CURRENCY_TOTAL', 4);
define('VIETNAM_CURRENCY', 'VND');//₫
define('STT', 'STT');
define('LENGTH_OF_TITLE_ALLOW_PHP_EXCEL', 31);

define('LIMIT_TOP_PRODUCT', 15);

define('SYSTEM_ERROR', 'Chưa lưu được dữ liệu vào hệ thống. Vui lòng thử lại.');
define('SYSTEM_ERROR_IMAGE', 'Hệ thống chỉ hỗ trợ hình ảnh và file theo các định dạng sau (jpg, jpeg, gif, png, pdf). Vui lòng thử lại.');

// ASC SYSTEM
define('CUSTOMER_CODE', 'KH');
define('ORDER_CODE', 'ĐH');
define('CONTRACT_CODE', 'HĐ');

// Method
define('METHOD_POST', 'POST');
define('METHOD_GET', 'GET');
define('METHOD_PUT', 'PUT');
define('METHOD_PATCH', 'PATCH');
define('INVALID_METHOD_POST_MSG', 'Không đúng phương thức POST');

// log
define('LOG_LEVEL_DEBUG_ZALO_SMS', 'debugZaloSMS');
define('LOG_CATEGORY_ZALO_SMS', 'zalo_sms');

define('DEFAULT_CACHE_TIME', 0);

define('LOGO', '/images/Logo_khoi_nguon.jpg');

define('SAVE_INFO_TEXT', 'Lưu thông tin');
define('FIELD_REQUIRE_MSG', 'thì không được trống.');
define('EXISTED_IN_SYS_MSG', 'đã tồn tại trong hệ thống.');
define('SEARCH_TEXT', 'Tìm kiếm ...');

define('OPERATOR_LIKE', 'like');
define('OPERATOR_EQUAL', 'equal');

define('STATUS_OF_PRODUCT_OPEN', 1);
define('STATUS_OF_PRODUCT_BOOKING', 2);
define('STATUS_OF_PRODUCT_BOOKED', 3);
define('STATUS_OF_PRODUCT_CLOSED', 4);

define('CONTRACT_ATTACH_IMAGE', 1);
define('CONTRACT_ATTACH_FILE', 2);

define('CONTRACT_PREFIX_IMAGE', 'contract_image');
define('CONTRACT_PREFIX_FILE', 'contract_file');

define('CONTRACT_DRAFT', 1);
define('CONTRACT_FINISHED', 2);

define('SCHEDULE_CALENDAR_TYPE_TASK', 1);
define('SCHEDULE_CALENDAR_TYPE_PLAN', 2);
define('SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT', 1);
define('SCHEDULE_CALENDAR_REPEAT_TYPE_DAILY', 2);
define('SCHEDULE_CALENDAR_REPEAT_TYPE_WEEKLY', 3);
define('SCHEDULE_CALENDAR_REPEAT_TYPE_MONTHLY', 4);

define('SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK', 1);
define('SCHEDULE_CALENDAR_CHILD_TASK_TYPE_ASSIGN_TASK', 2);
define('SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK_CHECK', 3);

define('SCHEDULE_CALENDAR_DEFAULT_COLOR', '#27ADCA');
define('SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR', '#FFFFFF');

define('USER_GROUP_WEB_ID', 9);
define('USER_WEB_ID', 5);

define('SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_COMPLETE', 1);
define('SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE', 2);
define('SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE', 3);

define('SYSTEM_ACCOUNT', 'nguyennd@1256');
define('SYSTEM_ACCOUNT_SESSION_KEY', 'sys_account_key');

define('SCHEDULE_CALENDAR_DELETE_TYPE_ALL', 1);
define('SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE', 2);
define('SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE_TO_END', 3);

define('SCHEDULE_CALENDAR_UPDATE_TYPE_ALL', 1);
define('SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE', 2);

define('SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT', 1);
define('SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE', 2);

define('SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE', 'schedule-incomplete');
define('SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED', 'schedule-completed');

define('LOG_TYPE_ADD', 1);
define('LOG_TYPE_EDIT', 2);
define('LOG_TYPE_DELETE', 3);
