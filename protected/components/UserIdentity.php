<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */

    private $_id;

    public function authenticate() {
        $userName = strtolower($this->username);
        $user = UserAccountExtend::model()->find('is_deleted = 0 AND LOWER(user_name)=?', array($userName));
        if ($user === null){
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (!CPasswordHelper::verifyPassword($this->password, $user->password)){
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->id;
            $this->username = $user->user_name;
            $this->errorCode = self::ERROR_NONE;

            //Set the role into Persistent States
            $this->setPersistentStates(array(
                'username' => $user->user_name, 
                'groupid' => $user->user_group_id, 
                'fullname' => $user->full_name,
                'image' => $user->image,
                'departmentId' => $user->id_department,
            ));
        }
        return $this->errorCode == self::ERROR_NONE;
    }

    public function getId() {
        return $this->_id;
    }

}
