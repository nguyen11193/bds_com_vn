<?php

/**
 * Helper Functions for application wide security
 *
 * Note From William
 * I have decided to define all the roles here to centralise all security definitions.  Making it easier
 * for future models/controllers declarations.
 */
class UtilSecurity {
    
    public static function accessValidate($type){
        //Check if user is login.
        if (Yii::app()->user->isGuest) {
            Yii::app()->request->redirect(Yii::app()->request->baseUrl . '/site/login');
            Yii::app()->end();
        }
        $user = Yii::app()->user;
        
        $perms = getPermissionsOfUserGroup($user->groupid);
        $perms = Common::assoc_item($perms, 'permission_code');
        
        $check = true;
        $permList = array();
        if (!is_array($type)) {
            $type = [$type];
        }
        foreach ($type as $v) {
            if (!empty($perms[$v])) {
                if ($check) {
                    $check = false;
                }
                $permList[$v] = array(
                    'code' => $v,
                    'detail' => $perms[$v],
                );
            }
        }
        return array(
            'check' => $check,
            'permList' => $permList,
            'user' => $user,
        );
    }

    /**
     * Check Roles
     */
    public static function accessCheck($type, $isAjax = false) {
        $check = self::accessValidate($type);
        $user = $check['user'];
        if ($check['check'] || ($user->groupid == USER_GROUP_WEB_ID && $user->id != USER_WEB_ID)) {
            if ($isAjax) {
                return false;
            }
            // no access
            redirectNoPermission();
        }
        return $check['permList'];
    }

}

?>