<!--  For DatePicker -->
<link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/jquery.datetimepicker.css"/ >
<!-- Load jQuery UI Main JS  -->
<script src="<?= Yii::app()->request->baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div class="col-lg-<?php echo $this->widthCol ?>">
    <div class="input-group m-a">				
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <input autocomplete="off" type="text" id="dateFrom" name="dateFrom" value="<?php echo $this->dateFrom ?>" class="form-control">
    </div>
    <div class="error" id="dateFrom_err" style="display: hidden"></div>
</div>
<div style="text-align: center" class="col-lg-1 label-custom"><strong>Đến</strong></div>
<div class="col-lg-<?php echo $this->widthCol ?>">
    <div class="input-group m-a">				
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <input autocomplete="off" type="text" id="dateTo" name="dateTo" value="<?php echo $this->dateTo ?>" class="form-control">
    </div>
    <div class="error" id="dateTo_err" style="display: hidden"></div>
</div>