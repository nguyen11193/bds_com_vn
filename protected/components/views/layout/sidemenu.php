<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">
        <!-- Logo box -->
        <div class="sidebar-user">
            <div class="category-content" >
                <div class="media" style="text-align:center">
                    <a href="#" class="media-body"><img style="height: 130px" src="<?php echo Yii::app()->request->baseUrl.LOGO; ?>" alt="logo"></a><!--70px-->
                </div>
            </div>
        </div>
        <!-- /Logo box -->
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Main -->
                    <li class="navigation-header"><span>MENU</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Dashboard/main'); ?>"><i class="icon-home4"></i> <span>Trang chủ</span></a></li>
                    <?php foreach ($menu as $parent): ?>
                    <li>
                        <a href="#">
                            <i class="<?php echo $parent['menu_parent_icon'] ?>"></i> 
                            <span><?php echo $parent['menu_parent_name'] ?>
                            </span>
                        </a>
                        <ul>
                        <?php foreach ($parent['menu_child'] as $child): ?>
                            <li>
                                <a href="<?php echo Yii::app()->createAbsoluteUrl($child['menu_url']); ?>">
                                    <i class="fa fa-angle-right"></i> <?php echo $child['menu_name'] ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php endforeach;?>                                   
                    <!-- /main -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->