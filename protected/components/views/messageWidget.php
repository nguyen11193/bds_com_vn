<a onclick="loadMessage()" href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="icon-bubbles4"></i>
    <span class="visible-xs-inline-block position-right">Tin nhắn</span>
    <?php if (!empty($countMsg)): ?>
        <span class="badge bg-warning-400"><?php echo $countMsg; ?></span>
    <?php endif; ?>
</a>
<script type="text/javascript">
    function loadMessage() {
        var data_string = {
            'limit': '<?php echo LIMIT_MESSAGE; ?>',
            'offset': 0,
        };
        $.ajax({
            async: false,
            url: '<?php echo Yii::app()->createAbsoluteUrl('/Site/loadMessage/'); ?>',
            type: 'POST',
            data: data_string,
            dataType: 'json',
            cache: false,
            success: function (result) {
                var message = result.data;
                var html = '';
                if (message.length > 0) {
                    for (var i = 0; i < message.length; i++) {
                        html += '<li onclick="window.location.href=\'/'+message[i].link+'\'" class="media">';
                        html +=     '<div class="media-left"><i class="text-maihan '+message[i].icon+'"></i></div>';
                        html +=     '<div class="media-body">';
                        html +=         '<a href="/'+message[i].link+'" class="media-heading"';
                        html +=             '<span class="text-semibold">'+message[i].name+'</span>';
                        html +=             '<span class="media-annotation pull-right">'+message[i].created_at+'</span>';
                        html +=         '</a>';
                        html +=         '<span class="text-muted">'+message[i].content+'</span>';
                        html +=     '</div>';
                        html += '</li>';
                    }
                } else {
                    html += '<li class="media">';
                    html +=     '<div class="media-left"><i class="icon-home4"></i></div>';
                    html +=     '<div class="media-body">';
                    html +=         '<span class="text-muted">Bạn không có thông báo nào.</span>';
                    html +=     '</div>';
                    html += '</li>';
                }
                $('#load_message').html(html);
            }
        });
    }
</script>
