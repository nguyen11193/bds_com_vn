<?php

/**
 * This is the model class for table "{{order_log}}".
 *
 * The followings are the available columns in table '{{order_log}}':
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_user
 * @property string $comment
 * @property integer $status_before
 * @property integer $status_after
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_deleted
 */
class OrderLog extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order_log}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrderLog the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
