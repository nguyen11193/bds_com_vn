<?php

/**
 * This is the model class for table "{{contract_attach}}".
 *
 * The followings are the available columns in table '{{contract_attach}}':
 * @property integer $id
 * @property integer $id_order
 * @property integer $type
 * @property string $file_name
 * @property string $created_at
 * @property string $updated_at
 */
class ContractAttach extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{contract_attach}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContractAttach the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
