<?php

/**
 * This is the model class for table "{{customer}}".
 *
 * The followings are the available columns in table '{{customer}}':
 * @property integer $id
 * @property string $code
 * @property string $phone
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $birthday
 * @property integer $id_sales
 * @property string $created_at
 * @property string $revenue
 * @property string $comment
 * @property string $updated_at
 * @property integer $is_deleted
 */
class Customer extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{customer}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Customer the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
