<?php

/**
 * This is the model class for table "{{project}}".
 *
 * The followings are the available columns in table '{{project}}':
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $address
 * @property integer $is_deleted
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Project extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{project}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Project the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function beforeSave() {
        deleteCache(PROJECT_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
}
