<?php

/**
 * This is the model class for table "{{order}}".
 *
 * The followings are the available columns in table '{{order}}':
 * @property integer $id
 * @property string $code
 * @property string $contract_code
 * @property integer $id_status
 * @property integer $id_sales
 * @property integer $id_customer
 * @property integer $id_project
 * @property integer $id_product
 * @property double $unit_price
 * @property double $discount
 * @property double $final_price
 * @property string $created_at
 * @property string $comments
 * @property string $updated_at
 * @property string $completed_at
 * @property integer $is_deleted
 * @property integer $use_tax
 * @property double $tax
 * @property string $contract_comment
 */
class Order extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
