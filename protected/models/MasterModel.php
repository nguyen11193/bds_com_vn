<?php

abstract class MasterModel extends CActiveRecord {

    public function beforeSave() {
        self::prepareData();
        return parent::beforeSave();
    }
    
    public function prepareData(){
        date_default_timezone_set(Yii::app()->params['timezone']);
        $currentTime = date('Y-m-d H:i:s');
        if ($this->isNewRecord) {
            $this->created_at = $currentTime;
        }
        $this->updated_at = $currentTime;
    }
    
    public static function findByCondition($model, $oneOrAll, $conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array(), $conditionAddNotInCondition = array()){
        $result = NULL;
        $critetia = new CDbCriteria;
        if(!empty($conditionToCompare)){
            foreach ($conditionToCompare as $k => $v){
                if(!empty($k)){
                    $critetia->compare($k, $v);
                }
            }
        }
        if(!empty($conditionOthers)) {
            foreach ($conditionOthers as $k => $v) {
                $critetia->$k = $v;
            }
        }
        if(!empty($conditionAddCondition)) {
            foreach ($conditionAddCondition as $v) {
                $critetia->addCondition($v);
            }
        }
        if(!empty($conditionAddInCondition)) {
            foreach ($conditionAddInCondition as $k => $v) {
                $critetia->addInCondition($k, $v);
            }
        }
        if (!empty($conditionAddNotInCondition)) {
            foreach ($conditionAddNotInCondition as $k => $v) {
                $critetia->addNotInCondition($k, $v);
            }
        }
        $result = ($oneOrAll == FIND_ONE) ? $model->find($critetia) : $model->findAll($critetia);
        return $result;
    }
    
    public static function saveInfo($model, $fields = array()) {
        $result = false;
        if (!empty($fields) && !empty($model)) {
            foreach ($fields as $k => $v) {
                if ($model->hasAttribute($k)) {
                    if (isset($v)) {
                        $model->$k = $v;
                    }
                }
            }
            $result = $model->save();
        }
        return $result;
    }

}
