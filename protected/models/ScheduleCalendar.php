<?php

class ScheduleCalendar extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{schedule_calendar}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
