<?php

/**
 * This is the model class for table "{{permission}}".
 *
 * The followings are the available columns in table '{{permission}}':
 * @property integer $id
 * @property integer $id_menu
 * @property string $name
 * @property integer $sort_by
 * @property string $code
 * @property string $updated_at
 * @property string $created_at
 */
class Permission extends MasterModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{permission}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_menu, name, code', 'required'),
			array('id_menu, sort_by', 'numerical', 'integerOnly'=>true),
			array('name, code', 'length', 'max'=>255),
			array('updated_at, created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_menu, name, sort_by, code, updated_at, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_menu' => 'Id Menu',
			'name' => 'Name',
			'sort_by' => 'Sort By',
			'code' => 'Code',
			'updated_at' => 'Updated At',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sort_by',$this->sort_by);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Permission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave() {
        deleteCache(PERMISSION_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
    
    public function beforeDelete() {
        deleteCache(PERMISSION_CACHE_KEY_PREFIX);
        return parent::beforeDelete();
    }
}
