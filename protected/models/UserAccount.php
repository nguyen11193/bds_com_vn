<?php

/**
 * This is the model class for table "{{user_account}}".
 *
 * The followings are the available columns in table '{{user_account}}':
 * @property integer $id
 * @property string $password
 * @property string $salt
 * @property string $user_name
 * @property string $full_name
 * @property string $email
 * @property integer $user_group_id
 * @property string $image
 * @property string $thumbnail
 * @property integer $is_deleted
 * @property string $date_last_login
 * @property string $updated_at
 * @property string $created_at
 * @property integer $id_department
 */
class UserAccount extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_account}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserAccount the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
