<?php

class Regulation extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{regulation}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function beforeSave() {
        deleteCache(REGULATION_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
}
