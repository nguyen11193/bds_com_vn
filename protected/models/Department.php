<?php

class Department extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{department}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function beforeSave() {
        deleteCache(DEPARTMENT_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
}
