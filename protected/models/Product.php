<?php

/**
 * This is the model class for table "{{product}}".
 *
 * The followings are the available columns in table '{{product}}':
 * @property integer $id
 * @property integer $id_project
 * @property string $name
 * @property double $acreage
 * @property double $price
 * @property integer $status
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{product}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function beforeSave() {
        deleteCache(PRODUCT_CACHE_KEY_PREFIX.$this->id_project);
        return parent::beforeSave();
    }
}
