<?php

class RegulationLog extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{regulation_log}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
