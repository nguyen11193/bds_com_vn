<?php

/**
 * This is the model class for table "{{customer_phone}}".
 *
 * The followings are the available columns in table '{{customer_phone}}':
 * @property integer $id
 * @property integer $id_customer
 * @property string $phone
 * @property string $updated_at
 * @property string $created_at
 */
class CustomerPhone extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{customer_phone}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CustomerPhone the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
