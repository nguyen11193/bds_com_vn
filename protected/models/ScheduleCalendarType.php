<?php

class ScheduleCalendarType extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{schedule_calendar_type}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave() {
        deleteCache(SCHEDULE_CALENDAR_TYPE_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
}
