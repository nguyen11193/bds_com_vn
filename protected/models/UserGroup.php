<?php

/**
 * This is the model class for table "{{user_group}}".
 *
 * The followings are the available columns in table '{{user_group}}':
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $is_deleted
 * @property string $updated_at
 * @property string $created_at
 * @property integer $id_department
 */
class UserGroup extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user_group}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserGroup the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave() {
        deleteCache(USER_GROUP_CACHE_KEY_PREFIX);
        deleteCache(USER_GROUP_PERMISSION_CACHE_KEY_PREFIX.$this->id);
        return parent::beforeSave();
    }
}
