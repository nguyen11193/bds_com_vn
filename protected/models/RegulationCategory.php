<?php

class RegulationCategory extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{regulation_category}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function beforeSave() {
        deleteCache(REGULATION_CATEGORY_CACHE_KEY_PREFIX);
        return parent::beforeSave();
    }
}
