<?php

/**
 * This is the model class for table "{{asc_system}}".
 *
 * The followings are the available columns in table '{{asc_system}}':
 * @property integer $id
 * @property string $description
 * @property string $value
 * @property integer $valcount
 * @property string $created_at
 * @property string $updated_at
 * @property integer $current_month
 */
class AscSystem extends MasterModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{asc_system}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AscSystem the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
