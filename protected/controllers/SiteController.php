<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    
    public function actionNoPermission() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(array('site/login'));
        } else {
            throw new CHttpException(403, 'BẠN KHÔNG CÓ QUYỀN TRUY CẬP TRANG NÀY.');
            //$this->render('error_permission');
        }
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (Yii::app()->user->isGuest){
            $this->redirect(array('site/login'));
        } else {
            $this->redirect(array('Dashboard/main'));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            $this->render('error', array(
                'error' => $error
            ));
        }
    }

    public function actionLoginSystem(){
        $this->layout = 'login';
        $request = Yii::app()->request;

        if ($request->isPostRequest) {
            $userName = trim($request->getPost('user_name_system'));

            if (!empty($userName) && $userName === SYSTEM_ACCOUNT) {
                set_session(SYSTEM_ACCOUNT_SESSION_KEY, $userName);
                $this->redirect(array('MenuParent/Main'));
            }
        }

        $this->render('login_system');
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(array('Dashboard/main'));
        }
        date_default_timezone_set(Yii::app()->params['timezone']);
        $this->layout = "login"; // Set default layout to empty layout
        $model = new LoginForm;
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                //Update the last login for the user
                date_default_timezone_set(Yii::app()->params['timezone']);
                $user = UserAccountExtend::findOneUserAccountByCondition(array('user_name' => $model->username), array('limit' => 1));
                $user->date_last_login = date('Y-m-d H:i:s');
                $user->save();
                $this->redirect(array('Dashboard/main'));
            }
        }
        // display the login form
        $this->render('login');
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
    
    public function actionLoadMessage(){
        $request = Yii::app()->request;
        $limit = $request->getParam('limit');
        $offset = $request->getParam('offset');
        $messages = MessageExtend::findAllMessageByUserID(Yii::app()->user->id, $limit, $offset);
        $data['data'] = $messages;
        echo CJSON::encode($data);
        return;
    }

}
