<?php

class OrderController extends Controller {

    private $_actionPath = "Order";
    private $_actionName = "Order";
    public $title = 'Đơn hàng';
    
    private function saveOrderLogMulti($statusId, $statusAfter, $orders, $userId, $comment, $action = 'main'){
        $orderLogModels = array();
        foreach ($orders as $order) {
            $model = new OrderLogExtend();
            $model->id_order = $order['id'];
            $model->id_user = $userId;
            $model->comment = $comment;
            $model->status_before = $statusId;
            $model->status_after = $statusAfter;
            $orderLogModels[] = $model;
        }
        if (!empty($orderLogModels)) {
            if (!insertMultipleModel($orderLogModels)) {
                Common::rollbackTransaction();
                $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'err' => SYSTEM_ERROR));
            }
        }
    }

    private function approve($statusId, $action, $orderIds, $comment, $orders = array()){
        $user = Yii::app()->user;
        $msg = $err = '';
        $statusAfter = 0;
        $checkPermissionApproveDenyOrder = GroupPermissionApproveMapExtend::checkPermissionForApproveDeny($user->groupid, $statusId);
        if (!empty($checkPermissionApproveDenyOrder)) {
            $statusAfter = OrderStatusExtend::findNextStatus($statusId);

            if ($statusId != HOAN_TAT) {
                $orderIdsArr = explode(',', $orderIds);
                if (empty($orders)) {
                    $orders = Yii::app()->db->createCommand()
                            ->select('o.id, o.code, o.id_sales, o.final_price, o.id_customer, o.id_oa, o.address_shipto, o.receive_date, c.revenue, c.name AS customer_name, c.phone, c.id_type')
                            ->from('tbl_order o')
                            ->join('tbl_customer c', 'o.id_customer = c.id')
                            ->where(['IN', 'o.id', $orderIdsArr])
                            ->queryAll();
                }
                
                $orders = Common::assoc_item($orders, 'id');

                if (!empty($orders)) {
                    $completedAt = null;
                    Common::beginTransaction();
                    if ($statusAfter == HOAN_TAT) {
                        $completedAt = date('Y-m-d H:i:s');
                    }
                    // Update status
                    OrderExtend::model()->updateAll(array('id_status' => $statusAfter, 'updated_at' => date('Y-m-d H:i:s'), 'completed_at' => $completedAt), 'id IN (' . $orderIds . ')');
                    // update batch
                    if ($statusId == CHO_XUAT_KHO) {
                        $warehouseLog = array();
                        $orderDetail = Yii::app()->db->createCommand()
                            ->select('id_product, quantity, id_warehouse, id_batch, id_order')
                            ->from('tbl_order_detail')
                            ->where('is_deleted = 0')
                            ->andWhere(['IN', 'id_order', $orderIdsArr])
                            ->queryAll();
                        $orderDetail = Common::assoc_array($orderDetail, 'id_order');
                        
                        $booking = OrderDetailBookingExtend::findQtyBooked(array(), array(), $orderIdsArr);
                        foreach ($booking as $keyB => $qty) {
                            $key = explode('_', $keyB);
                            $warehouseId = $key[0];
                            $batchId = $key[1];
                            Yii::app()->db->createCommand("UPDATE {{w_batch_warehouse_map}} SET quantity = quantity - " . $qty . ", updated_at = '" . date('Y-m-d H:i:s') . "' WHERE id_warehouse = " . $warehouseId . " AND id_batch = " . $batchId)->execute();
                        }
                        OrderDetailBookingExtend::deleteBooking($orderIds);
                        
                        foreach ($orderDetail as $keyDetail => $detail) {
                            $code = AscSystemExtend::findOneAscSystemByCondition(array('value' => EXPORT_CODE));
                            $export = new WExportExtend();
                            $export->code = generateLicenseCode($code);
                            if (!AscSystemExtend::autoIncrementValcount($code)) {
                                Common::rollbackTransaction();
                                $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'msg' => SYSTEM_ERROR));
                            }
                            $export->id_user = $user->id;
                            $export->reason = 'Xuất kho cho đơn hàng ' . $orders[$keyDetail]['code'];
                            $export->type = EXPORT_TYPE_ORDER;
                            $export->id_order = $keyDetail;
                            $export->id_reason = EXPORT_REASON_FOR_ORDER;
                            $export->status = 1;
                            if (!$export->save()) {
                                Common::rollbackTransaction();
                                $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'msg' => SYSTEM_ERROR));
                            }
                            $wExportDetailModels = array();
                            foreach ($detail as $d) {
                                $wId = $d['id_warehouse'];
                                $bId = $d['id_batch'];
                                $pId = $d['id_product'];
                                $quantity = intval($d['quantity']);
                                
                                $exportDetail = new WExportDetailExtend();
                                $exportDetail->id_export = $export->id;
                                $exportDetail->id_warehouse = $wId;
                                $exportDetail->id_batch = $bId;
                                $exportDetail->id_product = $pId;
                                $exportDetail->quantity = $quantity;
                                $wExportDetailModels[] = $exportDetail;

                                $warehouseLog[] = Common::prepareFieldWarehouseLog($wId, $bId, $pId, -$quantity, WAREHOUSE_LOG_TYPE_EXPORT);
                            }
                            if (!empty($wExportDetailModels)) {
                                if (!insertMultipleModel($wExportDetailModels)) {
                                    Common::rollbackTransaction();
                                    $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'msg' => SYSTEM_ERROR));
                                }
                            }
                        }
                        $insertWarehouseLog = WWarehouseLogExtend::insertWarehouseLog($warehouseLog);
                        if ($insertWarehouseLog == 0) {
                            Common::rollbackTransaction();
                            $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'msg' => SYSTEM_ERROR));
                        }
                    }
                    // insert Message
                    $this->saveMessageOfOrder($statusId, $statusAfter, $orders, $action);
                    // update revenue's customer
                    if ($statusAfter == HOAN_TAT) {
                        $levels = CustomerLevelExtend::getAllLevel();
                        $levels = Common::assoc_array($levels, 'id_customer_type');
                        $customer = $updateLevelSql = array();
                        foreach ($orders as $ord) {
                            $finalPrice = intval($ord['final_price']);
                            if (empty($customer[$ord['id_customer']])) {
                                $newRevenue = (intval($ord['revenue']) + $finalPrice);
                            } else {
                                $newRevenue = ($customer[$ord['id_customer']] + $finalPrice);
                            }
                            $customer[$ord['id_customer']] = $newRevenue;
                            $customerLevel = Common::findLevelForCustomer($levels, $newRevenue, $ord['id_type']);
                            $updateLevelSql[] = 'UPDATE {{customer}} SET revenue = '.$newRevenue.' , level = '.$customerLevel.', updated_at = "'.date('Y-m-d H:i:s').'" WHERE id = ' . $ord['id_customer'] . '';
                            //CustomerExtend::model()->updateAll(array('revenue' => $newRevenue, 'updated_at' => date('Y-m-d H:i:s'), 'level' => $customerLevel), 'id = ' . $ord['id_customer'] . ' ');
                        }
                        if (!empty($updateLevelSql)) {
                            Yii::app()->db->createCommand(implode(';', $updateLevelSql))->execute();
                        }
                    }
                    // insert Log
                    $this->saveOrderLogMulti($statusId, $statusAfter, $orders, $user->id, $comment, $action);
                    
                    Common::commitTransaction();
                    if ($statusId == CHO_DUYET && use_esms_api()) {
                        $znsTemplates = ZnsTemplateExtend::findAllZnsTemplate();
                        $znsTemplates = Common::assoc_array($znsTemplates, 'id_zoa');
                        if (!empty($znsTemplates)) {
                            $sendHistoryModels = $hasErr = array();
                            $orderDetail = Yii::app()->db->createCommand()
                                ->select('d.quantity, p.short_name AS product_name, d.id_order')
                                ->from('tbl_order_detail d')
                                ->join('tbl_product p', 'd.id_product = p.id')
                                ->where('d.is_deleted = 0')
                                ->andWhere(['IN', 'd.id_type', array(SAN_PHAM_BAN, KHUYEN_MAI)])
                                ->andWhere('d.final_price > 0')
                                ->andWhere(['IN', 'd.id_order', $orderIdsArr])
                                ->queryAll();
                            $orderDetail = Common::assoc_array($orderDetail, 'id_order');
                            
                            foreach ($orders as $order) {
                                if (empty($order['id_oa']) 
                                    || empty($orderDetail[$order['id']]) 
                                    || $order['id_type'] == KHACH_SI
                                    || empty($znsTemplates[$order['id_oa']])
                                    || empty($znsTemplates[$order['id_oa']][0])) {
                                    continue;
                                }
                                
                                $prepareProduct = array();
                                $countProduct = $keyArr = 0;
                                foreach ($orderDetail[$order['id']] as $orderDel) {
                                    $countProduct++;
                                    if ($countProduct > 2) {
                                        $countProduct = 1;
                                        $keyArr++;
                                    }
                                    if (empty($prepareProduct[$keyArr])) {
                                        $prepareProduct[$keyArr] = array(
                                            'quantity' => 0,
                                            'product_name' => array(),
                                        );
                                    }
                                    $prepareProduct[$keyArr]['quantity'] += $orderDel['quantity'];
                                    $prepareProduct[$keyArr]['product_name'][] = $orderDel['product_name'];
                                }
                                
                                if (!empty($prepareProduct)) {
                                    $paramsAPI = Common::getCommonParamsToSendZalo($order['phone'], $znsTemplates[$order['id_oa']][0]['id_template'], $order['id_oa']);
                                    foreach ($prepareProduct as $product) {
                                        $paramsAPI['Params'] = array(
                                            $order['customer_name'],
                                            $order['code'],
                                            $order['phone'],
                                            $order['address_shipto'],
                                            implode(', ', $product['product_name']),
                                            $product['quantity'],
                                            date('d/m/Y', strtotime($order['receive_date'])),
                                        );
                                        Common::sendZaloMsg($paramsAPI, $sendHistoryModels, $hasErr, SEND_TYPE_ORDER_HISTORY);
                                    }
                                }
                            }
                            
                            if (!empty($sendHistoryModels)) {
                                if (!insertMultipleModel($sendHistoryModels)) {
                                    Yii::log('Chưa lưu được ' . $sendHistoryModels, LOG_LEVEL_DEBUG_ZALO_SMS, LOG_CATEGORY_ZALO_SMS);
                                }
                            } else {
                                Yii::log('Các DH ko có sendHistoryModels ' . $orders, LOG_LEVEL_DEBUG_ZALO_SMS, LOG_CATEGORY_ZALO_SMS);
                            }
                            
                            if (!empty($hasErr)) {
                                $err = implode(' - ', $hasErr);
                            }
                        }
                    }
                    $msg = 'Đã duyệt đơn hàng thành công.';
                } else {
                    $msg = 'Không có đơn hàng nào';
                }
            }
        } else {
            Yii::app()->request->redirect(Yii::app()->request->baseUrl . '/site/noPermission');
            Yii::app()->end();
        }
        return array(
            'msg' => $msg,
            'statusAfter' => $statusAfter,
            'err' => $err,
        );
    }
    
    private function deny($statusId, $action, $orderIds, $comment, $orders = array()){
        $user = Yii::app()->user;
        $msg = '';
        $checkPermissionApproveDenyOrder = GroupPermissionApproveMapExtend::checkPermissionForApproveDeny($user->groupid, HUY_DON_HANG);
        if (!empty($checkPermissionApproveDenyOrder)) {
            if ($statusId != HUY_DON_HANG) {
                if (empty($orders)) {
                    $conditionOthers = array();
                    $conditionAddCondition = array();
                    $conditionOthers['select'] = 'id, code, id_sales';
                    $conditionAddCondition[] = 'id IN (' . $orderIds . ')';
                    $orders = OrderExtend::findAllOrdersByCondition($conditionToCompare = array(), $conditionOthers, $conditionAddCondition);
                }

                if (!empty($orders)) {
                    Common::beginTransaction();
                    // Update status
                    OrderExtend::model()->updateAll(array('id_status' => HUY_DON_HANG, 'updated_at' => date('Y-m-d H:i:s')), 'id IN (' . $orderIds . ')');
                    if ($statusId == CHO_DUYET || $statusId == CHO_XUAT_KHO) {
                        OrderDetailBookingExtend::deleteBooking($orderIds);
                    } else {
                        // phải trả lại số lượng đã bị trừ
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/' . $action, 'status' => $statusId, 'msg' => 'Hiện tại hệ thống chưa hỗ trợ hủy đơn hàng sau khi đã xuất kho'));
                    }
                    // insert Message
                    $this->saveMessageOfOrder($statusId, HUY_DON_HANG, $orders, $action);
                    // insert Log
                    $this->saveOrderLogMulti($statusId, HUY_DON_HANG, $orders, $user->id, $comment, $action);
                    Common::commitTransaction();
                    $msg = 'Đã hủy đơn hàng thành công';
                } else {
                    $msg = 'Không có đơn hàng nào';
                }
            }
        } else {
            Yii::app()->request->redirect(Yii::app()->request->baseUrl . '/site/noPermission');
            Yii::app()->end();
        }
        return $msg;
    }

    public function actionMain($msg = '', $err = ''){
        $permList = UtilSecurity::accessCheck('Order_List_All');
        $icon = $permList['Order_List_All']['detail']['menu_parent_icon'];
        $user = Yii::app()->user;
        
        $permissionAction = UtilSecurity::accessValidate(array(
            'Order_Edit',
            'Order_Delete',
            'Contract_Add',
            'Contract_Edit',
            'Contract_Approve_1',
            'Contract_Approve_2',
            'Contract_Deny',
        ));
        $editOrderAct = $deleteOrderAct = $addContractAct = $editContractAct = $approveContractAct1 = $approveContractAct2 = $denyContractAct = false;
        if (!empty($permissionAction['permList'])) {
            foreach ($permissionAction['permList'] as $k => $act) {
                if ($k == 'Order_Edit') {
                    $editOrderAct = true;
                } else if ($k == 'Order_Delete') {
                    $deleteOrderAct = true;
                } else if ($k == 'Contract_Add') {
                    $addContractAct = true;
                } else if ($k == 'Contract_Edit') {
                    $editContractAct = true;
                } else if ($k == 'Contract_Approve_1') {
                    $approveContractAct1 = true;
                } else if ($k == 'Contract_Approve_2') {
                    $approveContractAct2 = true;
                } else if ($k == 'Contract_Deny') {
                    $denyContractAct = true;
                }
            }
        }
        
        $idStatus = STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT;
        $listOrderStatus = getListStatusOfOrder();
        $title = 'Danh sách đơn hàng';
        
        $request = Yii::app()->request;
        
        if (!empty($request->getParam('status'))) {
            $idStatus = $request->getParam('status');
        }
        $itemPerPage = LIMIT_ITEMS;
        $page = 1;
        if (!empty($request->getParam('page'))) {
            $page = $request->getParam('page');
        }
        $offset = $itemPerPage * ($page - 1);
        $filterDate = Common::filterDate($request->getParam('from'), $request->getParam('to'));
        $dateFrom = correctDate($filterDate['dateFrom']);
        $dateTo = correctDate($filterDate['dateTo']);
        $filter = '';
        if (!empty($request->getParam('filter'))) {
            $filter = trim($request->getParam('filter'));
        }
        
        $sql = Yii::app()->db->createCommand()
                ->from('tbl_order o')
                ->join('tbl_customer c', 'o.id_customer = c.id')
                ->join('tbl_project p', 'o.id_project = p.id')
                ->join('tbl_product pr', 'o.id_product = pr.id')
                ->where('o.is_deleted = 0')
                ->andWhere('DATE(o.created_at) >= :dateFrom', [':dateFrom' => $dateFrom])
                ->andWhere('DATE(o.created_at) <= :dateTo', [':dateTo' => $dateTo])
                ->andWhere('o.id_status = :status', [':status' => $idStatus]);
        if (!empty($filter)) {
            if (is_numeric($filter)) {
                $custIdArr = Common::findCustomerByPhone($filter);
                $sql->andWhere(['IN', 'c.id', $custIdArr]);
            } else {
                $sql = $sql->andWhere('o.code LIKE "%'.$filter.'%" OR o.contract_code LIKE "%'.$filter.'%" OR c.code LIKE "%'.$filter.'%" OR c.name LIKE "%'.$filter.'%"');
            }
        }
        
        $totalSql = clone $sql;
        $totalPage = $totalSql->select('count(*)')->queryScalar();
        $totalPage = ceil($totalPage / $itemPerPage);
        
        $items = $sql->select('o.id, o.code AS order_code, o.contract_code, o.final_price, o.use_tax, o.tax, o.id_status, o.id_customer, c.code AS customer_code, c.name AS customer_name, p.name AS project_name, pr.name AS product_name')
                ->order('o.created_at DESC')
                ->limit($itemPerPage)
                ->offset($offset)
                ->queryAll();
        
        $phones = Common::getPhonesOfCustomer($items);
        
        $breadcrumbs = [
            [
                'title' => $title,
            ],
        ];
        $searchBox = array(
            'action' => "",
            'placeholder' => SEARCH_TEXT,
            'value' => $filter,
            'params' => [
                'status' => $idStatus,
            ],
        );

        $this->render('main', array(
            'items' => $items,
            'dateFrom' => $filterDate['dateFrom'],
            'dateTo' => $filterDate['dateTo'],
            'msg' => $msg,
            'totalPage' => $totalPage,
            'page' => $page,
            'offset' => $offset,
            'listOrderStatus' => $listOrderStatus,
            'idStatus' => $idStatus,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
            'searchBox' => $searchBox,
            'title' => $title,
            'icon' => $icon,
            'url' => '/'.$this->controllerName.'/'.$this->action->Id,
            'phones' => $phones,
            'editOrderAct' => $editOrderAct,
            'deleteOrderAct' => $deleteOrderAct,
            'addContractAct' => $addContractAct,
            'editContractAct' => $editContractAct,
            'approveContractAct1' => $approveContractAct1,
            'approveContractAct2' => $approveContractAct2,
            'denyContractAct' => $denyContractAct,
        ));
    }
    
    public function actionSearch(){
        $permList = UtilSecurity::accessCheck('Order_Add');
        $icon = $permList['Order_Add']['detail']['menu_parent_icon'];
        
        $title = $this->addTitle;
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];
        $this->render('search', array(
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'icon' => $icon,
        ));
    }

    public function actionSearchCustomerByPhone(){
        $request = Yii::app()->request;
        $type = trim($request->getParam('type'));
        $phone = trim($request->getParam('phone'));

        $customer = CustomerPhoneExtend::findCustomerByPhoneSearch($type, $phone, true);
        $data['customer'] = $customer;

        echo CJSON::encode($data);
        return;
    }
    
    public function actionApprove(){
        UtilSecurity::accessCheck(array('Contract_Approve_1', 'Contract_Approve_2'));
        $request = Yii::app()->request;
        $user = Yii::app()->user;
        $id = trim($request->getParam('id'));
        $comment = trim($request->getParam('approve_comment'));
        $model = $this->loadModel($id);
        $statusId = $model->id_status;
        Common::beginTransaction();
        if ($statusId == STATUS_OF_ORDER_PENDING_TO_APPROVE_1) {
            $model->id_status = STATUS_OF_ORDER_PENDING_TO_APPROVE_2;
        } else if ($statusId == STATUS_OF_ORDER_PENDING_TO_APPROVE_2) {
            $model->id_status = STATUS_OF_ORDER_NEXT;
        }
        if (!$model->save()) {
            Common::rollbackTransaction();
            $this->redirect(array($this->controllerName . '/main', 'status' => $statusId, 'err' => SYSTEM_ERROR));
        }
        if (!OrderLogExtend::saveNewOrderLog($model->id, $user->id, $comment, $statusId, $model->id_status)) {
            Common::rollbackTransaction();
            $this->redirect(array($this->controllerName . '/main', 'status' => $statusId, 'err' => SYSTEM_ERROR));
        }
        Common::commitTransaction();

        $this->redirect(array($this->controllerName . '/main', 'status' => STATUS_OF_ORDER_PENDING_TO_APPROVE_2, 'msg' => 'Hợp đồng '.$model->contract_code.' đã được duyệt thành công.'));
    }
    
    public function actionDeny(){
        UtilSecurity::accessCheck('Contract_Deny');
        $request = Yii::app()->request;
        $user = Yii::app()->user;
        $id = trim($request->getParam('id'));
        $comment = trim($request->getParam('deny_comment'));
        $model = $this->loadModel($id);
        $statusId = $model->id_status;
        Common::beginTransaction();
        $model->id_status = STATUS_OF_ORDER_CANCEL;
        if (!$model->save()) {
            Common::rollbackTransaction();
            $this->redirect(array($this->controllerName . '/main', 'status' => $statusId, 'err' => SYSTEM_ERROR));
        }
        if (!OrderLogExtend::saveNewOrderLog($model->id, $user->id, $comment, $statusId, $model->id_status)) {
            Common::rollbackTransaction();
            $this->redirect(array($this->controllerName . '/main', 'status' => $statusId, 'err' => SYSTEM_ERROR));
        }
        Common::commitTransaction();

        $this->redirect(array($this->controllerName . '/main', 'status' => $model->id_status, 'msg' => 'Hợp đồng '.$model->contract_code.' đã được từ chối thành công.'));
    }
    
    public function actionGetProductByProjectId(){
        $request = Yii::app()->request;
        $projectId = trim($request->getParam('projectId'));
        $productId = trim($request->getParam('productId'));
        $productArr = array();
        $products = ProductExtend::getAllProductsFromCacheByProject($projectId);
        if (!empty($products)) {
            foreach ($products as $product) {
                if ($product['status'] == STATUS_OF_PRODUCT_OPEN) {
                    $productArr[] = $product;
                } else if (!empty($productId) && $productId == $product['id']) {
                    $productArr[] = $product;
                }
            }
        }
        $data['products'] = $productArr;
        echo CJSON::encode($data);
        return;
    }

    private function prepareOrderInfo($order, $orderCode){
        $info = array(
            'id' => 0,
            'code' => createCode($orderCode),
            'comments' => '',
            'discount' => '',
            'final_price' => '',
            'tax' => '',
            'use_tax' => 0,
            'id_project' => 0,
            'id_product' => 0,
            'status' => STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT,
            'contract_comment' => '',
            'contract_code' => '',
            'contract_attach_images' => array(),
            'contract_attach_files' => array(),
        );
        if (!empty($order)) {
            $status = $order->id_status;
            $orderId = $order->id;
            $info['id'] = $orderId;
            $info['code'] = $order->code;
            $info['comments'] = $order->comments;
            $info['discount'] = ($order->discount > 0) ? number_format($order->discount) : '';
            $info['final_price'] = ($order->final_price > 0) ? number_format($order->final_price) : '';
            $info['tax'] = ($order->tax > 0) ? number_format($order->tax) : '';
            $info['use_tax'] = $order->use_tax;
            if ($order->use_tax == 1 && $order->tax > 0 && $order->final_price > 0) {
                $info['final_price'] = number_format(($order->final_price + $order->tax));
            }
            $info['id_project'] = $order->id_project;
            $info['id_product'] = $order->id_product;
            $info['status'] = $status;
            $info['contract_comment'] = $order->contract_comment;
            $info['contract_code'] = $order->contract_code;
            if ($status != STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT) {
                Common::getAllContractAttachs($info['contract_attach_files'], $info['contract_attach_images'], $orderId, '/');
            }
        }
        return $info;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $permList = UtilSecurity::accessCheck(array(
            'Order_Add',
            'Order_Edit',
            'Customer_Add',
            'Contract_Add',
            'Contract_Edit',
            'Contract_Approve_1',
            'Contract_Approve_2',
            'Contract_Deny',
        ));
        $title = $this->addTitle;
        
        $request = Yii::app()->request;
        $currentUser = Yii::app()->user;
        $userID = $currentUser->id;
        $ascSystem = AscSystemExtend::findAllAscSystemModel();
        $ascSystem = Common::assoc_item($ascSystem, 'value');
        $customerCode = $ascSystem[CUSTOMER_CODE];
        $orderCode = $ascSystem[ORDER_CODE];
        $contractCode = $ascSystem[CONTRACT_CODE];
        $justSaveCustomerInfo = false;
        
        $orderId = trim($request->getParam('orderId'));
        $createContract = trim($request->getParam('createContract'));
        $editContract = trim($request->getParam('editContract'));
        $isContract = trim($request->getParam('isContract'));
        
        if (empty($orderId)) {
            $order = null;
            $custId = trim($request->getParam('custId'));
        } else {
            $title = $this->editTitle;
            $order = $this->loadModel($orderId);
            $custId = $order->id_customer;
        }
        
        if (!empty($createContract)) {
            $title = 'Tạo hợp đồng';
        } else if (!empty($editContract)) {
            $title = 'Chỉnh sửa hợp đồng';
        }
        
        //$orderShow = $this->prepareOrderInfo($order, $orderCode);
        
        if (!empty($custId)) {
            $customer = CustomerExtend::model()->findByPk($custId);
            if (empty($customer)) {
                $this->redirect(array($this->controllerName . '/main', 'err' => 'Khách hàng này không tồn tại trong hệ thống'));
            }
        } else {
            $customer = null;
        }
        
        //$customerShow = Common::prepareCustomerInfo($customer);
        
        $isSubmit = (!empty($request->getParam('create'))) ? true : false;
        $customerFields = $customerPhoneModelExisted = $customerPhones = array();
        if ($isSubmit) {
            if (!empty($request->getParam('just_save_customer_info'))) {
                $justSaveCustomerInfo = true;
            }
            
            Common::beginTransaction();
            if (empty($customer)) {
                if (empty($permList['Customer_Add'])) {
                    redirectNoPermission();
                }
                if (isset($_POST['Customer'])) {
                    Common::prepareCustomerField($_POST['Customer'], $customerFields);
                }

                if (isset($_POST['CustomerPhone'])) {
                    $customerPhones = $_POST['CustomerPhone'];
                }

                $customerFields['code'] = createCode($customerCode);
                $customer = CustomerExtend::insertCustomer($customerCode, $customerFields);
                if ($customer === false) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                }
                $customerPhoneModels = CustomerPhoneExtend::prepareCustomerPhone($customerPhoneModelExisted, $customerPhones, $customer);
                if (!empty($customerPhoneModels) && !insertMultipleModel($customerPhoneModels)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                }
                if ($justSaveCustomerInfo) {
                    Common::commitTransaction();
                    $this->redirect(array('Customer/update', 'id' => $customer->id));
                }
            }
            
            if (empty($order) || (!empty($order) && $order->id_status == STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT && empty($isContract))) {
                $hasPermissionOfOrder = true;
                if (empty($order)) {
                    if (empty($permList['Order_Add'])) {
                        $hasPermissionOfOrder = false;
                    }
                } else {
                    if (empty($permList['Order_Edit'])) {
                        $hasPermissionOfOrder = false;
                    }
                }
                if (!$hasPermissionOfOrder) {
                    Common::rollbackTransaction();
                    redirectNoPermission();
                }
                $orderFields = array();
                if (isset($_POST['Order'])) {
                    foreach ($_POST['Order'] as $oK => $oV) {
                        if ($oK == 'ship_date' || $oK == 'receive_date') {
                            $orderFields[$oK] = correctDate(trim($oV));
                        } else if ($oK == 'final_price' || $oK == 'discount' || $oK == 'unit_price' || $oK == 'tax') {
                            $value = convertNumberWithCommaToNumber($oV);
                            $orderFields[$oK] = is_numeric($value) ? $value : 0;
                        } else {
                            $orderFields[$oK] = trim($oV);
                        }
                    }
                }
                
                if (empty($orderFields['id_project']) || empty($orderFields['id_product'])) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Đơn hàng thiếu sản phẩm hoặc dự án'));
                }
                $productId = $orderFields['id_product'];
                $product = ProductExtend::findOneProductByCondition(array('id' => $productId));
                if (empty($product)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Sản phẩm này không tồn tại trong hệ thống'));
                }
                
                if ($product->status != STATUS_OF_PRODUCT_OPEN && (empty($order) || (!empty($order) && $productId != $order->id_product))) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Sản phẩm '.$product->name.' đã được mua'));
                }
                
                if (empty($order) || (!empty($order) && $productId != $order->id_product)) {
                    if (!empty($order) && $productId != $order->id_product) {
                        ProductExtend::model()->updateAll(array('status' => STATUS_OF_PRODUCT_OPEN), 'id = '.$order->id_product.' ');
                    }
                    $product->status = STATUS_OF_PRODUCT_BOOKING;
                    if (!$product->save()) {
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                    }
                }
                
                if (empty($orderFields['use_tax'])) {
                    $orderFields['use_tax'] = 0;
                }
                if ($orderFields['use_tax'] == 1 && $orderFields['tax'] > 0 && $orderFields['final_price'] > 0) {
                    $orderFields['final_price'] = floatval($orderFields['final_price'] - $orderFields['tax']);
                }
                
                $isNewOrder = false;
                if (empty($order)) {
                    $isNewOrder = true;
                    $msg = 'Đã tạo đơn hàng ';
                    $orderFields['code'] = createCode($orderCode);
                    $orderFields['id_status'] = STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT;
                    $orderFields['id_sales'] = $userID;
                    $orderFields['id_customer'] = $customer->id;
                    $order = OrderExtend::insertOrder($orderCode, $orderFields);
                } else {
                    if ($productId != $order->id_product) {
                        
                    }
                    $msg = 'Đã cập nhật đơn hàng ';
                    $order = OrderExtend::updateOrder($order, $orderFields);
                }
                if ($order === false) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                }
                if ($isNewOrder) {
                    if (!OrderLogExtend::saveNewOrderLog($order->id, $userID, $order->comments, 0, $order->id_status)) {
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                    }
                }
                $msg = $msg . $order->code . ' thành công';
            } else if (!empty($isContract)) {
                $contractFile = $_FILES["contract_file"];
                $imagesFile = $_FILES["contract_images"];
                $totalFile = count($contractFile['name']);
                $totalImages = count($imagesFile['name']);
                $contractAttachModelsArr = $pathsArr = array();
                $maxFiles = $maxImages = 0;
                $isNewContract = false;
                if (empty($order->contract_code)) {
                    $isNewContract = true;
                    $oldStatus = $order->id_status;
                    $msg = 'Đã tạo hợp đồng ';
                    if (empty($permList['Contract_Add'])) {
                        Common::rollbackTransaction();
                        redirectNoPermission();
                    }
                    if ($totalFile <= 0) {
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/main', 'status' => $oldStatus, 'err' => 'Không có file hợp đồng'));
                    }
                    $order->contract_code = createCode($contractCode);
                    if (!AscSystemExtend::autoIncrementValcount($contractCode)) {
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/main', 'status' => $oldStatus, 'err' => SYSTEM_ERROR));
                    }
                } else {
                    $msg = 'Đã cập nhật hợp đồng ';
                    if (empty($permList['Contract_Edit'])) {
                        Common::rollbackTransaction();
                        redirectNoPermission();
                    }
                }
                $msg = $msg . $order->contract_code . ' thành công';
                $order->contract_comment = trim($request->getParam('contract_comment'));
                if ($isContract == CONTRACT_FINISHED) {
                    $oldStatus = $order->id_status;
                    $newStatus = STATUS_OF_ORDER_PENDING_TO_APPROVE_1;
                } else {
                    $newStatus = STATUS_OF_ORDER_CONTRACTED;
                }
                $order->id_status = $newStatus;
                if (!$order->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'status' => $oldStatus, 'err' => SYSTEM_ERROR));
                }
                if ($isNewContract || $order->id_status == STATUS_OF_ORDER_PENDING_TO_APPROVE_1) {
                    if (!OrderLogExtend::saveNewOrderLog($order->id, $userID, $order->contract_comment, $oldStatus, $order->id_status)) {
                        Common::rollbackTransaction();
                        $this->redirect(array($this->controllerName . '/main', 'status' => $oldStatus, 'err' => SYSTEM_ERROR));
                    }
                }
                if ($totalFile > 0 || $totalImages > 0) {
                    $maxContractAttach = Yii::app()->db->createCommand()
                            ->select('MAX(id) AS maxId, type')
                            ->from('tbl_contract_attach')
                            ->where('id_order = :orderId', [':orderId' => $order->id])
                            ->group('type')
                            ->queryAll();
                    if (!empty($maxContractAttach)) {
                        foreach ($maxContractAttach as $v) {
                            if ($v['type'] == CONTRACT_ATTACH_IMAGE) {
                                $maxImages = $v['maxId'];
                            } else if ($v['type'] == CONTRACT_ATTACH_FILE) {
                                $maxFiles = $v['maxId'];
                            }
                        }
                    }
                }
                if ($totalFile > 0) {
                    $this->prepareContractAttach($contractAttachModelsArr, $pathsArr, $totalFile, CONTRACT_ATTACH_FILE, $order, $contractFile, $maxFiles);
                }
                if ($totalImages > 0) {
                    $this->prepareContractAttach($contractAttachModelsArr, $pathsArr, $totalImages, CONTRACT_ATTACH_IMAGE, $order, $imagesFile, $maxImages);
                }
                if (!empty($contractAttachModelsArr) && !insertMultipleModel($contractAttachModelsArr)) {
                    Common::rollbackTransaction();
                    if (!empty($pathsArr)) {
                        $this->unLinkContractAttach($pathsArr);
                    }
                    $this->redirect(array($this->controllerName . '/main', 'status' => $order->id_status, 'err' => SYSTEM_ERROR));
                }
            }
            Common::commitTransaction();
            $this->redirect(array($this->controllerName . '/main', 'status' => $order->id_status, 'msg' => $msg));
        } else {
            $icon = '';
            $approveContractAct1 = $approveContractAct2 = $denyContractAct = false;
            foreach ($permList as $k => $perm) {
                if (empty($icon)) {
                    $icon = $perm['detail']['menu_parent_icon'];
                }
                if ($k == 'Contract_Approve_1') {
                    $approveContractAct1 = true;
                } else if ($k == 'Contract_Approve_2') {
                    $approveContractAct2 = true;
                } else if ($k == 'Contract_Deny') {
                    $denyContractAct = true;
                }
            }
            $orderShow = $this->prepareOrderInfo($order, $orderCode);
            $customerShow = Common::prepareCustomerInfo($customer);
            $projects = ProjectExtend::getAllProjectsFromCache();
            $breadcrumbs = [
                [
                    'title' => $this->title,
                    'link' => '/'.$this->controllerName.'/main',
                ],
                [
                    'title' => $title,
                ],
            ];
        }

        $this->render('create', array(
            'projects' => $projects,
            'breadcrumbs' => $breadcrumbs,
            'customerShow' => $customerShow['customer_info'],
            'orderShow' => $orderShow,
            'createContract' => $createContract,
            'editContract' => $editContract,
            'disabledOrder' => ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT) ? '' : 'disabled',
            'disabledContract' => ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT || $orderShow['status'] == STATUS_OF_ORDER_CONTRACTED) ? '' : 'disabled',
            'icon' => $icon,
            'title' => $title,
            'approveContractAct1' => $approveContractAct1,
            'approveContractAct2' => $approveContractAct2,
            'denyContractAct' => $denyContractAct,
        ));
    }
    
    protected function prepareContractAttach(&$contractAttachModelsArr, &$pathsArr, $totalItems, $type, $order, $attachs, $maxValue){
        $targetDir = CONTRACT_FILE_PATH;
        $prefix = CONTRACT_PREFIX_FILE;
        if ($type == CONTRACT_ATTACH_IMAGE) {
            $targetDir = CONTRACT_IMAGE_PATH;
            $prefix = CONTRACT_PREFIX_IMAGE;
        }
        $orderId = $order->id;
        $allowFiles = Common::allowImages();
        $allowFiles[] = 'pdf';
        $err = false;
        for ($i = 0; $i < $totalItems; $i++) {
            $originalName = $attachs['name'][$i];
            $tmpFilePath = $attachs['tmp_name'][$i];
            
            if (!empty($originalName) && !empty($tmpFilePath)) {
                $fileType = pathinfo($originalName, PATHINFO_EXTENSION);
                if (!in_array($fileType, $allowFiles)) {
                    $err = true;
                    break;   
                }
                $no = ++$maxValue;
                $fileNameSave = basename($prefix . '_' . $orderId . '_' . $no . '.' . $fileType);
                $targetFileFull = $targetDir . $fileNameSave;
                if (move_uploaded_file($tmpFilePath, $targetFileFull)) {
                    $attach = new ContractAttachExtend();
                    $attach->id_order = $orderId;
                    $attach->type = $type;
                    $attach->file_name = $fileNameSave;
                    $contractAttachModelsArr[] = $attach;
                    $pathsArr[] = $targetFileFull;
                    chmod($targetFileFull, 0777);
                }
            }
        }
        if ($err) {
            Common::rollbackTransaction();
            if (!empty($pathsArr)) {
                $this->unLinkContractAttach($pathsArr);
            }
            $this->redirect(array($this->_actionPath . '/main', 'err' => SYSTEM_ERROR_IMAGE));
        }
    }
    
    protected function unLinkContractAttach($pathsArr) {
        foreach ($pathsArr as $p) {
            @unlink($p);
        }
    }

    public function actionSearchCustomerPhone() {
        $request = Yii::app()->request;
        $term = $request->getParam('term');
        $match = addcslashes($term, '%_');

        $resutlts = CustomerPhoneExtend::findCustomerByPhoneSearch(OPERATOR_LIKE, $match);

        $json = array();
        foreach ($resutlts as $ret) {
            $json[] = array(
                'value' => $ret['id_customer'],
                'label' => $ret['phone'],
            );
        }
        echo CJSON::encode($json);
        return;
    }
    
    public function actionLoadCustomerInfo() {
        $request = Yii::app()->request;
        $cid = trim($request->getParam('cid'));
        
        if (!empty($cid)) {
            $customer = CustomerExtend::model()->findByPk($cid);
        } else {
            $customer = NULL;
        }
        
        if (empty($customer)) {
            $customerCode = AscSystemExtend::findOneAscSystemByCondition(array('value' => CUSTOMER_CODE));
            $code = createCode($customerCode);
            $name = $address = $birthday = $email = $comment = '';
            $disabled = 0;
        } else {
            $code = $customer->code;
            $name = $customer->name;
            $address = $customer->address;
            $birthday = (!empty($customer->birthday)) ? date('d/m/Y', strtotime($customer->birthday)) : '';
            
            $email = $customer->email;
            $comment = $customer->comment;
            $disabled = 1;
        }

        $json = array(
            'code' => $code,
            'name' => $name,
            'address' => $address,
            'birthday' => $birthday,
            'disabled' => $disabled,
            'comment' => $comment,
            'email' => $email,
        );
        echo json_encode($json);
        return;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OrderExtend the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = OrderExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public function actionDelete(){
        UtilSecurity::accessCheck('Order_Delete');
        $request = Yii::app()->request;
        $msg = $err = '';
        $id = trim($request->getParam('id'));
        $model = $this->loadModel($id);
        if ($model->id_status != STATUS_OF_ORDER_CANCEL) {
            $listStatus = getListStatusOfOrder();
            $statusName = $listStatus[$model->id_status];
            $this->redirect(array($this->controllerName . '/main', 'status' => $model->id_status,'msg' => 'Đơn hàng này đang ở trạng thái '.$statusName.' nên không thể xóa.'));
        }
        $model->is_deleted = 1;
        if (!$model->save()) {
            $err = SYSTEM_ERROR;
        } else {
            if (!empty($model->contract_code)) {
                $files = $images = array();
                Common::getAllContractAttachs($files, $images, $model->id);
                $attachs = array_merge($files, $images);
                if (!empty($attachs)) {
                    $this->unLinkContractAttach($attachs);
                }
            }
            $msg = 'Đã xóa đơn hàng '.$model->code.' thành công';
        }
        $this->redirect(array($this->controllerName . '/main', 'status' => $model->id_status, 'msg' => $msg, 'err' => $err));
    }

}
