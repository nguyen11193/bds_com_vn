<?php

class ScheduleCalendarController extends Controller {

    public $titleText = 'Tiêu đề';
    public $descriptionText = 'Mô tả';
    public $startDateText = 'Ngày';
    public $typeText = 'Loại lịch';
    public $endDateText = 'Ngày kết thúc';

    public function actionIndex() {
        UtilSecurity::accessCheck('Schedule_Calendar_List');
        $request = Yii::app()->request;
        $data = Common::prepareDataScheduleIndex($request);
        
        $this->render('index', $data);
    }

    public function actionGetCalendar(){
        $request = Yii::app()->request;
        $events = [];
        $currentUser = Yii::app()->user;

        if ($request->isPostRequest) {
            $startDate = trim($request->getPost('start_date'));
            $endDate = trim($request->getPost('end_date'));
            $userId = !empty($request->getPost('id_user')) ? (int) $request->getPost('id_user') : $currentUser->id;

            if (!empty($startDate) && !empty($endDate)) {
                $repeatTypes = [
                    SCHEDULE_CALENDAR_REPEAT_TYPE_DAILY,
                    SCHEDULE_CALENDAR_REPEAT_TYPE_WEEKLY,
                    SCHEDULE_CALENDAR_REPEAT_TYPE_MONTHLY,
                ];

                $dataResults = Yii::app()->db->createCommand()
                                ->select('*, start_date AS start, color AS textColor')
                                ->from('tbl_schedule_calendar')
                                ->where('id_user = '.$userId.' AND is_deleted = 0 AND start_date >= :startDate AND start_date <= :endDate', [
                                    ':startDate' => $startDate,
                                    ':endDate' => $endDate,
                                ])
                                ->orWhere('id_user = '.$userId.' AND is_deleted = 0 AND start_date <= :orEndDate AND repeat_type IN ('.implode(',', $repeatTypes).')', [
                                    ':orEndDate' => $endDate,
                                ])
                                ->queryAll();

                if (!empty($dataResults)) {
                    $scheduleCalendarIds = array_column($dataResults, 'id');
                    $actionHistoryData = Yii::app()->db->createCommand()
                                            ->select('*')
                                            ->from('tbl_schedule_calendar_action_history')
                                            ->where(['IN', 'id_schedule_calendar', $scheduleCalendarIds])
                                            ->order('action_date ASC')
                                            ->queryAll();
                    $actionHistoryData = Common::assoc_array($actionHistoryData, 'id_schedule_calendar');

                    $scheduleCalendarType = getScheduleCalendarType();
                    $scheduleCalendarType = Common::assoc_item($scheduleCalendarType, 'id');

                    foreach ($dataResults as $dataResult) {
                        $repeatType = $dataResult['repeat_type'];
                        $startDateData = $dataResult['start_date'];

                        if (!empty($dataResult['end_date']) && strtotime($dataResult['end_date']) < strtotime($endDate)) {
                            $endDate = $dataResult['end_date'];
                        }

                        $this->prepareCommonScheduleCalendar($dataResult, $scheduleCalendarType);

                        switch ($repeatType) {
                            case SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT:
                                $this->correctRepeatDate($startDateData, 
                                    $startDateData, 
                                    ' + 1 days', 
                                    $dataResult, 
                                    $events, 
                                    $actionHistoryData, 
                                    $scheduleCalendarType
                                );
                                break;
                            case SCHEDULE_CALENDAR_REPEAT_TYPE_DAILY:
                                $this->correctRepeatDate(
                                    $startDateData, 
                                    $endDate, 
                                    ' + 1 days', 
                                    $dataResult, 
                                    $events, 
                                    $actionHistoryData, 
                                    $scheduleCalendarType
                                );
                                break;
                            case SCHEDULE_CALENDAR_REPEAT_TYPE_WEEKLY:
                                $this->correctRepeatDate(
                                    $startDateData, 
                                    $endDate, 
                                    ' + 1 weeks', 
                                    $dataResult, 
                                    $events, 
                                    $actionHistoryData, 
                                    $scheduleCalendarType
                                );
                                break;
                            case SCHEDULE_CALENDAR_REPEAT_TYPE_MONTHLY:
                                $this->correctRepeatDate(
                                    $startDateData, 
                                    $endDate, 
                                    ' + 1 months', 
                                    $dataResult, 
                                    $events, 
                                    $actionHistoryData, 
                                    $scheduleCalendarType
                                );
                                break;
                        }
                    }
                }
            }
        }

        $data['events'] = $events;

        echo json_encode($data);
        return;
    }

    protected function prepareCommonScheduleCalendar(&$dataResult, $scheduleCalendarType){
        $dataResult['start_time'] = date('H:i', strtotime($dataResult['start_time']));
        $dataResult['end_time'] = date('H:i', strtotime($dataResult['end_time']));

        $dataResult['color'] = !empty($scheduleCalendarType[$dataResult['type']]['color'])
            ? $scheduleCalendarType[$dataResult['type']]['color']
            : SCHEDULE_CALENDAR_DEFAULT_COLOR;

        $dataResult['title_original'] = $dataResult['title'];
        if (empty($dataResult['textColor'])) {
            $dataResult['textColor'] = SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR;
        }

        if ($dataResult['type'] == SCHEDULE_CALENDAR_TYPE_TASK && empty($dataResult['id_child_task'])) {
            $dataResult['id_child_task'] = SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK;
        }

        if (!empty($dataResult['end_date'])) {
            $dataResult['end_date'] = date('d/m/Y', strtotime($dataResult['end_date']));
        }
    }

    protected function correctRepeatDate(
        $start, 
        $end, 
        $fomula, 
        $dataResult, 
        &$events, 
        $actionHistoryData, 
        $scheduleCalendarType
    ){
        $originDataResult = $dataResult;
        $completeData = [];
        $deleteData = [];
        $updateData = [];
        if (!empty($actionHistoryData[$dataResult['id']])) {
            $actionTypeArr = Common::assoc_array($actionHistoryData[$dataResult['id']], 'action_type');

            if (!empty($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_COMPLETE])) {
                $completeData = Common::assoc_item($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_COMPLETE], 'action_date');
            }

            if (!empty($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE])) {
                $deleteData = Common::assoc_item($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE], 'action_date');
            }

            if (!empty($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE])) {
                $updateData = Common::assoc_item($actionTypeArr[SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE], 'action_date');
            }
        }

        while ($start <= $end) {
            $date = date('Y-m-d', strtotime($start));

            if (!empty($deleteData[$date])) {
                $deleteType = $deleteData[$date]['delete_type'];
                
                if ($deleteType == SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE) {
                    $start = date('Y-m-d', strtotime($start . $fomula));
                    continue;
                } else if ($deleteType == SCHEDULE_CALENDAR_DELETE_TYPE_ALL
                            || $deleteType == SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE_TO_END) {
                    break;
                }
            }

            if (!empty($updateData[$date])) {
                $dataResult = jcustom_decode($updateData[$date]['updated_info']);
                $dataResult['start'] = $dataResult['start_date'];
                $this->prepareCommonScheduleCalendar($dataResult, $scheduleCalendarType);
            }

            $dataResult['cannot_edit'] = 0;
            if (strtotime($date) < strtotime(date('Y-m-d'))) {
                $dataResult['cannot_edit'] = 1;
            }

            $dataResult['is_completed'] = 0;
            $dataResult['className'] = SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE;
            if (!empty($completeData[$date])) {
                $dataResult['is_completed'] = 1;
                $dataResult['className'] = SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED;
            }

            if ($dataResult['is_all_day'] == 1) {
                $dataResult['start'] = $date;
                $dataResult['title'] = $date . ' | ' . $dataResult['title'];
            } else {
                $dataResult['start'] = $date . ' ' . $dataResult['start_time'];
                $dataResult['title'] = $dataResult['start_time'] . ' - ' . $dataResult['end_time'] . ' | ' . $dataResult['title'];
                $dataResult['end'] = $date . ' ' . $dataResult['end_time'];
            }
            $dataResult['start_date'] = date('d/m/Y', strtotime($date));

            $events[] = $dataResult;
            $dataResult = $originDataResult;

            $start = date('Y-m-d', strtotime($start . $fomula));
        }
    }

    public function actionComplete(){
        $request = Yii::app()->request;
        $data = ['result' => 0, 'msg' => ''];
        if ($request->isPostRequest) {
            $permList = UtilSecurity::accessCheck('Schedule_Calendar_Edit', true);

            if ($permList === false) {
                $data['msg'] = 'Bạn không có quyền sửa Lịch';
            } else {
                $isCompleted = (int) trim($request->getPost('is_completed'));
                $id = (int) trim($request->getPost('id'));
                $actionDate = correctDate(trim($request->getPost('action_date')));
                
                if (empty($actionDate)) {
                    $data['msg'] = 'Không đúng tham số ngày';
                } else {
                    $scheduleCalendar = ScheduleCalendarExtend::model()->findByPk($id);

                    if (empty($scheduleCalendar)) {
                        $data['msg'] = 'Lịch biểu này không tồn tại trong hệ thống';
                    } else {
                        if ($isCompleted == 1) {
                            $actionHistory = new ScheduleCalendarActionHistoryExtend();
                            $actionHistory->id_schedule_calendar = $scheduleCalendar->id;
                            $actionHistory->id_user = Yii::app()->user->id;
                            $actionHistory->action_date = $actionDate;
                            $actionHistory->action_type = SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_COMPLETE;
                            if (!$actionHistory->save()) {
                                $data['msg'] = SYSTEM_ERROR;
                            }
                        } else {
                            ScheduleCalendarActionHistoryExtend::model()->deleteAll('id_schedule_calendar = '.$scheduleCalendar->id.' AND action_type = '.SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_COMPLETE.' AND action_date = "'.$actionDate.'" ');
                        }

                        if (empty($data['msg'])) {
                            $data['result'] = 1;
                            $data['msg'] = 'thành công';
                        }
                    }
                }
            }
        } else {
            $data['msg'] = INVALID_METHOD_POST_MSG;
        }

        echo json_encode($data);
        return;
    }

    public function actionDelete(){
        $request = Yii::app()->request;
        $data = ['result' => 0, 'msg' => ''];
        if ($request->isPostRequest) {
            $permList = UtilSecurity::accessCheck('Schedule_Calendar_Delete', true);

            if ($permList === false) {
                $data['msg'] = 'Bạn không có quyền xóa Lịch';
            } else {
                $id = (int) trim($request->getPost('id'));
                $deleteType = (int) trim($request->getPost('delete_type'));
                $actionDate = correctDate(trim($request->getPost('action_date')));
                
                if (empty($actionDate)) {
                    $data['msg'] = 'Không đúng tham số ngày';
                } else {
                    $scheduleCalendar = ScheduleCalendarExtend::model()->findByPk($id);

                    if (empty($scheduleCalendar)) {
                        $data['msg'] = 'Lịch biểu này không tồn tại trong hệ thống';
                    } else {
                        if ($deleteType == SCHEDULE_CALENDAR_DELETE_TYPE_ALL) {
                            $scheduleCalendar->is_deleted = 1;
                            $scheduleCalendar->save();
                        }

                        $actionHistory = new ScheduleCalendarActionHistoryExtend();
                        $actionHistory->id_schedule_calendar = $scheduleCalendar->id;
                        $actionHistory->id_user = Yii::app()->user->id;
                        $actionHistory->action_date = $actionDate;
                        $actionHistory->action_type = SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE;
                        $actionHistory->delete_type = $deleteType;

                        if (!$actionHistory->save()) {
                            $data['msg'] = 'Chưa xóa được. Vui lòng thử lại!';
                        } else {
                            $data['result'] = 1;
                            $data['msg'] = 'Xóa thành công';
                        }
                    }
                }
            }
        } else {
            $data['msg'] = INVALID_METHOD_POST_MSG;
        }

        echo json_encode($data);
        return;
    }

    public function actionCreate(){
        $request = Yii::app()->request;
        $data = ['result' => 0, 'msg' => ''];

        if ($request->isPostRequest) {
            $user = Yii::app()->user;

            $id = (int) trim($request->getPost('id'));
            $type = (int) trim($request->getPost('type'));
            $title = trim($request->getPost('title'));
            $description = trim($request->getPost('description'));
            $startDate = correctDate(trim($request->getPost('start_date')));
            $startTime = trim($request->getPost('start_time'));
            $endTime = trim($request->getPost('end_time'));
            $isAllDay = (int) trim($request->getPost('is_all_day'));
            $repeatType = (int) trim($request->getPost('repeat_type'));
            $idChildTask = (int) trim($request->getPost('id_child_task'));
            $updateType = (int) trim($request->getPost('update_type'));
            $endDateOption = (int) trim($request->getPost('end_date_option'));
            $endDate = correctDate(trim($request->getPost('end_date')));
            $color = trim($request->getPost('color'));

            $currentDate = date('Y-m-d');

            $scheduleCalendar = null;
            $isEdit = false;
            if ($id > 0) {
                $msg1 = ' sửa ';
                $perm = 'Schedule_Calendar_Edit';

                $scheduleCalendar = ScheduleCalendarExtend::model()->findByPk($id);
            } else {
                $msg1 = ' thêm ';
                $perm = 'Schedule_Calendar_Add';
            }

            $permList = UtilSecurity::accessCheck($perm, true);

            if ($permList === false) {
                $data['msg'] = 'Bạn không có quyền' . $msg1 . 'Lịch';
            } else if (empty($title) || empty($startDate)) {
                $data['msg'] = 'Vui lòng nhập tiêu đề hoặc chọn ngày';
            } else if (strtotime($startDate) < strtotime($currentDate)) {
                $data['msg'] = 'Bạn không thể tạo lịch với ngày quá khứ';
            } else if (strtotime($startTime) >= strtotime($endTime) && $isAllDay == 0) {
                $data['msg'] = '"Từ giờ" không được lớn hơn "Đến giờ"';
            } else if ($repeatType != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT 
                    && $endDateOption == SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE
                    && empty($endDate)) {
                $data['msg'] = 'Vui lòng chọn ngày kết thúc';
            } else if ($repeatType != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT 
                    && $endDateOption == SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE
                    && strtotime($endDate) < strtotime($startDate)) {
                $data['msg'] = 'Ngày bắt đầu không được lớn hơn ngày kết thúc';
            } else {
                Common::beginTransaction();
                if (empty($scheduleCalendar)) {
                    $scheduleCalendar = new ScheduleCalendarExtend();
                    $scheduleCalendar->type = $type;
                    $scheduleCalendar->id_user = $user->id;
                } else {
                    $isEdit = true;
                }
    
                $scheduleCalendar->title = $title;
                $scheduleCalendar->description = $description;
                $scheduleCalendar->repeat_type = $repeatType;
                $scheduleCalendar->is_all_day = $isAllDay;
                $scheduleCalendar->start_time = null;
                $scheduleCalendar->end_time = null;
                $scheduleCalendar->id_child_task = $type == SCHEDULE_CALENDAR_TYPE_TASK ? $idChildTask : 0;
                $scheduleCalendar->color = $color;
                $scheduleCalendar->start_date = $startDate;
    
                if ($isAllDay == 0) {
                    $scheduleCalendar->start_time = $startTime;
                    $scheduleCalendar->end_time = $endTime;
                }

                $scheduleCalendar->end_date = null;
                if ($repeatType != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT 
                        && $endDateOption == SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE) {
                    $scheduleCalendar->end_date = $endDate;
                }

                if ($isEdit && $updateType == SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE) {
                    //$scheduleCalendar->start_date = $startDate;

                    $actionHistory = new ScheduleCalendarActionHistoryExtend();
                    $actionHistory->id_schedule_calendar = $scheduleCalendar->id;
                    $actionHistory->id_user = $user->id;
                    $actionHistory->action_date = $startDate;
                    $actionHistory->action_type = SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE;
                    $actionHistory->updated_info = json_encode($scheduleCalendar->getAttributes());

                    if (!$actionHistory->save()) {
                        $data['msg'] = SYSTEM_ERROR;
                    }
                } else {
                    if ($isEdit) {
                        ScheduleCalendarActionHistoryExtend::model()->deleteAll('id_schedule_calendar = '.$scheduleCalendar->id.' AND action_type = ' . SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE);
                    }

                    if (!$scheduleCalendar->save()) {
                        $data['msg'] = SYSTEM_ERROR;
                    }
                }

                if (empty($data['msg'])) {
                    $data['msg'] = 'Thành công';
                    $data['result'] = 1;
                    Common::commitTransaction();
                } else {
                    Common::rollbackTransaction();
                }
            }
        } else {
            $data['msg'] = INVALID_METHOD_POST_MSG;
        }

        echo json_encode($data);
        return;
    }
}