<?php

class ImportCsvController extends SystemController{
    
    public $title = 'Import CSV';


    public function actionMain($msg = '', $err = ''){
    	//Render the View
        $this->render('main', array(
            'msg' => $msg,
            'err' => $err,
        ));
    }
    
    public function actionImportSupplier(){
        $this->render('importBatch', array(
            'title' => 'Import Nhà cung cấp',
            'action' => 'uploadSupplier',
        ));
    }
    
    public function actionImportProductShortName(){
        $this->render('importBatch', array(
            'title' => 'Import Short name của SP',
            'action' => 'uploadShortName',
        ));
    }

    public function actionImportTest(){
        $this->render('importBatch', array(
            'title' => 'Import Test',
            'action' => 'uploadTest',
        ));
    }
    
    public function actionUploadTest(){
        $file = CUploadedFile::getInstanceByName('uploadBtn');
        $fileName = Yii::app()->basePath . "/../imports/" . $file->name;
        $file->saveAs($fileName);
        
        $connection = new Spreadsheet_Excel_Reader();
        $connection->setOutputEncoding('UTF-8');
        $connection->read($fileName);
        var_dump($connection->sheets[0]);die;
    }
    
    public function actionUploadShortName() {
        $request = Yii::app()->request;
        $file = $request->getParam('filename');
        $update = $request->getParam('update');
        if ($update == 1) {
            $output = array_map('str_getcsv', file($file));
            $count = count($output);
            
            $ignoredProduct = array();
            
            for ($i = 5; $i <= 46; $i++) {
                $ignoredProduct[] = $i;
            }
            
            for ($i = 83; $i <= 90; $i++) {
                $ignoredProduct[] = $i;
            }
            
            for ($i = 219; $i <= 229; $i++) {
                $ignoredProduct[] = $i;
            }
            
            for ($i = 231; $i <= 236; $i++) {
                $ignoredProduct[] = $i;
            }
            
            $ignoredProduct1 = array(63, 76, 77, 95, 96, 97, 98, 104, 108, 115, 124, 127, 137, 138, 139,
                153, 154, 155, 156, 157, 159, 161, 162, 173, 174, 175, 177, 178, 179, 180, 181, 182, 183,
                185, 186, 187, 188, 189, 192, 203, 204, 205, 238, 242, 244, 245, 246, 247, 249, 250, 251, 252, 253, 257,
                191, 259, 260, 261, 262, 258, 284, 296, 297, 298, 394, 398, 477, 541, 642, 643);
            
            $ignoredProduct = array_merge($ignoredProduct, $ignoredProduct1);
            
            $criteria = new CDbCriteria;
            $criteria->order = "id";
            $criteria->compare('is_deleted', 0);
            $criteria->addNotInCondition('id', $ignoredProduct);
            $products = ProductExtend::model()->findAll($criteria);
            $products = Common::assoc_item($products, 'code');
            
            $productModels = array();
            
            Common::beginTransaction();
            for ($x = 1; $x < $count; $x++) {
                $productCode = trim($output[$x][0]);
                $shortName = trim($output[$x][1]);
                $shortNameTrim = Common::changeAlias($shortName);
                
                if (strlen($shortNameTrim) > 45) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Short name vượt quá 45 ký tự ở dòng '.$x));
                }
                
                if (!empty($products[$productCode])) {
                    $product = $products[$productCode];
                    $product->short_name = $shortName;
                    $productModels[] = $product;
                    unset($products[$productCode]);
                }
            }
            
            $err = array();
            if (!empty($products)) {
                foreach ($products as $p) {
                    $err[] = $p->id . ' - ' . $p->code . ' - ' . $p->name;
                }
            }
            
            if (!empty($err)) {
                $productErr = implode('<br/>', $err);
                Common::rollbackTransaction();
                $this->redirect(array($this->controllerName . '/main', 'err' => 'Những sp sau chưa có short name<br/> ' . $productErr));
            }
            
            if (!empty($productModels)) {
                if (!insertMultipleModel($productModels)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
                }
            }
            
            Common::commitTransaction();
            $this->redirect(array($this->controllerName . '/main', 'msg' => 'Đã import thành công'));
        } else {
            $data = $this->getFileData();
            $data['title'] = 'Import Short name của SP';
            $data['action'] = 'importProductShortName';
            $data['header'] = array('Mã sp', 'Short name');
            $this->render('viewBatch', $data);
        }
    }

    public function actionUploadSupplier(){
        $request = Yii::app()->request;
        $file = $request->getParam('filename');
        $update = $request->getParam('update');
        if ($update == 1) {
            /*$output = array_map('str_getcsv', file($file));
            $count = count($output);
            $supplierCodeArr = array();
            Yii::app()->db->createCommand()->truncateTable('tbl_w_supplier');
            Common::beginTransaction();
            for ($x = 1; $x < $count; $x++) {
                $supplierCode = trim($output[$x][0]);
                $supplierName = trim($output[$x][1]);
                
                // Kiểm tra phải có những thông tin bắt buộc
                if (empty($supplierCode) || empty($supplierCode)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Thiếu những thông tin bắt buộc ở dòng '.$x));
                }
                // Kiểm tra có trùng mã lô hàng hay ko
                if (in_array(strtolower($supplierCode), $supplierCodeArr)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Đã trùng mã nhà cc ở dòng ' . $x));
                }
                $supplierCodeArr[] = strtolower($supplierCode);
                
                $supplier = new WSupplierExtend();
                $supplier->code = $supplierCode;
                $supplier->name = $supplierName;
                if (!$supplier->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc thông tin nhà cc ở dòng ' . $x));
                }
            }
            Common::commitTransaction();
            $this->redirect(array($this->controllerName . '/main', 'msg' => 'Đã import thành công'));*/
        } else {
            $data = $this->getFileData();
            $data['title'] = 'Import nhà cung cấp';
            $data['action'] = 'importSupplier';
            $data['header'] = array('Mã NCC', 'Tên NCC');
            $this->render('viewBatch', $data);
        }
    }
    
    public function actionImportBatch(){
        $this->render('importBatch', array(
            'title' => 'Import lô hàng',
            'action' => 'uploadBatch',
        ));
    }
    
    protected function getDataFromTable($table){
        $resultArr = array();
        $data = Yii::app()->db->createCommand()
                ->select('id, code')
                ->from($table)
                ->where('is_deleted = 0')
                ->queryAll();
        foreach ($data as $dt) {
            $resultArr[strtolower($dt['code'])] = $dt['id'];
        }
        return $resultArr;
    }

    public function actionUploadBatch(){
        $request = Yii::app()->request;
        $file = $request->getParam('filename');
        $update = $request->getParam('update');
        if ($update == 1) {
            /*$output = array_map('str_getcsv', file($file));
            $count = count($output);
            
            $productArr = $this->getDataFromTable('tbl_product');
            $warehouseArr = $this->getDataFromTable('tbl_warehouse');
            $supplierArr = $this->getDataFromTable('tbl_w_supplier');

            $batchCodeArr = $warehouseLog = array();
            Yii::app()->db->createCommand()->truncateTable('tbl_w_batch');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_batch_warehouse_map');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_export');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_export_detail');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_inventory_norm');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_receipt');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_receipt_detail');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_transfer');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_transfer_detail');
            Yii::app()->db->createCommand()->truncateTable('tbl_w_warehouse_log');
            Common::beginTransaction();
            for ($x = 1; $x < $count; $x++) {
                $batchCode = trim($output[$x][0]);
                $productCode = trim($output[$x][1]);
                $warehouseCode = trim($output[$x][2]);
                $supplierCode = trim($output[$x][3]);
                $quantity = convertNumberWithCommaToNumber(trim($output[$x][4]));
                $unitPrice = convertNumberWithCommaToNumber(trim($output[$x][5]));
                $orgPrice = convertNumberWithCommaToNumber(trim($output[$x][6]));
                $manufactoringDate = trim($output[$x][7]);
                $expiryDate = trim($output[$x][8]);
                $source = trim($output[$x][9]);
                $note = trim($output[$x][10]);
                
                // Kiểm tra phải có những thông tin bắt buộc
                if (empty($batchCode) || empty($productCode) || empty($warehouseCode) || empty($supplierCode) || empty($manufactoringDate) || empty($expiryDate)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Thiếu những thông tin bắt buộc ở dòng '.$x));
                }
                // Kiểm tra có trùng mã lô hàng hay ko
                if (in_array(strtolower($batchCode), $batchCodeArr)) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Đã trùng mã lô hàng ở dòng ' . $x));
                }
                // Kiểm tra mã kho hoặc mã sp có phải nằm trong hệ thống hay ko
                if (empty($warehouseArr[strtolower($warehouseCode)]) || empty($productArr[strtolower($productCode)]) || empty($supplierArr[strtolower($supplierCode)])) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Mã kho hoặc mã sp hoặc mã nhà cc không tồn tại trong hệ thống ở dòng ' . $x));
                }
                $batchCodeArr[] = strtolower($batchCode);
                
                $receipt = new WReceipt();
                $code = AscSystemExtend::findOneAscSystemByCondition(array('value' => RECEIPT_CODE));
                $receipt->code = generateLicenseCode($code);
                AscSystemExtend::autoIncrementValcount($code);

                $receipt->id_user = 1;
                $receipt->total_net_price = $orgPrice;
                $receipt->total_vat_price = 0;
                $receipt->total_gross_price = $orgPrice;
                $receipt->is_vat = NO;
                if (!$receipt->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc thông tin nhập hàng ở dòng ' . $x));
                }
                
                $productId = $productArr[strtolower($productCode)];
                
                $batch = new WBatchExtend();
                $batch->code = $batchCode;
                $batch->id_supplier = $supplierArr[strtolower($supplierCode)];
                $batch->id_product = $productId;
                $batch->unit_price = $unitPrice;
                $batch->note = (empty($note)) ? 'DEV Team import by excel' : $note;
                $batch->id_user = 1;
                $batch->source = (empty($source)) ? null : $source;
                $batch->manufactoringDate = correctDate($manufactoringDate);
                $batch->expiryDate = correctDate($expiryDate);
                if (!$batch->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc thông tin lô hàng ở dòng ' . $x));
                }
                
                $warehouseId = $warehouseArr[strtolower($warehouseCode)];
                
                $batchMap = new WBatchWarehouseMapExtend();
                $batchMap->id_batch = $batch->id;
                $batchMap->id_warehouse = $warehouseId;
                $batchMap->quantity = $quantity;
                if (!$batchMap->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc số lượng ở dòng ' . $x));
                }
                
                $detail = new WReceiptDetail();
                $detail->id_receipt = $receipt->id;
                $detail->id_batch = $batch->id;
                $detail->quantity = $quantity;
                $detail->unit_price = $unitPrice;
                $detail->net_price = $orgPrice;
                $detail->warehouse_id = $warehouseId;
                if (!$detail->save()) {
                    Common::rollbackTransaction();
                    $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc thông tin chi tiết nhập hàng ở dòng ' . $x));
                }
                $warehouseLog[] = Common::prepareFieldWarehouseLog($warehouseId, $batch->id, $productId, $quantity, WAREHOUSE_LOG_TYPE_RECEIPT);
            }
            $insertWarehouseLog = WWarehouseLogExtend::insertWarehouseLog($warehouseLog);
            if ($insertWarehouseLog == 0) {
                Common::rollbackTransaction();
                $this->redirect(array($this->controllerName . '/main', 'err' => 'Chưa lưu đc thông tin log'));
            }
            Common::commitTransaction();
            $this->redirect(array($this->controllerName . '/main', 'msg' => 'Đã import thành công'));*/
        } else {
            $data = $this->getFileData();
            $data['title'] = 'Import lô hàng';
            $data['action'] = 'importBatch';
            $data['header'] = array('Mã lô hàng', 'Mã sản phẩm', 'Mã kho', 'Mã NCC', 'Số lượng', 'Đơn giá', 'Giá vốn', 'Ngày sản xuất', 'Ngày hết hạn', 'Xuất sứ', 'Ghi chú');
            $this->render('viewBatch', $data);
        }
    }
    
    protected function getFileData(){
        $file = CUploadedFile::getInstanceByName('uploadBtn');
        $fileName = Yii::app()->basePath . "/../imports/" . $file->name;
        $file->saveAs($fileName);
        $encoding = mb_detect_encoding(file_get_contents($fileName), mb_detect_order(), TRUE);
        // Open the file for users to verify if everything is correct.
        $outPut = array_map('str_getcsv', file($fileName));
        return array(
            'outPut' => $outPut,
            'encoding' => $encoding,
            'fileName' => $fileName,
        );
    }
    
    protected function nguyen(){
                            for($i = 1; $i <= 3; $i++){
                        $FILE = $_FILES["customer_image_" . $i];
                        if ($FILE["name"] != "" && $FILE["name"] != NULL) {
                            $target_dir = PRECUSTOMER_IMAGE_PATH;
                            $target_file = $target_dir . basename($preCustomerInteraction->id . "_interaction" . $FILE["name"]);
                            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
                            if (!in_array($file_type, Common::allowImages())) {
                                Common::rollbackTransaction();
                                $this->redirect(array($this->_actionPath . '/main', 'msg' => SYSTEM_ERROR_IMAGE));
                            }
                            $file_name = basename('interaction_' . $preCustomerInteraction->id . "_image$i." . $file_type);
                            $target_file_full = $target_dir . $file_name;
                            if (move_uploaded_file($FILE["tmp_name"], $target_file)) {
                                $full = new EasyImage($target_file);
                                $full->save($target_file_full);
                                $consumerImage = new PrecustomerImageExtend();
                                $consumerImage->name = $file_name;
                                $consumerImage->id_precustomer = $preCustomerInteraction->id_precustomer;
                                $consumerImage->id_interaction = $preCustomerInteraction->id;
                                if (!$consumerImage->save()) {
                                    @unlink($target_file);
                                    @unlink($target_file_full);
                                    Common::rollbackTransaction();
                                    $this->redirect(array($this->_actionPath . '/main', 'msg' => SYSTEM_ERROR));
                                }
                            }
                            @unlink($target_file);
                        }
                    }
    }
}

