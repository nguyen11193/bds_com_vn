<?php

class MenuParentController extends SystemController {
    
    private $_actionPath = "MenuParent";
    private $_actionName = "MenuParent";
    
    public function actionMain($msg = '', $err = ''){
    	$items = MenuParentExtend::getAllMenuParentFromCache();

    	//Render the View
        $this->render('main', array(
            'items' => $items,
            'actionPath' => $this->_actionPath,
            'msg' => $msg,
            'err' => $err,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate(){
        date_default_timezone_set(Yii::app()->params['timezone']);
        $request = Yii::app()->request;

        if ($request->getParam('name') != null) {
            $model = new MenuParentExtend();
            $model->name = trim($request->getParam('name'));
            $model->icon = trim($request->getParam('icon'));
            $model->sort_by = trim($request->getParam('sortby'));
            if ($model->save()) {
                // Display view page
                $this->redirect(array($this->_actionPath . '/main', 'msg' => 'Đã thêm '.$model->name.' thành công'));
            }
        }

        //Render the View
        $this->render('create', array(
            'actionPath' => $this->_actionPath
        ));
    }
    
    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $name = trim($request->getParam('name'));
        $id = trim($request->getParam('id'));
        $conditionToCompare = array();
        $conditionAddCondition = array();
        $conditionToCompare['name'] = $name;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }
        $records = MenuParentExtend::findOneMenuParentByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($records)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }
    
    public function actionCheckHaveMenu(){
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $conditionToCompare = array();
        $conditionOthers = array();
        $conditionToCompare['id_menu_parent'] = $sid;
        $conditionOthers['limit'] = 1;
        $check = MenuExtend::findAllMenuByCondition($conditionToCompare, $conditionOthers);
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate(){
        date_default_timezone_set(Yii::app()->params['timezone']);
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $item = $this->loadModel($id);

        if ($request->getParam('name') != null) {
            $item->name = trim($request->getParam('name'));
            $item->icon = trim($request->getParam('icon'));
            $item->sort_by = trim($request->getParam('sortby'));
            if ($item->save()) {
                // Display view page
                $this->redirect(array($this->_actionPath . '/main', 'msg' => 'Đã sửa '.$item->name.' thành công'));
            }
        }

        //Render the View
        $this->render('update', array(
            'actionPath' => $this->_actionPath,
            'item' => $item
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $model = $this->loadModel($sid);
        if ($model->delete()) {
            $this->redirect(array($this->_actionPath . '/main', 'msg' => 'Đã xóa '.$model->name.' thành công'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MenuParent the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = MenuParent::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
