<?php

class PosController extends Controller {
    
    private $_actionPath = "Pos";
    private $_actionName = "Pos";
    private $mediaType = 'json';
    
    public function actionCreate() {
        $this->_checkAuth();
    }
    
    public function actionUpdate(){
        $this->_checkAuth();
        $adata = array();
        $adata['result'] = 0;
    }
    
     public function actionDelete(){
        $this->_checkAuth();
    }

    public function actionList(){
        $this->_checkAuth();
        $adata = array();
        $items = array();
        $adata['result'] = 0;
        switch ($_GET['list']) {
            //Get an instance of the respective model
            case 'listUser':
                $items = UserAccountExtend::sqlfindAllUserAccount();
                break;
            default:
                $this->_sendResponse(501, sprintf('REST is not implemented for this model, Go ahead and first implement it', $_GET['list']));
                Yii::app()->end();
        }
        $adata['result'] = 1;
        $adata['data'] = $items;
        $json = json_encode($adata);
        $this->_sendResponse(200, $json);
    }

    protected function beforeSave() {
        // author_id may have been posted via API POST
        if (is_null($this->author_id) or $this->author_id == '')
            $this->author_id = Yii::app()->user->id;
    }
    
    private function _checkAuth() {
        $headers = apache_request_headers();
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if (!(isset($headers['USERNAME']) and isset($headers['PASSWORD']))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $headers['USERNAME'];
        $password = $headers['PASSWORD'];
        // Find the user
        $user = UserAccountExtend::model()->find('active = 1 AND LOWER(username)=?', array(strtolower($username)));
        if ($user === null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if (!CPasswordHelper::verifyPassword($password, $user->password)) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }
}

