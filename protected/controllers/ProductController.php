<?php

class ProductController extends Controller {
    
    public $title = 'Sản phẩm';
    public $nameText = 'Tên sản phẩm';
    public $acreageText = 'Diện tích';
    public $priceText = 'Giá bán';

    public function loadModel($id) {
        $model = ProductExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionMain($msg = '', $err = ''){
        $permList = UtilSecurity::accessCheck('Product_List');
        $icon = $permList['Product_List']['detail']['menu_parent_icon'];
        $request = Yii::app()->request;
        $projectId = 0;
        if (!empty($request->getParam('projectId'))) {
            $projectId = trim($request->getParam('projectId'));
        }
        $title = 'Danh sách sản phẩm';
        $items = ProductExtend::getAllProductsFromCacheByProject($projectId);
        $projects = ProjectExtend::getAllProjectsFromCache();
        $breadcrumbs = [
            [
                'title' => $title,
            ],
        ];
        //Render the View
        $this->render('main', array(
            'items' => $items,
            'msg' => $msg,
            'err' => $err,
            'projectId' => $projectId,
            'icon' => $icon,
            'projects' => $projects,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
        ));
    }

    public function actionCreate(){
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $projectId = trim($request->getParam('projectId'));
        $isSubmit = (!empty($request->getParam('create'))) ? true : false;
        $project = ProjectExtend::model()->findByPk($projectId);
        if (empty($project)) {
            $this->redirect(array($this->controllerName . '/main', 'err' => 'Không có dự án'));
        }
        $msg = $err = '';
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('Product_Add');
            $icon = $permList['Product_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'name' => '',
                'acreage' => '',
                'price' => '',
            );
        } else {
            $permList = UtilSecurity::accessCheck('Product_Edit');
            $icon = $permList['Product_Edit']['detail']['menu_parent_icon'];
            $item = $this->loadModel($id);
            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $itemShow = array(
                'id' => $item->id,
                'name' => $item->name,
                'acreage' => $item->acreage,
                'price' => $item->price,
            );
        }
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];
        if ($isSubmit) {
            if (empty($item)) {
                $item = new ProductExtend();
                $item->is_deleted = 0;
                $item->status = STATUS_OF_PRODUCT_OPEN;
                $item->id_project = $projectId;
            }
            $item->name = trim($request->getParam('name'));
            $item->acreage = floatval(trim($request->getParam('acreage')));
            $item->price = floatval(trim($request->getParam('price')));

            if ($item->save()) {
                $msg = 'Đã'.$msg1.$item->name.' thành công';
            } else {
                $err = SYSTEM_ERROR;
            }
            $this->redirect(array($this->controllerName . '/main', 'projectId' => $projectId, 'msg' => $msg, 'err' => $err));
        }
    
        $this->render('create', array(
            'icon' => $icon,
            'item' => $item,
            'breadcrumbs' => $breadcrumbs,
            'itemShow' => $itemShow,
            'project' => $project,
            'title' => $title,
            'projectId' => $projectId,
        ));
    }

    public function actionDelete(){
        UtilSecurity::accessCheck('Product_Delete');
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        $msg = $err = '';
        if (!$model->save()) {
            $err = SYSTEM_ERROR;
        } else {
            $msg = 'Đã xóa sản phẩm '.$model->name.' thành công';
        }
        $this->redirect(array($this->controllerName . '/main', 'projectId' => $model->id_project, 'msg' => $msg, 'err' => $err));
    }

    /**
     * AJAX Function to check if product is in use
     */
    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $name = trim($request->getParam('name'));
        $projectId = trim($request->getParam('projectId'));
        $id = trim($request->getParam('id'));
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['name'] = $name;
        $conditionToCompare['id_project'] = $projectId;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }
        $product = ProductExtend::findOneProductByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);
        if (!empty($product)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }
    
    public function actionCheckHaveOrder(){
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $check = OrderExtend::findOneOrdersByCondition(array('id_product' => $id), array('limit' => 1));
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }
}