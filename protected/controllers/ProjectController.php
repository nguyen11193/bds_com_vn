<?php

class ProjectController extends Controller {
    
    public $title = 'Dự án';
    public $codeText = 'Mã dự án';
    public $nameText = 'Tên dự án';
    public $addressText = 'Vị trí';
    
    public function loadModel($id) {
        $model = ProjectExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionMain($msg = '', $err = ''){
        $permList = UtilSecurity::accessCheck('Project_List');
        $icon = $permList['Project_List']['detail']['menu_parent_icon'];
        $request = Yii::app()->request;
        $title = 'Danh sách dự án';
        
        $itemPerPage = LIMIT_ITEMS;
        $page = 1;
        if (!empty($request->getParam('page'))) {
            $page = $request->getParam('page');
        }
        $offset = $itemPerPage * ($page - 1);
        
        $filter = '';
        if (!empty($request->getParam('filter'))) {
            $filter = trim($request->getParam('filter'));
        }
        
        $sql = Yii::app()->db->createCommand()->from('tbl_project')->where('is_deleted = 0');
        if (!empty($filter)) {
            $sql = $sql->andWhere('code LIKE "%'.$filter.'%" OR name LIKE "%'.$filter.'%"');
        }
        
        $totalSql = clone $sql;
        $totalPage = $totalSql->select('count(*)')->queryScalar();
        $totalPage = ceil($totalPage / $itemPerPage);
        
        $items = $sql->select('id, code, name, address')
                ->order('created_at DESC')
                ->limit($itemPerPage)
                ->offset($offset)
                ->queryAll();
        $breadcrumbs = [
            [
                'title' => $title,
            ],
        ];
        $searchBox = array(
            'action' => "",
            'placeholder' => SEARCH_TEXT,
            'value' => $filter,
        );

        $this->render('main', array(
            'items' => $items,
            'msg' => $msg,
            'err' => $err,
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'searchBox' => $searchBox,
            'title' => $title,
            'totalPage' => $totalPage,
            'page' => $page,
        ));
    }
    
    public function actionCreate(){
        $request = Yii::app()->request;
        $isSubmit = (!empty($request->getParam('create'))) ? true : false;
        $id = trim($request->getParam('id'));
        
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('Project_Add');
            $icon = $permList['Project_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'code' => '',
                'name' => '',
                'address' => '',
            );
        } else {
            $permList = UtilSecurity::accessCheck('Project_Edit');
            $icon = $permList['Project_Edit']['detail']['menu_parent_icon'];
            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $item = $this->loadModel($id);
            $itemShow = array(
                'id' => $item->id,
                'code' => $item->code,
                'name' => $item->name,
                'address' => $item->address,
            );
        }
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];
        
        if ($isSubmit) {
            if (empty($item)) {
                $item = new ProjectExtend();
                $item->is_deleted = 0;
                $item->status = 1;
            }
            $item->code = trim($request->getParam('code'));
            $item->name = trim($request->getParam('name'));
            $item->address = trim($request->getParam('address'));
            if ($item->save()) {
                $msg = 'Đã'.$msg1.$item->name.' thành công';
            } else {
                $err = SYSTEM_ERROR;
            }
            $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
        }
        
        $this->render('create', array(
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'itemShow' => $itemShow,
        ));
    }
    
    public function actionCheckDuplicity(){
        $request = Yii::app()->request;
        $code = trim($request->getParam('code'));
        $id = trim($request->getParam('id'));
        
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['code'] = $code;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $project = ProjectExtend::findOneProjectByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($project)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }
    
    public function actionDelete(){
        UtilSecurity::accessCheck('Project_Delete');
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        $msg = $err = '';
        if (!$model->save()) {
            $err = SYSTEM_ERROR;
        } else {
            $msg = 'Đã xóa dự án '.$model->name.' thành công';
        }
        $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
    }
    
    public function actionCheckHaveOrder(){
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $check = OrderExtend::findOneOrdersByCondition(array('id_project' => $id), array('limit' => 1));
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }
}

