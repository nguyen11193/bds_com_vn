<?php

class DepartmentController extends Controller {
    
    public $title = 'Bộ phận';
    public $nameText = 'Tên bộ phận';

    public function loadModel($id) {
        $model = DepartmentExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $uname = trim($request->getParam('uname'));
        $id = trim($request->getParam('id'));
        
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['name'] = $uname;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $department = DepartmentExtend::findOneDepartmentByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($department)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }

    public function actionIndex($msg = '', $err = '') {
        $permList = UtilSecurity::accessCheck('Department_List');
        $icon = $permList['Department_List']['detail']['menu_parent_icon'];
        $items = DepartmentExtend::findAllDepartmentFromCache();

        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];

        $this->render('index', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
        ));
    }

    public function actionCreate($msg = '', $err = '') {
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('Department_Add');
            $icon = $permList['Department_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'name' => '',
            );
        } else {
            $permList = UtilSecurity::accessCheck('Department_Edit');
            $icon = $permList['Department_Edit']['detail']['menu_parent_icon'];
            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $item = $this->loadModel($id);
            $itemShow = $item->getAttributes();
        }
        
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/index',
            ],
            [
                'title' => $title,
            ],
        ];

        if ($request->isPostRequest) {
            if (empty($item)) {
                $item = new DepartmentExtend();
            }

            $item->name = trim($request->getPost('name'));

            if (!$item->save()) {
                $err = SYSTEM_ERROR;
            } else {
                $msg = 'Đã'.$msg1.$item->name.' thành công';
            }

            $this->redirect(array($this->controllerName . '/index', 'msg' => $msg, 'err' => $err));
        }

        $this->render('create', array(
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'itemShow' => $itemShow,
        ));
    }

    public function actionDelete() {
        UtilSecurity::accessCheck('Department_Delete');
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $model = $this->loadModel($sid);
        $msg = '';
        $err = '';

        if (empty($model->is_deleted)) {
            $model->is_deleted = 1;

            if (!$model->save()) {
                $err = SYSTEM_ERROR;
            } else {
                $msg = 'Đã xóa bộ phận '.$model->name.' thành công';
            }
        } else {
            $err = 'Bộ phận '.$model->name.' đã được xóa trước đó';
        }

        $this->redirect(array($this->controllerName . '/index', 'msg' => $msg, 'err' => $err));
    }
    
    public function actionCheckHaveUserGroup(){
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $check = UserGroupExtend::findOneUserGroupByCondition(['id_department' => $sid]);
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }

}