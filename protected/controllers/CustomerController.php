<?php

class CustomerController extends Controller {

    private $_actionPath = "Customer";
    private $_actionName = "Customer";
    public $title = 'Khách hàng';

    public function actionMain($msg = '', $err = ''){
        $permList = UtilSecurity::accessCheck('Customer_List_All');
        $icon = $permList['Customer_List_All']['detail']['menu_parent_icon'];
        $request = Yii::app()->request;
        
        $itemPerPage = LIMIT_ITEMS;
        $page = 1;
        if (!empty($request->getParam('page'))) {
            $page = $request->getParam('page');
        }
        $offset = $itemPerPage * ($page - 1);
        
        $filter = '';
        if (!empty($request->getParam('filter'))) {
            $filter = trim($request->getParam('filter'));
        }
        
        $sql = Yii::app()->db->createCommand()
                ->from('tbl_customer')
                ->where('is_deleted = 0');
        if (!empty($filter)) {
            if (is_numeric($filter)) {
                $custIdArr = Common::findCustomerByPhone($filter);
                $sql->andWhere(['IN', 'id', $custIdArr]);
            } else {
                $sql = $sql->andWhere('code LIKE "%'.$filter.'%" OR name LIKE "%'.$filter.'%"');
            }
        }
        
        $totalSql = clone $sql;
        $totalPage = $totalSql->select('count(*)')->queryScalar();
        $totalPage = ceil($totalPage / $itemPerPage);
        
        $items = $sql->select('id AS id_customer, code, name, email, address')
                ->order('created_at DESC')
                ->limit($itemPerPage)
                ->offset($offset)
                ->queryAll();
        
        $phones = Common::getPhonesOfCustomer($items);
        
        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];
        $searchBox = array(
            'action' => "",
            'placeholder' => SEARCH_TEXT,
            'value' => $filter,
        );

        //Render the View
        $this->render('main', array(
            'msg' => $msg,
            'err' => $err,
            'phones' => $phones,
            'breadcrumbs' => $breadcrumbs,
            'searchBox' => $searchBox,
            'totalPage' => $totalPage,
            'page' => $page,
            'offset' => $offset,
            'items' => $items,
            'icon' => $icon,
        ));
    }
    
    public function loadModel($id) {
        $model = CustomerExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionUpdate(){
        $permList = UtilSecurity::accessCheck('Customer_Edit');
        $icon = $permList['Customer_Edit']['detail']['menu_parent_icon'];
        $title = $this->editTitle;

        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $customer = $this->loadModel($id);
        $customerShow = Common::prepareCustomerInfo($customer);
        
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];

        $customerFields = $customerPhones = array();
        $isSubmit = (!empty($request->getParam('create'))) ? true : false;
        if ($isSubmit) {
            if (isset($_POST['Customer'])) {
                Common::prepareCustomerField($_POST['Customer'], $customerFields);
            }

            if (isset($_POST['CustomerPhone'])) {
                $customerPhones = $_POST['CustomerPhone'];
            }
            Common::beginTransaction();
            $customer = CustomerExtend::updateCustomer($customer, $customerFields);
            if ($customer === false) {
                Common::rollbackTransaction();
                $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
            }
            $customerPhoneModelExisted = $customerShow['customer_phone'];
            $customerPhoneModelExisted = Common::assoc_item($customerPhoneModelExisted, 'phone');
            $customerPhoneModels = CustomerPhoneExtend::prepareCustomerPhone($customerPhoneModelExisted, $customerPhones, $customer);
            if (!empty($customerPhoneModels) && !insertMultipleModel($customerPhoneModels)) {
                Common::rollbackTransaction();
                $this->redirect(array($this->controllerName . '/main', 'err' => SYSTEM_ERROR));
            }
            $phoneDeleteIds = array();
            if (!empty($customerPhoneModelExisted)) {
                foreach ($customerPhoneModelExisted as $p) {
                    $phoneDeleteIds[] = $p['id'];
                }
            }
            if (!empty($phoneDeleteIds)) {
                CustomerPhoneExtend::model()->deleteAll('id IN ('. implode(',', $phoneDeleteIds).') ');
            }
            Common::commitTransaction();
            $this->redirect(array($this->controllerName . '/main', 'msg' => 'Đã chỉnh sửa thông tin KH ' . $customer->name . ' thành công.'));
        }

        $this->render('update', array(
            'icon' => $icon,
            'customerShow' => $customerShow['customer_info'],
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
        ));
    }

    public function actionDelete($id){
        UtilSecurity::accessCheck('Customer_Delete');
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        $msg = $err = '';
        if (!$model->save()) {
            $err = SYSTEM_ERROR;
        } else {
            CustomerPhoneExtend::deleteCustomerPhonesByCustomerId($id);
            $msg = 'Đã xóa khách hàng '.$model->name.' thành công';
        }
        $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
    }
    
    public function actionCheckHaveOrder(){
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $check = OrderExtend::findOneOrdersByCondition(array('id_customer' => $id), array('limit' => 1));
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }
}