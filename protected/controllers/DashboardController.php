<?php

class DashboardController extends Controller {

    public $titleText = 'Tiêu đề';
    public $descriptionText = 'Mô tả';
    public $startDateText = 'Ngày';
    public $typeText = 'Loại lịch';
    public $endDateText = 'Ngày kết thúc';

    public function actionMain() {
        $user = Yii::app()->user;
        if ($user->isGuest) {
            Yii::app()->request->redirect(Yii::app()->request->baseUrl . '/site/login');
            Yii::app()->end();
        }

        $request = Yii::app()->request;
        $data = Common::prepareDataScheduleIndex($request);

        $this->render('main', $data);
    }

}
