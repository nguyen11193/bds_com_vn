<?php

class RegulationController extends Controller {
    
    public $title = 'Quản lý quy định';
    public $nameText = 'Tên quy định';
    public $categoryText = 'Hạng mục';
    public $contentText = 'Nội dung';
    public $disciplinedText = 'Xử lý kỷ luật';
    public $exampleText = 'Ví dụ dẫn chứng';

    public function loadModel($id) {
        $model = RegulationExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $uname = trim($request->getParam('uname'));
        $id = trim($request->getParam('id'));
        
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['name'] = $uname;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $department = RegulationExtend::findOneRegulationByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($department)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }

    public function actionIndex($msg = '', $err = '') {
        $request = Yii::app()->request;
        $user = Yii::app()->user;

        $viewOnly = false;
        $departments = [];
        if (!empty($request->getParam('viewOnly'))) {
            $viewOnly = true;
            $permList = UtilSecurity::accessCheck('Regulation_View');
            $icon = $permList['Regulation_View']['detail']['menu_parent_icon'];
        } else {
            $permList = UtilSecurity::accessCheck('Regulation_List');
            $icon = $permList['Regulation_List']['detail']['menu_parent_icon'];
            $departments = DepartmentExtend::findAllDepartmentFromCache();
        }
        
        $regulationCategoryId = 0;
        $regulationCategory = RegulationCategoryExtend::findAllRegulationCategoryFromCache();

        if (!empty($request->getParam('regulationCategoryId'))) {
            $regulationCategoryId = (int) trim($request->getParam('regulationCategoryId'));
        } else if (!empty($regulationCategory)) {
            $regulationCategoryId = $regulationCategory[0]['id'];
        }

        $departmentId = 0;

        $filter = '';
        if (!empty($request->getParam('filter'))) {
            $filter = trim($request->getParam('filter'));
        }

        foreach ($regulationCategory as $regulationCat) {
            if ($regulationCat['id'] != $regulationCategoryId) {
                continue;
            }

            if ($regulationCat['is_department'] == 1) {
                if ($viewOnly) {
                    $departmentId = $user->departmentId;
                } else if (!empty($request->getParam('departmentId'))) {
                    $departmentId = (int) trim($request->getParam('departmentId'));
                }
            }
            break;
        }

        $searchBox = array(
            'action' => "",
            'placeholder' => SEARCH_TEXT,
            'value' => $filter,
            'params' => [
                'regulationCategoryId' => $regulationCategoryId,
                'departmentId' => $departmentId,
            ],
        );

        $viewOnlyUrlParams = '';
        if ($viewOnly) {
            $searchBox['params']['viewOnly'] = 1;
            $viewOnlyUrlParams = '&viewOnly=1';
        }

        $items = RegulationExtend::getAllRegulationByParams([
            'id_regulation_category' => $regulationCategoryId,
            'id_department' => $departmentId,
            'filter' => $filter,
        ]);

        $itemsDetail = [];
        if (!empty($items)) {
            $regulationIds = array_column($items, 'id');
            $itemsDetail = RegulationContentExtend::getAllRegulationContent($regulationIds);
        }

        $title = !$viewOnly ? $this->title : 'Chính sách & Quy định';
        $breadcrumbs = [
            [
                'title' => $title,
            ],
        ];

        $this->render('index', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
            'regulationCategory' => $regulationCategory,
            'regulationCategoryId' => $regulationCategoryId,
            'departmentId' => $departmentId,
            'departments' => $departments,
            'searchBox' => $searchBox,
            'itemsDetail' => $itemsDetail,
            'showDepartment' => false,
            'viewOnly' => $viewOnly,
            'viewOnlyUrlParams' => $viewOnlyUrlParams,
            'title' => $title,
        ));
    }

    public function actionCreate($msg = '', $err = '') {
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        
        $regulationContents = [];
        $newRegulationContents = [];
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('Regulation_Add');
            $icon = $permList['Regulation_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'name' => '',
                'id_regulation_category' => 1,
                'id_department' => 1,
            );
        } else {
            $permList = UtilSecurity::accessCheck('Regulation_Edit');
            $icon = $permList['Regulation_Edit']['detail']['menu_parent_icon'];
            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $item = $this->loadModel($id);
            $itemShow = $item->getAttributes();
            $regulationContents = RegulationContentExtend::model()->findAll('id_regulation = ' . $id);
                
            foreach ($regulationContents as $regulationContent) {
                $newRegulationContents[] = $regulationContent->getAttributes();
            }
        }
        
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/index',
            ],
            [
                'title' => $title,
            ],
        ];

        if ($request->isPostRequest) {
            $regulationConntentData = trim($request->getPost('regulation_content_data'));
            $regulationConntentData = jcustom_decode($regulationConntentData);

            $regulationCategoryId = (int) trim($request->getPost('id_regulation_category'));
            $isDepartment = (int) trim($request->getPost('is_department'));
            $departmentId = (int) trim($request->getPost('id_department'));

            $name = trim($request->getPost('name'));

            if ($regulationCategoryId <= 0 
                    || ($isDepartment == 1 && $departmentId <= 0) 
                    || empty($regulationConntentData)
                    || empty($name)) {
                $this->redirect(array($this->controllerName . '/index', 'err' => 'Vui lòng nhập đầy đủ thông tin cần thiết của quy định'));
            }

            Common::beginTransaction();
            $logType = LOG_TYPE_EDIT;
            if (empty($item)) {
                $item = new RegulationExtend();
                $logType = LOG_TYPE_ADD;
            }

            $item->name = $name;
            $item->id_regulation_category = $regulationCategoryId;
            $item->id_department = $isDepartment == 1 ? $departmentId : 0;

            if (!$item->save()) {
                $err = SYSTEM_ERROR;
                Common::rollbackTransaction();
            } else {
                $regulationContents = Common::assoc_item($regulationContents, 'id');
                $contentModelsArr = [];
                foreach ($regulationConntentData as $contentData) {
                    $contentId = $contentData['id'];

                    if (empty($contentData['category'])
                            || empty($contentData['content'])) {
                        continue;
                    }

                    if (empty($regulationContents[$contentId])) {
                        $contentModel = new RegulationContentExtend();
                        $contentModel->id_regulation = $item->id;
                    } else {
                        $contentModel = $regulationContents[$contentId];
                        unset($regulationContents[$contentId]);
                    }

                    $contentModel->category = $contentData['category'];
                    $contentModel->content = $contentData['content'];
                    $contentModel->disciplined = !empty($contentData['disciplined']) ? $contentData['disciplined'] : '';
                    $contentModel->example = !empty($contentData['example']) ? $contentData['example'] : '';

                    $contentModelsArr[] = $contentModel;
                }

                if (empty($contentModelsArr)) {
                    $err = 'Vui lòng nhập thông tin của quy định';
                } else if (!insertMultipleModel($contentModelsArr)) {
                    $err = SYSTEM_ERROR;
                } else if (!$this->saveRegulationLog($item->id, $logType, $itemShow, $newRegulationContents)) {
                    $err = SYSTEM_ERROR;
                }

                if (!empty($err)) {
                    Common::rollbackTransaction();
                } else {
                    if (!empty($regulationContents)) {
                        RegulationContentExtend::model()->deleteAll('id IN ('. implode(',', array_keys($regulationContents)) .')');
                    }
                    $msg = 'Đã'.$msg1.$item->name.' thành công';
                    Common::commitTransaction();
                }
            }

            $this->redirect(array($this->controllerName . '/index', 'regulationCategoryId' => $regulationCategoryId,'msg' => $msg, 'err' => $err));
        }

        $regulationCategory = RegulationCategoryExtend::findAllRegulationCategoryFromCache();
        $departments = DepartmentExtend::findAllDepartmentFromCache();

        $this->render('create', array(
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'itemShow' => $itemShow,
            'regulationCategory' => $regulationCategory,
            'departments' => $departments,
            'regulationContents' => $newRegulationContents,
        ));
    }

    private function saveRegulationLog($regulationId, $logType, $item, $contentsData){
        $content = [
            'regulation' => $item,
            'regulation_content' => $contentsData,
        ];

        $log = new RegulationLogExtend();
        $log->id_regulation = $regulationId;
        $log->id_user = Yii::app()->user->id;
        $log->content = json_encode($content);
        $log->log_type = $logType;

        return $log->save();
    }

    public function actionDelete() {
        UtilSecurity::accessCheck('Regulation_Delete');
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $model = $this->loadModel($sid);
        $msg = '';
        $err = '';

        if (empty($model->is_deleted)) {
            $regulationId = $model->id;
            Common::beginTransaction();
            $model->is_deleted = 1;

            $regulationContent = RegulationContentExtend::getAllRegulationContent([$regulationId]);
            $regulationContent = $regulationContent[$regulationId];
            
            if (!$model->save()) {
                $err = SYSTEM_ERROR;
            } else if (!$this->saveRegulationLog($regulationId, LOG_TYPE_DELETE, $model->getAttributes(), $regulationContent)) {
                $err = SYSTEM_ERROR;
            }

            if (!empty($err)) {
                Common::rollbackTransaction();
            } else {
                RegulationContentExtend::model()->deleteAll('id_regulation = ' . $regulationId);
                $msg = 'Đã xóa '.$model->name.' thành công';
                Common::commitTransaction();
            }
        } else {
            $err = $model->name.' đã được xóa trước đó';
        }

        $this->redirect(array($this->controllerName . '/index', 'regulationCategoryId' => $model->id_regulation_category, 'msg' => $msg, 'err' => $err));
    }

}