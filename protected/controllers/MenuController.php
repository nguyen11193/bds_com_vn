<?php

class MenuController extends SystemController {
    
    private $_actionPath = "Menu";
    private $_actionName = "Menu";
    
    public function actionMain($msg = '', $err = ''){
        $menuParent = MenuParentExtend::getAllMenuParentFromCache();
        $menu = MenuExtend::findAllMenu();

    	//Render the View
        $this->render('main', array(
            'menuParent' => $menuParent,
            'menu' => $menu,
            'actionPath' => $this->_actionPath,
            'msg' => $msg,
            'err' => $err,
        ));
    }
    
    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $name = trim($request->getParam('name'));
        $id = trim($request->getParam('id'));
        $conditionToCompare = array();
        $conditionAddCondition = array();
        $conditionToCompare['name'] = $name;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }
        $records = MenuExtend::findOneMenuByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($records)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }
    
    public function actionCheckHavePermission(){
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $conditionToCompare = array();
        $conditionOthers = array();
        $conditionToCompare['id_menu'] = $sid;
        $conditionOthers['limit'] = 1;
        $check = PermissionExtend::findAllPermissionByCondition($conditionToCompare, $conditionOthers);
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate(){
        date_default_timezone_set(Yii::app()->params['timezone']);
        $request = Yii::app()->request;
        $data['success'] = 0;
        $data['msg'] = '';

        $model = new MenuExtend();
        $model->id_menu_parent = trim($request->getParam('idmenuparent'));
        $model->name = trim($request->getParam('name'));
        $model->url = trim($request->getParam('url'));
        $model->sort_by = trim($request->getParam('sortby'));
        if ($model->save()) {
            $data['success'] = 1;
            $data['msg'] = 'Đã thêm ' . $model->name . ' thành công';
        }
        echo CJSON::encode($data);
        return;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate(){
        date_default_timezone_set(Yii::app()->params['timezone']);
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        $item = $this->loadModel($id);
        $data['success'] = 0;
        $data['msg'] = '';

        $item->name = trim($request->getParam('name'));
        $item->url = trim($request->getParam('url'));
        $item->sort_by = trim($request->getParam('sortby'));
        if ($item->save()) {
            $data['success'] = 1;
            $data['msg'] = 'Đã sửa ' . $item->name . ' thành công';
        }
        echo CJSON::encode($data);
        return;
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $model = $this->loadModel($sid);
        if ($model->delete()) {
            $this->redirect(array($this->_actionPath . '/main', 'msg' => 'Đã xóa '.$model->name.' thành công'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Menu the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Menu::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
