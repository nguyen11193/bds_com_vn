<?php

class RegulationLogController extends Controller {
    
    public $title = 'Lịch sử thay đổi quy định';

    public function actionIndex($msg = '', $err = '') {
        $permList = UtilSecurity::accessCheck('Regulation_Log_List');
        $icon = $permList['Regulation_Log_List']['detail']['menu_parent_icon'];
        
        $request = Yii::app()->request;
        $itemPerPage = LIMIT_ITEMS;
        $page = 1;
        if (!empty($request->getParam('page'))) {
            $page = $request->getParam('page');
        }
        $offset = $itemPerPage * ($page - 1);

        $regulationId = 0;
        if (!empty($request->getParam('regulationId'))) {
            $regulationId = (int) trim($request->getParam('regulationId'));
        }
        
        $sql = Yii::app()->db->createCommand()->from('tbl_regulation_log');
        if ($regulationId > 0) {
            $sql = $sql->where('id_regulation = ' . $regulationId);
        }

        $totalSql = clone $sql;
        $totalPage = $totalSql->select('count(*)')->queryScalar();
        $totalPage = ceil($totalPage / $itemPerPage);
        
        $items = $sql->select('*')
                ->order('created_at DESC')
                ->limit($itemPerPage)
                ->offset($offset)
                ->queryAll();

        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];

        $regulations = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tbl_regulation')
                        ->order('name')
                        ->queryAll();
        $regulations = Common::assoc_item($regulations, 'id');

        $this->render('index', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
            'regulationId' => $regulationId,
            'regulations' => $regulations,
            'totalPage' => $totalPage,
            'page' => $page,
        ));
    }

}