<?php

class ScheduleCalendarTypeController extends Controller {
    
    public $title = 'Loại lịch';
    public $nameText = 'Tên loại lịch';
    public $colorText = 'Màu';

    public function loadModel($id) {
        $model = ScheduleCalendarTypeExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $uname = trim($request->getParam('uname'));
        $id = trim($request->getParam('id'));
        
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['name'] = $uname;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $department = ScheduleCalendarTypeExtend::findOneScheduleCalendarTypeByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($department)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }

    public function actionIndex($msg = '', $err = '') {
        $permList = UtilSecurity::accessCheck('Schedule_Calendar_Type_List');
        $icon = $permList['Schedule_Calendar_Type_List']['detail']['menu_parent_icon'];
        $items = ScheduleCalendarTypeExtend::findAllScheduleCalendarTypeFromCache();

        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];

        $this->render('index', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
        ));
    }

    public function actionCreate($msg = '', $err = '') {
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('Schedule_Calendar_Type_Add');
            $icon = $permList['Schedule_Calendar_Type_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'name' => '',
                'color' => SCHEDULE_CALENDAR_DEFAULT_COLOR,
            );
        } else {
            $permList = UtilSecurity::accessCheck('Schedule_Calendar_Type_Edit');
            $icon = $permList['Schedule_Calendar_Type_Edit']['detail']['menu_parent_icon'];
            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $item = $this->loadModel($id);
            $itemShow = $item->getAttributes();

            if (empty($itemShow['color'])) {
                $itemShow['color'] = SCHEDULE_CALENDAR_DEFAULT_COLOR;
            }
        }
        
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/index',
            ],
            [
                'title' => $title,
            ],
        ];

        if ($request->isPostRequest) {
            if (empty($item)) {
                $item = new ScheduleCalendarTypeExtend();
            }

            $item->name = trim($request->getPost('name'));
            $item->color = trim($request->getPost('default_color'));

            if (!$item->save()) {
                $err = SYSTEM_ERROR;
            } else {
                $msg = 'Đã'.$msg1.$item->name.' thành công';
            }

            $this->redirect(array($this->controllerName . '/index', 'msg' => $msg, 'err' => $err));
        }

        $this->render('create', array(
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'itemShow' => $itemShow,
        ));
    }

    public function actionDelete() {
        UtilSecurity::accessCheck('Schedule_Calendar_Type_Delete');
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));

        if ($sid == SCHEDULE_CALENDAR_TYPE_TASK) {
            $this->redirect(array($this->controllerName . '/index', 'err' => 'Bạn không được xóa loại lịch này'));
        }

        $model = $this->loadModel($sid);

        if (empty($model->is_deleted)) {
            $model->is_deleted = 1;
            $msg = $err = '';

            if (!$model->save()) {
                $err = SYSTEM_ERROR;
            } else {
                $msg = 'Đã xóa '.$model->name.' thành công';
            }
        } else {
            $err = $model->name.' đã được xóa trước đó';
        }

        $this->redirect(array($this->controllerName . '/index', 'msg' => $msg, 'err' => $err));
    }
    
    public function actionCheckHaveScheduleCalendar(){
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $check = ScheduleCalendarExtend::findOneScheduleCalendarByCondition(['type' => $sid]);
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }

}