<?php

class UserGroupController extends Controller {
    
    public $title = 'Chức vụ';
    public $nameText = 'Tên chức vụ';
    public $descText = 'Mô tả';
    public $departmentText = 'Bộ phận';

    public function loadModel($id) {
        $model = UserGroupExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $uname = trim($request->getParam('uname'));
        $id = trim($request->getParam('id'));
        $departmentId = trim($request->getParam('id_department'));
        
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['name'] = $uname;
        $conditionToCompare['id_department'] = $departmentId;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $userGroup = UserGroupExtend::findOneUserGroupByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($userGroup)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }
    
    private function getMenus(){
        $menus = MenuExtend::findAllMenu();
        $permissions = PermissionExtend::findAllPermission();
        $permissions = Common::assoc_array($permissions, 'id_menu');
        foreach ($menus as $k => $v) {
            $menus[$k]['permissions'] = (!empty($permissions[$v['id']])) ? $permissions[$v['id']] : array();
        }
        return $menus;
    }

    public function actionView() {
        $permList = UtilSecurity::accessCheck('User_Group_View');
        $icon = $permList['User_Group_View']['detail']['menu_parent_icon'];
        $request = Yii::app()->request;
        $sid = trim($request->getParam('id'));
        //Load the model
        $model = $this->loadModel($sid);
        // users
        $users = UserAccountExtend::sqlfindAllUserAccount([$sid]);
        $menus = $this->getMenus();

        $permissionMaps = GroupPermissionMapExtend::getAllGroupPermissionMap($sid);
        $permissionMaps = Common::assoc_item($permissionMaps, 'id_permission');
        $title = 'Xem chi tiết';
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];
        $this->render('view', array(
            'model' => $model,
            'users' => $users,
            'menus' => $menus,
            'permissionMaps' => $permissionMaps,
            'breadcrumbs' => $breadcrumbs,
            'icon' => $icon,
            'title' => $title,
        ));
    }
    
    public function actionMain($msg = '', $err = '') {
        $permList = UtilSecurity::accessCheck('User_Group_List');
        $icon = $permList['User_Group_List']['detail']['menu_parent_icon'];
        $userId = Yii::app()->user->id;
        $items = Yii::app()->db->createCommand()
                ->select('g.id, g.name, g.desc, d.name AS department_name')
                ->from('tbl_user_group g')
                ->join('tbl_department d', 'g.id_department = d.id')
                ->where('g.is_deleted = 0');
        if ($userId != USER_WEB_ID) {
            $items = $items->andWhere('g.id != ' . USER_GROUP_WEB_ID);
        }
        $items = $items->queryAll();
        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];

        $this->render('main', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'err' => $err,
            'breadcrumbs' => $breadcrumbs,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($msg = '', $err = '') {
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));
        
        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('User_Group_Add');
            $icon = $permList['User_Group_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $title = $this->addTitle;
            $itemShow = array(
                'id' => 0,
                'name' => '',
                'desc' => '',
                'id_department' => '',
            );
        } else {
            $permList = UtilSecurity::accessCheck('User_Group_Edit');
            $icon = $permList['User_Group_Edit']['detail']['menu_parent_icon'];

            $this->denyAccessWebGroup($id);

            $msg1 = ' sửa ';
            $title = $this->editTitle;
            $item = $this->loadModel($id);
            $itemShow = $item->getAttributes();
        }
        $menus = $this->getMenus();
        
        $breadcrumbs = [
            [
                'title' => $this->title,
                'link' => '/'.$this->controllerName.'/main',
            ],
            [
                'title' => $title,
            ],
        ];

        if ($request->isPostRequest) {
            $permissionMapModels = array();
            if (empty($item)) {
                $item = new UserGroupExtend();
                $item->is_deleted = 0;
            }
            $item->name = trim($request->getPost('groupname'));
            $item->desc = trim($request->getPost('groupdesc'));
            $item->id_department = (int) trim($request->getPost('id_department'));
            if ($item->save()) {
                if (!empty($id)) {
                    GroupPermissionMapExtend::model()->deleteAll('id_user_group = ' . $id);
                }
                foreach ($menus as $menu) {
                    foreach ($menu['permissions'] as $v1) {
                        if($request->getParam('perm_' . $v1['id']) == 1){
                            $permissionMap = new GroupPermissionMapExtend();
                            $permissionMap->id_permission = $v1['id'];
                            $permissionMap->id_user_group = $item->id;
                            $permissionMapModels[] = $permissionMap;
                        }
                    }
                }
                if (!empty($permissionMapModels)) {
                    if (!insertMultipleModel($permissionMapModels)) {
                        $err = SYSTEM_ERROR;
                    } 
                    /* else {
                        if (!empty($id)) {
                            $users = UserAccountExtend::sqlfindAllUserAccount([$id]);
                            foreach ($users as $user) {
                                UserAccountExtend::deleteUserPermissionCache($user['id']);
                            }
                        }
                    } */
                }
            } else {
                $err = SYSTEM_ERROR;
            }
            if (empty($err)) {
                $msg = 'Đã'.$msg1.$item->name.' thành công';
            }
            $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
        } else {
            $permissionMaps = array();
            if (!empty($id)) {
                $permissionMaps = GroupPermissionMapExtend::getAllGroupPermissionMap($id);
                $permissionMaps = Common::assoc_item($permissionMaps, 'id_permission');
            }

            $departments = DepartmentExtend::findAllDepartmentFromCache();
        }

        $this->render('create', array(
            'menus' => $menus,
            'permissionMaps' => $permissionMaps,
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
            'title' => $title,
            'itemShow' => $itemShow,
            'departments' => $departments,
        ));
    }

    protected function denyAccessWebGroup($groupId){
        if ($groupId == USER_GROUP_WEB_ID && Yii::app()->user->id != USER_WEB_ID) {
            redirectNoPermission();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {
        UtilSecurity::accessCheck('User_Group_Delete');
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));

        $this->denyAccessWebGroup($sid);

        $model = $this->loadModel($sid);
        $model->is_deleted = 1;
        $msg = $err = '';
        if (!$model->save()) {
            $err = SYSTEM_ERROR;
        } else {
            $msg = 'Đã xóa nhóm '.$model->name.' thành công';
            GroupPermissionMapExtend::model()->deleteAll('id_user_group = ' . $model->id);
        }
        $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
    }
    
    public function actionCheckHaveUserAccount(){
        $request = Yii::app()->request;
        $sid = trim($request->getParam('sid'));
        $check = UserAccountExtend::sqlfindAllUserAccount([$sid]);
        
        if (!empty($check)) {
            $data['check'] = "yes";
        } else {
            $data['check'] = "no";
        }
        
        echo CJSON::encode($data);
        return;
    }

}
