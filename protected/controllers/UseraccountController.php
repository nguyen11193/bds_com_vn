<?php

class UseraccountController extends Controller {

    public $title = 'Người dùng';

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = UserAccountExtend::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page is not found.');
        return $model;
    }

    public function actionMain($msg = "") {
        $permList = UtilSecurity::accessCheck('User_List');
        $icon = $permList['User_List']['detail']['menu_parent_icon'];
        
        $items = UserAccountExtend::sqlfindAllUserAccount();
        $breadcrumbs = [
            [
                'title' => $this->title,
            ],
        ];

        $this->render('main', array(
            'items' => $items,
            'msg' => $msg,
            'icon' => $icon,
            'breadcrumbs' => $breadcrumbs,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($msg = '', $err = '') {
        $request = Yii::app()->request;
        $isProfile = (!empty($request->getParam('profile'))) ? true : false;
        $id = ($isProfile) ? Yii::app()->user->id : trim($request->getParam('id'));
        //$msg = $err = '';
        $breadcrumbs = $breadcrumbLast = array();

        if (empty($id)) {
            $permList = UtilSecurity::accessCheck('User_Add');
            $icon = $permList['User_Add']['detail']['menu_parent_icon'];
            $item = null;
            $msg1 = ' thêm ';
            $breadcrumbLast['title'] = $this->addTitle;
        } else {
            if ($isProfile) {
                $permList = UtilSecurity::accessCheck('User_Profile');
                $icon = $permList['User_Profile']['detail']['menu_parent_icon'];
                $breadcrumbLast['title'] = 'Thông tin cá nhân';
            } else {
                $permList = UtilSecurity::accessCheck('User_Edit');
                $icon = $permList['User_Edit']['detail']['menu_parent_icon'];
                $msg1 = ' sửa ';
                $breadcrumbLast['title'] = $this->editTitle;
            }

            $this->denyAccessWebUser($id);

            $item = $this->loadModel($id);
        }

        if ($request->isPostRequest) {
            if (empty($item)) {
                $item = new UserAccountExtend();
                $saltKey = "_" . rand(100000, 900000);
                $item->salt = $saltKey;
                $item->is_deleted = 0;
            }

            $item->user_name = trim($request->getPost('username'));
            $item->full_name = trim($request->getPost('fullname'));

            if (!empty($request->getPost('password'))) {
                $item->password = CPasswordHelper::hashPassword(trim($request->getPost('password')));
            }

            $item->email = trim($request->getPost('email'));

            if ($isProfile) {
                if (!empty($_FILES["imgItem"]["name"])) {
                    $targetDir = USER_PROFILE_PATH;
                    $targetFile = $targetDir . basename($id . "_" . $_FILES["imgItem"]["name"]);
                    $fileType = pathinfo($targetFile, PATHINFO_EXTENSION);
                    $fileName = basename($id . "." . $fileType);
                    $fileNameThumb = basename($id . "_thumb." . $fileType);
                    $targetFileFull = $targetDir . $fileName;
                    $targetFileThumb = $targetDir . $fileNameThumb;

                    if (move_uploaded_file($_FILES["imgItem"]["tmp_name"], $targetFile)) {
                        $full = new EasyImage($targetFile);
                        $full->save($targetFileFull);
                        $item->image = $fileName;

                        $thumb = new EasyImage($targetFile);
                        $thumb->resize(128, 128);
                        $thumb->save($targetFileThumb);
                        $item->thumbnail = $fileNameThumb;
                        chmod($targetFileFull, 0777);
                        chmod($targetFileThumb, 0777);
                    }
                    @unlink($targetFile);
                }
            } else {
                $item->user_group_id = (int) trim($request->getPost('user_groups_id'));
                $item->id_department = (int) trim($request->getPost('id_department'));
            }

            if (!$item->save()) {
                $err = SYSTEM_ERROR;
            } else {
                $msg = (!$isProfile) ? 'Đã'.$msg1.$item->user_name.' thành công' : 'Thông tin cá nhân của bạn đã được cập nhật.';
            }

            if ($isProfile) {
                $this->redirect(array($this->controllerName . '/' . $this->action->Id, 'profile' => 1, 'msg' => $msg, 'err' => $err));
            } else {
                $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
            }
        } else {
            $groups = UserGroupExtend::findAllUserGroups();
            $groupsByDepartment = Common::assoc_array($groups, 'id_department');
            $groups = Common::assoc_item($groups, 'id');

            $departments = DepartmentExtend::findAllDepartmentFromCache();
            $departments = Common::assoc_item($departments, 'id');

            if (!$isProfile) {
                $breadcrumbs[] = array(
                    'title' => $this->title,
                    'link' => '/'.$this->controllerName.'/main',
                );
            }

            $breadcrumbs[] = $breadcrumbLast;

            $itemShow = array(
                'id' => 0,
                'user_name' => '',
                'full_name' => '',
                'email' => '',
                'image' => '',
                'user_group_id' => '',
                'id_department' => '',
            );
            if (!empty($item)) {
                $itemShow = $item->getAttributes();
            }
        }

        $this->render('create', array(
            'groups' => $groups,
            'icon' => $icon,
            'itemShow' => $itemShow,
            'isProfile' => $isProfile,
            'breadcrumbs' => $breadcrumbs,
            'breadcrumbLast' => $breadcrumbLast,
            'msg' => $msg,
            'err' => $err,
            'departments' => $departments,
            'groupsByDepartment' => $groupsByDepartment,
        ));
    }

    /**
     * Actual Delete Function
     */
    public function actionDelete() {
        UtilSecurity::accessCheck('User_Delete');
        $userID = Yii::app()->user->id;
        $request = Yii::app()->request;
        $id = trim($request->getParam('id'));

        $this->denyAccessWebUser($id);

        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        if ($model->save()) {
            if ($userID == $model->id) {
                Yii::app()->user->logout();
                $this->redirect(Yii::app()->homeUrl);
            }
            $msg = 'Đã xóa '.$model->user_name.' thành công';
        } else {
            $err = SYSTEM_ERROR;
        }
        $this->redirect(array($this->controllerName . '/main', 'msg' => $msg, 'err' => $err));
    }

    protected function denyAccessWebUser($userId){
        if ($userId == USER_WEB_ID && Yii::app()->user->id != USER_WEB_ID) {
            redirectNoPermission();
        }
    }

    /**
     * AJAX Function to check if username is in use
     */
    public function actionCheckDuplicity() {
        $request = Yii::app()->request;
        $uname = trim($request->getParam('uname'));
        $id = trim($request->getParam('id'));
        $conditionToCompare = $conditionAddCondition = array();
        $conditionToCompare['user_name'] = $uname;
        if (!empty($id)) {
            $conditionAddCondition[] = 'id != "'.$id.'" ';
        }

        $user = UserAccountExtend::findOneUserAccountByCondition($conditionToCompare, $conditionOthers = array(), $conditionAddCondition);

        if (!empty($user)) {
            $data['errName'] = "yes";
        } else {
            $data['errName'] = "no";
        }

        echo CJSON::encode($data);
        return;
    }

}
