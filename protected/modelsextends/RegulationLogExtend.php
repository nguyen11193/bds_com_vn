<?php

class RegulationLogExtend extends RegulationLog{
    
    public static function findOneRegulationLogByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

