<?php

class RegulationContentExtend extends RegulationContent{

    public static function getAllRegulationContent($regulationIds = []){
        $sql = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_regulation_content');
                
        if (!empty($regulationIds)) {
            $sql = $sql->where(['IN', 'id_regulation', $regulationIds]);
        }

        $data = $sql->queryAll();
        return Common::assoc_array($data, 'id_regulation');
    }
    
    public static function findOneRegulationContentByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

