<?php

class UserAccountExtend extends UserAccount{
    
    public static function findOneUserAccountByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
    public static function sqlfindAllUserAccount($userGroupsId = array()){
        $userId = Yii::app()->user->id;
        $sqlQuery = "SELECT u.id, u.full_name, u.user_name, d.name AS department_name, g.name as group_name, DATE_FORMAT(u.date_last_login, '%d %b %Y, %h:%i %p') AS date_last_login, u.email, u.full_name AS column_name";
        $sqlQuery .= " FROM {{user_account}} u INNER JOIN {{user_group}} g ON u.user_group_id = g.id";
        $sqlQuery .= " INNER JOIN {{department}} d ON u.id_department = d.id";
        $sqlQuery .= " WHERE u.is_deleted = 0";
        if (!empty($userGroupsId)) {
            $sqlQuery .= " AND u.user_group_id IN (". implode(',', $userGroupsId).")";
        }
        if ($userId != USER_WEB_ID) {
            $sqlQuery .= " AND u.id != " . USER_WEB_ID;
        }
        $sqlQuery .= " ORDER BY u.full_name";
        $result = Yii::app()->db->createCommand($sqlQuery)->queryAll();
        return $result;
    }
    
    public static function deleteUserPermissionCache($userId){
        deleteCache(USER_PERMISSION_CACHE_KEY_PREFIX . $userId);
    }
}
