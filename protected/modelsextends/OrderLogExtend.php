<?php

class OrderLogExtend extends OrderLog{
    
    public static function saveNewOrderLog($orderId, $userId, $comment, $statusBefore, $statusAfter){
        $model = new OrderLogExtend();
        $model->id_order = $orderId;
        $model->id_user = $userId;
        $model->comment = $comment;
        $model->status_before = $statusBefore;
        $model->status_after = $statusAfter;
        return $model->save();
    }
}

