<?php

class AscSystemExtend extends AscSystem{
    
    public static function findAllAscSystemModel($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
    public static function findOneAscSystemByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
    public static function autoIncrementValcount($model){
        $model->valcount = $model->valcount + 1;
        return $model->save();
    }
}
