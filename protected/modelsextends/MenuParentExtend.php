<?php

class MenuParentExtend extends MenuParent {

    public static function getAllMenuParentFromCache() {
        return getCache(MENU_PARENT_CACHE_KEY_PREFIX, function() {
            $sql = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_menu_parent')
                ->order('sort_by')
                ->queryAll();
            return $sql;
        });
    }
    
    public static function findAllMenuParentByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        $conditionOthers['order'] = 'sort_by';
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
    
    public static function findOneMenuParentByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
}

