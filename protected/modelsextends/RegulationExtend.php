<?php

class RegulationExtend extends Regulation{
    
    public static function findAllRegulationFromCache(){
        return getCache(REGULATION_CACHE_KEY_PREFIX, function() {
            return Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_regulation')
                ->where('is_deleted = 0')
                ->order('name')
                ->queryAll();
        });
    }
    
    public static function findOneRegulationByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
    public static function getAllRegulationByParams($params = []){
        $dataResult = [];
        $regulationCategoryId = isset($params['id_regulation_category']) ? (int) $params['id_regulation_category'] : 0;
        $departmentId = isset($params['id_department']) ? (int) $params['id_department'] : 0;
        $filter = isset($params['filter']) ? $params['filter'] : '';

        if ($regulationCategoryId == 0 && $departmentId == 0) {
            return $dataResult;
        }

        if (!empty($filter)) {
            $data = Yii::app()->db->createCommand()
                        ->select('*')
                        ->from('tbl_regulation')
                        ->where('is_deleted = 0')
                        ->andWhere('name LIKE :name', [':name' => '%' . $filter . '%']);
            if ($regulationCategoryId > 0) {
                $data = $data->andWhere('id_regulation_category = ' . $regulationCategoryId);
            }

            if ($departmentId > 0) {
                $data = $data->andWhere('id_department = ' . $departmentId);
            }

            return $data->queryAll();
        }

        $data = self::findAllRegulationFromCache();

        if ($regulationCategoryId > 0) {
            $data = Common::assoc_array($data, 'id_regulation_category');
            
            if (!empty($data[$regulationCategoryId])) {
                if ($departmentId > 0) {
                    $data = Common::assoc_array($data[$regulationCategoryId], 'id_department');
                    
                    if (!empty($data[$departmentId])) {
                        $dataResult = $data[$departmentId];
                    }
                } else {
                    $dataResult = $data[$regulationCategoryId];
                }
            }
        } else if ($departmentId > 0) {
            $data = Common::assoc_array($data, 'id_department');

            if (!empty($data[$departmentId])) {
                $dataResult = $data[$departmentId];
            }
        }

        return $dataResult;
    }
}

