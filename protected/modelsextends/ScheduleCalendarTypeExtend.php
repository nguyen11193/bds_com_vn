<?php

class ScheduleCalendarTypeExtend extends ScheduleCalendarType{
    
    public static function findAllScheduleCalendarTypeFromCache(){
        return getCache(SCHEDULE_CALENDAR_TYPE_CACHE_KEY_PREFIX, function() {
            return Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_schedule_calendar_type')
                ->where('is_deleted = 0')
                ->order('id')
                ->queryAll();
        });
    }
    
    public static function findOneScheduleCalendarTypeByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

