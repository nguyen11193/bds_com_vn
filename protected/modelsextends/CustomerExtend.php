<?php

class CustomerExtend extends Customer{

    public static function insertCustomer($customerCode, $fields = array()){
        $model = new CustomerExtend();
        if (!self::saveInfo($model, $fields)) {
            return false;
        }
        if (!AscSystemExtend::autoIncrementValcount($customerCode)) {
            return false;
        }
        return $model;
    }
    
    public static function updateCustomer($model, $fields = array()){
        if (!self::saveInfo($model, $fields)) {
            return false;
        }
        return $model;
    }
}

