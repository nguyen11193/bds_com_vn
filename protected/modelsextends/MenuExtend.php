<?php

class MenuExtend extends Menu{

    public static function findAllMenu(){
        return getCache(MENU_CACHE_KEY_PREFIX, function() {
            $sql = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_menu')
                ->order('sort_by')
                ->queryAll();
            return $sql;
        });
    }
    
    public static function findAllMenuByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
    
    public static function findOneMenuByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
}

