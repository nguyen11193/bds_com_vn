<?php

class CustomerPhoneExtend extends CustomerPhone{
    
    public static function findOneCustomerPhonesByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array(), $conditionAddNotInCondition = array()){
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition, $conditionAddNotInCondition);
    }

    public static function deleteCustomerPhonesByCustomerId($customerId){
        self::model()->deleteAll('id_customer = ' . $customerId);
    }
    
    public static function getAllPhones($custIds = array()){
        $sql = Yii::app()->db->createCommand()
                ->select('phone, id_customer')
                ->from('tbl_customer_phone');
        if (!empty($custIds)) {
            $sql = $sql->where(['IN', 'id_customer', $custIds]);
        }
        return $sql->queryAll();
    }

    public static function findCustomerByPhoneSearch($type, $phone, $isGroupBy = false){
        $sql = Yii::app()->db->createCommand()
                ->select('cp.id_customer, cp.phone, c.name')
                ->from('tbl_customer_phone cp')
                ->join('tbl_customer c', 'cp.id_customer = c.id')
                ->where('c.is_deleted = 0');
        if ($type == OPERATOR_LIKE) {
            $sql = $sql->andWhere('cp.phone LIKE :phone', [':phone' => "%$phone%"]);
        } else {
            $sql = $sql->andWhere('cp.phone = :phone', [':phone' => $phone]);
        }
        if ($isGroupBy) {
            $sql = $sql->group('cp.id_customer')->order('cp.phone');
        }
        return $sql->queryAll();
    }
    
    public static function prepareCustomerPhone(&$modelExisted, $customerPhones, $customer){
        $customerPhoneModelArr = array();
        if (!empty($customerPhones) && !empty($customer)) {
            foreach ($customerPhones as $phone) {
                $ph = trim($phone);
                if (!empty($ph)) {
                    if (empty($modelExisted[$ph])) {
                        $modelPhone = new CustomerPhoneExtend();
                        $modelPhone->id_customer = $customer->id;
                        $modelPhone->phone = $ph;
                    } else {
                        $modelPhone = $modelExisted[$ph];
                        unset($modelExisted[$ph]);
                    }
                    $customerPhoneModelArr[] = $modelPhone;
                }
            }
        }
        return $customerPhoneModelArr;
    }
}

