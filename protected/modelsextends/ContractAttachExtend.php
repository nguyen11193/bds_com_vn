<?php

class ContractAttachExtend extends ContractAttach{
    
    public static function getAllContractAttach($orderId) {
        return Yii::app()->db->createCommand()
                ->select('id, type, file_name')
                ->from('tbl_contract_attach')
                ->where('id_order = :orderId', [':orderId' => $orderId])
                ->queryAll();
    }
}

