<?php

class ProductExtend extends Product{

    public static function findOneProductByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array(), $conditionAddNotInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition, $conditionAddNotInCondition);
    }
    
    public static function getAllProductsFromCacheByProject($projectId) {
        return getCache(PRODUCT_CACHE_KEY_PREFIX . $projectId, function() use($projectId) {
            return Yii::app()->db->createCommand()
                    ->select('id, id_project, name, acreage, price, status')
                    ->from('tbl_product')
                    ->where('id_project = :productId', [':productId' => $projectId])
                    ->andWhere('is_deleted = 0')
                    ->order('name')
                    ->queryAll();
        });
    }

}
