<?php

class ProjectExtend extends Project{
    
    public static function sqlFindAllProjects(){
        $sql = Yii::app()->db->createCommand()
                ->select('id, code, name, address, status')
                ->from('tbl_project')
                ->where('is_deleted = 0')
                ->order('created_at DESC');
        return $sql;
    }
    
    public static function getAllProjectsFromCache() {
        return getCache(PROJECT_CACHE_KEY_PREFIX, function() {
            $sql = self::sqlFindAllProjects();
            $sql = $sql->andWhere('status = 1')->queryAll();
            return $sql;
        });
    }
    
    public static function findOneProjectByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array(), $conditionAddNotInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition, $conditionAddNotInCondition);
    }

}
