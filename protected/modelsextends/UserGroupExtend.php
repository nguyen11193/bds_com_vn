<?php

class UserGroupExtend extends UserGroup{
    
    public static function findAllUserGroups(){
        return getCache(USER_GROUP_CACHE_KEY_PREFIX, function() {
            return Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_user_group')
                ->where('is_deleted = 0')
                ->order('name')
                ->queryAll();
        });
    }
    
    public static function findOneUserGroupByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

