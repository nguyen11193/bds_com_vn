<?php

class RegulationCategoryExtend extends RegulationCategory{
    
    public static function findAllRegulationCategoryFromCache(){
        return getCache(REGULATION_CATEGORY_CACHE_KEY_PREFIX, function() {
            return Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_regulation_category')
                ->where('is_deleted = 0')
                ->order('name')
                ->queryAll();
        });
    }
    
    public static function findOneRegulationCategoryByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

