<?php

class DepartmentExtend extends Department{
    
    public static function findAllDepartmentFromCache(){
        return getCache(DEPARTMENT_CACHE_KEY_PREFIX, function() {
            return Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_department')
                ->where('is_deleted = 0')
                ->order('name')
                ->queryAll();
        });
    }
    
    public static function findOneDepartmentByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

