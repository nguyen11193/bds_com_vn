<?php

class PermissionExtend extends Permission{

    public static function findAllPermission(){
        return getCache(PERMISSION_CACHE_KEY_PREFIX, function() {
            $sql = Yii::app()->db->createCommand()
                ->select('*')
                ->from('tbl_permission')
                ->order('sort_by')
                ->queryAll();
            return $sql;
        });
    }
    
    public static function findAllPermissionByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
    
    public static function findOnePermissionByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
}

