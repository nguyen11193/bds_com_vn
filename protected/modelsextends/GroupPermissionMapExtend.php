<?php

class GroupPermissionMapExtend extends GroupPermissionMap{
    
    public static function getAllGroupPermissionMap($groupId = 0){
        $query = Yii::app()->db->createCommand()
                ->select('id, id_user_group, id_permission')
                ->from('tbl_group_permission_map');
        if (!empty($groupId)) {
            $query = $query->where('id_user_group = :groupId', [':groupId' => $groupId]);
        }
        $query = $query->queryAll();
        return $query;
    }

    public static function findOneByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
    
    public static function findAllByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array()){
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition);
    }
}

