<?php

class ScheduleCalendarActionHistoryExtend extends ScheduleCalendarActionHistory{
    
    public static function findOneScheduleCalendarActionHistoryByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
}

