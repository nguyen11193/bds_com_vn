<?php

class OrderExtend extends Order {
    
    public static function insertOrder($code, $fields = array()){
        $model = new OrderExtend();
        if (!self::saveInfo($model, $fields)) {
            return false;
        }
        if (!AscSystemExtend::autoIncrementValcount($code)) {
            return false;
        }
        return $model;
    }
    
    public static function updateOrder($model, $fields = array()){
        if (!self::saveInfo($model, $fields)) {
            return false;
        }
        return $model;
    }
    
    public static function findAllOrdersByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ALL, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition);
    }
    
    public static function findOneOrdersByCondition($conditionToCompare = array(), $conditionOthers = array(), $conditionAddCondition = array(), $conditionAddInCondition = array(), $conditionAddNotInCondition = array()){
        $conditionToCompare['is_deleted'] = 0;
        return self::findByCondition(self::model(), FIND_ONE, $conditionToCompare, $conditionOthers, $conditionAddCondition, $conditionAddInCondition, $conditionAddNotInCondition);
    }
}

