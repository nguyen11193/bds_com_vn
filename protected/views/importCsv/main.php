<?php
$breadcrumbs = [
    [
        'title' => $this->title,
    ],
];
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title
));
?>
<?php if (!empty($msg)): ?>
    <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold"><?php echo $msg ?></span>
    </div>
<?php endif; ?>
<?php if (!empty($err)): ?>
    <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <span class="text-semibold"><?php echo $err ?>
    </div>
<?php endif; ?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-4">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/ImportCsv/importBatch'); ?>">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="text-center">Import Lô Hàng</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/ImportCsv/importSupplier'); ?>">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="text-center">Import Nhà cung cấp</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/ImportCsv/importProductShortName'); ?>">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="text-center">Import Short name của SP</h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4">
                <a href="<?php echo Yii::app()->createAbsoluteUrl('/ImportCsv/importTest'); ?>">
                    <div class="panel bg-blue-400">
                        <div class="panel-body">
                            <h3 class="text-center">Import Test</h3>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

