<?php
$breadcrumbs = [
    [
        'title' => $this->title,
        'link' => '/'.$this->controllerName.'/main',
    ],
    [
        'title' => $title,
    ],
];
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
));
$count = count($outPut);
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
    </div>
    <div class="panel-body">
        <?php if ($encoding == 'UTF-8'): ?>
        <div class="alert alert-info">
            Confirm that the data is correct, then click on the "Import Data" button. Note that Importing may take a while.
        </div>
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal">
            <input type="hidden" name="filename" value="<?php echo $fileName; ?>" />
            <input type="hidden" name="update" value="1" />
            <div class="table-responsive">
                <table id="tablelist" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <?php foreach ($header as $value): ?>
                            <th><?php echo $value ?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php for ($x = 1; $x < $count; $x++): ?>
                        <tr>
                            <td><?php echo $x; ?></td>
                            <td><?php echo $outPut[$x][0]; ?></td>
                            <td><?php echo $outPut[$x][1]; ?></td>
                            <?php if ($action == 'importBatch'): ?>
                            <td><?php echo $outPut[$x][2]; ?></td>
                            <td><?php echo $outPut[$x][3]; ?></td>
                            <td><?php echo $outPut[$x][4]; ?></td>
                            <td><?php echo $outPut[$x][5]; ?></td>
                            <td><?php echo $outPut[$x][6]; ?></td>
                            <td><?php echo $outPut[$x][7]; ?></td>
                            <td><?php echo $outPut[$x][8]; ?></td>
                            <td><?php echo $outPut[$x][9]; ?></td>
                            <td><?php echo $outPut[$x][10]; ?></td>
                            <?php endif; ?>
                        </tr>
                    <?php endfor; ?>
                    </tbody>
                </table>
            </div><hr/>
            <div class="text-right">
                <button id='butCreate' name='butCreate' type="submit" class="btn bg-dark"><i class="icon-floppy-disk"></i> Import</button>
            </div>
        </form>
        <?php else: ?>
        <div class="alert alert-danger">
            Invalid file encoding. Please save CSV file as UTF-8.
        </div>
        <a href="<?php echo Yii::app()->createAbsoluteURL('/'.$this->controllerName.'/'.$action); ?>" class="btn btn-danger">Try again</a>
        <?php endif; ?>
    </div>
</div>
