<?php
$breadcrumbs = [
    [
        'title' => $this->title,
        'link' => '/'.$this->controllerName.'/main',
    ],
    [
        'title' => $title,
    ],
];
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$action); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal" enctype="multipart/form-data">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success">Uploading and Importing will take some time.</h5>
                </div>
                <div class="panel-body">
                    <div class="row" id="frm-customer-skin-images">
                        <div class="col-lg-4">
                            <div class="form-group col-lg-12">
                                <input type="file" class="file-styled-maihan-file" id="uploadBtn" name="uploadBtn">
                            </div>
                        </div>
                    </div><hr/>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
            </div>
        </form>
    </div>
</div>
