<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $title,
    'searchBox' => $searchBox,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <?php
        $arrForm = array();
        $arrForm['dateFrom'] = $dateFrom;
        $arrForm['dateTo'] = $dateTo;
        $this->widget('application.components.DateFilter', $arrForm);
        ?>&nbsp;
        <div class="col-lg-1">
            <button type="button" id="btnSearchReport" class="btn bg-maihan"><i class="icon-search4"></i></button>
        </div>
        <h5 class="panel-title text-success"></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/search'); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Tạo đơn hàng</button></a></li>
            </ul>
        </div>
    </div><hr/>
    <div class="panel-body">
        <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-highlight-maihan">
                <?php if (!empty($listOrderStatus)): ?>
                <?php 
                foreach ($listOrderStatus as $k => $lOS):
                    $classActive = ($k == $idStatus) ? 'active' : '';
                ?>
                <li class="<?php echo $classActive ?>">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl($url) . '?status=' . $k; ?>">
                        <?php echo $lOS ?>
                    </a>
                </li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="table-responsive">
                        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mã ĐH</th>
                                    <?php if ($idStatus == STATUS_OF_ORDER_CONTRACTED || $idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_1 || $idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_2 || $idStatus == STATUS_OF_ORDER_CANCEL): ?>
                                    <th>Mã HĐ</th>
                                    <?php endif; ?>
                                    <th>Số ĐT</th>
                                    <th>Tên KH</th>
                                    <th>Dự án</th>
                                    <th>Sản phẩm</th>
                                    <th>Tổng tiền TT</th>
                                    <th class="text-center"><?php echo $this->actionText ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($items as $k1 => $v1):
                                    $action = '';
                                    $urlUpdate = Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create') . "?orderId=" . $v1['id'];
                                    $editAction = $deleteAction = $createContractAction = $approveAction = '';
                                    if ($idStatus == STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT) {
                                        if ($editOrderAct) {
                                            $editAction = '<a href="' . $urlUpdate . '"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa đơn hàng" data-placement="top"></i></a>';
                                        }
                                        if ($addContractAct) {
                                            $createContractAction = '<a href="' . $urlUpdate.'&createContract=1' . '"><i class="icon-plus3 text-success" data-popup="tooltip" title="Tạo hợp đồng" data-placement="top"></i></a>';
                                        }
                                    } else if ($idStatus == STATUS_OF_ORDER_CONTRACTED && $editContractAct) {
                                        $createContractAction = '<a href="' . $urlUpdate.'&editContract=1' . '"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa hợp đồng" data-placement="top"></i></a>';
                                    } else if (($idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_1 || $idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_2) && ($approveContractAct1 || $approveContractAct2 || $denyContractAct)) {
                                        $approveAction = '<a href="' . $urlUpdate.'&editContract=1' . '"><i class="icon-checkmark4" data-popup="tooltip" title="Duyệt / Hủy hợp đồng" data-placement="top"></i></a>';
                                    } else if ($idStatus == STATUS_OF_ORDER_CANCEL && $deleteOrderAct) {
                                        $deleteAction = '<a class="delete" data-id="' . $v1['id'] . '" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>';
                                    }
                                    
                                    if (!empty($editAction)) {
                                        $action .= $editAction.'&nbsp;&nbsp;';
                                    }
                                    if (!empty($createContractAction)) {
                                        $action .= $createContractAction.'&nbsp;&nbsp;';
                                    }
                                    if (!empty($approveAction)) {
                                        $action .= $approveAction.'&nbsp;&nbsp;';
                                    }
                                    if (!empty($deleteAction)) {
                                        $action .= $deleteAction.'&nbsp;&nbsp;';
                                    }
                                    
                                    $finalPrice = $v1['final_price'];
                                    if ($v1['use_tax'] == 1 && $finalPrice > 0 && $v1['tax'] > 0) {
                                        $finalPrice = ($finalPrice + $v1['tax']);
                                    }
                                ?>
                                <tr>
                                    <td><?php echo ($offset + $k1 + 1)  ?></td>
                                    <td><?php echo $v1['order_code']  ?></td>
                                    <?php if ($idStatus == STATUS_OF_ORDER_CONTRACTED || $idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_1 || $idStatus == STATUS_OF_ORDER_PENDING_TO_APPROVE_2 || $idStatus == STATUS_OF_ORDER_CANCEL): ?>
                                    <td><?php echo $v1['contract_code'] ?></td>
                                    <?php endif; ?>
                                    <td><?php echo (!empty($phones[$v1['id_customer']])) ? implode(' / ', array_column($phones[$v1['id_customer']], 'phone')) : '' ?></td>
                                    <td><?php echo $v1['customer_name'] ?></td>
                                    <td><?php echo $v1['project_name'] ?></td>
                                    <td><?php echo $v1['product_name'] ?></td>
                                    <td><?php echo number_format($finalPrice).VIETNAM_CURRENCY ?></td>
                                    <td class="btn-group-custom"><?php echo $action ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pagination -->
    <div class="text-right bootpag-default"></div><hr>
    <!-- /pagination -->
</div>

<script>
    var url = '<?php echo Yii::app()->createAbsoluteUrl($url) . '?status=' . $idStatus; ?>';//reURL

    $(document).ready(function () {
        $('#btnSearchReport').click(function(){
            blockSystem();
            var reloadURL = url + "&from=" + $("#dateFrom").val() + "&to=" + $("#dateTo").val();
            window.location = reloadURL;
        });
        
        //settingDatatableDefaults(9, 'Gõ Mã ĐH hoặc Mã KH lọc');
        
        $('.bootpag-default').bootpag({
            total: <?php echo $totalPage; ?>,
            page: <?php echo $page; ?>, //initial page
            maxVisible: <?php echo MAX_VISIBLE; ?>,
            firstLastUse: true,
            first: '←',
            last: '→',
            leaps: false
        }).on("page", function(event, num){
            event.preventDefault();
            var fromDate = $('#dateFrom').val();
            var toDate = $('#dateTo').val();
            var customerType = $("#customer_type").val();
            $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl($url) . "?page="; ?>' + num + '&from=' + fromDate + '&to=' + toDate + '&status=<?php echo $idStatus ?>') ;
        });
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?id="; ?>' + delID;
            SwalConfirm("Xóa đơn hàng này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
        });
        
    });
</script>