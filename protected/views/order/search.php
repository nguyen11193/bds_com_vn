<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="col-lg-4">
                    <ul class="icons-list">
                        <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create'); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm đơn hàng mới</button></a></li>
                    </ul>&nbsp;
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <input type="text" onblur="findCustomerByPhone('<?php echo OPERATOR_LIKE ?>')" class="form-control input_number_only" id="like" name="like" placeholder="Số ĐT - Tìm gợi ý" value="">
                        <span class="input-group-btn">
                            <button class="btn bg-maihan" type="button"><i class="icon-search4"></i></button>
                        </span>
                    </div>&nbsp;
                    <div class="input-group">
                        <input type="text" onblur="findCustomerByPhone('<?php echo OPERATOR_EQUAL ?>')" class="form-control input_number_only" id="equal" name="equal" placeholder="Số ĐT - Tìm chính xác" value="">
                        <span class="input-group-btn">
                            <button class="btn bg-maihan" type="button"><i class="icon-search4"></i></button>
                        </span>
                    </div>&nbsp;
                </div>
                <div class="col-lg-12" id="customer-result"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function createTableHtml(data){
        var html = '';
        html += '<div class="col-lg-6">';
        html +=     '<div class="table-responsive">';
        html +=         '<table class="table table-bordered table-striped table-hover">';
        html +=             '<thead>';
        html +=                 '<tr>';
        html +=                     '<th colspan="3" style="text-align:center"><strong>Khách hàng</strong></th>';
        html +=                 '</tr>';
        html +=                 '<tr>';
        html +=                     '<th>Số điện thoại</th>';
        html +=                     '<th>Tên KH</th>';
        html +=                     '<th>Tạo đơn hàng</th>';
        html +=                 '</tr>';
        html +=             '</thead>';
        html +=             '<tbody>';
        for (var i = 0; i < data.length; i++) {
        html +=                 '<tr>';
        html +=                     '<td>'+data[i].phone+'</td>';
        html +=                     '<td>'+data[i].name+'</td>';
        html +=                     '<td style="text-align:center"><a href="<?php echo Yii::app()->createAbsoluteUrl('/Order/create') . '?custId='; ?>'+data[i].id_customer+'"><i class="icon-cart"></i></a></td>';
        html +=                 '</tr>';
        }
        html +=             '</tbody>';
        html +=         '</table>';
        html +=     '</div>';
        html += '</div>';
        return html;
    }
    
    function findCustomerByPhone(type){
        var phone = '';
        if (type == '<?php echo OPERATOR_LIKE ?>'){
            phone = $('#like').val();
        } else {
            phone = $('#equal').val();
        }
        if(phone.length > 0) {
            var data_string = {
                'type': type,
                'phone': phone,
            };
            $.ajax({
                async: false,
                url: '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/searchCustomerByPhone/'); ?>',
                type: 'POST',
                data: data_string,
                dataType: 'json',
                cache: false,
                success: function (result) {
                    var html = '';
                    if (empty(result.customer)) {
                        html +=  '<div class="alert alert-warning alert-styled-left alert-arrow-left alert-bordered">';
                        html +=     '<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>';
                        html +=     '<span class="text-semibold">Không có kết quả nào. Vui lòng tạo đơn hàng mới.</span>'
                        html += '</div>';
                    } else {
                        html += createTableHtml(result.customer);
                    }
                    $('#customer-result').html(html);
                }
            });
        } else {
            $('#customer-result').html('');
        }
    }
</script>