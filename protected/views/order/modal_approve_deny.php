<div id="approve-modal" class="modal modal-custom">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-maihan">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Duyệt hợp đồng</h6>
            </div>
            <div class="modal-body">
                <form action="<?php echo Yii::app()->createAbsoluteUrl('/Order/'.$actionApprove) . '?' . $param; ?>" method="post" id="frmObjApprove" name="frmObjApprove" class="form-horizontal">
                    <input type="hidden" id="idOrdersApprove" name="idOrdersApprove" value="" />
                    <div class="form-group" id="frm-comment-approve">
                        <div class="col-lg-12">
                            <label class="control-label">Ghi chú:</label>
                            <textarea id="approve_comment" name="approve_comment" rows="4" cols="4" class="form-control elastic elastic-manual"></textarea>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butApprove' name='butApprove' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> Lưu thông tin duyệt hợp đồng</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="deny-modal" class="modal modal-custom">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-maihan">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Từ chối hợp đồng</h6>
            </div>
            <div class="modal-body">
                <form action="<?php echo Yii::app()->createAbsoluteUrl('/Order/'.$actionDeny) . '?' . $param; ?>" method="post" id="frmObjDeny" name="frmObjDeny" class="form-horizontal">
                    <input type="hidden" id="idOrdersDeny" name="idOrdersDeny" value="" />
                    <div class="form-group" id="frm-comment-deny">
                        <div class="col-lg-12">
                            <label class="control-label">Lý do:</label>
                            <textarea onkeyup="RemoveErr('#frm-comment-deny','#deny_comment_err')" placeholder="Vui lòng cho một lý do" id="deny_comment" name="deny_comment" rows="4" cols="4" class="form-control elastic elastic-manual"></textarea>
                            <div class="error error_hide" id="deny_comment_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butDeny' name='butDeny' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> Lưu thông tin và từ chối hợp đồng</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        $('#approve-modal').on('hidden.bs.modal', function () {
            $('#approve-modal').removeData('bs.modal');
            $('.modal-content').find('textarea').val('');
        });
        
        $('#deny-modal').on('hidden.bs.modal', function () {
            $('#deny-modal').removeData('bs.modal');
            $('.modal-content').find('textarea').val('');
            $('.modal-content').find('.error').hide();
            $('.modal-content').find('.form-group').removeClass("has-error");
        });
        
        $('#selectall').click(function(){
            if($(this).prop("checked") == true){
               $('input:checkbox.sids').not(this).prop('checked', this.checked);
            } else {
                $('input:checkbox.sids').not(this).prop('checked', this.checked);
            }
            $.uniform.update();
        });
        
        $('#butApprove').click(function () {
            var boolNoErrors = true;
            var checkedValues = '';
            if (boolNoErrors) {
                swal({
                    title: "Bạn có chắc muốn duyệt?",
                    text: "Bạn sẽ không thể quay lại.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Xác nhận",
                    cancelButtonText: "Thoát",
                    closeOnConfirm: true
                },
                function () {
                    $('#idOrdersApprove').val(checkedValues);
                    $('#approve-modal').hide();
                    document.frmObjApprove.submit(blockSystem());
                })
            }
        });
        
        $('#butDeny').click(function () {
            var boolNoErrors = true;
            var checkedValues = '';
            if(ShowErr("#deny_comment", "#frm-comment-deny", "#deny_comment_err", "Lý do", " thì không được trống.") == false){
                boolNoErrors = false;
                return;
            }
            if (boolNoErrors) {
                swal({
                    title: "Bạn có chắc muốn từ chối hợp đồng này?",
                    text: "Bạn sẽ không thể quay lại.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Xác nhận",
                    cancelButtonText: "Thoát",
                    closeOnConfirm: true
                },
                function () {
                    $('#idOrdersDeny').val(checkedValues);
                    $('#deny-modal').hide();
                    document.frmObjDeny.submit(blockSystem());
                })
            }
        });
    });
</script>

