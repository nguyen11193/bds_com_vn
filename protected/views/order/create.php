<?php

$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" id="create" name="create" value="1">
            <input type="hidden" name='orderId' id='orderId' value='<?php echo $orderShow['id']; ?>'>
            <input type="hidden" id="isContract" name="isContract" value="0">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5></h5>
                    <?php
                    if ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_APPROVE_1 || $orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_APPROVE_2):
                        $approveButon = '<a><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#approve-modal"><i class="icon-checkmark4"></i>&nbsp;&nbsp;Duyệt hợp đồng</button></a>&nbsp;';
                        if ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_APPROVE_1 && $approveContractAct1) {
                            echo $approveButon;
                        } else if ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_APPROVE_2 && $approveContractAct2) {
                            echo $approveButon;
                        }
                    ?>
                    <?php if ($denyContractAct): ?>
                    <a><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deny-modal"><i class="icon-cross2"></i>&nbsp;&nbsp;Từ chối hợp đồng</button></a>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="panel-body">
                    <div class="tabbable">
                        <?php
                        $classActiveCustomer = $classActiveOrder = $classActiveContract = '';
                        if (empty($orderShow['id'])) {
                            $classActiveCustomer = 'active';
                        } else if (!empty($createContract) || !empty($editContract)) {
                            $classActiveContract = 'active';
                        } else {
                            $classActiveOrder = 'active';
                        }
                        ?>
                        <ul class="nav nav-tabs nav-tabs-highlight-maihan">
                            <li id="customer-tab" class="<?php echo $classActiveCustomer ?>"><a href="#customer-info" data-toggle="tab"><i class="icon-info22"></i> <i>Thông tin Khách hàng</i></a></li>
                            <li id="order-tab" class="<?php echo $classActiveOrder ?>"><a href="#order-header-info" data-toggle="tab"><i class="icon-cart"></i> <i>Thông tin Đơn hàng</i></a></li>
                            <?php if (!empty($orderShow['id'])): ?>
                            <li id="contract-tab" class="<?php echo $classActiveContract ?>"><a href="#contract-info" data-toggle="tab"><i class="icon-book"></i> <i>Thông tin Hợp đồng</i></a></li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <?php echo $classActiveCustomer ?>" id="customer-info">
                                <?php
                                $this->renderPartial('/common/customer_info', array(
                                    'customerShow' => $customerShow,
                                    'disabled' => (!empty($customerShow['id'])) ? 'disabled' : '',
                                    'isEditOrder' => $orderShow['id'],
                                ));
                                ?>
                            </div>
                            <div class="tab-pane <?php echo $classActiveOrder ?>" id="order-header-info">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group col-lg-12" id="frm-order-code">
                                            <label class="control-label">Mã đơn hàng:</label>
                                            <input disabled="" type="text" id="order_code" name="order_code" value="<?php echo $orderShow['code']; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group col-lg-12" id="frm-order-comments">
                                            <label class="control-label">Ghi chú:</label>
                                            <textarea <?php echo $disabledOrder ?> placeholder="Tối đa 2000 ký tự" id="order_comments" name="Order[comments]" rows="4" cols="4" class="form-control elastic elastic-manual"><?php echo $orderShow['comments'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table id="tablelist" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Dự án</th>
                                                        <th class="text-center">sản phẩm</th>
                                                        <th class="text-center">Giá</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="tr-order-detail">
                                                        <td>
                                                            <select <?php echo $disabledOrder ?> onchange="changeProjectOrder(this)" class="select" id="id_project" name="Order[id_project]">
                                                                <option value="0">Chọn dự án</option>
                                                                <?php foreach ($projects as $v1): ?>
                                                                    <option value="<?php echo $v1['id'] ?>"><?php echo $v1['code'] . ' - ' . $v1['name'] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select <?php echo $disabledOrder ?> onchange="changeProduct(this)" class="form-control" id="id_product" name="Order[id_product]">
                                                                <option value="0">Chọn sản phẩm</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input readonly="" type="text" id="order_price" name="Order[unit_price]" value="" class="form-control number-with-comma input_number_only">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right" colspan="2">Chiết khấu (%)</td>
                                                        <td>
                                                            <input <?php echo $disabledOrder ?> onkeyup="changeDiscount()" type="text" id="order_discount" name="Order[discount]" value="<?php echo $orderShow['discount'] ?>" class="form-control number-with-comma input_number_only">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right" colspan="2">
                                                            <label class="control-label">Sử dụng thuế (VAT 10%)?</label>
                                                            <input <?php echo $disabledOrder ?> <?php if ($orderShow['use_tax'] == 1) echo 'checked' ?> class="styled form-control" type="checkbox" id="order_use_tax" name="Order[use_tax]" value="<?php echo $orderShow['use_tax'] ?>"/>
                                                        </td>
                                                        <td>
                                                            <input readonly="" type="text" id="order_tax" name="Order[tax]" value="<?php echo $orderShow['tax'] ?>" class="form-control number-with-comma input_number_only">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right control-label" colspan="2">TỔNG TIỀN THANH TOÁN</td>
                                                        <td>
                                                            <input readonly="" type="text" id="order_final_price" name="Order[final_price]" value="<?php echo $orderShow['final_price'] ?>" class="form-control control-label">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><hr/>
                                    </div>
                                </div>
                                <?php if ($orderShow['status'] == STATUS_OF_ORDER_PENDING_TO_DO_CONTRACT): ?>
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> <?php echo (empty($orderShow['id'])) ? 'Tạo đơn hàng' : 'Cập nhật đơn hàng' ?></button>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="tab-pane <?php echo $classActiveContract ?>" id="contract-info">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group col-lg-12" id="frm-order-code">
                                            <label class="control-label">Mã hợp đồng:</label>
                                            <input disabled="" type="text" id="contract_code" name="contract_code" value="<?php echo $orderShow['contract_code']; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group col-lg-12" id="frm-contract-comments">
                                            <label class="control-label">Ghi chú:</label>
                                            <textarea <?php echo $disabledContract ?> placeholder="Tối đa 2000 ký tự" id="contract_comment" name="contract_comment" rows="4" cols="4" class="form-control elastic elastic-manual"><?php echo $orderShow['contract_comment'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-12">
                                            <label class="control-label">Hình ảnh:</label>
                                            <input <?php echo $disabledContract ?> multiple type="file" class="images-contract-styled-longtoc" name="contract_images[]" accept="image/*">
                                            <span class="help-block">Định dạng file: <?php echo implode(', ', Common::allowImages()) ?>.</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-12">
                                            <label class="control-label">Hợp đồng:</label>
                                            <input <?php echo $disabledContract ?> multiple id="contract_file" type="file" class="file-contract-styled-longtoc" name="contract_file[]" accept="application/pdf">
                                            <span class="help-block">Định dạng file: pdf.</span>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($orderShow['contract_attach_images'])): ?>
                                <div class="row">
                                    <?php foreach ($orderShow['contract_attach_images'] as $km => $attachImages): ?>
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="thumbnail">
                                            <div class="thumb">
                                                <a href="<?php echo $attachImages ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded">
                                                    <img class="img-rounded img-preview img-responsive" src="<?php echo $attachImages ?>" alt="" style="width:200px;height:200px;object-fit:cover;">
                                                    <span class="zoom-image"><i class="icon-plus2"></i></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <?php if (!empty($orderShow['contract_attach_files'])): ?>
                                <div class="row">
                                    <?php foreach ($orderShow['contract_attach_files'] as $kf => $attachFiles): ?>
                                    <!--<div class="col-lg-12">-->
                                        <embed src="<?php echo $attachFiles ?>#toolbar=0" type="application/pdf" height="400px" width="100%" />
                                    <!--</div><br/>-->
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                                <?php if (empty($disabledContract)): ?>
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <button onclick="createContract(<?php echo CONTRACT_DRAFT ?>)" type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> Lưu thông tin nháp</button>
                                        <button onclick="createContract(<?php echo CONTRACT_FINISHED ?>)" type="button" class="btn bg-danger" ><i class="icon-drawer-in"></i> Lưu thông tin và kết thúc</button>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$this->renderPartial('/order/modal_approve_deny', array(
    'actionApprove' => 'approve',
    'actionDeny' => 'deny',
    'param' => 'id=' . $orderShow['id'],
));
?>
<script type="text/javascript">
    var products = [];
    var idProject = <?php echo $orderShow['id_project'] ?>;
    var idProduct = <?php echo $orderShow['id_product'] ?>;
    
    function changeProjectOrder(obj){
        var projectId = $(obj).val();
        $.ajax({
            async: false,
            url: '/Order/getProductByProjectId',
            type: "POST",
            data: {projectId: projectId, productId: idProduct},
            dataType: 'json',
            cache: false,
            success: function (result) {
                products = result.products;
                renderProductOptions(result.products);
            }
        });
    }
    
    function renderProductOptions(data) {
        var html = '<option value="0">Chọn sản phẩm</option>';
        if (!empty(data)) {
            for (var i = 0; i < data.length; i++) {
                var product = data[i];
                var selected = (idProduct == product.id) ? 'selected' : '';
                html += '<option '+selected+' value="'+product.id+'">'+product.name+'</option>';
            }
        }
        $('#id_product').html(html);
        $('#id_product').trigger('change');
    }
    
    function changeProduct(obj){
        var productId = $(obj).val();
        var price = 0;
        if (!empty(products)) {
            for (var i = 0; i < products.length; i++) {
                var product = products[i];
                if (product.id == productId) {
                    price = parseFloat(product.price);
                    break;
                }
            }
        }
        $('#order_price').val(formatAmount(price));
        calculateGrandTotal();
    }
    
    function createContract(type){
        var boolNoErrors = true;
        <?php if (empty($orderShow['contract_code'])): ?>
        var contractFile = $('#contract_file').val();
        if (empty(contractFile)) {
            SwalWarning('Hợp đồng thì không được trống', 'Vui lòng chọn hợp đồng');
            boolNoErrors = false;
            return;
        }
        <?php endif; ?>
        if (boolNoErrors) {
            $('#just_save_customer_info').val(0);
            $('#isContract').val(type);
            document.frmObj.submit(blockSystem());
        }
    }
    
    $(document).ready(function($){
        
        $('[data-popup="lightbox"]').fancybox({
            padding: 3
        });
        
        /*$(document).on('change', '.nguyen', function() {
            const $input = $(this),
                  filenames = $.map($input[0].files, file => file.name);
            $input.siblings('.filename').html(filenames.join(', '));
        });*/
        
        if (!empty(idProject)) {
            $('#id_project').val(idProject);
            $('#id_project').change();
        }
        
        $('#order_use_tax').change(function() {
            if (this.checked) {
                $(this).val(1);
            } else {
                $(this).val(0);
            }
            calculateGrandTotal();
        });
        
        $('#nextTabCustomer').click(function(){
            activeTabOrder('#order-tab', '#order-header-info', '#consumer-info-hide-show', '#consumer-info', '#customer-tab', '#customer-info');
            scrollToErrorField('#order-tab');
        });
        
        $('#butCreate').click(function(){
            var boolNoErrors = true;
            var isDisabledCustomer = $('#customer_name').prop('disabled');
            if (!isDisabledCustomer) {
                boolNoErrors = validateCustomerInfo(boolNoErrors);
            }
            if (!boolNoErrors) {
                activeTabOrder('#customer-tab', '#customer-info', '#order-tab', '#order-header-info', '#consumer-info-hide-show', '#consumer-info');
                return;
            }
            
            /*boolNoErrors = validateOrderInfo(boolNoErrors);
            if (!boolNoErrors) {
                activeTabOrder('#order-tab', '#order-header-info', '#consumer-info-hide-show', '#consumer-info', '#customer-tab', '#customer-info');
                return;
            }*/
            //Perform Submission
            if (boolNoErrors) {
                if (!validateOrderDetail('Bạn chưa thể tạo đơn hàng')){
                    return;
                }
                $('#just_save_customer_info').val(0);
                $('#isContract').val(0);
                document.frmObj.submit(blockSystem());
            }
        });
    });
</script>

