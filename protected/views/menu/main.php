<?php 
$breadcrumbs =[
  [
    'title' => 'Menu',
  ],
];
$this->renderPartial('/layouts/pageheader', array(
  'breadcrumbs' => $breadcrumbs,
  'title' => 'Menu'
)); 

$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-lg-3">
            <select onchange="changeMenuParent(this)" id="menu_parent" name="menu_parent" class="form-control">
                <option value="#content">-- Tất cả --</option>
                <?php foreach ($menuParent as $v1): ?>
                    <option value="#menu_parent_<?php echo $v1['id'] ?>"><?php echo $v1['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>&nbsp;
        <h5 class="panel-title text-success"></h5>
    </div>
    <div class="panel-body">
        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Menu Parent</th>
                    <th>Menu</th>
                </tr>
            </thead>
            <?php foreach ($menuParent as $mp):
                $menus = array();
                foreach ($menu as $m) {
                    if ($mp['id'] == $m['id_menu_parent']) {
                        $menus[] = ['id' => $m['id'], 'name' => $m['name'], 'url' => $m['url'], 'sort_by' => $m['sort_by']];
                    }
                }
            ?>
            <tbody>
                <tr>
                    <td id="menu_parent_<?php echo $mp['id'] ?>" style="text-align: center;" rowspan="<?= count($menus) + 2; ?>"><label><?= $mp['name'] ?></label></td>
                </tr>
                <?php foreach ($menus as $me): ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name<?php echo $me['id'] ?>">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name<?php echo $me['id'] ?>', '#name<?php echo $me['id'] ?>_err')" type="text" id="name<?php echo $me['id'] ?>" name="name" maxlength="255" value="<?php echo $me['name'] ?>" class="form-control">
                                        <div class="error" id="name<?php echo $me['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-url<?php echo $me['id'] ?>">
                                    <label class="col-lg-3 control-label">URL:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-url<?php echo $me['id'] ?>', '#url<?php echo $me['id'] ?>_err')" type="text" id="url<?php echo $me['id'] ?>" name="url" maxlength="255" value="<?php echo $me['url'] ?>" class="form-control">
                                        <div class="error" id="url<?php echo $me['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby<?php echo $me['id'] ?>">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-sortby<?php echo $me['id'] ?>', '#sortby<?php echo $me['id'] ?>_err')" type="number" id="sortby<?php echo $me['id'] ?>" min="0" name="sortby" value="<?php echo $me['sort_by'] ?>" class="form-control">
                                        <div class="error" id="sortby<?php echo $me['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="save" data-id="<?php echo $me['id'] ?>" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>&nbsp;
                        <a class="delete" data-id="<?php echo $me['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name-create<?php echo $mp['id'] ?>">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name-create<?php echo $mp['id'] ?>', '#name_create<?php echo $mp['id'] ?>_err')" type="text" id="name_create<?php echo $mp['id'] ?>" name="name" maxlength="255" value="" class="form-control">
                                        <div class="error" id="name_create<?php echo $mp['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-url-create<?php echo $mp['id'] ?>">
                                    <label class="col-lg-3 control-label">URL:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-url-create<?php echo $mp['id'] ?>', '#url_create<?php echo $mp['id'] ?>_err')" type="text" id="url_create<?php echo $mp['id'] ?>" name="url" maxlength="255" value="" class="form-control">
                                        <div class="error" id="url_create<?php echo $mp['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby-create<?php echo $mp['id'] ?>">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-sortby-create<?php echo $mp['id'] ?>', '#sortby_create<?php echo $mp['id'] ?>_err')" type="number" id="sortby_create<?php echo $mp['id'] ?>" min="0" name="sortby" value="" class="form-control">
                                        <div class="error" id="sortby_create<?php echo $mp['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="create" data-id="<?php echo $mp['id'] ?>" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>
                    </td>
                </tr>
            </tbody>
            <?php endforeach; ?>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".error").hide();
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = 'sid=' + delID;
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkHavePermission/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Tồn tại permission thuộc menu này", "Vui lòng xóa permission trước.");
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/delete") . "?sid="; ?>' + delID;
                SwalConfirm("Xóa menu này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
        $('#tablelist').on('click', '.save', function () {
            var id = $(this).data("id");
            var boolNoErrors = true;
            if (ShowErr("#name"+id, "#frm-name"+id, "#name"+id+"_err", "Tên", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (ShowErr("#url"+id, "#frm-url"+id, "#url"+id+"_err", "URL", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (ShowErr("#sortby"+id, "#frm-sortby"+id, "#sortby"+id+"_err", "Sort By", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (boolNoErrors) {
                var data = {
                    'name': $("#name"+id).val(),
                    'url': $("#url"+id).val(),
                    'sortby': $("#sortby"+id).val(),
                    'id': id
                };
                var result = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkDuplicity/"); ?>");
                if ((CheckUnique(result.errName, "Tên", "#frm-name"+id, "#name"+id+"_err", " đã tồn tại") == true)) {
                    var resultSave = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/update/"); ?>");
                    if (resultSave.success) {
                        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/Menu/main') . "?msg="; ?>' + resultSave.msg) ;
                    }
                }
            }
        });
        
        $('#tablelist').on('click', '.create', function () {
            var id = $(this).data("id");
            var boolNoErrors = true;
            if (ShowErr("#name_create"+id, "#frm-name-create"+id, "#name_create"+id+"_err", "Tên", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (ShowErr("#url_create"+id, "#frm-url-create"+id, "#url_create"+id+"_err", "URL", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (ShowErr("#sortby_create"+id, "#frm-sortby-create"+id, "#sortby_create"+id+"_err", "Sort By", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (boolNoErrors) {
                var data = {
                    'name': $("#name_create"+id).val(),
                    'url': $("#url_create"+id).val(),
                    'sortby': $("#sortby_create"+id).val(),
                    'id': 0,
                    'idmenuparent': id
                };
                var result = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkDuplicity/"); ?>");
                if ((CheckUnique(result.errName, "Tên", "#frm-name-create"+id, "#name_create"+id+"_err", " đã tồn tại") == true)) {
                    var resultSave = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/create/"); ?>");
                    if (resultSave.success) {
                        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/Menu/main') . "?msg="; ?>' + resultSave.msg) ;
                    }
                }
            }
        });
        
    });
</script>