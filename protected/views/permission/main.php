<?php 
$breadcrumbs =[
  [
    'title' => 'Permission',
  ],
];
$this->renderPartial('/layouts/pageheader', array(
  'breadcrumbs' => $breadcrumbs,
  'title' => 'Permission'
));

$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-lg-3">
            <select onchange="changeMenuParent(this)" id="menu" name="menu" class="form-control">
                <option value="#content">-- Tất cả --</option>
                <?php foreach ($menu as $v1): ?>
                    <option value="#menu_<?php echo $v1['id'] ?>"><?php echo $v1['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>&nbsp;
        <h5 class="panel-title text-success"></h5>
    </div>
    <div class="panel-body">
        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Menu</th>
                    <th>Permission</th>
                </tr>
            </thead>
            <?php foreach ($menu as $m):
                $permissions = array();
                foreach ($permission as $p) {
                    if ($m['id'] == $p['id_menu']) {
                        $permissions[] = ['id' => $p['id'], 'name' => $p['name'], 'sort_by' => $p['sort_by'], 'code' => $p['code']];
                    }
                }
            ?>
            <tbody>
                <tr>
                    <td id="menu_<?php echo $m['id'] ?>" style="text-align: center;" rowspan="<?= count($permissions) + 2; ?>"><label><?= $m['name'] ?></label></td>
                </tr>
                <?php foreach ($permissions as $per): ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name<?php echo $per['id'] ?>">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name<?php echo $per['id'] ?>', '#name<?php echo $per['id'] ?>_err')" type="text" id="name<?php echo $per['id'] ?>" name="name" maxlength="255" value="<?php echo $per['name'] ?>" class="form-control">
                                        <div class="error" id="name<?php echo $per['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby<?php echo $per['id'] ?>">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input type="number" id="sortby<?php echo $per['id'] ?>" min="0" name="sortby" value="<?php echo $per['sort_by'] ?>" class="form-control">
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-code<?php echo $per['id'] ?>">
                                    <label class="col-lg-3 control-label">Code:</label>
                                    <div class="col-lg-9">
                                        <input disabled="" type="text" id="code<?php echo $per['id'] ?>" name="url" maxlength="255" value="<?php echo $per['code'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="save" data-id="<?php echo $per['id'] ?>" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>&nbsp;
                        <a class="delete" data-id="<?php echo $per['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name-create<?php echo $m['id'] ?>">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name-create<?php echo $m['id'] ?>', '#name_create<?php echo $m['id'] ?>_err')" type="text" id="name_create<?php echo $m['id'] ?>" name="name" maxlength="255" value="" class="form-control">
                                        <div class="error" id="name_create<?php echo $m['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby-create<?php echo $m['id'] ?>">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input type="number" id="sortby_create<?php echo $m['id'] ?>" min="0" name="sortby" value="" class="form-control">
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-code-create<?php echo $m['id'] ?>">
                                    <label class="col-lg-3 control-label">Code:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-code-create<?php echo $m['id'] ?>', '#code_create<?php echo $m['id'] ?>_err')" type="text" id="code_create<?php echo $m['id'] ?>" name="code" maxlength="255" value="" class="form-control">
                                        <div class="error" id="code_create<?php echo $m['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="create" data-id="<?php echo $m['id'] ?>" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>
                    </td>
                </tr>
            </tbody>
            <?php endforeach; ?>
            <tbody>
                <tr>
                    <td style="text-align: center;" rowspan="<?= count($permissionsSpecial) + 2; ?>"><label></label></td>
                </tr>
                <?php if (!empty($permissionsSpecial)): ?>
                <?php foreach ($permissionsSpecial as $special): ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name<?php echo $special['id'] ?>">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name<?php echo $special['id'] ?>', '#name<?php echo $special['id'] ?>_err')" type="text" id="name<?php echo $special['id'] ?>" name="name" maxlength="255" value="<?php echo $special['name'] ?>" class="form-control">
                                        <div class="error" id="name<?php echo $special['id'] ?>_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby<?php echo $special['id'] ?>">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input type="number" id="sortby<?php echo $special['id'] ?>" min="0" name="sortby" value="<?php echo $special['sort_by'] ?>" class="form-control">
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-code<?php echo $special['id'] ?>">
                                    <label class="col-lg-3 control-label">Code:</label>
                                    <div class="col-lg-9">
                                        <input disabled="" type="text" id="code<?php echo $special['id'] ?>" name="url" maxlength="255" value="<?php echo $special['code'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="save" data-id="<?php echo $special['id'] ?>" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>&nbsp;
                        <a class="delete" data-id="<?php echo $special['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                <tr>
                    <td>
                        <div class="panel panel-flat">
                            <div class="panel-body">
                                <div class="form-group" id="frm-name-create0">
                                    <label class="col-lg-3 control-label">Tên:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-name-create0', '#name_create0_err')" type="text" id="name_create0" name="name" maxlength="255" value="" class="form-control">
                                        <div class="error" id="name_create0_err" style="display: hidden"></div>
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-sortby-create0">
                                    <label class="col-lg-3 control-label">Sort By:</label>
                                    <div class="col-lg-9">
                                        <input type="number" id="sortby_create0" min="0" name="sortby" value="" class="form-control">
                                    </div>
                                </div><br/>
                                <div class="form-group" id="frm-code-create0">
                                    <label class="col-lg-3 control-label">Code:</label>
                                    <div class="col-lg-9">
                                        <input onkeyup="RemoveErr('#frm-code-create0', '#code_create0_err')" type="text" id="code_create0" name="code" maxlength="255" value="" class="form-control">
                                        <div class="error" id="code_create0_err" style="display: hidden"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="btn-group-custom">
                        <a class="create" data-id="0" href="javascript:;"><i class="icon-floppy-disk" data-popup="tooltip" title="Lưu" data-placement="top"></i></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".error").hide();
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = 'sid=' + delID;
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkHavePermissionMap/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Tồn tại group permission map thuộc permission này", "Vui lòng xóa group permission map trước.");
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/delete") . "?sid="; ?>' + delID;
                SwalConfirm("Xóa permission này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
        $('#tablelist').on('click', '.save', function () {
            var id = $(this).data("id");
            var boolNoErrors = true;
            if (ShowErr("#name"+id, "#frm-name"+id, "#name"+id+"_err", "Tên", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (boolNoErrors) {
                var data = {
                    'name': $("#name"+id).val(),
                    'sortby': $("#sortby"+id).val(),
                    'id': id
                };
                var result = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkDuplicity/"); ?>");
                if ((CheckUnique(result.errName, "Tên", "#frm-name"+id, "#name"+id+"_err", " đã tồn tại") == true)) {
                    var resultSave = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/update/"); ?>");
                    if (resultSave.success) {
                        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/Permission/main') . "?msg="; ?>' + resultSave.msg) ;
                    }
                }
            }
        });
        
        $('#tablelist').on('click', '.create', function () {
            var id = $(this).data("id");
            var boolNoErrors = true;
            if (ShowErr("#name_create"+id, "#frm-name-create"+id, "#name_create"+id+"_err", "Tên", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (ShowErr("#code_create"+id, "#frm-code-create"+id, "#code_create"+id+"_err", "Code", " không được trống.") == false) {
                boolNoErrors = false;
            }
            if (boolNoErrors) {
                var data = {
                    'name': $("#name_create"+id).val(),
                    'code': $("#code_create"+id).val(),
                    'sortby': $("#sortby_create"+id).val(),
                    'id': 0,
                    'idmenu': id
                };
                var result = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkDuplicity/"); ?>");
                if ((CheckUnique(result.errName, "Tên", "#frm-name-create"+id, "#name_create"+id+"_err", " đã tồn tại") == true)) {
                    var resultSave = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/create/"); ?>");
                    if (resultSave.success) {
                        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/Permission/main') . "?msg="; ?>' + resultSave.msg) ;
                    }
                }
            }
        });
        
    });
</script>