<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-success">Thông tin chi tiết</h5>
            </div>
            <div class="panel-body form-horizontal">
                <div class="form-group">
                    <label class="col-lg-12">Tên nhóm:</label>
                    <div class="col-lg-12"><?php echo $model->name; ?></div>
                </div>
                <div class="form-group">
                    <label class="col-lg-12">Mô tả:</label>
                    <div class="col-lg-12"><?php echo  $model->desc; ?></div>
                </div>
                <div class="form-group">
                    <label class="col-lg-12">Quyền chức năng:</label>
                </div>
                <div class="form-group">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th style="text-align:center">Mô-đun</th>
                                <th style="text-align:center">Chức năng</th>
                            </tr>
                        </thead>
                        <?php foreach ($menus as $v1): ?>
                        <tbody>
                            <tr>
                                <td style="text-align:center" rowspan="<?php echo count($v1['permissions']) + 1; ?>"><label><?php echo $v1['name'] ?></label></td>
                            </tr>
                            <?php 
                            foreach ($v1['permissions'] as $v2):
                                $color = 'danger';
                                $icon = 'times';
                                if (!empty($permissionMaps[$v2['id']])) {
                                    $color = 'success';
                                    $icon = 'check';
                                }
                            ?>
                            <tr>
                                <td style="text-align:center"><label><?php echo $v2['name']; ?></label></td>
                                <td style="text-align: center;">
                                    <span class="pull-right"><i class="fa fa-lg fa-fw fa-<?php echo $icon; ?> text-<?php echo $color; ?>"></i></span>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-success">Người dùng trong nhóm này</h5>
            </div>
            <div class="panel-body">
                <table class="table datatable-basic table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên đăng nhập</th>
                            <th>Tên đầy đủ</th>
                            <th>Lần đăng nhập cuối</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $k1 => $v1): ?>
                        <tr>
                            <td><?php echo ($k1 + 1) ?></td>
                            <td><?php echo $v1['user_name'] ?></td>
                            <td><?php echo $v1['full_name'] ?></td>
                            <td><?php echo $v1['date_last_login'] ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        settingDatatableDefaults();
        
    });
</script>