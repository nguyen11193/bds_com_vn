<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
$this->renderPartial('/common/main', array(
    'btnAdd' => 'Thêm mới ' . $this->title,
    'head' => array($this->nameText, $this->descText, $this->departmentText),
    'items' => $items,
    'ignoreArr' => array('id'),
    'updateAction' => 'create',
    'deleteAction' => 'delete',
    'createAction' => 'create',
    'viewAction' => 'view',
));
?>
<script>
    $(document).ready(function () {
        
        settingDatatableDefaults(4, 'Gõ bất kỳ để lọc');
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = 'sid=' + delID;
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkHaveUserAccount/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Tồn tại người dùng đang thuộc nhóm này", "Vui lòng xóa người dùng trước.");
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?sid="; ?>' + delID;
                SwalConfirm("Xóa nhóm người dùng này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
    });
</script>