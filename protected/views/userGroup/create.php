<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal">
            <input type="hidden" id="create" name="create" value="1>">
            <input type="hidden" name="id" value="<?php echo $itemShow['id'] ?>">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success"></h5>
                </div>
                <div class="panel-body">
                    <div class="form-group" id="frm-groupname">
                        <label class="col-lg-3 control-label"><?php echo $this->nameText ?>:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr2(this, 2)" type="text" id="groupname" name="groupname" maxlength="255" value="<?php echo $itemShow['name'] ?>" class="form-control">
                            <div class="error error_hide" id="groupname_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-groupdesc">
                        <label class="col-lg-3 control-label"><?php echo $this->descText ?>:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr2(this, 2)" type="text" id="groupdesc" name="groupdesc" maxlength="255" value="<?php echo $itemShow['desc'] ?>" class="form-control">
                            <div class="error error_hide" id="groupdesc_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-department">
                        <label class="col-lg-3 control-label"><?php echo $this->departmentText ?>:</label>
                        <div class="col-lg-9">
                            <select name="id_department" id="id_department" onchange="RemoveErr2(this, 2)" class="form-control">
                                <option value="">-- Chọn bộ phận --</option>
                                <?php 
                                foreach ($departments as $department):
                                    $departmentId =  $department['id'];
                                    $selected = $departmentId == $itemShow['id_department'] ? 'selected' : '';
                                ?>
                                    <option <?php echo $selected ?> value="<?php echo $departmentId  ?>"><?php echo $department['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <div class="error error_hide" id="department_err"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-permissions">
                        <label class="col-lg-3 control-label">
                            Quyền chức vụ:
                            <br/><br/>
                            <div class="col-lg-12">
                                <select onchange="changeMenuParent(this)" id="menu" name="menu" class="form-control">
                                    <option value="#content">-- Tất cả --</option>
                                    <?php foreach ($menus as $v1): ?>
                                        <option value="#menu_<?php echo $v1['id'] ?>"><?php echo $v1['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </label>
                        <div class="col-lg-9">
                            <table class="table datatable-basic table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th style="text-align:center">Mô-đun</th>
                                        <th style="text-align:center">Quyền</th>
                                    </tr>
                                </thead>
                                <?php foreach ($menus as $v1): ?>
                                <tbody>
                                    <tr>
                                        <td id="menu_<?php echo $v1['id'] ?>" style="text-align: center;" rowspan="<?php echo count($v1['permissions']) + 1; ?>"><label><?php echo $v1['name'] ?></label></td>
                                    </tr>
                                    <?php 
                                    foreach ($v1['permissions'] as $v2):
                                        $checked = (!empty($permissionMaps[$v2['id']])) ? 'checked' : '';
                                    ?>
                                    <tr>
                                        <td style="text-align: center;"><label><?php echo $v2['name']; ?></label></td>
                                        <td style="text-align: center;" style="text-align: center;">
                                            <input type="hidden" id="hiddenperm_<?php echo $v2['id']; ?>" name="perm_<?php echo $v2['id']; ?>" value="0"/>
                                            <input class="styled" type="checkbox" id="perm_<?php echo $v2['id']; ?>" name="perm_<?php echo $v2['id']; ?>" <?php echo $checked ?> value="1"/>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i>&nbsp;&nbsp;&nbsp;<?php echo SAVE_INFO_TEXT ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($){
            
	$('#butCreate').click(function(){
            var boolNoErrors = true;
            if (ShowErr("#groupname", "#frm-groupname", "#groupname_err", "<?php echo $this->nameText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#groupdesc", "#frm-groupdesc", "#groupdesc_err", "<?php echo $this->descText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#id_department", "#frm-department", "#department_err", "<?php echo $this->departmentText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }
            
            //Perform Submission
            if (boolNoErrors) {
                var data_string = {
                    'uname': $("#groupname").val(),
                    'id': <?php echo $itemShow['id'] ?>,
                    'id_department': $('#id_department').val(),
                };
                var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkDuplicity/"); ?>");
                if((CheckUnique(result.errName, "<?php echo $this->nameText ?>", "#frm-groupname", "#groupname_err", " <?php echo EXISTED_IN_SYS_MSG ?>") == true)){
                    document.frmObj.submit(blockSystem());
                }		
            }
        });
    });
</script>