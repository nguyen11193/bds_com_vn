<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $title,
    'searchBox' => $searchBox,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <?php if (!$viewOnly): ?>
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create'); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm mới quy định</button></a></li>
            </ul>
        </div>
    </div><hr/>
    <?php endif; ?>
    <div class="panel-body">
        <div class="tabbable">
            <ul class="nav nav-tabs nav-tabs-highlight-maihan">
                <?php if (!empty($regulationCategory)): ?>
                <?php 
                foreach ($regulationCategory as $category):
                    $classActive = '';
                    if ($category['id'] == $regulationCategoryId) {
                        $classActive = 'active';
                        if (!empty($category['is_department'])) {
                            $showDepartment = true;
                        }
                    }
                    $classActive = ($category['id'] == $regulationCategoryId) ? 'active' : '';
                ?>
                <li class="<?php echo $classActive ?>">
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . '?regulationCategoryId=' . $category['id'] . $viewOnlyUrlParams; ?>">
                        <?php echo $category['name'] ?>
                    </a>
                </li>
                <?php endforeach; ?>
                <?php endif; ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active">
                    <?php if ($showDepartment && !$viewOnly): ?>
                    <div class="row">
                        <div class="col-lg-3">
                            <select onchange="changeDepartmentFilter(this)" id="id_department" name="id_department" class="form-control select">
                                <option value="0">-- Chọn bộ phận để lọc --</option>
                                <?php 
                                foreach ($departments as $department):
                                    $departmentSelected =  $department['id'] == $departmentId ? 'selected' : '';
                                ?>
                                    <option <?php echo $departmentSelected ?> value="<?php echo $department['id'] ?>"><?php echo $department['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div><br/>
                    <?php endif; ?>
                    <div class="table-responsive">
                        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover white-space-break-spaces">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?php echo $this->nameText ?></th>
                                    <?php if (!$viewOnly): ?>
                                    <th class="text-center"><?php echo $this->actionText ?></th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($items as $k1 => $v1): ?>
                                <tr>
                                    <td><?php echo ($k1 + 1) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i onclick="showHideOrderDetail(this, <?php echo $v1['id'] ?>, 'quy định')" class="icon-plus3" data-popup="tooltip" title="Xem chi tiết" data-placement="top"></i></td>
                                    <td><?php echo $v1['name']  ?></td>
                                    <?php if (!$viewOnly): ?>
                                    <td class="btn-group-custom">
                                        <a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create').'?id='.$v1['id'] ?>"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa" data-placement="top"></i></a>&nbsp;&nbsp;
                                        <a class="delete" data-id="<?php echo $v1['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                                    </td>
                                    <?php endif; ?>
                                </tr>
                                <?php if (!empty($itemsDetail[$v1['id']])): ?>
                                <tr style="display: none" id="tr_order_detail_<?php echo $v1['id'] ?>">
                                    <td colspan="<?php echo !$viewOnly ? '3' : '2' ?>">
                                        <table class="table table-bordered" style="background-color:#f7f7f7">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th><?php echo $this->categoryText ?></th>
                                                    <th><?php echo $this->contentText ?></th>
                                                    <th><?php echo $this->disciplinedText ?></th>
                                                    <th><?php echo $this->exampleText ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($itemsDetail[$v1['id']] as $indexContent => $content): ?>
                                                <tr>
                                                    <td><?php echo ($indexContent + 1)  ?></td>
                                                    <td><?php echo $content['category'] ?></td>
                                                    <td><div><?php echo $content['content'] ?></div></td>
                                                    <td><?php echo $content['disciplined'] ?></td>
                                                    <td><?php echo $content['example'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var regulationCategoryId = <?php echo $regulationCategoryId ?>;
    function changeDepartmentFilter(obj) {
        var departmentId = $(obj).val();
        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . "?regulationCategoryId="; ?>' + regulationCategoryId + '&departmentId=' + departmentId) ;
    }

    $(document).ready(function () {
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?sid="; ?>' + delID;
            SwalConfirm("Xóa quy định này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
        });
        
    });
</script>