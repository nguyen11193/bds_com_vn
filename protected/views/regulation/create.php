<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal">
            <input type="hidden" id="create" name="create" value="1>">
            <input type="hidden" name="id" value="<?php echo $itemShow['id'] ?>">
            <input type="hidden" name="regulation_content_data" id="regulation_content_data" value="">
            <input type="hidden" name="is_department" id="is_department" value="0">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success"></h5>
                </div>
                <div class="panel-body">
                    <div class="form-group" id="frm-name">
                        <label class="col-lg-3 control-label"><?php echo $this->nameText ?>:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr2(this, 2)" type="text" id="name" name="name" maxlength="255" value="<?php echo $itemShow['name'] ?>" class="form-control">
                            <div class="error error_hide" id="name_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-regulation-category">
                        <label class="col-lg-3 control-label">Loại quy định:</label>
                        <div class="col-lg-9">
                            <select name="id_regulation_category" id="id_regulation_category" class="form-control select">
                                <?php 
                                foreach ($regulationCategory as $category):
                                    $regulationCategoryId =  $category['id'];
                                    $categorySelected = $regulationCategoryId == $itemShow['id_regulation_category'] ? 'selected' : '';
                                ?>
                                    <option data-isdepartment="<?php echo $category['is_department'] ?>" <?php echo $categorySelected ?> value="<?php echo $regulationCategoryId  ?>"><?php echo $category['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div style="display: none;" class="form-group" id="frm-department">
                        <label class="col-lg-3 control-label">Bộ phận:</label>
                        <div class="col-lg-9">
                            <select name="id_department" id="id_department" class="form-control select">
                                <?php 
                                foreach ($departments as $department):
                                    $departmentId =  $department['id'];
                                    $departmentSelected = $departmentId == $itemShow['id_department'] ? 'selected' : '';
                                ?>
                                    <option <?php echo $departmentSelected ?> value="<?php echo $departmentId  ?>"><?php echo $department['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th style="width:20%" class="text-center"><?php echo $this->categoryText ?></th>
                                    <th style="width:30%" class="text-center"><?php echo $this->contentText ?></th>
                                    <th style="width:30%" class="text-center"><?php echo $this->disciplinedText ?></th>
                                    <th style="width:19%" class="text-center"><?php echo $this->exampleText ?></th>
                                    <th style="width:1%" class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody id="regulation_content_body">

                            </tbody>
                        </table>
                    </div><br/>
                    <div class="row text-right">
                        <div class="col-md-12">
                            <button type="button" class="btn bg-maihan pull-left" onclick="addRegulation()"><i class="icon-plus2"></i>&nbsp;&nbsp;&nbsp;Thêm quy định</button>
                        </div>
                    </div>
                    <hr/>
                    <div class="text-right">
                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i>&nbsp;&nbsp;&nbsp;<?php echo SAVE_INFO_TEXT ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var regulationContents = <?php echo (!empty($regulationContents)) ? json_encode($regulationContents) : json_encode([]) ?>;
    var regulationCategoryId = <?php echo $itemShow['id_regulation_category'] ?>;

    function addRegulation(dataTemplate = []) {
        if (empty(dataTemplate)) {
            dataTemplate.push({
                id: 0,
                category: '',
                content: '',
                disciplined: '',
                example: '',
            });
        }

        for (var i = 0; i < dataTemplate.length; i++) {
            var template = dataTemplate[i];
            var html = '';
            html += '<tr data-id="' + template.id + '" class="tr_regulation_content">';
            html +=     '<td><input placeholder="Nhập <?php echo $this->categoryText ?>" type="text" value="'+ template.category +'" class="form-control regulation-category"></td>';
            html +=     '<td><textarea placeholder="Nhập <?php echo $this->contentText ?>" rows="4" cols="4" class="form-control elastic elastic-manual regulation-content">'+ template.content +'</textarea></td>';
            html +=     '<td><textarea placeholder="Nhập <?php echo $this->disciplinedText ?>" rows="4" cols="4" class="form-control elastic elastic-manual regulation-disciplined">'+ template.disciplined +'</textarea></td>';
            html +=     '<td><textarea placeholder="Nhập <?php echo $this->exampleText ?>" rows="4" cols="4" class="form-control elastic elastic-manual regulation-example">'+ template.example +'</textarea></td>';
            html +=     '<td><a class="delete-regulation-content-line" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger pull-right" title="Xóa" data-placement="top"></i></a></td>';
            html += '</tr>';

            $('#regulation_content_body').append(html);
            $('.tr_regulation_content').first().find('.delete-regulation-content-line').remove();
        }
    }

    $(document).on('click', '.delete-regulation-content-line', function() {
        $(this).closest('.tr_regulation_content').remove();
    });

    $(document).ready(function($){
        addRegulation(regulationContents);

    $('#id_regulation_category').change(function() {
        $('#frm-department').hide();
        $('#is_department').val(0);
        var isDepartment = $(this).find('option:selected').data('isdepartment');
        if (isDepartment == 1) {
            $('#is_department').val(1);
            $('#frm-department').show();
        }
    });

    $('#id_regulation_category').val(regulationCategoryId).change();
            
	$('#butCreate').click(function(){
            var boolNoErrors = true;
            if (ShowErr("#name", "#frm-name", "#name_err", "<?php echo $this->nameText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            var regulationContentData = [];
            if (boolNoErrors) {
                $('.tr_regulation_content').each(function (index, obj) {
                    var category = $(this).find('.regulation-category').val();
                    var content = $(this).find('.regulation-content').val();
                    var disciplined = $(this).find('.regulation-disciplined').val();
                    var example = $(this).find('.regulation-example').val();
                    var regulationContentId = $(this).data('id');

                    if (empty(category) || empty(content)) {
                        boolNoErrors = false;
                        SwalWarning('Hạng mục và nội dung thì không được trống', 'Vui lòng kiểm tra lại dòng thứ ' + (index + 1));
                        return false;
                    }

                    regulationContentData.push({
                        id : regulationContentId,
                        category : category,
                        content : content,
                        disciplined : disciplined,
                        example : example,
                    });
                });
            }

            //Perform Submission
            if (boolNoErrors) {
                $('#regulation_content_data').val(JSON.stringify(regulationContentData));
                var data_string = {
                    'uname': $("#name").val(),
                    'id': <?php echo $itemShow['id'] ?>
                };
                var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkDuplicity/"); ?>");
                if((CheckUnique(result.errName, "<?php echo $this->nameText ?>", "#frm-name", "#name_err", " <?php echo EXISTED_IN_SYS_MSG ?>") == true)){
                    document.frmObj.submit(blockSystem());
                }		
            }
        });
    });
</script>
