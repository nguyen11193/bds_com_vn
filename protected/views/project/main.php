<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $title,
    'searchBox' => $searchBox,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create'); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm mới Dự án</button></a></li>
            </ul>
        </div>
    </div><hr/>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?php echo $this->codeText ?></th>
                        <th><?php echo $this->nameText ?></th>
                        <th><?php echo $this->addressText ?></th>
                        <th class="text-center"><?php echo $this->actionText ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $k => $item): ?>
                    <tr>
                        <td><?php echo ($k + 1) ?></td>
                        <td><?php echo $item['code'] ?></td>
                        <td><?php echo $item['name'] ?></td>
                        <td><?php echo $item['address'] ?></td>
                        <td class="btn-group-custom">
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/create').'?id='.$item['id'] ?>"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa" data-placement="top"></i></a>&nbsp;&nbsp;
                            <a class="delete" data-id="<?php echo $item['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Pagination -->
    <div class="text-right bootpag-default"></div><hr>
    <!-- /pagination -->
</div>
<script>
    $(document).ready(function () {
        
        $('#tablelist').on('click', '.delete', function () {
            var delId = $(this).data("id");
            var data_string = {id: delId};
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkHaveOrder/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Dự án này đã được sử dụng", '');
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?id="; ?>' + delId;
                SwalConfirm("Xóa dự án này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
        $('.bootpag-default').bootpag({
            total: <?php echo $totalPage; ?>,
            page: <?php echo $page; ?>, //initial page
            maxVisible: <?php echo MAX_VISIBLE; ?>,
            firstLastUse: true,
            first: '←',
            last: '→',
            leaps: false
        }).on("page", function(event, num){
            $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . "?page="; ?>' + num) ;
        });
        
    });
</script>