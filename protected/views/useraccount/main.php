<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
$this->renderPartial('/common/main', array(
    'btnAdd' => 'Thêm mới Người dùng',
    'head' => array('Tên đầy đủ', 'Tên đăng nhập', 'Bộ phận', 'Chức vụ', 'Lần đăng nhập cuối'),
    'items' => $items,
    'ignoreArr' => array('id', 'email', 'column_name'),
    'updateAction' => 'create',
    'deleteAction' => 'delete',
    'createAction' => 'create',
));
?>
<script>
    $(document).ready(function () {
        
        settingDatatableDefaults(6, 'Gõ bất kỳ để lọc');
        
        $('#tablelist').on('click', '.delete', function () {
            var delId = $(this).data("id");
            var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?id="; ?>' + delId;
            SwalConfirm("Xóa người dùng này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
        });
        
    });
</script>