<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $breadcrumbLast['title'],
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="row">
    <?php if ($isProfile): $col = 8;  ?>
    <div class="col-lg-4">
        <div class="thumbnail">
            <div class="thumb thumb-rounded thumb-slide">
                <img src="<?php echo Yii::app()->request->baseUrl . '/' . USER_PROFILE_PATH . $itemShow['image']; ?>" alt="">
            </div>
            <div class="caption text-center">
                <h6 class="text-semibold no-margin"><?php echo $itemShow['user_name'] ; ?>
                    <small class="display-block">Bộ phận: <?php echo (!empty($departments[$itemShow['id_department']]['name'])) ? $departments[$itemShow['id_department']]['name'] : ''; ?></small>
                    <small class="display-block">Chức vụ: <?php echo (!empty($groups[$itemShow['user_group_id']]['name'])) ? $groups[$itemShow['user_group_id']]['name'] : ''; ?></small>
                </h6>
            </div>
        </div>
    </div>
    <?php else: $col = 12; endif; ?>
    <div class="col-lg-<?php echo $col ?>">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" id="create" name="create" value="1>">
            <input type="hidden" name="id" value="<?php echo $itemShow['id'] ?>">
            <input type="hidden" name="profile" value="<?php echo ($isProfile) ? 1 : 0; ?>">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success"></h5>
                </div>
                <div class="panel-body">
                    <div class="form-group" id="frm-username">
                        <label class="col-lg-3 control-label">Tên đăng nhập:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr('#frm-username','#username_err')" type="text" id="username" name="username" maxlength="100" value="<?php echo $itemShow['user_name'] ?>" class="form-control">
                            <div class="error error_hide" id="username_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-fullname">
                        <label class="col-lg-3 control-label">Tên đầy đủ:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr('#frm-fullname','#fullname_err')" type="text" id="fullname" name="fullname" maxlength="100" value="<?php echo $itemShow['full_name'] ?>" class="form-control">
                            <div class="error error_hide" id="fullname_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-password">
                        <label class="col-lg-3 control-label">Mật khẩu:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr('#frm-password','#password_err')" type="password" id="password" name="password" maxlength="255" value="" placeholder="<?php echo (!empty($itemShow['id'])) ? 'Để trống nếu không muốn thay đổi mật khẩu' : '' ?>" class="form-control">
                            <div class="error error_hide" id="password_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-email">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr('#frm-email','#email_err')" type="text" id="email" name="email" maxlength="100" value="<?php echo $itemShow['email'] ?>" class="form-control">
                            <div class="error error_hide" id="email_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <?php if (!$isProfile): ?>
                    <div class="form-group" id="frm-department">
                        <label class="col-lg-3 control-label">Bộ phận:</label>
                        <div class="col-lg-9">
                            <select onchange="RemoveErr2(this, 2)" id="id_department" name="id_department" class="form-control" >
                                <option value="">-- Chọn bộ phận --</option>
                                <?php foreach ($departments as $department): ?>
                                    <option <?php echo ($itemShow['id_department'] == $department['id']) ? 'selected' : '' ?> value="<?php echo $department['id']; ?>"><?php echo $department['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="error error_hide" id="department_err"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-user-groups">
                        <label class="col-lg-3 control-label">Chức vụ:</label>
                        <div class="col-lg-9">
                            <select onchange="RemoveErr2(this, 2)" id="user_groups_id" name="user_groups_id" class="form-control" >
                                
                            </select>
                            <div class="error error_hide" id="user_group_err"></div>
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="form-group" id="frm-image">
                        <label class="col-lg-3 control-label">Hình đại diện:</label>
                        <div class="col-lg-9">
                            <input type="file" class="file-styled-maihan" name="imgItem" id="imgItem" onchange="readImg('#imgItem', '#frm-image', '#imgItemDisplay', '#remove_imgItem', '#imgItem_err');" accept="image/*">
                            <!--<input type="file" name="imgItem" id="imgItem" onchange="readImg('#imgItem', '#frm-image', '#imgItemDisplay', '#remove_imgItem', '#imgItem_err');" accept="image/*" />-->
                            <img class="error_hide" id="imgItemDisplay" src="#" alt="" />
                            <a id="remove_imgItem" style="vertical-align: top; display: hidden" href="javascript:;" data-id="" class="delete text-danger error_hide"><i class=" fa fa-fw fa-times"></i></a>
                            <div class="error error_hide" id="imgItem_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="text-right">
                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i>&nbsp;&nbsp;&nbsp;<?php echo SAVE_INFO_TEXT ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var groupsByDepartment = <?php echo (!empty($groupsByDepartment)) ? json_encode($groupsByDepartment) : json_encode([]) ?>;
    var userGroupId = '<?php echo $itemShow['user_group_id'] ?>';
    var userGroupWebId = <?php echo USER_GROUP_WEB_ID ?>;
    var userWebId = <?php echo USER_WEB_ID ?>;
    var currentUserId = <?php echo Yii::app()->user->id ?>;

    $(document).ready(function($){

        $('#id_department').change(function() {
            var partmentId = $(this).val();
            var html = '<option value="">-- Chọn chức vụ --</option>';

            $.each(groupsByDepartment, function (k, v) {
                if (k == partmentId && !empty(v)) {
                    for (var i = 0; i < v.length; i++) {
                        var userGroup = v[i];
                        if (currentUserId != userWebId && userGroup.id == userGroupWebId) {
                            continue;
                        }
                        var selected = (userGroupId == userGroup.id) ? 'selected' : '';
                        html += '<option '+selected+' value="' + userGroup.id + '">' + userGroup.name + '</option>';
                    }
                }
                return;
            });

            $('#user_groups_id').html(html);
        });

        $('#id_department').change();
        
        $('#frm-image').on('click', '.delete', function () {
            $("#imgItem").val("");
            $('#imgItemDisplay').hide();
            $("#remove_imgItem").hide();
            $('.filename').html('');
        });
            
	    $('#butCreate').click(function(){
            var boolNoErrors = true;
            if (ShowErr("#username", "#frm-username", "#username_err", "Tên đăng nhập", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#fullname", "#frm-fullname", "#fullname_err", "Tên đầy đủ", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }
            
            <?php if (empty($itemShow['id'])): ?>
            if (ShowErr("#password", "#frm-password", "#password_err", "Mật khẩu", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }
            <?php endif; ?>

            if (ShowErr("#email", "#frm-email", "#email_err", "Email", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErrEmail("#email", "#frm-email", "#email_err", "Vui lòng nhập đúng dạng của email.") == false) {
                boolNoErrors = false;
            }

            if (!empty($("#password").val())) {
                if ($("#password").val().length < 8) {
                    boolNoErrors = false;
                    showErrMsg("#password", "#frm-password", "#password_err", "Mật khẩu phải nhiều hơn hoặc bằng 8 ký tự.");
                } else {
                    RemoveErr("#frm-password", "#password_err");
                }
            }

            <?php if (!$isProfile): ?>
            if (ShowErr("#id_department", "#frm-department", "#department_err", "Bộ phận", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#user_groups_id", "#frm-user-groups", "#user_group_err", "Chức vụ", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }
            <?php endif; ?>

            //Perform Submission
            if (boolNoErrors) {
                var data_string = {
                    'uname': $("#username").val(),
                    'id': <?php echo $itemShow['id'] ?>
                };
                var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkDuplicity/"); ?>");
                if((CheckUnique(result.errName, "Tên đăng nhập", "#frm-username", "#username_err", " <?php echo EXISTED_IN_SYS_MSG ?>") == true)){
                    document.frmObj.submit(blockSystem());
                }		
            }
        });
    });
</script>