<?php
$this->renderPartial('/scheduleCalendar/index', array(
    'users' => $users,
    'defaultDate' => $defaultDate,
    'currentUserId' => $currentUserId,
    'defaultView' => 'agendaDay',
    'rightHeaderSetting' => 'agendaWeek,agendaDay',
));
?>