<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" id="create" name="create" value="1">
            <input type="hidden" id="id" name="id" value="<?php echo $customerShow['id'] ?>">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <?php
                    $this->renderPartial('/common/customer_info', array(
                        'customerShow' => $customerShow,
                        'disabled' => '',
                        'isEditCustomer' => 1,
                    ));
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>