<link href="<?= Yii::app()->request->baseUrl; ?>/css/contextmenu/jquery.contextMenu.min.css" rel="stylesheet">
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/contextmenu/jquery.contextMenu.js"></script>
<script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/contextmenu/jquery.ui.position.min.js"></script>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>-->
<style>
    /* .fc-day-grid-event > .fc-content{
        white-space: unset !important;
    } */

    .sp-container {
        z-index: 9999 !important;
    }
</style>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-lg-3">
            <select id="id_user" name="id_user" class="form-control select">
                <option value="0">-- Chọn người mà bạn muốn xem --</option>
                <?php foreach ($users as $user) : ?>
                    <option value="<?php echo $user['id'] ?>"><?php echo $user['full_name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-lg-1">
            <button type="button" id="btnViewSomeone" class="btn bg-maihan"><i class="icon-search4"></i></button>
        </div>
        <div class="col-lg-8">
            <button type="button" class="btn bg-maihan pull-right" onclick="showScheduleCalendarModal()"><i class="icon-plus3"></i>&nbsp;&nbsp;Tạo lịch làm việc</button>
        </div>
    </div><br />
    <hr />
    <div class="panel-body">
        <div id="plan_calendar" class="fullcalendar-languages"></div>
    </div>
</div>

<div style="z-index: 9999;" id="schedule-calendar-delete-option-modal" class="modal modal-custom">
    <div class="modal-dialog modal-xs pull-right"><!--margin-top: 31% !important; margin-right: 10% !important-->
        <div class="modal-content">
            <div class="modal-header bg-maihan">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="frm_delete_option" name="frm_delete_option" class="form-horizontal">
                    <div class="form-group row" id="frm-delete-option">
                        <div class="col-lg-12">
                            <select id="delete_type" name="delete_type" class="form-control delete-option">
                                <?php foreach (getScheduleCalendarDeleteOption() as $kDeleteOption => $vDeleteOption) : ?>
                                    <option value="<?php echo $kDeleteOption ?>"><?php echo $vDeleteOption ?></option>
                                <?php endforeach ?>
                            </select>
                            <select id="update_type" name="update_type" class="form-control update-option">
                                <?php foreach (getScheduleCalendarUpdateOption() as $kUpdateOption => $vkUpdateOption) : ?>
                                    <option value="<?php echo $kUpdateOption ?>"><?php echo $vkUpdateOption ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butDeleteOption' name='butDeleteOption' type="button" class="btn bg-dark delete-option"><i class="icon-floppy-disk"></i> OK</button>
                        <button id='butUpdateOption' name='butUpdateOption' type="button" class="btn bg-dark update-option"><i class="icon-floppy-disk"></i> OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="schedule-calendar-modal" class="modal modal-custom">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-maihan">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">Thêm lịch làm việc</h6>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="frmObjDeny" name="frmObjDeny" class="form-horizontal">
                    <input type="hidden" id="id_schedule_calendar" name="id_schedule_calendar" value="0" />
                    <input type="hidden" id="correct_start_date" name="correct_start_date" value="" />
                    <input type="hidden" id="correct_repeat_type" name="correct_repeat_type" value="<?php echo SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT ?>"/>
                    <div class="form-group row" id="frm-type">
                        <div class="col-lg-12">
                            <label class="control-label"><?php echo $this->typeText ?>:</label>
                            <select onchange="changeScheduleCalendarType(this)" id="type" name="type" class="form-control">
                                <?php foreach (getScheduleCalendarType() as $scheduleCalendarType) : ?>
                                    <option value="<?php echo $scheduleCalendarType['id'] ?>"><?php echo $scheduleCalendarType['name'] ?></option>
                                <?php endforeach ?>
                            </select>
                            <div class="error error_hide" id="type_err"></div>
                        </div>
                    </div>
                    <div id="div-child-task" class="form-group row" id="frm-child-task">
                        <div class="col-lg-12">
                            <label class="control-label">Loại công việc:</label>
                            <select id="id_child_task" name="id_child_task" class="form-control">
                                <?php foreach (getScheduleCalendarChildTask() as $kChildTask => $vChildTask) : ?>
                                    <option value="<?php echo $kChildTask ?>"><?php echo $vChildTask ?></option>
                                <?php endforeach ?>
                            </select>
                            <div class="error error_hide" id="type_err"></div>
                        </div>
                    </div>
                    <div class="form-group row" id="frm-title">
                        <div class="col-lg-12">
                            <label class="control-label"><?php echo $this->titleText ?>:</label>
                            <input placeholder="Nhập <?php echo $this->titleText ?> đối đa 255 ký tự" value="" maxlength="255" onkeyup="RemoveErr2(this, 2)" type="text" id="title" name="title" class="form-control" />
                            <div class="error error_hide" id="title_err"></div>
                        </div>
                    </div>
                    <div class="form-group row" id="frm-description">
                        <div class="col-lg-12">
                            <label class="control-label"><?php echo $this->descriptionText ?>:</label>
                            <textarea placeholder="Nhập <?php echo $this->descriptionText ?> tối đa 1000 ký tự" maxlength="1000" id="description" name="description" rows="2" cols="2" class="form-control elastic elastic-manual"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <div class="form-start-date">
                                <label class="control-label"><?php echo $this->startDateText ?>:</label>
                                <div class="input-group m-a">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="start_date" id="start_date" onchange="RemoveErr2(this, 2)" autocomplete="off" type="text" value="" class="form-control date-custom">
                                </div>
                                <div class="error error_hide" id="start_date_err"></div>
                            </div>
                        </div>
                        <div id="div-start-time" class="col-lg-3 for-time">
                            <div class="form-start-time">
                                <label class="control-label">Từ giờ:</label>
                                <select id="start_time" name="start_time" class="form-control select">
                                    <?php echo getTimes() ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-1 for-time">
                            <div class="form-~">
                                <label class="control-label text-center"></label>
                                <div style="margin-top: 16px;" class="text-center"><b>~</b></div>
                            </div>
                        </div>
                        <div class="col-lg-3 for-time">
                            <div class="form-end-time">
                                <label class="control-label">Đến giờ:</label>
                                <select id="end_time" name="end_time" class="form-control select">
                                    <?php echo getTimes() ?>
                                </select>
                            </div>
                        </div>
                        <div style="margin-top: 34px;" class="col-lg-2">
                            <input class="styled form-control" type="checkbox" id="is_all_day" name="is_all_day" value="0" />&nbsp;&nbsp;
                            <label class="control-label">Cả ngày</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3">
                            <div class="form-repeat-type">
                                <label class="control-label">Tùy chỉnh lặp lại:</label>
                                <select onchange="changeRepeatTypeOption(this)" id="repeat_type" name="repeat_type" class="form-control">
                                    <?php foreach (getScheduleCalendarRepeatType() as $kRepeatType => $vRepeatType) : ?>
                                        <option value="<?php echo $kRepeatType ?>"><?php echo $vRepeatType ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div id="div-end-date-option" class="col-lg-3 for-end-date">
                            <div class="form-end-date-option">
                                <label class="control-label">Kết thúc:</label>
                                <select onchange="changeEndDateOption(this)" id="end_date_option" name="end_date_option" class="form-control">
                                    <option value="<?php echo SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT ?>">Không bao giờ</option>
                                    <option value="<?php echo SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE ?>">Vào ngày</option>
                                </select>
                            </div>
                        </div>
                        <div id="div-end-date" class="col-lg-4 for-end-date">
                            <div class="form-end-date">
                                <label class="control-label"><?php echo $this->endDateText ?>:</label>
                                <div class="input-group m-a">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="end_date" id="end_date" onchange="RemoveErr2(this, 2)" autocomplete="off" type="text" value="" class="form-control date-custom">
                                </div>
                                <div class="error error_hide" id="end_date_err"></div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-color">
                                <label class="control-label">&nbsp;</label><br/>
                                <input id="color" name="color" type="text" class="form-control" value="<?php echo SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR ?>">
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butCreateScheduleCalendar' name='butCreateScheduleCalendar' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> <?php echo SAVE_INFO_TEXT ?></button>
                        <button onclick="completeScheduleCalendar(1)" id='butCompleteScheduleCalendar' name='butCompleteScheduleCalendar' type="button" class="btn btn-info btn-hide"><i class="fa fa-calendar-check-o"></i> Hoàn thành</button>
                        <button onclick="completeScheduleCalendar(0)" id='butInCompleteScheduleCalendar' name='butInCompleteScheduleCalendar' type="button" class="btn btn-info btn-hide"><i class="fa fa-calendar-times-o"></i> Chưa hoàn thành</button>
                        <button id='butDeleteScheduleCalendar' name='butDeleteScheduleCalendar' type="button" class="btn btn-danger btn-hide"><i class="glyphicon glyphicon-trash"></i> Xóa</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var event = <?php echo json_encode([]) ?>;
    var defaultDate = '<?php echo $defaultDate ?>';
    var currentUserId = <?php echo $currentUserId ?>;
    var otherUserId = 0;
    var defaultCalendarView = '<?php echo !empty($defaultView) ? $defaultView : 'month' ?>';
    var rightHeaderSettingCalendar = '<?php echo !empty($rightHeaderSetting) ? $rightHeaderSetting : 'month,agendaWeek,agendaDay,listMonth' ?>';
    var SCHEDULE_CALENDAR_TYPE_TASK = <?php echo SCHEDULE_CALENDAR_TYPE_TASK ?>;
    var SCHEDULE_CALENDAR_TYPE_PLAN = <?php echo SCHEDULE_CALENDAR_TYPE_PLAN ?>;
    var startDefaultDate = '<?php echo date('d/m/Y') ?>';
    var typeDefault = SCHEDULE_CALENDAR_TYPE_TASK;
    var currentDate = '<?php echo date('Y-m-d') ?>';
    var SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR = '<?php echo SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR ?>';
    var defaultColor = SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR;
    var SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK = <?php echo SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK ?>;
    var SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT = <?php echo SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT ?>;
    var SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE = <?php echo SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE ?>;
    var SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE = <?php echo SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE ?>;
    var SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE = <?php echo SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE ?>;
    var SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE = <?php echo SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE ?>;
    var SCHEDULE_CALENDAR_DELETE_TYPE_ALL = <?php echo SCHEDULE_CALENDAR_DELETE_TYPE_ALL ?>;
    var SCHEDULE_CALENDAR_UPDATE_TYPE_ALL = <?php echo SCHEDULE_CALENDAR_UPDATE_TYPE_ALL ?>;
    var SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE = <?php echo SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE ?>;
    var SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT = <?php echo SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT ?>;
    var SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE = '<?php echo SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE ?>';
    var SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED = '<?php echo SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED ?>';

    function changeEndDateOption(obj) {
        var val = $(obj).val();
        $('#end_date_err').hide();
        $('.form-end-date').removeClass("has-error");
        $('#div-end-date').hide();

        if (val == SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE) {
            $('#div-end-date').show();
        }
    }

    function changeRepeatTypeOption(obj) {
        var val = $(obj).val();
        $('.for-end-date').hide();

        if (val != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT) {
            $('#div-end-date-option').show();
            $('#end_date_option').change();
        }
    }

    function changeScheduleCalendarType(obj) {
        RemoveErr2(obj, 2);
        var val = $(obj).val();
        $('#div-child-task').hide();

        if (val == SCHEDULE_CALENDAR_TYPE_TASK) {
            $('#div-child-task').show();
        }
    }

    function denyAccessOtherUser(){
        var deny = false;
        if (!empty(otherUserId) && otherUserId != currentUserId) {
            SwalWarning('Xin lỗi!', 'Bạn không thể tạo/sửa lịch biểu của người khác');
            resetCalendarInfo();
            deny = true;
        }
        return deny;
    }

    function showScheduleCalendarModal() {
        hideTitlePopover();
        
        if (denyAccessOtherUser()) {
            return;
        }

        $('#start_date').val(startDefaultDate);
        $('#correct_start_date').val(startDefaultDate);
        $('#type').val(typeDefault).change();

        $('#color').spectrum({
            color: defaultColor,
            showPalette: true,
            showPaletteOnly: true,
            palette: colorDataTemplate,
        });

        $('#schedule-calendar-modal').modal('show');
    }

    function completeAjaxPost(id, isCompleted, actionDate){
        $.ajax({
            url: '/ScheduleCalendar/Complete',
            type: "POST",
            data: {
                id: id,
                is_completed: isCompleted,
                action_date: actionDate,
            },
            dataType: 'json',
            success: function(res) {
                if (!empty(res.result)) {
                    $('#schedule-calendar-modal').modal('hide');
                    renderCalendarEvents();
                } else {
                    SwalWarning('Xin lỗi!', res.msg);
                }
            }
        });
    }

    function completeScheduleCalendar(isCompleted) {
        var id = $('#id_schedule_calendar').val();
        var actionDate = $('#correct_start_date').val();
        completeAjaxPost(id, isCompleted, actionDate);
    }

    function deleteScheduleCalendar(type = SCHEDULE_CALENDAR_DELETE_TYPE_ALL){
        swal({
            title: 'Xóa lịch biểu này',
            text: 'Bạn sẽ không được hoàn tác lại.',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Xác nhận xóa',
            cancelButtonText: 'Hủy bỏ',
            closeOnConfirm: true
        },
        function() {
            $.ajax({
                url: '/ScheduleCalendar/Delete',
                type: "POST",
                data: {
                    id: $('#id_schedule_calendar').val(),
                    delete_type: type,
                    action_date: $('#correct_start_date').val(),
                },
                dataType: 'json',
                success: function(res) {
                    $('#schedule-calendar-delete-option-modal').modal('hide');
                    if (!empty(res.result)) {
                        $('#schedule-calendar-modal').modal('hide');
                        renderCalendarEvents();
                    } else {
                        SwalWarning('Xin lỗi!', res.msg);
                    }
                }
            });
        });
    }

    function showUpdateDeleteOptionModal(type){
        $('.delete-option').hide();
        $('.update-option').hide();

        if (type == SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE) {
            $('.delete-option').show();
        } else if (type == SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE) {
            $('.update-option').show();
        }

        $('#schedule-calendar-delete-option-modal').modal('show');
    }

    function createUpdateScheduleCalendar(updateType = SCHEDULE_CALENDAR_UPDATE_TYPE_ALL){
        $.ajax({
            url: '/ScheduleCalendar/Create',
            type: "POST",
            data: {
                id: $('#id_schedule_calendar').val(),
                type: $('#type').val(),
                title: $('#title').val(),
                description: $('#description').val(),
                start_date: $('#start_date').val(),
                start_time: $('#start_time').val(),
                end_time: $('#end_time').val(),
                is_all_day: $('#is_all_day').val(),
                repeat_type: $('#repeat_type').val(),
                color: $("#color").spectrum('get').toHexString(),
                id_child_task: $('#id_child_task').val(),
                update_type: updateType,
                end_date_option: $('#end_date_option').val(),
                end_date: $('#end_date').val(),
            },
            dataType: 'json',
            success: function(res) {
                $('#schedule-calendar-delete-option-modal').modal('hide');
                if (!empty(res.result)) {
                    $('#schedule-calendar-modal').modal('hide');
                    renderCalendarEvents();
                } else {
                    SwalWarning('Xin lỗi!', res.msg);
                }
            }
        });
    }

    function mouseRightClickEvent(cls){
        var items = {};
        if (cls == SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED) {
            items.SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED = {
                name: 'Chưa hoàn thành', 
                icon: 'fa-calendar-times-o',
            };
        } else if (cls == SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE) {
            items.SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE = {
                name: 'Hoàn thành', 
                icon: 'fa-calendar-check-o',
            };
        }
        /*items.delete = {
            name: 'Delete',
            icon: 'delete',
        }*/
        //items.sep1 = "---------";
        /*"cut": {name: "Cut", icon: "cut"},
            copy: {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function(){
                return 'context-menu-icon context-menu-icon-quit';
            }}*/
        $.contextMenu({
            selector: '.' + cls, 
            callback: function(key, options) {
                if (denyAccessOtherUser()) {
                    return;
                }

                var id = $(this).data('id');
                var actionDate = $(this).data('startdate');
                if (key == 'SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED') {
                    completeAjaxPost(id, 0, actionDate);
                } else if (key == 'SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE') {
                    completeAjaxPost(id, 1, actionDate);
                }
            },
            items: items,
        });
    }

    function resetCalendarInfo(){
        startDefaultDate = '<?php echo date('d/m/Y') ?>';
        $('#schedule-calendar-modal').removeData('bs.modal');
        $('.modal-content').find('textarea').val('');
        $('.modal-content').find('input').val('');
        $('.modal-content').find('.error').hide();
        $('#id_schedule_calendar').val(0);
        $('.modal-content').find('.form-group').removeClass("has-error");
        $('#is_all_day').prop('checked', false).change();
        $.uniform.update();
        $('.btn-hide').hide();
        $('#type').prop('disabled', false);
        $('#color').spectrum('destroy');
        $('#color').val(SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR);
        defaultColor = SCHEDULE_CALENDAR_DEFAULT_TEXT_COLOR;
        typeDefault = SCHEDULE_CALENDAR_TYPE_TASK;
        $('#start_time').val('00:00').change();
        $('#end_time').val('00:00').change();
        $('#id_child_task').val(SCHEDULE_CALENDAR_CHILD_TASK_TYPE_WORK);
        $('#butCreateScheduleCalendar').show();
        $('#correct_repeat_type').val(SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT);
        $('.for-end-date').hide();
        $('#repeat_type').val(SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT).change();
        $('#end_date_option').val(SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT).change();
        hideTitlePopover();
    }

    function renderCalendarEvents() {
        var startDate = $('#plan_calendar').fullCalendar('getView').start.format('YYYY-MM-DD');
        var endDate = $('#plan_calendar').fullCalendar('getView').end.format('YYYY-MM-DD');

        $.ajax({
            url: '/ScheduleCalendar/GetCalendar',
            type: "POST",
            data: {
                start_date: startDate,
                end_date: endDate,
                id_user: (!empty(otherUserId)) ? otherUserId : currentUserId,
            },
            dataType: 'json',
            success: function(result) {
                $('#plan_calendar').fullCalendar('removeEvents');
                $('#plan_calendar').fullCalendar('addEventSource', result.events);
            }
        });
    }

    function hideTitlePopover(){
        //$('.fc-event, .fc-list-item').popover('destroy');
        $('.popover.in').remove();
    }

    $(document).ready(function($) {
        $('.btn-hide').hide();
        $('.for-end-date').hide();

        $('#is_all_day').change(function() {
            if (this.checked) {
                $(this).val(1);
                $('.for-time').hide();
            } else {
                $(this).val(0);
                $('.for-time').show();
            }
        });

        $('#butDeleteOption').click(function() {
            var deleteType = $('#delete_type').val();
            deleteScheduleCalendar(deleteType);
        });

        $('#butUpdateOption').click(function() {
            var updateType = $('#update_type').val();
            createUpdateScheduleCalendar(updateType);
        });

        $('#butDeleteScheduleCalendar').click(function() {
            var repeatType = $('#correct_repeat_type').val();
            if (!empty(repeatType)) {
                if (repeatType != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT) {
                    showUpdateDeleteOptionModal(SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_DELETE);
                } else {
                    deleteScheduleCalendar();
                }
            } else {
                SwalWarning('Xin lỗi!', 'Tham số không hợp lệ');
            }
        });

        $('#butCreateScheduleCalendar').click(function() {
            var boolNoErrors = true;
            if (ShowErr("#type", "#frm-type", "#type_err", "<?php echo $this->typeText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#title", "#frm-title", "#title_err", "<?php echo $this->titleText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if (ShowErr("#start_date", "#frm-start-date", "#start_date_err", "<?php echo $this->startDateText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            if ($('#repeat_type').val() != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT
                && $('#end_date_option').val() == SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE) {
                if (ShowErr("#end_date", "#frm-end-date", "#end_date_err", "<?php echo $this->endDateText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                    boolNoErrors = false;
                }
            }

            if (boolNoErrors) {
                var id = $('#id_schedule_calendar').val();
                var repeatType = $('#correct_repeat_type').val();

                if (!empty(id)) {
                    if (!empty(repeatType)) {
                        if (repeatType != SCHEDULE_CALENDAR_REPEAT_TYPE_NO_REPEAT) {
                            showUpdateDeleteOptionModal(SCHEDULE_CALENDAR_ACTION_HISTORY_TYPE_UPDATE);
                            return;
                        }
                    } else {
                        SwalWarning('Xin lỗi!', 'Tham số không hợp lệ');
                        return;
                    }
                }
                createUpdateScheduleCalendar();
            }
        });

        $('#schedule-calendar-delete-option-modal').on('hidden.bs.modal', function() {
            $('#schedule-calendar-delete-option-modal').removeData('bs.modal');
            $('#delete_type').val(SCHEDULE_CALENDAR_DELETE_TYPE_THIS_ONE);
            $('#update_type').val(SCHEDULE_CALENDAR_UPDATE_TYPE_THIS_ONE);
        });

        $('#schedule-calendar-modal').on('hidden.bs.modal', function() {
            resetCalendarInfo();
        });

        // Render calendar
        $('#plan_calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: rightHeaderSettingCalendar
            },
            defaultView: defaultCalendarView,
            defaultDate: defaultDate,
            locale: 'vi',
            buttonIcons: false, // show the prev/next text
            weekNumbers: false,
            editable: false,
            selectHelper: true,
            nowIndicator: true,
            //contentHeight: 'auto',
            //aspectRatio: 1.5,
            //height: 'auto',
            //slotEventOverlap: false,
            //agendaEventMinHeight: 100,
            events: [],
            eventClick: function(event) {
                startDefaultDate = event.start_date;
                typeDefault = event.type;

                repeatType = event.repeat_type;

                $('#id_schedule_calendar').val(event.id);
                $('#title').val(event.title_original);
                $('#description').val(event.description);
                $('#repeat_type').val(repeatType).change();
                $('#start_time').val(event.start_time).change();
                $('#end_time').val(event.end_time).change();
                $('#id_child_task').val(event.id_child_task);
                $('#color').val(event.textColor);
                $('#correct_repeat_type').val(repeatType);
                defaultColor = event.textColor;

                $('#butDeleteScheduleCalendar').show();

                if (!empty(event.cannot_edit)) {
                    $('#butCreateScheduleCalendar').hide();
                }

                var endDateOption = SCHEDULE_CALENDAR_END_DATE_TYPE_NO_LIMIT;
                $('#end_date').val('');
                if (!empty(event.end_date)) {
                    endDateOption = SCHEDULE_CALENDAR_END_DATE_TYPE_LIMIT_BY_DATE;
                    $('#end_date').val(event.end_date);
                }
                $('#end_date_option').val(endDateOption).change();

                if (event.is_completed == 1) {
                    $('#butInCompleteScheduleCalendar').show();
                } else {
                    $('#butCompleteScheduleCalendar').show();
                }

                showScheduleCalendarModal();

                $('#type').prop('disabled', true);

                if (event.is_all_day == 1) {
                    $('#is_all_day').prop('checked', true).change();
                } else {
                    $('#is_all_day').prop('checked', false);
                }
                $.uniform.update();

                //SwalWarning(event.start_date, event.description);
                // modify the event
                /*event.allDay = false;
                event.start = '2023-05-04T12:00:00'; // 12 noon
                event.end = '2023-05-04T14:00:00'; // 2pm
                $('.fullcalendar-languages').fullCalendar('updateEvent', event);*/
            },
            eventRender: function(eventObj, $el) {
                hideTitlePopover();

                //var originalClass = $el[0].className;
                //$el[0].className = originalClass + ' context-menu-one';

                $el.attr('data-id', eventObj.id);
                $el.attr('data-status', eventObj.is_completed);
                $el.attr('data-startdate', eventObj.start_date);

                $el.popover({
                    title: eventObj.title,
                    content: eventObj.description,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body'
                });

                $el.find('.fc-time').html('');

                if (eventObj.is_completed == 1) {
                    $el.find('.fc-title, .fc-list-item-title').html('<del>' + eventObj.title + '</del>');
                }
            },
            dayClick: function(date, jsEvent, view) {
                var selectDate = date.format('YYYY-MM-DD');

                if (selectDate < currentDate) {
                    SwalWarning('Xin lỗi!', 'Bạn không thể tạo lịch với ngày quá khứ');
                } else {
                    startDefaultDate = date.format('DD/MM/YYYY');
                    showScheduleCalendarModal();
                }
                //alert('Clicked on: ' + date.format('DD/MM/YYYY'));
                //alert('Clicked on: ' + date.format('YYYY-MM-DD HH:mm'));
                //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                //alert('Current view: ' + view.name);

                // change the day's background color just for fun
                //$(this).css('background-color', 'red');
            },
        });

        mouseRightClickEvent(SCHEDULE_CALENDAR_EVENT_CLASS_COMPLETED);
        mouseRightClickEvent(SCHEDULE_CALENDAR_EVENT_CLASS_INCOMPLETE);

        $('#btnViewSomeone').click(function() {
            var userId = $('#id_user').val();
            otherUserId = 0;
            if (empty(userId)) {
                SwalWarning('Xin lỗi!', 'Vui lòng chọn 1 người để xem lịch của người đó');
            } else {
                otherUserId = userId;
                renderCalendarEvents();
            }
        });

        renderCalendarEvents();

        $('.fc-next-button, .fc-prev-button, .fc-today-button').click(function() {
            renderCalendarEvents();
        });

        /*$('.fc-agendaWeek-button').click(function() {
            var newhiddendays = [ 1, 3, 5 ];
            $('#plan_calendar').fullCalendar('option', 'hiddenDays', newhiddendays);
            $('#plan_calendar').fullCalendar({
                hiddenDays: [ 1, 3, 5 ],
            });
        });*/
    });
</script>