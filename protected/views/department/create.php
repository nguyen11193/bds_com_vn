<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title . ' - ' . $title,
    'mainIcon' => $icon,
));
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id); ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal">
            <input type="hidden" id="create" name="create" value="1>">
            <input type="hidden" name="id" value="<?php echo $itemShow['id'] ?>">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success"></h5>
                </div>
                <div class="panel-body">
                    <div class="form-group" id="frm-name">
                        <label class="col-lg-3 control-label"><?php echo $this->nameText ?>:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr2(this, 2)" type="text" id="name" name="name" maxlength="255" value="<?php echo $itemShow['name'] ?>" class="form-control">
                            <div class="error error_hide" id="name_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i>&nbsp;&nbsp;&nbsp;<?php echo SAVE_INFO_TEXT ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($){
            
	$('#butCreate').click(function(){
            var boolNoErrors = true;
            if (ShowErr("#name", "#frm-name", "#name_err", "<?php echo $this->nameText ?>", " <?php echo FIELD_REQUIRE_MSG ?>") == false) {
                boolNoErrors = false;
            }

            //Perform Submission
            if (boolNoErrors) {
                var data_string = {
                    'uname': $("#name").val(),
                    'id': <?php echo $itemShow['id'] ?>
                };
                var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkDuplicity/"); ?>");
                if((CheckUnique(result.errName, "<?php echo $this->nameText ?>", "#frm-name", "#name_err", " <?php echo EXISTED_IN_SYS_MSG ?>") == true)){
                    document.frmObj.submit(blockSystem());
                }		
            }
        });
    });
</script>
