<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-lg-3">
            <select onchange="changeRegulation(this)" id="id_regulation" name="id_regulation" class="form-control select">
                <option value="0">-- Chọn quy định --</option>
                <?php 
                foreach ($regulations as $regulation) : 
                    $regulationSelected = ($regulation['id'] == $regulationId) ? 'selected' : '';
                ?>
                    <option <?php echo $regulationSelected ?> value="<?php echo $regulation['id'] ?>"><?php echo $regulation['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div><br/>
        <h5 class="panel-title text-success"></h5>
    </div><hr/>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên quy định</th>
                        <th>Trạng thái</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach ($items as $k => $item): 
                        $regulationName = !empty($regulations[$item['id_regulation']]) ? $regulations[$item['id_regulation']]['name'] : '';
                        $status = 'Thêm mới';
                        if ($item['log_type'] == LOG_TYPE_EDIT) {
                            $status = 'Chỉnh sửa';
                        } else if ($item['log_type'] == LOG_TYPE_DELETE) {
                            $status = 'Xóa';
                        }

                        $contents = jcustom_decode($item['content']);
                    ?>
                    <tr>
                        <td><?php echo ($k + 1) ?>
                            <?php if (!empty($contents['regulation_content'])): ?>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i onclick="showHideOrderDetail(this, <?php echo $item['id'] ?>, 'quy định')" class="icon-plus3" data-popup="tooltip" title="Xem chi tiết" data-placement="top"></i>
                            <?php endif ?>
                        </td>
                        <td><?php echo $regulationName ?></td>
                        <td><?php echo $status ?></td>
                    </tr>
                    <?php if (!empty($contents['regulation_content'])): ?>
                    <tr style="display: none" id="tr_order_detail_<?php echo $item['id'] ?>">
                        <td colspan="3">
                            <table class="table table-bordered white-space-break-spaces" style="background-color:#f7f7f7">
                                <thead>
                                    <tr>
                                        <th>Hạng mục</th>
                                        <th>Nội dung</th>
                                        <th>Xử lý kỷ luật</th>
                                        <th>Ví dụ dẫn chứng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($contents['regulation_content'] as $content): ?>
                                    <tr>
                                        <td><?php echo $content['category'] ?></td>
                                        <td><?php echo $content['content'] ?></td>
                                        <td><?php echo $content['disciplined'] ?></td>
                                        <td><?php echo $content['example'] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Pagination -->
    <div class="text-right bootpag-default"></div><hr>
    <!-- /pagination -->
</div>
<script>
    function changeRegulation(obj) {
        var regulationId = $(obj).val();
        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . "?regulationId="; ?>' + regulationId) ;
    }

    $(document).ready(function () {
        
        $('.bootpag-default').bootpag({
            total: <?php echo $totalPage; ?>,
            page: <?php echo $page; ?>, //initial page
            maxVisible: <?php echo MAX_VISIBLE; ?>,
            firstLastUse: true,
            first: '←',
            last: '→',
            leaps: false
        }).on("page", function(event, num){
            var regulationId = $('#id_regulation').val();
            $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . "?regulationId="; ?>' + regulationId + '&page=' + num) ;
        });
        
    });
</script>