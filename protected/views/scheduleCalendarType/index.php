<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $this->title,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
$this->renderPartial('/common/main', array(
    'btnAdd' => 'Thêm mới ' . $this->title,
    'head' => array($this->nameText),
    'items' => $items,
    'ignoreArr' => array('id', 'is_deleted', 'created_at', 'updated_at', 'color'),
    'updateAction' => 'create',
    'deleteAction' => 'delete',
    'createAction' => 'create',
));
?>
<script>
    $(document).ready(function () {
        
        settingDatatableDefaults(2, 'Gõ <?php echo $this->nameText ?> để lọc');
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = 'sid=' + delID;
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkHaveScheduleCalendar/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Tồn tại lịch biểu đang sử dụng loại lịch này", "Vui lòng xóa lịch biểu trước.");
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?sid="; ?>' + delID;
                SwalConfirm("Xóa lịch biểu này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
    });
</script>