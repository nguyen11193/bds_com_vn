<?php 
$breadcrumbs =[
  [
    'title' => 'Menu Parent',
  ],
];
$this->renderPartial('/layouts/pageheader', array(
  'breadcrumbs' => $breadcrumbs,
  'title' => 'Menu Parent'
));

$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?= Yii::app()->createAbsoluteUrl('/MenuParent/create'); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm Menu Parent</button></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên</th>
                    <th>Icon</th>
                    <th>Thứ tự xuất hiện</th>
                    <th class="text-center">Hành động</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                foreach ($items as $v1) {
                    echo '
                        <tr>
                            <td>' . $count++ . '</td>
                            <td>' . $v1['name'] . '</td>
                            <td><i class="'.$v1['icon'].'"></i></td>
                            <td>' . $v1['sort_by'] . '</td>
                            <td class="btn-group-custom">
                                <a href="' . Yii::app()->createAbsoluteUrl('/MenuParent/update') . "?id=" . $v1['id'] . '"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa" data-placement="top"></i></a>&nbsp;&nbsp;
                                <a class="delete" data-id="' . $v1['id'] . '" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                            </td>
                        </tr>
                    ';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        
        settingDatatableDefaults(4, 'Gõ Tên để lọc');
        
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = 'sid=' + delID;
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkHaveMenu/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Tồn tại menu thuộc menu parent này", "Vui lòng xóa menu trước.");
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/delete") . "?sid="; ?>' + delID;
                SwalConfirm("Xóa menu parent này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
        
    });
</script>