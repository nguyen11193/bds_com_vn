<?php 
$breadcrumbs =[
  [
    'title' => 'Menu Parent',
    'link' => '/MenuParent/main',
  ],
  [
    'title' => 'Chỉnh sửa',
  ],
];
$this->renderPartial('/layouts/pageheader', array(
  'breadcrumbs' => $breadcrumbs,
  'title' => 'Menu Parent - Chỉnh sửa'
)); 
?>
<div class="row">
    <div class="col-lg-12">
        <form action="<?php echo Yii::app()->createAbsoluteUrl('/MenuParent/update') . '?id=' . $item->id; ?>" method="post" id="frmObj" name="frmObj" class="form-horizontal">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title text-success"></h5>
                </div>
                <div class="panel-body">
                    <div class="form-group" id="frm-name">
                        <label class="col-lg-3 control-label">Tên:</label>
                        <div class="col-lg-9">
                            <input onkeyup="RemoveErr('#frm-name', '#name_err')" type="text" id="name" name="name" maxlength="255" value="<?php echo $item->name ?>" class="form-control">
                            <div class="error" id="name_err" style="display: hidden"></div>
                        </div>
                    </div>
                    <div class="form-group" id="frm-icon">
                        <label class="col-lg-3 control-label">Icon:<i class="<?php echo $item->icon ?>"></i></label>
                        <div class="col-lg-6">
                            <input type="text" id="icon" name="icon" maxlength="255" value="<?php echo $item->icon ?>" class="form-control">
                        </div>
                        <div class="col-lg-3">
                            <a><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#icon-modal">Xem trước Icon</button></a>
                        </div>
                    </div>
                    <div class="form-group" id="frm-sortby">
                        <label class="col-lg-3 control-label">Thứ tự xuất hiện:</label>
                        <div class="col-lg-9">
                            <input type="number" id="sortby" name="sortby" min="0" value="<?php echo $item->sort_by ?>" class="form-control">
                        </div>
                    </div>
                    <div class="text-right">
                        <button id='butCreate' name='butCreate' type="button" class="btn bg-dark"><i class="icon-floppy-disk"></i> Lưu thông tin</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->widget('application.components.IconWidget'); ?>
<script type="text/javascript">
  $(document).ready(function ($) {
      $("#name_err").hide();
  
      $('#butCreate').click(function () {
          var boolNoErrors = true;
          if (ShowErr("#name", "#frm-name", "#name_err", "Tên", " không được trống.") == false) {
              boolNoErrors = false;
          }
          //Perform Submission
          if (boolNoErrors) {
              var data = {
                  'name': $("#name").val(),
                  'id': <?php echo $item->id; ?>
              };
              var result = AjaxURL(data, "<?php echo Yii::app()->createAbsoluteUrl("/" . $actionPath . "/checkDuplicity/"); ?>");
              if ((CheckUnique(result.errName, "Tên", "#frm-name", "#name_err", " đã tồn tại") == true)) {
                  document.frmObj.submit(blockSystem());
              }
          }
      });
  });
</script>

