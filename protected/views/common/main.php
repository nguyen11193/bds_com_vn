<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-success"></h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$createAction); ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;<?php echo $btnAdd ?></button></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <?php foreach ($head as $h): ?>
                    <th><?php echo $h ?></th>
                    <?php endforeach; ?>
                    <th class="text-center"><?php echo $this->actionText ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $k => $v): ?>
                <tr>
                    <td><?php echo ($k + 1) ?></td>
                    <?php foreach ($v as $k1 => $v1): ?>
                    <?php 
                    if (in_array($k1, $ignoreArr)) {
                        continue;
                    } 
                    ?>
                    <td><?php echo $v1 ?></td>
                    <?php endforeach; ?>
                    <td class="btn-group-custom">
                        <?php if (!empty($viewAction)): ?>
                        <a style="color:green" href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$viewAction).'?id='.$v['id'] ?>"><i class="icon-eye" data-popup="tooltip" title="Xem chi tiết" data-placement="top"></i></a>&nbsp;&nbsp;
                        <?php endif; ?>
                        <?php if (!empty($updateAction)): ?>
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$updateAction).'?id='.$v['id'] ?>"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa" data-placement="top"></i></a>&nbsp;&nbsp;
                        <?php endif; ?>
                        <?php if (!empty($deleteAction)): ?>
                        <a class="delete" data-id="<?php echo $v['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>