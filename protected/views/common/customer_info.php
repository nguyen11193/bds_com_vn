<input type="hidden" name='custId' id='customer_id' value='<?php echo $customerShow['id']; ?>'>
<input type="hidden" name='just_save_customer_info' id='just_save_customer_info' value='0'>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group col-lg-12" id="frm-customer-code">
            <label class="control-label">Mã Khách hàng:</label>
            <input disabled="" type="text" id="customer_code" name="customer_code" value="<?php echo $customerShow['code'] ?>" class="form-control">
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group col-lg-12" id="frm-customer-phone">
            <label class="control-label">Số điện thoại:</label>
            <?php foreach ($customerShow['phone'] as $k => $phone): ?>
            <?php if ($k == 0): ?>
            <?php
            $onblur = '';
            $placeholder = 'Số ĐT';
            if (empty($isEditCustomer)) {
                $onblur = 'onblur="findCustomerByPhone()"';
                $placeholder = 'Tìm KH bằng SĐT...';
            }
            ?>
            <input <?php echo $onblur ?> <?php echo $disabled; ?> placeholder="<?php echo $placeholder ?>" onkeyup="RemoveErr('#frm-customer-phone','#customer_phone_err')" type="text" id="customer_phone_<?php echo $k ?>" name="CustomerPhone[phone<?php echo $k ?>]" maxlength="255" value="<?php echo $phone['phone'] ?>" class="form-control input_number_only customer_info">
            <?php else: ?>
            <br/><input type="text" id="customer_phone_<?php echo $k ?>" <?php echo $disabled; ?> name="CustomerPhone[phone<?php echo $k ?>]" maxlength="255" value="<?php echo $phone['phone'] ?>" class="form-control customer_info">
            <?php endif; ?>
            <?php endforeach; ?>
            <div class="error error_hide" id="customer_phone_err" style="display: hidden"></div>
        </div>
    </div>
    <div class="col-lg-4">
        <button <?php echo $disabled; ?> type="button" onclick="addPhone()" id="btnAddPhoneCustomer" class="btn bg-maihan customer_info"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm Số điện thoại</button>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group col-lg-12" id="frm-customer-name">
            <label class="control-label">Họ tên Khách hàng:</label>
            <input <?php echo $disabled; ?> onkeyup="RemoveErr('#frm-customer-name','#customer_name_err')" type="text" id="customer_name" name="Customer[name]" maxlength="255" value="<?php echo $customerShow['name'] ?>" class="form-control customer_info">
            <div class="error error_hide" id="customer_name_err" style="display: hidden"></div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group col-lg-12" id="frm-customer-birthday">
            <label class="control-label">Ngày sinh:</label>
            <div class="input-group m-a">				
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input autocomplete="off" <?php echo $disabled; ?> onchange="calculateAge('#customer_birthday', '#customer_age')" type="text" id="customer_birthday" name="Customer[birthday]" value="<?php echo $customerShow['birthday'] ?>" class="form-control date-custom customer_info">
            </div>
            <div class="error error_hide" id="customer_birthday_err" style="display: hidden"></div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group col-lg-12" id="frm-customer-email">
            <label class="control-label">Email:</label>
            <input <?php echo $disabled; ?> onkeyup="RemoveErr('#frm-customer-email','#customer_email_err')" type="text" id="customer_email" name="Customer[email]" maxlength="255" value="<?php echo $customerShow['email'] ?>" class="form-control customer_info">
            <div class="error error_hide" id="customer_email_err" style="display: hidden"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group col-lg-12" id="frm-customer-address">
            <label class="control-label">Địa chỉ:</label>
            <input <?php echo $disabled; ?> onkeyup="RemoveErr('#frm-customer-address','#customer_address_err')" type="text" id="customer_address" name="Customer[address]" maxlength="255" value="<?php echo $customerShow['address'] ?>" class="form-control customer_info">
            <div class="error error_hide" id="customer_address_err" style="display: hidden"></div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group col-lg-12" id="frm-order-comments">
            <label class="control-label">Ghi chú:</label>
            <textarea <?php echo $disabled; ?> id="customer_comments" name="Customer[comment]" rows="4" cols="4" placeholder="Tối đa 2000 ký tự" class="form-control elastic elastic-manual customer_info"><?php echo $customerShow['comment']; ?></textarea>
        </div>
    </div>
</div>
<?php if (empty($isEditOrder)): ?>
<div class="row">
    <div class="col-lg-12 text-right">
        <button <?php echo $disabled; ?> id='butCreateCustomer' name='butCreateCustomer' type="button" class="btn bg-dark customer_info"><i class="icon-floppy-disk"></i>&nbsp;&nbsp;Lưu thông tin Khách Hàng</button>
        &nbsp;&nbsp;
        <?php if (empty($isEditCustomer)): ?>
        <button title="Làm mới" id='refreshCustomer' name='refreshCustomer' type="button" class="btn btn-default"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Nhập lại</button>
        &nbsp;&nbsp;
        <button id='nextTabCustomer' name='nextTabCustomer' type="button" class="btn bg-maihan">Tiếp tục <i class="icon-arrow-right14 position-right"></i></button>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<script type="text/javascript">
    var row_no = <?php echo count($customerShow['phone']) ?>;
    var urlLoadCustomerInfo = '<?php echo Yii::app()->createAbsoluteURL('/order/loadCustomerInfo'); ?>';
    
    function loadCustInfo(cid){
        $.getJSON(urlLoadCustomerInfo, {
            cid: cid,
        }, function (data) {
            $('#customer_id').val(cid);
            loadCustomerInfo(data);
        });
    }
    
    function findCustomerByPhone(){
        var phone = $('#customer_phone_0').val();
        var url = '<?php echo Yii::app()->createAbsoluteUrl('/Order/searchCustomerByPhone/'); ?>';
        var urlLoadInfo = '<?php echo Yii::app()->createAbsoluteURL('/order/loadCustomerInfo'); ?>';
        var data_string = {
            'type': '<?php echo OPERATOR_EQUAL ?>',
            'phone': phone,
        };
        $.ajax({
            async: false,
            url: url,
            type: 'POST',
            data: data_string,
            dataType: 'json',
            cache: false,
            success: function (result) {
                var cid = 0;
                if (!empty(result.customer)) {
                    cid = result.customer[0].id_customer;
                }
                loadCustInfo(cid);
            }
        });
    }
        
    $(document).ready(function($){
        $('#refreshCustomer').click(function(){
           disabledRefeshCustomerField(false);
           $('#customer_phone_0').val('-1');
           findCustomerByPhone();
           $('#customer_phone_0').val('');
        });
        
        $('#butCreateCustomer').click(function(){
            var boolNoErrors = true;
            var isDisabledCustomer = $('#customer_name').prop('disabled');
            if (!isDisabledCustomer) {
                boolNoErrors = validateCustomerInfo(boolNoErrors);
            }
            //Perform Submission
            if (boolNoErrors) {
                $('#just_save_customer_info').val(1);
                document.frmObj.submit(blockSystem());
            }
        });
        
        <?php if (empty($isEditCustomer)): ?>
            $("#customer_phone_0").autocomplete({
                minLength: 3,
                source: "<?php echo Yii::app()->createAbsoluteURL('/order/searchCustomerPhone'); ?>",
                response: function (event, ui) {
                    if (ui.content.length == 0) {
                        loadCustInfo(0);
                        return false;
                    }
                },
                focus: function( event, ui ) {
                    $("#customer_phone_0").val(ui.item.label);
                    loadCustInfo(ui.item.value);
                    return false;
                },
                select: function( event, ui ) {
                    $("#customer_phone_0").val(ui.item.label);
                    loadCustInfo(ui.item.value);
                    return false;
                }
            });
        <?php endif; ?>
    });
</script>

