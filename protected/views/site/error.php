<!-- Error title -->
<div class="text-center content-group">
    <h1 class="error-title"><?= $error['code']; ?></h1>
    <h5><?= $error['message']; ?></h5>
</div>
<!-- /error title -->
<!-- Error content -->
<div class="row">
    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
        <div class="row">
            <div class="col-sm-12">
                <a href="<?= Yii::app()->createAbsoluteUrl('/Dashboard/main'); ?>" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Trở về Trang chủ</a>
            </div>
        </div>
    </div>
</div>
<!-- /error content -->