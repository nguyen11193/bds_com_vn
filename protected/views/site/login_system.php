<form id="frmObj" name="frmObj" action="<?php echo Yii::app()->createAbsoluteUrl('/site/loginSystem'); ?>" method="post">
    <div class="login-form">
        <div class="text-center">
            <div class="icon-object border-slate-300 text-slate-300"><img style="height: 70px" src="<?php echo LOGO; ?>" class="m-b" alt="logo"></div>
            <h5 class="content-group text-default"><strong>XIN CHÀO!</strong><small class="display-block"></small></h5>
        </div>
        <div id="frm-username" class="form-group has-success has-feedback has-feedback-left">
            <input value="" onkeyup="RemoveErr2(this)" name="user_name_system" id="user_name_system" type="password" class="form-control input-rounded" placeholder="Tên đăng nhập" autofocus>
            <div class="form-control-feedback">
                <i class="icon-user text-muted"></i>
            </div>
            <div class="error error_hide" id="LoginForm_username_err" style="display: hidden"></div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn bg-maihan-inverse btn-rounded" style="padding: 5px 40px">Đăng nhập</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('form').submit(function () {
            var boolNoErrors = true;
            
            if (ShowErr("#user_name_system", "#frm-username", "#LoginForm_username_err", "Tên đăng nhập", " thì không được trống.") == false) {
                boolNoErrors = false;
            }

            if (boolNoErrors) {
		        return true;
            } else {
                return false;
            }
        });
    });
</script>