<form id="frmObj" name="frmObj" action="<?php echo Yii::app()->createAbsoluteUrl('/site/login'); ?>" method="post">
    <div class="login-form">
        <div class="text-center">
            <div class="icon-object"><img style="height: 142px" src="<?php echo LOGO; ?>" class="m-b" alt="logo"></div><!--border-slate-300 text-slate-300 height: 70px-->
            <h5 class="content-group text-default"><strong>XIN CHÀO!</strong><small class="display-block"></small></h5>
        </div>
        <div id="frm-username" class="form-group has-success has-feedback has-feedback-left">
            <input value="" onkeyup="RemoveErr('#frm-username', '#LoginForm_username_err')" name="LoginForm[username]" id="LoginForm_username" type="text" class="form-control input-rounded" placeholder="Tên đăng nhập" autofocus>
            <div class="form-control-feedback">
                <i class="icon-user text-muted"></i>
            </div>
            <div class="error error_hide" id="LoginForm_username_err" style="display: hidden"></div>
        </div>
        <div id="frm-password" class="form-group has-success has-feedback has-feedback-left">
            <input value="" onkeyup="RemoveErr('#frm-password', '#LoginForm_password_err')" name="LoginForm[password]" id="LoginForm_password" type="password" class="form-control input-rounded" placeholder="Mật khẩu">
            <div class="form-control-feedback">
                <i class="icon-lock2 text-muted"></i>
            </div>
            <div class="error error_hide" id="LoginForm_password_err" style="display: hidden"></div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn bg-maihan-inverse btn-rounded" style="padding: 5px 40px">Đăng nhập</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('form').submit(function () {
            var boolNoErrors = true;
            
            if(ShowErr("#LoginForm_username", "#frm-username", "#LoginForm_username_err", "Tên đăng nhập", " thì không được trống.") == false){
                boolNoErrors = false;
            }
            if(ShowErr("#LoginForm_password", "#frm-password", "#LoginForm_password_err", "Mật Khẩu", " thì không được trống.") == false){
                boolNoErrors = false;
            }
            if (boolNoErrors) {
		return true;
            } else {
                return false;
            }
        });
    });
</script>