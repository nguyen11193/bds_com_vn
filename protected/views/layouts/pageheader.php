<div class="page-header page-header-default">
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/Dashboard/main'); ?>"><i class="icon-home2 position-left"></i> Trang chủ</a></li>
            <?php
            if (!empty($breadcrumbs)):
                foreach($breadcrumbs as $key => $item):
            ?>
                <li class="active">
                    <?php if (!empty($item['link'])): ?>
                    <a href="<?php echo Yii::app()->createAbsoluteUrl($item['link']); ?>"><?php echo $item['title'] ?></a>
                    <?php else: ?>
                    <?php echo $item['title'] ?>
                    <?php endif; ?>
                </li>
            <?php
                endforeach;
            endif;
            ?>
        </ul>
    </div>
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i class="text-maihan-heavy <?php echo (!empty($mainIcon)) ? $mainIcon : $this->mainIcon ?>"></i> 
                &nbsp;
                <?php 
                    if (!empty($title)) {
                        $titleArr = explode("-",$title);
                    }
                ?>
                <span class="text-semibold"><?php if(!empty($titleArr[0])){echo trim($titleArr[0]);} ?></span> 
                <?php if(!empty($titleArr[1])){echo " - " . trim($titleArr[1]);} ?>
            </h4>
        </div>
        <?php if (!empty($searchBox)): ?>
        <div class="page-content">
            <form action="<?php if(!empty($searchBox['action'])){ echo $searchBox['action'];} ?>" method="get" class="main-search">
                <div class="input-group content-group">
                    <div class="has-feedback has-feedback-left">
                        <input name="filter" placeholder="<?php if(!empty($searchBox['placeholder'])){ echo $searchBox['placeholder'];} ?>" type="text" class="form-control input-rounded input-sm" value="<?php if(!empty($searchBox['value'])){ echo $searchBox['value'];} ?>">
                        <div class="form-control-feedback">
                            <i class="icon-search4 text-muted text-size-base"></i>
                        </div>
                        <?php if(!empty($searchBox['params'])): ?>
                        <?php foreach ($searchBox['params'] as $key => $value): ?>
                        <input name="<?php echo $key ?>" type="hidden" value="<?php echo $value ?>"/>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="input-group-btn">
                        <button type="submit" class="btn bg-maihan btn-rounded btn-sm">Tìm</button>
                    </div>
                </div>
                <?php if (!empty($searchBox['radioFilter'])): ?>
                <div class="form-group">
                    <?php foreach ($searchBox['radioFilter'] as $radio): ?>
                    <label class="radio-inline">
                        <input id="<?php echo $radio['id'] ?>" type="radio" value="<?php echo $radio['value'] ?>" name="<?php echo $radio['name'] ?>" <?php echo $radio['checked'] ?> class="styled"><?php echo $radio['label'] ?>
                    </label>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </form>
        </div>
        <?php endif; ?>
    </div>
</div>