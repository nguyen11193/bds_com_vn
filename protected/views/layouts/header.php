<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title ?></title>
    <link rel="shortcut icon" href="<?= Yii::app()->request->baseUrl.LOGO; ?>">
    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="<?= Yii::app()->request->baseUrl; ?>/assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <!-- Custom -->
    <link href="<?= Yii::app()->request->baseUrl; ?>/css/sweetalert/sweetalert.css" rel="stylesheet">
    <link href="<?= Yii::app()->request->baseUrl; ?>/css/maihan_custom_style.css?cv=<?php echo CSS_MAIHAN_CUSTOM_STYLE_VERSION ?>" rel="stylesheet">
    <!-- /Custom -->
    <!-- Core JS files -->
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/angular/lib/angular.min.js"></script> -->
    <!-- /core JS files -->
    <!-- Theme JS files -->
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/core/app.js"></script>

    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/pages/form_layouts.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/forms/inputs/autosize.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/pagination/bootpag.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/assets/js/plugins/visualization/c3/c3.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/plugins/ui/fullcalendar/lang/all.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/plugins/pickers/color/spectrum.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- /theme JS files -->
    <!-- Custom -->
    <!--  For DatePicker -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.datetimepicker.css"/ >
          <!-- Load jQuery UI Main JS  -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.datetimepicker.js"></script>
    <script src="<?= Yii::app()->request->baseUrl; ?>/js/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/custom.nguyen.js?cv=<?php echo JS_CUSTOM_NGUYEN_VERSION ?>"></script>
    <!-- /Custom -->
    <script type="text/javascript">
        $(document).ready(function () {
            var href = window.location.href;
            $('a[href="' + href + '"]').parents('li').addClass("active");
            $('a[href="' + href + '"]').parents('ul').removeClass('hidden-ul');
        });
    </script>
</head>