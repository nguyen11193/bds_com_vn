<!DOCTYPE html>
<html lang="en">
    <?php
    $this->renderPartial('/layouts/header', array(
        'title' => Yii::app()->name,
    ));
    ?>
    <body>
        <?php if(!empty(Yii::app()->user)): 
            $user = Yii::app()->user;
            if ($user->isGuest) {
                $this->redirect(array('site/login'));
            }
        ?>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= Yii::app()->createAbsoluteUrl('/Dashboard/main'); ?>"></a>
                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right narbar-ul-custom">
                    <li class="narbar-li-custom">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl('/Order/search'); ?>" ><button class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm đơn hàng mới</button></a>
                    </li>
                    <!--<li>
                        <?php //$this->widget('application.components.MessageWidget'); ?>
                        <div class="dropdown-menu dropdown-content width-350">
                            <ul id="load_message" class="media-list dropdown-content-body">
                                
                            </ul>
                            <div class="dropdown-content-footer">
                                <a onclick="loadMoreMessage()" href="#" data-popup="tooltip" title="Xem Thêm 10 Tin Nhắn"><i class="icon-menu display-block"></i></a>
                            </div>
                        </div>
                    </li>-->
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <?php if(!empty($user->image)){ ?>
                            <img src="<?= Yii::app()->request->baseUrl; ?>/<?= USER_PROFILE_PATH . $user->image; ?>" alt="">
                            <?php } ?>
                            <span><?= $user->username; ?></span>
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?= Yii::app()->createAbsoluteUrl('/Useraccount/create?profile=1'); ?>"><i class="icon-user-plus"></i> Thông tin cá nhân</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createAbsoluteUrl('/site/logout'); ?>">Đăng xuất&nbsp;&nbsp;<i class="icon-switch2"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <?php
                $this->widget('application.components.SideMenu');
                ?>
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Content area -->
                    <div class="content">
                        <?= $content; ?>
                        <!-- Footer -->
                        <div class="footer text-muted">
                            &copy; <?php echo date('Y') ?> <a href="#"><?php echo Yii::app()->name ?></a> by <a href="mailto:<?php echo Yii::app()->params['devEmail'] ?>" target="_blank"><?php echo Yii::app()->params['devName'] ?></a>
                        </div>
                        <!-- /footer -->
                    </div>
                    <!-- /content area -->
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
        <?php endif; ?>
    </body>
    <div id="scrollToTop">
        <a title="Lên đầu trang" href="javascript:void(0);"></a>
    </div>
</html>
<script type="text/javascript">
    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('#scrollToTop').fadeIn();
        } else {
            $('#scrollToTop').fadeOut();
        }
    });
    
    $(document).ready(function () {
        $('#scrollToTop').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });
    });
</script>