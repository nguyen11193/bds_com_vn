<!DOCTYPE html>
<html lang="en">
    <?php
    $this->renderPartial('/layouts/header', array(
        'title' => Yii::app()->name,
    ));
    ?>
    <body>
        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <!-- Logo box -->
                        <div class="sidebar-user">
                            <div class="category-content" >
                                <div class="media" style="text-align:center">
                                    <a href="#" class="media-body"><img style="height: 70px" src="<?= Yii::app()->request->baseUrl.LOGO; ?>" alt="logo"></a>
                                </div>
                            </div>
                        </div>
                        <!-- /Logo box -->
                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <li class="navigation-header"><span>MENU</span> <i class="icon-menu" title="Main pages"></i></li>
                                    <li><a href="<?= Yii::app()->createAbsoluteUrl('/MenuParent/main'); ?>"><i class="icon-home4"></i> <span>Menu Parent</span></a></li>
                                    <li><a href="<?= Yii::app()->createAbsoluteUrl('/Menu/main'); ?>"><i class="icon-home4"></i> <span>Menu</span></a></li>
                                    <li><a href="<?= Yii::app()->createAbsoluteUrl('/Permission/main'); ?>"><i class="icon-home4"></i> <span>Permission</span></a></li>
                                    <li><a href="<?= Yii::app()->createAbsoluteUrl('/GroupPermissionApproveMap/main'); ?>"><i class="icon-home4"></i> <span>Quyền duyệt ĐH</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->
                    </div>
                </div>
                <!-- /main sidebar -->
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Content area -->
                    <div class="content">
                        <?= $content; ?>
                        <!-- Footer -->
                        <div class="footer text-muted">
                            &copy; 2019. <a href="#">Mai Hân CRM</a> by <a href="http://thanhdon.com/" target="_blank">VNW team</a>
                        </div>
                        <!-- /footer -->
                    </div>
                    <!-- /content area -->
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
    <div id="scrollToTop">
        <a title="Lên đầu trang" href="javascript:void(0);"></a>
    </div>
</html>
<script type="text/javascript">
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('#scrollToTop').fadeIn();
        } else {
            $('#scrollToTop').fadeOut();
        }
    });
    
    $(document).ready(function () {
        $('#scrollToTop').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });
    });
</script>