<!DOCTYPE html>
<html lang="en">
    <?php
    $this->renderPartial('/layouts/header', array(
        'title' => Yii::app()->name . ' - Đăng Nhập',
    ));
    ?>
    <body class="login-container bg-login-maihan"><!--bg-slate-800-->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Content area -->
                    <div class="content">
                        <!-- Simple login form -->
                        <?= $content; ?>
                        <!-- /simple login form -->
                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            &copy; <?php echo date('Y') ?>. <a href="#"><?php echo Yii::app()->name ?></a> by <a href="mailto:<?php echo Yii::app()->params['devEmail'] ?>" target="_blank"><?php echo Yii::app()->params['devName'] ?></a>
                        </div>
                        <!-- /footer -->
                    </div>
                    <!-- /content area -->
                </div>
                <!-- /main content -->
            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->
    </body>
</html>
