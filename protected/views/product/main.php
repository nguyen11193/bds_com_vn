<?php
$this->renderPartial('/layouts/pageheader', array(
    'breadcrumbs' => $breadcrumbs,
    'title' => $title,
    'mainIcon' => $icon,
));
$this->renderPartial('/common/alert', array(
    'msg' => $msg,
    'err' => $err,
));
?>
<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-lg-3">
            <select onchange="changeProject(this)" id="project_id" name="project_id" class="form-control">
                <option value="0">-- Lựa chọn dự án --</option>
                <?php foreach ($projects as $v1): ?>
                    <option value="<?php echo $v1['id'] ?>" <?php if ($projectId == $v1['id']) echo 'selected' ?>><?php echo $v1['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>&nbsp;
        <h5 class="panel-title text-success"></h5>
        <?php if (!empty($projectId)): ?>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a href="<?php echo Yii::app()->createAbsoluteUrl('/' . $this->controllerName . '/create') .'?projectId='.$projectId; ?>"><button type="button" class="btn bg-maihan"><i class="icon-plus3"></i>&nbsp;&nbsp;&nbsp;Thêm Sản Phẩm</button></a></li>
            </ul>
        </div>
        <?php endif; ?>
    </div>
    <div class="panel-body">
        <table id="tablelist" class="table datatable-basic table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo $this->nameText ?></th>
                    <th><?php echo $this->acreageText ?> (m2)</th>
                    <th><?php echo $this->priceText ?></th>
                    <th class="text-center">Hành động</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $i => $item) { ?>
                    <tr>
                        <td><?php echo ($i + 1) ?></td>
                        <td><?php echo $item['name'] ?></td>
                        <td><?php echo $item['acreage'] ?></td>
                        <td><?php echo number_format(floatval($item['price'])) ?></td>
                        <td class="btn-group-custom">
                            <a href="<?= Yii::app()->createAbsoluteUrl('/' . $this->controllerName . '/create') . '?id=' . $item['id'] . '&projectId=' . $item['id_project']; ?>"><i class="icon-pencil" data-popup="tooltip" title="Chỉnh sửa" data-placement="top"></i></a>
                            &nbsp;&nbsp;
                            <a class="delete" data-id="<?php echo $item['id'] ?>" href="javascript:;"><i class="glyphicon glyphicon-trash text-danger" data-popup="tooltip" title="Xóa" data-placement="top"></i></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function changeProject(obj) {
        var projectId = $(obj).val();
        $(location).attr('href', '<?php echo Yii::app()->createAbsoluteUrl('/'.$this->controllerName.'/'.$this->action->Id) . "?projectId="; ?>' + projectId) ;
    }
    
    $(document).ready(function () {
      
        settingDatatableDefaults(4, 'Tìm kiếm ...');
      
        $('#tablelist').on('click', '.delete', function () {
            var delID = $(this).data("id");
            var data_string = {id: delID};
            var result = AjaxURL(data_string, "<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/checkHaveOrder/"); ?>");
            if (result.check == "yes"){
                SwalWarning("Không thể xóa. Sản phẩm này đã được bán", '');
            } else {
                var url = '<?php echo Yii::app()->createAbsoluteUrl("/" . $this->controllerName . "/delete") . "?id="; ?>' + delID;
                SwalConfirm("Xóa sản phẩm này?", "Bạn sẽ không được hoàn tác lại.", url, "Xác nhận xóa", "Hủy bỏ");
            }
        });
      
    });
</script>