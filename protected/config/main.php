<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
require_once('protected/components/CachesPrefix.php');
require_once('protected/components/Constants.php');
require_once('protected/components/Helpers.php');
require_once('local.define.php');
return array(
    //'language' => 'en',
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Long Tộc - Hệ Thống Nội Bộ',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.modelsextends.*',
        'application.models.*',
        'application.modelsold.*',
        'application.components.*',
        'ext.EasyMultiLanguage.*',
        'ext.easyimage.EasyImage',
        'ext.bootstraplinkpager.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'password',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'cache' => array(
            'class' => 'system.caching.CDbCache'
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            //'class' => 'EMUrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                array('api/pos/create', 'pattern' => 'api/pos/<create:\w+>', 'verb' => 'POST'),
                array('api/pos/list', 'pattern' => 'api/pos/<list:\w+>', 'verb' => 'POST'),
                array('api/pos/update', 'pattern' => 'api/pos/<update:\w+>', 'verb' => 'POST'),
                array('api/pos/delete', 'pattern' => 'api/pos/<delete:\w+>', 'verb' => 'POST'),
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        // database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'easyImage' => array(
            'class' => 'application.extensions.easyimage.EasyImage',
        ),
        'oldDb' => array(
            // 'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
            // uncomment the following lines to use a MySQL database
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=maihan_vnw_vn_19',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => LOG_LEVEL_DEBUG_ZALO_SMS,
                    'logFile' => 'debug_zalo_sms.log',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'timezone' => 'Asia/Ho_Chi_Minh', // Important!  Set to ensure correct timezone
        'langArr' => array(),
        /*'languages' => array(
            'en' => 'English',
            'vi' => 'Vietnamese',
            'th' => 'Thai',
            'cn' => 'Chinese',
        ),*/
        //'default_language' => 'en',

        // Config to Send Email
        'config_email' => array(
            'adminEmail' => 'admin@eponamobile.com', // this is used in contact page
            'password' => '+}_{)P9o*I',
        ),
        'keyGoogleMap' => 'AIzaSyDShCY7kkWCnWHywhWbRH-jcb1ZrAkpiiE',
        'set_time_limit' => 43200,
        'devName' => 'Nguyễn Đình Nguyên',
        'devEmail' => 'nguyendinhnguyen11193@gmail.com',
    ),
);
