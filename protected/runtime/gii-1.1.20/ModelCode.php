<?php
return array (
  'template' => 'custom',
  'connectionId' => 'db',
  'tablePrefix' => 'tbl_',
  'modelPath' => 'application.models',
  'baseClass' => 'MasterModel',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
